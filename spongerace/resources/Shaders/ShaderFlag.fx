cbuffer param
{ 
	float4x4 matViewProj;
	float4x4 matWorld;
	float4 variables; //x  phi
}

struct VS_Sortie
{
	float4 Pos : SV_Position;
	float2 coordTex : TEXCOORD0; 
};

VS_Sortie FlagVS(float4 Pos : POSITION, float3 Normale : NORMAL, float2 coordTex: TEXCOORD)  
{
VS_Sortie sortie = (VS_Sortie)0;
	
	
	
	float4 Po = mul(Pos, matWorld); 
	
	float angle = 1/50.0f;
	float w = 1/5000.0f;//puissance variation
	float a = 1*exp(-w * (Po.y*Po.y + Po.z*Po.z)*100)+0.0f;           
	Po.y =Po.y + sin(angle * (Po.y*Po.y + Po.z*Po.z) + variables.x) * a; //�cart
	
	sortie.Pos = mul(Po,matViewProj);
    sortie.coordTex = coordTex;

	return sortie;
}

Texture2D textureEntree;  // la texture
SamplerState SampleState;  // l'�tat de sampling

float4 FlagPS( VS_Sortie vs ) : SV_Target
{
	float4 couleur = textureEntree.Sample(SampleState, vs.coordTex);
	return couleur;
}
//324.147 7.36 0.68
technique11 FlagShader
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, FlagVS()));
        SetPixelShader(CompileShader(ps_4_0, FlagPS()));
        SetGeometryShader(NULL);
    }
}
