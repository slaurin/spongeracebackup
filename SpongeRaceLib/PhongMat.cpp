#include "stdafx.h"
#include "PhongMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	PhongMat::PhongMat(const Material * material)
		: Material(*material), specularExponent_(1)
	{
		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		// Initialise la structure des param�tres globaux
		initGlobalParams();

		// Initialise la structure des param�tres de mat�riau
		texLessParams_ = TexLessParams();

		// Initialise la structure des param�tres de textures
		initTexParams();

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(GlobalParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &globalBuffer_);


		// On cr�e le buffer associ� aux param�tres de mat�riau
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(TexLessParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &texLessBuffer_);

		// Met � jour la structure des param�tres de mat�riau
		updateTexLessParams();

		//Met � jour la structure des param�tres de textures
		updateTexParams();
	}

	void PhongMat::initGlobalParams()
	{
		// Initialisation de la structure des param�tres
		globalParams_.matWorldViewProj = XMMATRIX();
		globalParams_.matWorld = XMMATRIX(); 
		globalParams_.vCamera = XMVECTOR();
		globalParams_.vAmbientLight = XMVECTOR();
		globalParams_.numPointLights = 0;
		globalParams_.numDirLights = 0;		
		globalParams_.numSpotLights = 0;
		globalParams_.exponent = specularExponent_;

		// Lumi�res ponctuelles
		for(unsigned int i = 0; i < MAX_POINTLIGHTS; ++i)
		{
			globalParams_.PointLights[i] = PointLightStruct();
		}

		// Lumi�res directionnelles
		for(unsigned int i = 0; i < MAX_DIRECTIONALLIGHTS; ++i)
		{
			globalParams_.DirLights[i] = DirLightStruct();
		}

		// Lumi�res spot
		for(unsigned int i = 0; i < MAX_SPOTLIGHTS; ++i)
		{
			globalParams_.SpotLights[i] = SpotLightStruct();
		}
	}

	void PhongMat::initTexParams()
	{
		for(unsigned int i = 0; i < MAX_TEXTURES; ++i)
		{
			loadedTex_[i] = false;
			texArray_[i] = nullptr;
			samplerArray_[i] = nullptr;//static_cast<void *>(texture->GetSamplerState());
		}
	}

	void PhongMat::updateTexParams()
	{
		for(auto it = textures_.begin(); it != textures_.end(); ++it)
		{
			updateTexParam(it->first - 1, it->second);
		}
	}

	bool PhongMat::InsertTexture(aiTextureType type, Texture * texture)
	{
		if(Material::InsertTexture(type, texture))
		{
			updateTexParam(type - aiTextureType_DIFFUSE, texture);
			return true;
		}
		return false;
	}

	void PhongMat::updateTexParam(unsigned int key, Texture * texture)
	{
		// On update la pr�sence de texture
		loadedTex_[key] = true;
		
		// On update la texture
		texArray_[key] = texture->GetTexture();
		
		// On update le sampler associ� dans le tableau
		samplerArray_[key] = texture->GetSamplerState();
	}

	void PhongMat::ReplaceTexture(aiTextureType type, Texture * texture)
	{
		Material::ReplaceTexture(type, texture);
		updateTexParam(type - aiTextureType_DIFFUSE, texture);
	}

	void PhongMat::updateTexLessParams()
	{
		// On recharge les valeurs
		texLessParams_.vAmbient = XMLoadFloat4(&lighting_.GetAmbient());
		texLessParams_.vDiffusion = XMLoadFloat4(&lighting_.GetDiffuse());
		texLessParams_.vSpecular = XMLoadFloat4(&lighting_.GetSpecular());
		texLessParams_.vEmissive = XMLoadFloat4(&lighting_.GetEmissive());

		// On met � jour le buffer
		GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(texLessBuffer_, 0, NULL, &texLessParams_, 0, 0);
	}

	void PhongMat::loadPointLights()
	{
		auto lights = GraphicEngine::GetInstance().GetCurrentScene()->GetPointLights();
		globalParams_.numPointLights = 0;
		for(auto it = lights.begin(); it != lights.end() && globalParams_.numPointLights < MAX_POINTLIGHTS; ++it)
		{
			if((*it)->IsEnabled() && true)//todo LOD : check range et objectboudingbox
			{
				PointLightStruct pls = {
					XMLoadFloat4(&(*it)->GetPosition()),
					XMLoadFloat4(&(*it)->GetDiffuse()),
					XMLoadFloat4(&(*it)->GetSpecular()),
					XMFLOAT3(
						(*it)->GetAttenuationConstant(),
						(*it)->GetAttenuationLinear(),
						(*it)->GetAttenuationQuadratic())
				};
				globalParams_.PointLights[globalParams_.numPointLights++] = pls;
			}
		}
	}

	void PhongMat::loadDirrectionalLights()
	{
		auto lights = GraphicEngine::GetInstance().GetCurrentScene()->GetDirectionalLights();
		globalParams_.numDirLights = 0;
		for(auto it = lights.begin(); it != lights.end() && globalParams_.numDirLights < MAX_DIRECTIONALLIGHTS; ++it)
		{
			if((*it)->IsEnabled() && true)//todo LOD : check range et objectboudingbox
			{
				DirLightStruct dls = {
					XMLoadFloat4(&(*it)->GetDirection()),
					XMLoadFloat4(&(*it)->GetDiffuse()),
					XMLoadFloat4(&(*it)->GetSpecular())
				};
				globalParams_.DirLights[globalParams_.numDirLights++] = dls;
			}
		}
	}

	void PhongMat::loadSpotLights()
	{
		auto lights = GraphicEngine::GetInstance().GetCurrentScene()->GetSpotLights();
		globalParams_.numSpotLights = 0;
		for(auto it = lights.begin(); it != lights.end() && globalParams_.numSpotLights < MAX_SPOTLIGHTS; ++it)
		{
			if((*it)->IsEnabled() && true)//todo LOD : check range et objectboudingbox
			{
				SpotLightStruct sls = {
					XMLoadFloat4(&(*it)->GetPosition()),
					XMLoadFloat4(&(*it)->GetDirection()),
					XMLoadFloat4(&(*it)->GetDiffuse()),
					XMLoadFloat4(&(*it)->GetSpecular()),
					XMFLOAT3(
						(*it)->GetAttenuationConstant(),
						(*it)->GetAttenuationLinear(),
						(*it)->GetAttenuationQuadratic()),
					(*it)->GetAngleCos(),
					(*it)->GetExponent()
				};
				globalParams_.SpotLights[globalParams_.numSpotLights++] = sls;
			}
		}
	}

	void PhongMat::SetLighting(const Lighting & lighting)
	{
		Material::SetLighting(lighting);
		updateTexLessParams();
	}

	string PhongMat::GetClassName()
	{
		return "PhongMat";
	}

	PhongMat::~PhongMat()
	{
		DXRelacher(globalBuffer_);
		DXRelacher(texLessBuffer_);
	}
}