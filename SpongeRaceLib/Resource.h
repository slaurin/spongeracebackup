//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by spongerace.rc
//

#define IDS_APP_TITLE			103
#define IDR_MAINFRAME			128
#define IDD_SPONGERACE_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define DXE_ERREURCREATIONDEVICE        104
#define IDM_EXIT				105
#define DXE_ERREUROBTENTIONBUFFER       105
#define DXE_ERREURCREATIONRENDERTARGET  106
#define IDI_SPONGERACE			107
#define DXE_CREATION_VS                 107
#define IDI_SMALL				108
#define DXE_FICHIER_VS                  108
#define IDC_SPONGERACE			109
#define DXE_CREATION_PS                 110
#define DXE_FICHIER_PS                  111
#define DXE_CREATIONVERTEXBUFFER        112
#define DXE_CREATIONINDEXBUFFER         113
#define DXE_CREATIONLAYOUT              114
#define DXE_ERREURCREATIONTEXTURE       115
#define DXE_ERREURCREATIONDEPTHSTENCILTARGET 116
#define DXE_ERREURCREATION_FX           117
#define DXE_FICHIERTEXTUREINTROUVABLE   118
#define DXE_ERREURCREATION_BLENDSTATE   119
#define IDR_MAINFRAME                   128
#define DXE_ERREURCHARGEMENT_FX           129
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Valeurs par d�faut suivantes des nouveaux objets
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
