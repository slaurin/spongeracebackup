#pragma once

#include "Light.h"

namespace GE
{
	//! Représente une lampe directionnelle
	class DirectionalLight
		: public Light
	{
		//! Direction de la lampe
		XMFLOAT4 direction_;

	public:
		DirectionalLight(const XMFLOAT4 & direction, XMFLOAT4 diffuse, XMFLOAT4 specular);

		//! Fixe la direction de la lampe
		void SetDirection(const XMFLOAT4 &);

		//! Retourne la direction de la lampe
		XMFLOAT4 GetDirection() const;
	};
}