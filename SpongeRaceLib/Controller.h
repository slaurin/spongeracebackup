#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "DynamicActor.h"
#include "ConstantsControl.h"


class Controller
{
public:

	virtual void update(float timeTick){};

	void setActor(DynamicActor *actor) {actor_=actor;};
	DynamicActor * getActor(){return actor_;};
private:
	DynamicActor *actor_;
};

#endif