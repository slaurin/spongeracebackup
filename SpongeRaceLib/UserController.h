#ifndef USERCONTROLLER_H
#define USERCONTROLLER_H

#include "ConstantsControl.h"
#include "Controller.h"
#include "UserInput.h"
#include "Singleton.h"

class DynamicActor;
class Controller;



class UserController 
	: public Singleton<UserController>, public Controller
{
	typedef void (DynamicActor::*methode_action)(float);

	methode_action action_[ACTION_COUNT];

	std::bitset<DEVICE_COUNT> supportedDevices_;

	static const DWORD COOP_LVL_WINDOWED = DISCL_BACKGROUND | DISCL_NONEXCLUSIVE;
	static const DWORD COOP_LVL_FULLSCREEN = DISCL_FOREGROUND | DISCL_EXCLUSIVE;

	bool initialised_;

	DIMOUSESTATE mouseCurrState_;
	DIMOUSESTATE mouseLastState_;
	DIJOYSTATE2 jsCurrState_;
	DIJOYSTATE2 jsLastState_;

	//DynamicActor* actor_;
	UserInput &input_;
	bool Space_Button_down;
	
	UserController()
	: initialised_(false),Space_Button_down(false),
	input_(Singleton<UserInput>::GetInstance())
	{
		supportedDevices_[MOUSE]=1;
		supportedDevices_[KEYBOARD]=1;
	}



	friend Singleton<UserController>;

public :

	bool isUsingJoystick() {return supportedDevices_[MOUSE];};
	bool isUsingKeyboard(){return supportedDevices_[KEYBOARD];};
	bool isUsingMouse(){return supportedDevices_[JOYSTICK];};

	void setUsingJoystick(bool val) {supportedDevices_[JOYSTICK]=val;};
	void setUsingKeyboard(bool val) {supportedDevices_[KEYBOARD]=val;};
	void setUsingMouse(bool val) {supportedDevices_[MOUSE]=val;};

	void cleanup(){
		input_.Shutdown();
	};

	//Setup the directInput
	void Initialise(bool fullScreen=false){
		
		if (fullScreen) {
			initialised_ = Singleton<UserInput>::GetInstance().Initialize(COOP_LVL_FULLSCREEN, supportedDevices_);
		} else 
		{
			initialised_ = Singleton<UserInput>::GetInstance().Initialize(COOP_LVL_WINDOWED, supportedDevices_);
		}		

		input_.GetMouseState(mouseLastState_);
	}

	bool isInitialised(){return initialised_;};

	void setActor(DynamicActor* actor);

	void update(float timeTick){
		input_.Frame();	
		if (supportedDevices_[JOYSTICK]) updateJoystick(timeTick);
		/*if (supportedDevices_[KEYBOARD]) */updateKeyboard(timeTick);
		if (supportedDevices_[MOUSE]) updateMouse(timeTick);
	};

	void updateMouse(float timeTick);

	void updateKeyboard(float timeTick);

	void updateJoystick(float timeTick);
};

#endif