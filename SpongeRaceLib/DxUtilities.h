#pragma once

namespace DxUtilities
{
	// Essayer en envoyant le code d'erreur comme r�sultat
	// Il ne faut pas oublier de "rattraper" le code...
	template <class Type>
	inline void DXEssayer(const Type& Resultat)
	{
		if (Resultat != S_OK)
			throw Resultat;
	}

	// Plus pratique, essayer en envoyant un code sp�cifique 
	// comme r�sultat
	template <class Type1, class Type2>
	inline void DXEssayer(const Type1& Resultat, const Type2& unCode)
	{
		if (Resultat != S_OK)
			throw unCode;
	}

	// Valider un pointeur
	template <class Type>
	inline void DXValider(const void* UnPointeur, const Type& unCode)
	{
		if (UnPointeur == NULL)
			throw unCode;
	}

	// Rel�cher un objet COM (un objet DirectX dans notre cas)
	template <class Type>
	inline void DXRelacher(Type& UnPointeur)
	{
		if (UnPointeur != NULL)
		{
			UnPointeur->Release(); 
			UnPointeur=NULL;
		}
	}

	// Donne un nom � l'objet COM pour v�rifier s'il est bien lib�r� seulement en debug et pour pix
	template<UINT TNameLength>
	inline void DXSetDebugObjectName(_In_ ID3D11DeviceChild* resource, _In_z_ const char (&name)[TNameLength])
	{
#if defined(_DEBUG) || defined(PROFILE)
		resource->SetPrivateData(WKPDID_D3DDebugObjectName, TNameLength - 1, name);
#endif
	}
	
	inline void DXSetDebugObjectName(_In_ ID3D11DeviceChild* resource, _In_z_ const std::string & name)
	{
#if defined(_DEBUG) || defined(PROFILE)
		std::string realName = name.substr(name.find_last_of('\\') + 1);
		UINT i = name.size();
		char * truc = new char[i];
		resource->GetPrivateData(WKPDID_D3DDebugObjectName, &i, truc);

		if(i == 0)
			resource->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.c_str());
#endif
	}

	inline void DXSetDebugObjectName(_In_ ID3D11DeviceChild* resource, _In_z_ const std::wstring & name)
	{
#if defined(_DEBUG) || defined(PROFILE)
		std::wstring realName = name.substr(name.find_last_of('\\') + 1);
		std::string Name;
		std::for_each(begin(realName), end(realName), [&](const wchar_t c){
			Name.push_back(static_cast<char>(c));
		});

		UINT i = name.size();
		char * truc = new char[i];
		resource->GetPrivateData(WKPDID_D3DDebugObjectName, &i, truc);

		if(i == 0)
			resource->SetPrivateData(WKPDID_D3DDebugObjectName, Name.size(), Name.c_str());
#endif
	}
}
