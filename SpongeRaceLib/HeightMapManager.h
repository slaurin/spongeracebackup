#ifndef HEIGHT_MAP_MANAGER_H
#define HEIGHT_MAP_MANAGER_H

#include "Terrain.h"
#include "Manager.h"

#include <string>

class HeightMapManager : public Manager<HeightMapManager, unsigned int, GE::Terrain>
{
public:
    friend class Singleton<HeightMapManager>;
protected:
    GE::Terrain Process(std::vector<unsigned char> &rawData) override
    {
        return GE::Terrain(rawData);
    }
};

#endif