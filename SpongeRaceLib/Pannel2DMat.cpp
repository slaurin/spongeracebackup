#include "stdafx.h"
#include "Pannel2DMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	Pannel2DMat::Pannel2DMat(const Material * material, Type2D type, float zIndex, bool useAlphaBlending)
		: Material(*material), type_(type), zIndex_(zIndex), useAlphaBlending_(useAlphaBlending)
	{
	}

	string Pannel2DMat::GetClassName()
	{
		return "Pannel2DMat";
	}

	Pannel2DMat::~Pannel2DMat()
	{
	}
}