#pragma once

#include "Singleton.h"
#include "Device.h"
#include "GraphicObject.h"
#include "Scene.h"

namespace GE
{
	//!Template servant � construire un objet Moteur qui implantera les aspects "g�n�riques" du moteur de jeu
	/*!
	    Comme plusieurs de nos objets repr�senteront des �l�ments uniques 
		du syst�me (ex: le moteur lui-m�me, le lien vers 
	    le dispositif Direct3D), l'utilisation d'un singleton 
	    nous simplifiera plusieurs aspects.
	*/
	class GraphicEngine
		: public Singleton<GraphicEngine>
	{
		friend Singleton<GraphicEngine>;

		class InvalidCurrentScene{};

		static const float	FPRECISION;
		static const double	DPRECISION;


	public:
		typedef size_t sceneIndex;

		GraphicEngine();
		~GraphicEngine();

		static bool IsNull(const float value) {return value <= FPRECISION;}
		static bool IsNull(const double value) {return value <= DPRECISION;}

		//! Initialise l'Engin Graphique
		int Initialisations();
		
		//! Retourne la matrice de Vision
		XMMATRIX GetMatView();

		//! Retourne la matrice de Projection
		XMMATRIX GetMatProj();

		//! Retourne la matrice VP (Vision * Projection)
		XMMATRIX GetMatViewProj();

		//! Retourne le dispositif de rendu
		Device * GetDevice();

		//! Anime la sc�ne courrante
		void AnimeScene(float timeElapsed);

		//! Rend la sc�ne courrante (sans la pr�senter)
		bool RenderScene(bool PostEffect = false);

		//! Ajoute un objet au HUD de la sc�ne
		bool AddObjectToHud(GraphicObject * object);

		//! Rend les objets du HUD
		void RenderHud();

		bool RenderPostEffect();

		//! Pr�sente la sc�ne courrante
		void Present();

		//! Nettoie la sc�ne courrante
		void Cleanup();

		//! Ajoute un objet graphique � la sc�ne courrante
		bool AddObjectToScene(GraphicObject *);

		//! Ajoute un objet graphique � la sc�ne concern�e
		bool AddObjectToScene(GraphicObject *, sceneIndex);

		//! Retourne la sc�ne courrante
		Scene * GetCurrentScene();

		//! Retourne la sc�ne
		Scene * GetScene(sceneIndex);

		//! Retourne l'index de la sc�ne courrante
		sceneIndex GetCurrentSceneIndex();

		//! Change la sc�ne courrante et renvoit l'index de la pr�c�dente
		sceneIndex SwitchScene(sceneIndex);

		//! Cr�e une nouvelle sc�ne
		sceneIndex NewScene(const std::string & name, bool enablePostEffect);

		void ResizeDevice(int width, int height);
		void SwitchRasterizer();
		
		ID3D11ShaderResourceView*  GetPostEffectShaderResourceView(){return pResourceView;};



	private:

		//Gestion des post effect
		void InitPostEffect();
		void StartPostEffect();
		void EndPostEffect();

		//! Initialise les matrices
		int initMatrices();

		//! Le dispositif de rendu
		Device * device_;

		//! Index de la sc�ne courrante
		sceneIndex currentScene_;

		//! Index de la prochaine sc�ne
		sceneIndex nextScene_;

		//! Liste de sc�nes
		std::vector<Scene *> scenes_;

		//! Objets du HUD de la sc�ne
		std::vector<GraphicObject *> hudObjects_;

		// Variable pour GDI+
		static ULONG_PTR gdiToken_;

		//! Matrice de Projection
		XMMATRIX projMat_;

		// Texture de rendu pour effets
		ID3D11RenderTargetView* pOldRenderTargetView;
		ID3D11DepthStencilView* pOldDepthStencilView;

		ID3D11Texture2D* pTextureScene;
		ID3D11RenderTargetView* pRenderTargetView;
		ID3D11RenderTargetView * pOldTargetView;
		ID3D11ShaderResourceView* pResourceView;
		ID3D11Texture2D* pDepthTexture;
		ID3D11DepthStencilView* pDepthStencilView;

	};
}

	