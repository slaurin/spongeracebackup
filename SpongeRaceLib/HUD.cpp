#include "StdAfx.h"
#include "HUD.h"
#include "SpriteVertex.h"
#include "Pannel2DMat.h"
#include "Material.h"
#include "GraphicEngine.h"
#include "EffectManager.h"
#include "Sprite.h"
#include "TextureManager.h"
#include "TextDrawer.h"
#include <GdiPlus.h>

using namespace GE;
using namespace std;
using namespace Gdiplus;

namespace GameUI
{
	HUD::HUD()
		: text_(TextDrawer(81, 21, new Font(new FontFamily(L"Bauhaus 93", nullptr), 20.f, FontStyleBold, UnitPixel), Color(255, 241, 194, 50), Color(0, 0, 0, 0))), timerMat_(nullptr), hudMat_(nullptr)
	{
		// On r�cup�re l'effet (QUI DOIT ETRE CHARGE!)
		auto spriteFX = EffectManager::GetInstance().getEffect<SpriteVertex, Pannel2DMat>("SpriteFX");

		// On cr�e un sampler pour les sprites
		D3D11_SAMPLER_DESC sampler = Texture::defaultSamplerDesc;
		sampler.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;

		// On charge les images de HUD
		TextureManager::GetInstance().AddTexture(L"HUD_0", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_0.png", sampler));
		TextureManager::GetInstance().AddTexture(L"HUD_1", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_1.png", sampler));
		TextureManager::GetInstance().AddTexture(L"HUD_2", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_2.png", sampler));
		TextureManager::GetInstance().AddTexture(L"HUD_3", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_3.png", sampler));
		TextureManager::GetInstance().AddTexture(L"HUD_4", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_4.png", sampler));
		TextureManager::GetInstance().AddTexture(L"HUD_5", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_5.png", sampler));
		TextureManager::GetInstance().AddTexture(L"HUD_6", new Texture(L"..\\SpongeRace\\Resources\\Textures\\UI\\HUD_6.png", sampler));
		
		// On cr�e le mat�riel du HUD, on charge la premi�re texture, puis on l'ajoute � la sc�ne
		hudMat_ = new Pannel2DMat(new Material(), Pannel2DMat::SPRITE, 0.f, true);
		hudMat_->InsertTexture(aiTextureType_AMBIENT, TextureManager::GetInstance().GetTexture(L"HUD_0"));
		GraphicEngine::GetInstance().AddObjectToHud(new Sprite(spriteFX, hudMat_, XMFLOAT2(0.f, (1.f - (328/1024.f)))));

		// On cr�e le mat�riel du TIMER, on charge la premi�re texture, puis on l'ajoute � la sc�ne
		timerMat_ = new Pannel2DMat(new Material(), Pannel2DMat::SPRITE, 0.f, true);
		timerMat_->InsertTexture(aiTextureType_AMBIENT, text_.GetTexture());
		GraphicEngine::GetInstance().AddObjectToHud(new Sprite(spriteFX, timerMat_, XMFLOAT2((1.f - (81/1024.f)), 0.f), XMFLOAT2(1.f, (21/768.f))));
		
		//auto tex = new Texture(GraphicEngine::GetInstance().GetPostEffectShaderResourceView(), sampler);
		//billboardMat_ = new Pannel2DMat(new Material(), Pannel2DMat::SPRITE, 0.f, false);
		//billboardMat_->InsertTexture(aiTextureType_AMBIENT, tex);
		//GraphicEngine::GetInstance().GetCurrentScene()->AddObjectToHud(new Sprite(spriteFX, billboardMat_, XMFLOAT2(0.8f, 0.f), XMFLOAT2(1.f, 0.2f)));
	}

	void HUD::UpdateHUD(int nbTurbo)
	{
		wstringstream wss;
		wss << "HUD_" << nbTurbo;
		wstring name = wss.str();
		hudMat_->ReplaceTexture(aiTextureType_AMBIENT, TextureManager::GetInstance().GetTexture(wss.str()));
	}

	void HUD::UpdateChrono(time_t seconds)
	{
		wstringstream wss2;
		wss2 << setw(2) << setfill(L'0') <<  seconds / 60 << "\"" << setw(2) << setfill(L'0') << seconds % 60;
		wstring timer = wss2.str();
		text_.Write(timer);
		timerMat_->ReplaceTexture(aiTextureType_AMBIENT, text_.GetTexture());
	}

	HUD::~HUD()
	{
		delete hudMat_;
	}
}
