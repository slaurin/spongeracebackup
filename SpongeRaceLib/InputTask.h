#pragma once

#include "GameTask.h"

class InputTask: public GameTask
{
public:
	virtual void init() override;
	virtual void cleanup() override;
	virtual void update() override;
};