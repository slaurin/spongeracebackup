#pragma once
//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "ActorFactory.h"

//=============================================================================
// FORWARD DECLARATIONS
//=============================================================================
class IActor;

//=============================================================================
// CLASS SpawnManager
//=============================================================================
class SpawnManager
{
public:
	SpawnManager();
	~SpawnManager();

public:
	void update();
	void unspawn(IActor *aActorToUnspawn);
	void unspawnAll();

	IActor* spawn(const ActorFactory::ActorID &aActorID, const physx::PxTransform &aPose, GE::GraphicObject* obj = nullptr);
	IActor* spawn(const ActorFactory::ActorID &aActorID, const physx::PxTransform &aPose, physx::PxGeometry* mesh);
	IActor* spawn(const ActorFactory::ActorID &aActorID, const physx::PxTransform &aPose, physx::PxConvexMesh* convexMesh, GE::GraphicObject* obj = nullptr, const physx::PxMeshScale & scaling = physx::PxMeshScale());
	IActor* spawn(const ActorFactory::ActorID &aActorID, const physx::PxTransform &aPose, physx::PxTriangleMesh* triangleMesh, GE::GraphicObject* obj = nullptr, const physx::PxMeshScale & scaling = physx::PxMeshScale());
	

	template<class T>
	T* spawn(const physx::PxTransform &aInitialPose, GE::GraphicObject* obj = nullptr)
	{
		return static_cast<T*>(spawn(T::typeId(), aInitialPose, obj));
	}

	template<class T>
	T* spawn(const physx::PxTransform &aInitialPose, physx::PxGeometry* mesh)
	{
		return static_cast<T*>(spawn(T::typeId(), aInitialPose, mesh));
	}

	template<class T>
	T* spawn(const physx::PxTransform &aInitialPose, physx::PxConvexMesh* convexMesh, GE::GraphicObject* obj = nullptr, const physx::PxMeshScale & scaling = physx::PxMeshScale())
	{
		return static_cast<T*>(spawn(T::typeId(), aInitialPose, convexMesh, obj, scaling));
	}

	template<class T>
	T* spawn(const physx::PxTransform &aInitialPose, physx::PxTriangleMesh* triangleMesh, GE::GraphicObject* obj = nullptr, const physx::PxMeshScale & scaling = physx::PxMeshScale())
	{
		return static_cast<T*>(spawn(T::typeId(), aInitialPose, triangleMesh, obj, scaling));
	}


private:
	class SpawnManagerImp;
	SpawnManagerImp *_imp;
};


