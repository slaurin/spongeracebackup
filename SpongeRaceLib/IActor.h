#pragma once

//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "GraphicObject.h"
#include "SpotLight.h"

//=============================================================================
// FORWARD DECLARATIONS
//=============================================================================


//=============================================================================
// CLASS IActor
//=============================================================================
class IActor
{
public:
	class GivenGeometryIsInvalid{};
	virtual ~IActor() {};

	virtual void update() = 0;
	virtual void onContact(const physx::PxContactPair &aContactPair) = 0;
	virtual void onTrigger(bool triggerEnter, physx::PxShape *actorShape, physx::PxShape *contactShape) = 0;
	virtual void onSpawn(const physx::PxTransform &aPose, physx::PxGeometry * geometry = nullptr) {};
	virtual void onUnspawn() = 0;
	virtual physx::PxTransform pose() = 0;
	virtual void setGraphicObj(GE::GraphicObject*){};
	virtual GE::GraphicObject* getGraphicObj(){return nullptr;};
	virtual GE::SpotLight * GetRightLight(){return nullptr;};
	virtual GE::SpotLight * GetLeftLight(){return nullptr;};

	virtual const char *debugName() const = 0;
};

//=============================================================================
// CLASS IActorData
//=============================================================================
class IActorData
{
public:
	virtual ~IActorData() {}
};

typedef std::shared_ptr<IActorData> IActorDataRef;
