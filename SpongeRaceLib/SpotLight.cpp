#include "stdafx.h"
#include "SpotLight.h"

namespace GE
{
	SpotLight::SpotLight(const XMFLOAT4 & position, const XMFLOAT4 & direction, const float angle, const float exponent, const XMFLOAT4 & diffuse, const XMFLOAT4 & specular)
		: PointLight(position, diffuse, specular), direction_(direction), angle_(angle), angleCos_(cos(angle)), exponent_(exponent)
	{}

	XMFLOAT4 SpotLight::GetDirection() const
	{
		return direction_;
	}

	void SpotLight::SetDirection(const XMFLOAT4 & direction)
	{
		direction_ = direction;
	}

	void SpotLight::SetAngle(const float angle)
	{
		angle_ = angle;
		angleCos_ = cos(angle);
	}

	void SpotLight::SetExponent(const float exponent)
	{
		exponent_ = exponent;
	}

	float SpotLight::GetAngle() const
	{
		return angle_;
	}

	float SpotLight::GetAngleCos() const
	{
		return angleCos_;
	}

	float SpotLight::GetExponent() const
	{
		return exponent_;
	}
}