#include "stdafx.h"
#include "InputTask.h"
#include "CameraActor.h"
#include "SpongeRace.h"
#include "UserController.h"
#include "LoopTime.h"
#include "GraphicEngine.h"

#include "Inputs.h"

using namespace GE;
void InputTask::update()
{

	MSG msg;
	while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if(msg.message == WM_QUIT)
		{
			GAME->requestExit();
			break;
		}
		// Catch l'appuie d'une touche (r�p�t� si maintenu)
		else if (msg.message == WM_KEYDOWN || msg.message == WM_KEYUP)
		{
			bool KeyDown = msg.message == WM_KEYDOWN;
			int key = static_cast<int>(msg.wParam);
			switch(key)
			{
				// ECHAP -> Quit Game
			case(VK_ESCAPE):
				if(KeyDown) GAME->requestExit();
				break;
				// F1 -> Wireframe / Solid
			case(VK_F1):
				if(KeyDown) GraphicEngine::GetInstance().SwitchRasterizer();
				break;	
				// From player to freecam and vice versa
			case(VK_F2):
				if(KeyDown) GAME->camera()->switchToFromFreeCam();
				break;					
				//from 1st to 3rd person
			case(VK_F3):
				if(KeyDown) GAME->camera()->switchPlayerCamera();
				break;
			case(VK_RETURN):
				if(KeyDown) GAME->startGame();
				break;
				//Pause game
			case(VK_PAUSE):
				if(KeyDown) GAME->switchPause();
				break;
			default:
				if(KeyDown)KeyBoardController::GetInstance().onKeyDown(key);
				else KeyBoardController::GetInstance().onKeyUp(key);
				break;
			}			
		}
		


	}
	if(!GAME->isPause())
	{
		// Update le User Controller avec le temps �coul�
		UserController::GetInstance().update(LoopTime::GetInstance().ElapsedTimeSeconds());
	}
}

void InputTask::init()
{

}

void InputTask::cleanup()
{

}
