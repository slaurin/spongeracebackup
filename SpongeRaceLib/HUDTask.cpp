#include "stdafx.h"
#include "HUDTask.h"
#include "SpongeRace.h"

using namespace GameUI;

HUDTask::TimeTrigger::TimeTrigger()
	: notified_(false)
{
}

void HUDTask::TimeTrigger::Notify()
{
	notified_ = true;
}

HUDTask::HUDTask()
	: hud_(nullptr), needTimer_(true)
{
}

auto HUDTask::GetTriggerStart() -> TimeTrigger *
{
	return &start_;
}

auto HUDTask::GetTriggerEnd() -> TimeTrigger *
{
	return &end_;
}

HUDTask::~HUDTask()
{
	delete hud_;
	chrono_.Reset();
}

void HUDTask::init()
{
	// Chargement du HUD
	hud_ = new HUD();
}

void HUDTask::cleanup()
{
	chrono_.Reset();
}

void HUDTask::SetPause(bool pause)
{
	if(pause)
		chrono_.Pause();
	else
		chrono_.Start();
}

void HUDTask::update()
{
	if(needTimer_)
	{
		if(chrono_.IsRunning())
		{
			hud_->UpdateChrono(chrono_.GetTime());
			if(end_.notified_)
			{
				GAME->EndOfRace(chrono_.GetTime());
				needTimer_ = false;
				chrono_.Reset();
			}
		}
		else if(start_.notified_)
		{
			chrono_.Start();
		}
	}

	hud_->UpdateHUD(GAME->player()->getNumberBoost() % 7);
}