#include "stdafx.h"
#include "SimpleVertex.h"

namespace GE
{
	D3D11_INPUT_ELEMENT_DESC SimpleVertex::Layout_[] =
	{
		{"POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},  
		{"COLOR",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT ,	D3D11_INPUT_PER_VERTEX_DATA,	0}
	};

	D3D11_INPUT_ELEMENT_DESC * SimpleVertex::Begin()
	{return Layout_;}

	UINT SimpleVertex::Size()
	{return ARRAYSIZE(Layout_);}

	SimpleVertex::SimpleVertex(XMFLOAT3 position, XMFLOAT3 color)
		: position_(position), color_(color)
	{}

	XMFLOAT3 SimpleVertex::GetPosition() const throw()
	{return position_;}

	XMFLOAT3 SimpleVertex::GetColor() const throw()
	{return color_;}

	void SimpleVertex::SetPosition(const XMFLOAT3 position) throw()
	{position_ = position;}

	void SimpleVertex::SetColor(const XMFLOAT3 color) throw()
	{color_ = color;}
}
