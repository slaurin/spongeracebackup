#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"

namespace GE
{
	class ColorMat
		: public Material
	{
		struct GlobalParams // toujours un multiple de 16 pour les constantes
		{
			XMMATRIX matWorldViewProj;	// Matrice WVP
		};

		//! Param�tres globaux
		GlobalParams globalParams_;

		//! Buffer correspondant aux param�tres globaux
		ID3D11Buffer * globalBuffer_;

	public:
		ColorMat(const Material *);

		~ColorMat();

		//! Retourne le nom de la classe
		static std::string GetClassName();

		//! Applique l'effet sur l'object � partir des valeurs de ce mat�riau
		template<class VertexT>
		void Apply(Effect<VertexT, ColorMat> * effect, const Object3D<VertexT, ColorMat> & object)
		{
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			globalParams_.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj);
			
			// On charge les constantes globales
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(globalBuffer_, 0, NULL, &globalParams_, 0, 0);
			effect->GetConstantBufferByName("GlobalParams")->SetConstantBuffer(globalBuffer_);

			effect->Apply(object);
		}

	private:
		void initGlobalParams();
	};
}