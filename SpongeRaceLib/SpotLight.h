#pragma once

#include "PointLight.h"

namespace GE
{
	//! Repr�sente une lumi�re de type spot
	class SpotLight
		: public PointLight
	{
		//! Direction de la source de lumi�re
		XMFLOAT4 direction_;

		//! Demi angle en radian du c�ne de lumi�re
		float angle_;

		//! Cosinus de l'angle - Evite les redondances de calcul
		float angleCos_;

		//! Exposant du calcul d'att�nuation du spot
		float exponent_;

	public:
		SpotLight(const XMFLOAT4 & position, const XMFLOAT4 & direction, const float angle, const float exponent, const XMFLOAT4 & diffuse, const XMFLOAT4 & specular);

		//! Fixe la direction de la lampe
		void SetDirection(const XMFLOAT4 &);

		//! Retourne la direction de la lampe
		XMFLOAT4 GetDirection() const;

		//! Fixe la valeur de l'angle en radian du c�ne
		void SetAngle(const float);

		//! Fixe la valeur de l'exposant d'att�nuation
		void SetExponent(const float);

		//! Retourne la valeur de l'angle en radian du c�ne
		float GetAngle() const;

		//! Retourne la valeur du cosinus de l'angle
		float GetAngleCos() const;

		//! Retourne la valeur de l'exposant d'att�nuation
		float GetExponent() const;
	};
}
