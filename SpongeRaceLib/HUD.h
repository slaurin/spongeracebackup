#pragma once

#include "Pannel2DMat.h"
#include "TextDrawer.h"

namespace GameUI
{
	class HUD
	{
		GE::Pannel2DMat * hudMat_;
		GE::Pannel2DMat * timerMat_;
		//GE::Pannel2DMat * billboardMat_;
		GE::TextDrawer text_;

	public:
		HUD();
		~HUD();

		void UpdateHUD(int nbTurbo);
		void UpdateChrono(time_t seconds);
	};
}
