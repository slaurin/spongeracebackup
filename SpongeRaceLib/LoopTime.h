#ifndef LOOP_TIME_H
#define LOOP_TIME_H

#include "FasterTimer.h"
#include "Singleton.h"

const int FPS = 60;

//////////////////////////////////////////////////////////////////////////
// All the times are in Milliseconds unless otherwise specified //////////
//////////////////////////////////////////////////////////////////////////
class LoopTime : public Singleton<LoopTime>
{
public:
    friend class Singleton<LoopTime>;
private:
    LoopTime();

    FasterTimer timer_;
    bool canUpdate_;

    LONGLONG previousTime_;
    LONGLONG nextTime_;
	int gapTime_;

    double elapsedTime_;
    double elapsedTimeSeconds_;

    unsigned long frame_;
    double rate_;
    double active_;
public:
    bool CanUpdate() const throw();
    
    float ElapsedTimeMilliseconds();
    float ElapsedTimeSeconds();

    unsigned long Frame() const throw();

    double GetRate() const throw();
    void SetRate(double rate);

    void Start() throw();
    void Stop() throw();
    void SetPause(bool isPaused) throw();
    void Update() throw();
};
#endif