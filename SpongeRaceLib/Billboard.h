#ifndef BILLBOARD_H
#define BILLBOARD_H

#include "stdafx.h"
#include "Object3D.h"
#include "MeshVertex.h"

namespace GE
{
    class Billboard : public Object3D<MeshVertex, PhongMat>
    {
    private:
        XMFLOAT3 scale_;
    public:
        Billboard(
            Effect<MeshVertex, PhongMat> *effect, 
            PhongMat *pMaterial,
            XMFLOAT3 scale = XMFLOAT3(1.0f, 1.0f, 1.0f));

        virtual void Anime(float timeElapsed);

    protected:
        static MeshVertex billboardVertices_[];
        static index_t billboardIndices_[];

    };
}
#endif