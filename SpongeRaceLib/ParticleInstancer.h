#ifndef PARTICLE_INSTANCER_H
#define PARTICLE_INSTANCER_H

#include "Object3D.h"
#include "Quad.h"
#include "ParticleVertex.h"
#include "ParticleMat.h"
#include <vector>

namespace GE
{
    //template<class TObject>
    class ParticleInstancer : public Object3D<ParticleVertex, ParticleMat>
    {
        friend class ParticleInstancer;
        enum{MAX_NUMBER_OF_PARTICLES = 1500};
    private:
        Quad* instance_;

        // Particle System Variables
        float particleDeviationX_, particleDeviationY_, particleDeviationZ_;
        float particleVelocity_, particleVelocityVariation_;
        float particleSize_, particlesPerSecond_;
        int maxParticles_;
        int currentParticleCount_;
        float accumulatedTime_;
        std::vector<Quad*> particles_;

    public:
        ParticleInstancer(Effect<ParticleVertex, ParticleMat> *effect,
                          ParticleMat *pMaterial,
                          int maxParticles = MAX_NUMBER_OF_PARTICLES);
        ~ParticleInstancer();

        virtual void Anime(float timeElapsed);
		virtual void Draw();

        void EmitParticles(float timeElapsed);
        void UpdateParticles(float timeElapsed);
        void KillParticles();

    private:
        void InitializeParticleSystem();
        void ShutdownParticleSystem();

        void AnimeParticles(float timeElapsed);
        void DrawParticles();
    };
}

#endif