#include "stdafx.h"

#include "GraphicObject.h"
#include "SpongeRace.h"
#include "ConstantsControl.h"
#include "ObstacleActor.h"
#include "Controller.h"
#include "PxPhysicsAPI.h"

using namespace physx;
using namespace GE;

using std::cout;
using std::endl;


//=============================================================================
// CLASS ObstacleActorData
//=============================================================================
class ObstacleActorData: public IActorData
{
public:
	// physics
	PxMaterial *material_;
	PxShape* actorShape_;


	// rendering
	GraphicObject* model_;

public:
	ObstacleActorData()
		: material_(nullptr)
		, model_(nullptr)
	{}

	int load()
	{
		//---------------------------------------------------------------------
		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		//static friction, dynamic friction, restitution
		material_ = physics.createMaterial(0.5f, 0.5f, 0.1f); 

		return 0;
	}

};


class ObstacleActorState
{
public:
	ObstacleActorState()
		: _lastFireTime(0)
	{}

	double _lastFireTime;
};


class ObstacleActorImp : public Actor<ObstacleActorData, ObstacleActor>
{
public:

	// physics	
	GraphicObject* graphicObj_;

	ObstacleActorState state_;

public:
	ObstacleActorImp(const IActorDataRef &aDataRef)
		: Actor(aDataRef, typeId()), graphicObj_(nullptr)
	{}

public:

	void setGraphicObj(GraphicObject* obj) override
	{
		graphicObj_=obj;
	}

	//-------------------------------------------------------------------------
	//
	static ActorFactory::ActorID typeId()
	{
		return ObstacleActor::typeId();
	}

	//-------------------------------------------------------------------------
	//
	static IActorData* loadData()
	{
		ObstacleActorData *data = new ObstacleActorData();
		data->load();
		return data;
	}

	//-------------------------------------------------------------------------
	//
	static IActor* createInstance(const IActorDataRef &aDataRef)
	{
		ObstacleActorImp *actor = new ObstacleActorImp(aDataRef);
		return actor;
	}

	//-------------------------------------------------------------------------
	//
	virtual void onSpawn(const PxTransform &aPose, PxGeometry * geometry) override
	{
		if(!geometry)
			throw GivenGeometryIsInvalid();

		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		pxActor_ = GAME->scene()->getPhysics().createRigidStatic(aPose);
		pxActor_->userData = this;
		pxActor_->setName("Obstacle");

		actorShape_ = pxActor_->createShape(*geometry, *_data->material_);
		actorShape_->setName("Obstacle");

		pxActor_->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
		GAME->scene()->addActor(*pxActor_);


		PxFilterData filterData;
		filterData.word0 = eACTOR_OBSTACLE;
		filterData.word1 = 0;
		actorShape_->setSimulationFilterData(filterData);

		state_._lastFireTime = 0;

		if (graphicObj_) graphicObj_->SetWorldMatrix(GetWorldTransform());
	}

	//-------------------------------------------------------------------------
	//
	virtual void onUnspawn() override
	{
		// cleanup physics objects
		if (pxActor_)
		{
			pxActor_->release();
			pxActor_= nullptr;
		}
	}


};


//-----------------------------------------------------------------------------
//
ActorFactory::ActorID ObstacleActor::typeId()
{	return "ObstacleActor"; }

RegisterActorType<ObstacleActorImp> gRegisterActor;