#include "StdAfx.h"
#include "Chrono.h"


Chrono::Chrono()
	: isPaused_(true), amountOfTime_(time_t()), startTime_(time_t()), isRunning_(false)
{
}

void Chrono::Start()
{
	isRunning_ = true;

	if(isPaused_)
	{
		isPaused_ = false;
		startTime_ = time(nullptr);
	}
}

void Chrono::Pause()
{
	if(!isPaused_)
	{
		amountOfTime_ += time(nullptr) - startTime_;
		isPaused_ = true;
	}
}

bool Chrono::IsPaused()
{
	return isPaused_;
}

bool Chrono::IsRunning()
{
	return isRunning_;
}

time_t Chrono::GetTime()
{
	return amountOfTime_ + (time(nullptr) - startTime_);
}

void Chrono::Reset()
{
	amountOfTime_ = time_t();
	isPaused_ = true;
	isRunning_ = false;
}

Chrono::~Chrono()
{
}
