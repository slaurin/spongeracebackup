#pragma once

#include "StaticActor.h"
#include "ActorFactory.h"

class GE::GraphicObject;
class Controller;

class RaceTrack : public StaticActor
{
public:

	RaceTrack(const ActorFactory::ActorID &aActorId)
		: StaticActor(aActorId)
	{}


	static ActorFactory::ActorID typeId();
};

