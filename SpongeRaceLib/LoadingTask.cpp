#include "stdafx.h"
#include "LoadingTask.h"

#include "PlayerActor.h"
#include "UserController.h"
#include "SpawnManager.h"
#include "LoopTime.h"
#include "IEffect.h"
#include "Effect.h"
#include "EffectManager.h"
#include "GeometryManager.h"
#include "Instancer.h"
#include "MeshVertex.h"
#include "PhongMat.h"
#include "MiniPhongMat.h"
#include "Axes.h"
#include "ColorMat.h"
#include "SimpleVertex.h"
#include "SimpleVertex4.h"
#include "TangentMeshVertex.h"
#include "SpriteVertex.h"
#include "BubbleMat.h"
#include "TransparentMat.h"
#include "ParticleMat.h"
#include "ParticleVertex.h"
#include "SimpleMat.h"
#include "FlagWaveMat.h"
#include "Splines.h"
#include "ObstacleActor.h"
#include "RaceTrack.h"
#include "PhysXConverter.h"
#include "TextureManager.h"
#include "ParticleVertex.h"
#include "ParticleMat.h"
#include "ParticleInstancer.h"
#include "Billboard.h"

using namespace std;
using namespace physx;
using namespace GE;

class LoadingTask::LoadingTaskImp
{
	bool scene1Loaded_;
	bool scene2Loaded_;
	bool notified_;
	vector<GraphicEngine::sceneIndex> indexScenes_;

public:
	LoadingTaskImp()
		: scene1Loaded_(false), scene2Loaded_(false), notified_(false)
	{}

	void init()
	{	
		if(!InitShaders())
			throw FailedToLoadShaders();
	}

	void cleanup()
	{
		
	}

	GraphicEngine::sceneIndex GetSceneIndex(unsigned int scene)
	{
		assert(scene < indexScenes_.size());
		return indexScenes_[scene];
	}

	void update()
	{
		if(!scene1Loaded_)
			loadScene1();

		else if(!scene2Loaded_)
			loadScene2();
	}

	bool InitShaders()
	{
		// Chargement des effets
		EffectManager::GetInstance().Add("Phong", new Effect<MeshVertex, PhongMat>(L"..\\SpongeRace\\Resources\\Shaders\\Phong.fx"));
		EffectManager::GetInstance().Add("Color", new Effect<SimpleVertex, ColorMat>(L"..\\SpongeRace\\Resources\\Shaders\\Color.fx"));
		EffectManager::GetInstance().Add("Color4", new Effect<SimpleVertex4, ColorMat>(L"..\\SpongeRace\\Resources\\Shaders\\Color.fx"));
		EffectManager::GetInstance().Add("SpriteFX", new Effect<SpriteVertex, Pannel2DMat>(L"..\\SpongeRace\\Resources\\Shaders\\Sprite.fx"));		
		EffectManager::GetInstance().Add("BubbleFx", new Effect<TangentMeshVertex, BubbleMat>(L"..\\SpongeRace\\Resources\\Shaders\\BubbleFx.fx"));
		EffectManager::GetInstance().Add("TransAdv", new Effect<MeshVertex, TransparentMat>(L"..\\SpongeRace\\Resources\\Shaders\\AdvancedTransparent.fx"));
		EffectManager::GetInstance().Add("SimpleTex", new Effect<MeshVertex, SimpleMat>(L"..\\SpongeRace\\Resources\\Shaders\\SimpleShader.fx"));
		EffectManager::GetInstance().Add("Particle", new Effect<ParticleVertex, ParticleMat>(L"..\\SpongeRace\\Resources\\Shaders\\Particle.fx"));
		EffectManager::GetInstance().Add("FlagWave", new Effect<MeshVertex, FlagWaveMat>(L"..\\SpongeRace\\Resources\\Shaders\\ShaderFlag.fx"));

		return true;
	}

	void Notify()
	{
		if(!notified_)
		{
			GraphicEngine::GetInstance().SwitchScene(GetSceneIndex(1));
			GAME->camera()->switchToSpline(1);
			notified_ = true;
		}
		
	}

	void loadScene1()
	{
		// R�cup�ration des effets
		auto phong = EffectManager::GetInstance().getEffect<MeshVertex, PhongMat>("Phong");
		auto color = EffectManager::GetInstance().getEffect<SimpleVertex, ColorMat>("Color");
		auto color4 = EffectManager::GetInstance().getEffect<SimpleVertex4, ColorMat>("Color4");
		auto spriteFX = EffectManager::GetInstance().getEffect<SpriteVertex, Pannel2DMat>("SpriteFX");
		auto simpleFX = EffectManager::GetInstance().getEffect<MeshVertex, SimpleMat>("SimpleTex");
		auto particleEffect = EffectManager::GetInstance().getEffect<ParticleVertex, ParticleMat>("Particle");
		auto TransEffect = EffectManager::GetInstance().getEffect<MeshVertex, TransparentMat>("TransAdv");
		auto WaveEffect = EffectManager::GetInstance().getEffect<MeshVertex, FlagWaveMat>("FlagWave");

		indexScenes_.push_back(GraphicEngine::GetInstance().NewScene("Baignoire", true));
		auto scene1 = GraphicEngine::GetInstance().GetScene(indexScenes_.back());
		
		///// Scene Lights /////
		DirectionalLight dl = DirectionalLight(
				XMFLOAT4(0.f, 1.f, 0.f, 0.0f),// Direction
				XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f),// Illumination diffuse
				XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f));// Illumination sp�culaire
	
		// Ajoute la lumi�re directionnelle �clairant la sc�ne depuis le haut
		scene1->AddLight(dl);

		// G�re la lumi�re ambiente de la sc�ne
		scene1->SetAmbientLighting(XMFLOAT4(0.4f, 0.4f, 0.4f, 1.f));

		// Graphique player
		scene1->AddObject(GAME->player()->getGraphicObj());

	
		//Spline Tuyau
		auto splineGeo = GeometryManager::GetInstance().Request("sppline", "..\\SpongeRace\\Resources\\Models\\SplineTuyaux.obj", true, false, false, true);
		DrawableBSpline * spline = Instancer::CreateBSpline(color4, &(splineGeo->parts().front()),new ColorMat(new Material));
		Splines::GetInstance().addSpline(1,spline);
		//DrawableBSpline* spline = new DrawableBSpline(color4, new ColorMat(new Material), spline->vertexBuffer_, 100);
		//scene1->AddObject(spline);

		//Graphique Jouets
		auto jouetsGeo = GeometryManager::GetInstance().Request("Jouets", "..\\SpongeRace\\Resources\\Models\\Jouets\\jouets.obj", true, false, false,true);
		auto jouets = Instancer::Create(phong, jouetsGeo);
		scene1->AddObject(jouets);
		

		// Graphique Baignoire
		auto baignoireGeo = GeometryManager::GetInstance().Request("Baignoire", "..\\SpongeRace\\Resources\\Models\\Baignoire\\baignoire.obj", true, false, false,true);
		auto baignoire = Instancer::Create(phong, &baignoireGeo->parts().front());
		scene1->AddObject(baignoire);
		
		// Bubbles
		GAME->CreateBubbles(100, scene1);
	
		// Graphique Tuyau
		auto TuyauGeo = GeometryManager::GetInstance().Request("Tuyau", "..\\SpongeRace\\Resources\\Models\\tuyau.obj", true, false, false, true);
		auto Tuyau = Instancer::Create(phong, &(TuyauGeo->parts().front()));
		scene1->AddObject(Tuyau);

		// Graphique D�part
		auto DrapeauGeo = GeometryManager::GetInstance().Request("drapeau", "..\\SpongeRace\\Resources\\Models\\Depart\\Drapeau.obj", true, false, false, true);
		auto Drapeau = Instancer::Create(WaveEffect, &(DrapeauGeo->parts().front()));
		scene1->AddObject(Drapeau);

		auto PoteauxGeo = GeometryManager::GetInstance().Request("Poteaux", "..\\SpongeRace\\Resources\\Models\\Depart\\Poteau.obj", true, false, false, true);
		auto Poteaux = Instancer::Create(phong, &(PoteauxGeo->parts().front()));
		scene1->AddObject(Poteaux);

		// Graphique  Depart
		auto BarreBotGeo = GeometryManager::GetInstance().Request("BarreBot", "..\\SpongeRace\\Resources\\Models\\Depart\\BarreSol.obj", true, false, false, true);
		auto BarreBot = Instancer::Create(TransEffect, &(BarreBotGeo->parts().front()));
		scene1->AddObject(BarreBot);

		// Graphique Skybox
		auto BoxBathGeo = GeometryManager::GetInstance().Request("BoxBath", "..\\SpongeRace\\Resources\\Models\\BoxBath\\BoxBath.obj", true, false, false, true);
		auto BoxBath = Instancer::Create(simpleFX, &(BoxBathGeo->parts().front()));
		scene1->AddObject(BoxBath);

		//Graphique Barrieres
		auto barrieresGeo = GeometryManager::GetInstance().Request("Barriere", "..\\SpongeRace\\Resources\\Models\\BarriereSmooth.obj", true, false, false, true);
		auto barriere = Instancer::Create(TransEffect, &(barrieresGeo->parts().front()));
		scene1->AddObject(barriere);

        // Particles 
        D3D11_SAMPLER_DESC sampler = Texture::defaultSamplerDesc;
        sampler.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
        TextureManager::GetInstance().AddTexture(L"eau", new Texture(L"..\\SpongeRace\\Resources\\Textures\\EauSprite2.png", sampler));
        ParticleMat* pmat = new ParticleMat(new Material);
        pmat->InsertTexture(aiTextureType_AMBIENT, TextureManager::GetInstance().GetTexture(L"eau"));
        ParticleInstancer* particleInstancer = new ParticleInstancer(particleEffect, pmat);
        scene1->AddObject(particleInstancer);

		//Physique Jouets	
		auto jouetsPhy = GeometryManager::GetInstance().Request("JouetsPhy","..\\SpongeRace\\Resources\\Models\\WallCollision_PHY.obj", true, false, false, true);
		physx::PxTriangleMesh* physxObstacleMesh = GenerateTriangleMeshFromGeometry(GAME->scene()->getPhysics(), &(jouetsPhy->parts().front()));
		GAME->spawnManager()->spawn<ObstacleActor>(PxTransform(PxVec3(0, 0, 0)), physxObstacleMesh, jouets);
		
		// Physique Baignoire
		physx::PxTriangleMesh* physxRaceTrackMesh = GenerateTriangleMeshFromGeometry(GAME->scene()->getPhysics(), &(baignoireGeo->parts().front()));
		GAME->spawnManager()->spawn<RaceTrack>(PxTransform(PxVec3(0, 0, 0)), physxRaceTrackMesh, baignoire);

		scene1Loaded_ = true;
	}

	void loadScene2()
	{
		// R�cup�ration des effets
		auto phong = EffectManager::GetInstance().getEffect<MeshVertex, PhongMat>("Phong");
		auto color = EffectManager::GetInstance().getEffect<SimpleVertex, ColorMat>("Color");
		auto color4 = EffectManager::GetInstance().getEffect<SimpleVertex4, ColorMat>("Color4");
		auto spriteFX = EffectManager::GetInstance().getEffect<SpriteVertex, Pannel2DMat>("SpriteFX");
		auto simpleFX = EffectManager::GetInstance().getEffect<MeshVertex, SimpleMat>("SimpleTex");
		auto particleEffect = EffectManager::GetInstance().getEffect<ParticleVertex, ParticleMat>("Particle");
		auto TransEffect = EffectManager::GetInstance().getEffect<MeshVertex, TransparentMat>("TransAdv");
		auto bubbleEffect = EffectManager::GetInstance().getEffect<TangentMeshVertex, BubbleMat>("BubbleFx");
		auto WaveEffect = EffectManager::GetInstance().getEffect<MeshVertex, FlagWaveMat>("FlagWave");

		indexScenes_.push_back(GraphicEngine::GetInstance().NewScene("Tuyau", true));
		auto scene2 = GraphicEngine::GetInstance().GetScene(indexScenes_.back());

		for(float i = 0.f; i < 1.f; i += 0.1f)
		{
			XMFLOAT4 pos = Splines::GetInstance().getSpline(1)->getPositionByInterpolate(i);
			PointLight pl(
				pos,// Direction
				XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f),// Illumination diffuse
				XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f));// Illumination sp�culaire
			pl.SetAttenuation(0., 0.f, 0.1f, 0.f);
			scene2->AddLight(pl);
		}

		scene2->AddLight(GAME->player()->GetRightLight());
		scene2->AddLight(GAME->player()->GetLeftLight());

		// G�re la lumi�re ambiente de la sc�ne
		scene2->SetAmbientLighting(XMFLOAT4(0.f, 0.f, 0.f, 0.f));

		// Graphique player
		scene2->AddObject(GAME->player()->getGraphicObj());

		// Graphique Tuyau
		auto TuyauGeo = GeometryManager::GetInstance().Request("Tuyau", "..\\SpongeRace\\Resources\\Models\\tuyau.obj", true, false, false, true);
		auto Tuyau = Instancer::Create(phong, &(TuyauGeo->parts().front()));
		scene2->AddObject(Tuyau);

		// Physique Tuyau
		physx::PxTriangleMesh* physxTuyauMesh = GenerateTriangleMeshFromGeometry(GAME->scene()->getPhysics(), &(TuyauGeo->parts().front()));
		StaticActor* TuyauActor = GAME->spawnManager()->spawn<RaceTrack>(PxTransform(PxVec3(0, 0, 0)), physxTuyauMesh, Tuyau);

		scene2Loaded_ = true;
	}
};

void LoadingTask::update()
{
	imp_->update();
}

void LoadingTask::init()
{
	imp_->init();
}

GraphicEngine::sceneIndex LoadingTask::GetSceneIndex(unsigned int scene)
{
	return imp_->GetSceneIndex(scene);
}

void LoadingTask::cleanup()
{
	imp_->cleanup();
}

void LoadingTask::Notify()
{
	imp_->Notify();
}

LoadingTask::LoadingTask()
	: imp_(new LoadingTaskImp())
{}

LoadingTask::~LoadingTask()
{
	delete imp_;
}