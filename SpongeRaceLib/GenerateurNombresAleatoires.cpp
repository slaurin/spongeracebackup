#include "stdafx.h"
#include "GenerateurNombresAleatoires.h"
#include <cstdlib>
using std::rand;
using std::srand;
#include <ctime>
using std::time;

GenerateurNombresAleatoires GenerateurNombresAleatoires::singleton;

GenerateurNombresAleatoires::GenerateurNombresAleatoires() throw()
   { srand(static_cast<unsigned int>(time(0))); }

GenerateurNombresAleatoires::value_type
   GenerateurNombresAleatoires::generer() const throw()
      { return (float)(rand()/RAND_MAX); }

//
// Bornes inclusives
//
GenerateurNombresAleatoires::value_type
   GenerateurNombresAleatoires::generer
      (const value_type b_min, const value_type b_max) const
{
   const value_type INTERVALLE = b_max - b_min + 1;
   if (INTERVALLE <= 0) throw BornesInvalides();
   return (b_min + (float)rand()/((float)RAND_MAX/(b_max-b_min)));
}