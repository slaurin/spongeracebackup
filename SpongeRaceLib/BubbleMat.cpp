#include "stdafx.h"
#include "BubbleMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace DxUtilities;
using namespace std;

namespace GE
{
	BubbleMat::BubbleMat(const Material * material)
		: Material(*material), timeVal(0.f)
	{
		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ShadersParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &constantBuffer);

	}

	BubbleMat::BubbleMat(const BubbleMat & material)
		: Material(material), timer_(material.timer_), constantBuffer(material.constantBuffer), timeVal(float())
	{
		constantBuffer->AddRef();
	}

	string BubbleMat::GetClassName()
	{
		return "BubbleMat";
	}

	BubbleMat::~BubbleMat(void)
	{
		DXRelacher(constantBuffer);
	}

}
