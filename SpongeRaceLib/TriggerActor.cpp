#include "StdAfx.h"
#include "TriggerActor.h"

#include "SpongeRace.h"
#include "ConstantsControl.h"
#include "PxPhysicsAPI.h"

using namespace physx;
using namespace GE;


class TriggerActorData: public IActorData
{
public:
	// physics
	PxMaterial *material_;
	PxShape* actorShape_;
	

public:
	TriggerActorData()
		: material_(nullptr)
	{}

	int load()
	{
		//---------------------------------------------------------------------
		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		//static friction, dynamic friction, restitution
		material_ = physics.createMaterial(0.5f, 0.5f, 0.1f); 

		return 0;
	}

};

class TriggerActorImp : public Actor<TriggerActorData, TriggerActor>
{
	std::string triggerWith_;
	ITriggerCallback* callbackObj_;

public:
	TriggerActorImp(const IActorDataRef &aDataRef)
		: Actor(aDataRef, typeId()), callbackObj_(nullptr), triggerWith_()
	{}

public:
	
	void SetTriggerWith(std::string name)
	{
		triggerWith_=name;
	}

	void SetCallbackObj(ITriggerCallback* callbackObj)
	{
		callbackObj_ = callbackObj;
	}

	//-------------------------------------------------------------------------
	//
	static IActorData* loadData()
	{
		TriggerActorData *data = new TriggerActorData();
		data->load();
		return data;
	}


	//-------------------------------------------------------------------------
	//
	static ActorFactory::ActorID typeId()
	{
		return TriggerActor::typeId();
	}

	//-------------------------------------------------------------------------
	//
	static IActor* createInstance(const IActorDataRef &aDataRef)
	{
		TriggerActorImp *actor = new TriggerActorImp(aDataRef);
		return actor;
	}

	void onTrigger(bool triggerEnter, physx::PxShape *actorShape, physx::PxShape *contactShape) 
	{
		if(triggerWith_.empty()) return;

		//if (actorShape && actorShape->getName() && std::string(actorShape->getName())==triggerWith_) {
		//	if (callbackObj_) callbackObj_->Notify();
		//}

		if (contactShape && contactShape->getName() && std::string(contactShape->getName())==triggerWith_) {
			if (callbackObj_) callbackObj_->Notify();
		}
		
	};




	//-------------------------------------------------------------------------
	//
	virtual void onSpawn(const PxTransform &aPose, PxGeometry * geometry) override
	{
		if(!geometry)
			throw GivenGeometryIsInvalid();

		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		pxActor_ = GAME->scene()->getPhysics().createRigidStatic(aPose);
		pxActor_->userData = this;
		pxActor_->setName("Trigger");

		actorShape_ = pxActor_->createShape(*geometry, *_data->material_);
		actorShape_->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		actorShape_->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);		
		actorShape_->setName("Trigger");

		GAME->scene()->addActor(*pxActor_);
	}

	//-------------------------------------------------------------------------
	//
	virtual void onUnspawn() override
	{
		// cleanup physics objects
		if (pxActor_)
		{
			pxActor_->release();
			pxActor_= nullptr;
		}
	}


};


//-----------------------------------------------------------------------------
//
ActorFactory::ActorID TriggerActor::typeId()
{	return "TriggerActor"; }

RegisterActorType<TriggerActorImp> gRegisterActor;