#include "stdafx.h"
#include "AssimpLoader.h"
#include "Object3D.h"
#include "Texture.h"
#include "Material.h"
#include "Mesh.h"
#include "TextureManager.h"
#include "LogManager.h"
#include "Pathing.h"

using namespace std;
using namespace Assimp;
using namespace GE;

namespace GE
{
	AssimpLoader::AssimpLoader(void)
	{}

	void AssimpLoader::DoTheErrorLogging(const char * error)
	{
		LogManager log("AssimpLogging");
		log.write(error);
	}

	Geometry * AssimpLoader::Import(
			const string& pFile, 
			bool flipUV,
			bool genUV,
			bool flipWindingOrder,
			bool genSmoothNormal)
		{
			string fullFilePath = pFile;
			string filePath = pFile.substr(0, pFile.find_last_of('\\') + 1);
			string fileName = pFile.substr(pFile.find_last_of('\\') + 1);

			// Le destructeur de l'importer nettoira la structure interne de Assimp
			Assimp::Importer importer;

			unsigned int flags =
				aiProcess_CalcTangentSpace
				| aiProcess_Triangulate
				| aiProcess_JoinIdenticalVertices
				| aiProcess_SortByPType
				| aiProcess_Debone;

			if(flipUV)
				flags |= aiProcess_FlipUVs;
			if(flipWindingOrder)
				flags |= aiProcess_FlipWindingOrder;
			if(genSmoothNormal)
				flags |= aiProcess_GenSmoothNormals;
			if(genUV)
				flags |= aiProcess_GenUVCoords;

			//unsigned int size = GeometryManager::GetInstance().size();
			Geometry* geo = new Geometry(fileName);

			// Charge l'asset � partir du fichier dans la structure interne de Assimp
			const aiScene* scene = importer.ReadFile(pFile, flags);

			// S'il y a une erreur, on la log
			if(!scene)
			{
				const char* errorChars = importer.GetErrorString();
				DoTheErrorLogging(errorChars);
				throw AssimpException(string(errorChars));
			}

			vector<Material *> materials;
			//materials.push_back(MaterialManager::GetInstance().getDefault<Material>());

			// S'il y a des mat�riaux, on les charge
			if(scene->HasMaterials())
			{
				// On charge les mat�riaux (0 = Default, on zappe)
				for(unsigned int i = 0; i < scene->mNumMaterials; ++i)
				{
					aiString name; 
					scene->mMaterials[i]->Get(AI_MATKEY_NAME, name);
					Material* mat = nullptr;

					if(MaterialManager::GetInstance().Contains(name.C_Str()))
					{
						mat = MaterialManager::GetInstance().getMaterial<Material>(name.C_Str());
					}
					else
					{
						MaterialManager::GetInstance().Add(name.C_Str(), loadMaterial(filePath, scene->mMaterials[i]));
						mat = &MaterialManager::GetInstance()[name.C_Str()];
					}

					materials.push_back(mat);
				}
			}

			// On charge le mesh
			if(scene->HasMeshes())
			{
				// Pour chaque sous-mesh
				for(unsigned int m = 0; m < scene->mNumMeshes; ++m)
				{
					aiMesh * mesh = scene->mMeshes[m];
					string meshName = mesh->mName.C_Str();
					if(meshName == "")
						meshName = fileName + Convert::convertPrimitiveToString(m);

					geo->parts().push_back(GeometryPart(meshName, materials[mesh->mMaterialIndex], mesh));
				}
			}

			return geo;
		}

	void AssimpLoader::loadTextures(string filePath, const aiMaterial * aiMaterial, Material & material)
	{
		// On charge les textures du mat�riau
		for(unsigned int j = 0; j < aiTextureType_UNKNOWN; ++j)
		{
			for(unsigned int k = 0; k < aiMaterial->GetTextureCount(aiTextureType(j)); ++k)
			{
				aiString path;
				aiTextureMapping mapping;
				unsigned int uvindex;
				float blend;
				aiTextureOp op;
				aiTextureMapMode mapmode;
				aiMaterial->GetTexture(aiTextureType(j), k, &path, &mapping, &uvindex, &blend, &op, &mapmode);

				string a(path.C_Str()); 
				wstring wpath(a.length(), L' '); // Make room for characters

				// Copy string to wstring.
				copy(a.begin(), a.end(), wpath.begin());
				Texture * tex = TextureManager::GetInstance()[wpath];
				boost::filesystem::path p = Pathing::Path();
				if(wpath.length() != 0 && tex == nullptr)
				{
					if(wpath.find_last_of('\\') == -1)
						tex = new Texture(wstring().assign(filePath.begin(), filePath.end()) + wpath);
					else
						tex = new Texture(wpath);
					TextureManager::GetInstance().Add(wpath, tex);
				}
				material.InsertTexture(aiTextureType(j), tex);
			}
		}
	}

	Lighting AssimpLoader::loadLighting(const aiMaterial * material)
	{
		// On charge les lumi�res
		aiColor3D ambient;
		material->Get(AI_MATKEY_COLOR_AMBIENT, ambient);

		aiColor3D diffuse;
		material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);

		aiColor3D specular;
		material->Get(AI_MATKEY_COLOR_SPECULAR, specular);

		aiColor3D emissive;
		material->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);

		aiColor3D transparent;
		material->Get(AI_MATKEY_COLOR_TRANSPARENT, transparent);

		return move(Lighting(Convert::aiToXMFloat4(ambient), Convert::aiToXMFloat4(diffuse), Convert::aiToXMFloat4(specular), Convert::aiToXMFloat4(emissive), Convert::aiToXMFloat4(transparent)));
	}

	Material AssimpLoader::loadMaterial(string filePath, const aiMaterial * aiMaterial)
	{
		aiString name; 
		aiMaterial->Get(AI_MATKEY_NAME, name);

		Material material(name.C_Str(), loadLighting(aiMaterial));
		loadTextures(filePath, aiMaterial, material);

		return move(material);
	}

	AssimpLoader::~AssimpLoader(void)
	{}
}