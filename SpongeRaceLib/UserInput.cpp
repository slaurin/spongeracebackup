#include "stdafx.h"
#include "UserInput.h"
#include "GraphicEngine.h"
#include "Window.h"


UserInput::UserInput() :	
	m_directInput(0),
	m_keyboard(0),
	m_mouse(0)
{
}


bool UserInput::Initialize(DWORD coopLevel, std::bitset<DEVICE_COUNT> devices)
{
	HRESULT result;
	HINSTANCE hInstance = Window::getHAppInstance();
	HWND hwnd = Window::getHMainWnd();

	result = DirectInput8Create(hInstance,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void**)&m_directInput,
		NULL);

	if(FAILED(result))
	{
		return false;
	}

	if(devices[KEYBOARD]) {
		// Initialize the direct input interface for the keyboard.
		result = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL);
		if(FAILED(result))
		{
			return false;
		}

		// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
		result = m_keyboard->SetDataFormat(&c_dfDIKeyboard);
		if(FAILED(result))
		{
			return false;
		}

		// Set the cooperative level of the keyboard to not share with other programs.
		result = m_keyboard->SetCooperativeLevel(hwnd, coopLevel);
		if(FAILED(result))
		{
			return false;
		}

			// Now acquire the keyboard.
		result = m_keyboard->Acquire();
		if(FAILED(result))
		{
			return false;
		}
	}

	if(devices[MOUSE]) {
			// Initialize the direct input interface for the mouse.
		result = m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
		if(FAILED(result))
		{
            devices[MOUSE]=false;
			//return false;
		}

		// Set the data format for the mouse using the pre-defined mouse data format.
		result = m_mouse->SetDataFormat(&c_dfDIMouse);
		if(FAILED(result))
		{
            devices[MOUSE]=false;
			//return false;
		}

			// Set the cooperative level of the mouse to share with other programs.
		result = m_mouse->SetCooperativeLevel(hwnd, coopLevel);
		if(FAILED(result))
		{
            devices[MOUSE]=false;
			//return false;
		}

			// Acquire the mouse.
		result = m_mouse->Acquire();
		if(FAILED(result))
		{
            devices[MOUSE]=false;
			//return false;
		}
	}

	if (devices[JOYSTICK]) {
		//joystick
		result =m_directInput->CreateDevice(IID_IDirectInput8, &m_joystick, NULL); 
		if (FAILED(result)) {
            devices[JOYSTICK]=false;
				//return result;
		}

		// Look for the first simple joystick we can find.
		result = m_directInput->EnumDevices(DI8DEVCLASS_GAMECTRL, enumCallback, NULL, DIEDFL_ATTACHEDONLY);
		if (FAILED(result)) {
            devices[JOYSTICK]=false;
			//return result;
		}

		// Make sure we got a joystick
		if (m_joystick == NULL) {
            devices[JOYSTICK]=false;
			//return false;
		}

		// Set the data format to "simple joystick" - a predefined data format 
		//
		// A data format specifies which controls on a device we are interested in,
		// and how they should be reported. This tells DInput that we will be
		// passing a DIJOYSTATE2 structure to IDirectInputDevice::GetDeviceState().
		result = m_joystick->SetDataFormat(&c_dfDIJoystick2);
		if (FAILED(result)) {
            devices[JOYSTICK]=false;
			//return false;
		}

		// Set the cooperative level to let DInput know how this device should
		// interact with the system and with other DInput applications.
		result = m_joystick->SetCooperativeLevel(NULL, DISCL_EXCLUSIVE | DISCL_FOREGROUND);
		if (FAILED(result)) {
            devices[JOYSTICK]=false;
				//return false;
		}

		// Determine how many axis the joystick has (so we don't error out setting
		// properties for unavailable axis)
		capabilities.dwSize = sizeof(DIDEVCAPS);
		result = m_joystick->GetCapabilities(&capabilities);
		if (FAILED(result)) {
            devices[JOYSTICK]=false;
			//return result;
		}

		if (FAILED(result = m_joystick->EnumObjects(enumAxesCallback, NULL, DIDFT_AXIS))) {
            devices[JOYSTICK]=false;
			//return result;
		}
	}

	return true;
}

void UserInput::Shutdown()
{
	// Release the mouse.
	if(m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = 0;
	}

	// Release the keyboard.
	if(m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = 0;
	}

	if (m_joystick) { 
		m_joystick->Unacquire();
		m_joystick->Release();
		m_joystick=0;
	}

	// Release the main interface to direct input.
	if(m_directInput)
	{
		m_directInput->Release();
		m_directInput = 0;
	}

	return;
}

bool UserInput::Frame()
{
	bool result;

	if(m_mouse)
	{
		result = ReadMouse();
		if(!result)
		{
			return false;
		}
	}

	if(m_keyboard)
	{
		// Read the current state of the keyboard.
		result = ReadKeyboard();
		if(!result)
		{
			return false;
		}
	}

	if (m_joystick) { 
		// Read the current state of the mouse.
		result = ReadJoystick();
		if(!result)
		{
			return false;
		}
	}

	// Release the main interface to direct input.
	if(m_directInput)
	{
		m_directInput->Release();
		m_directInput = 0;
	}


	// Process the changes in the mouse and keyboard.
	ProcessInput();

	return true;
}

bool UserInput::ReadKeyboard()
{
	HRESULT result;


	// Read the keyboard device.
	result = m_keyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
	if(FAILED(result))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_keyboard->Acquire();
		}
		else
		{
			return false;
		}
	}
		
	return true;
}



bool UserInput::ReadMouse()
{
	HRESULT result;


	// Read the mouse device.
	result = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_mouseState);
	if(FAILED(result))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_mouse->Acquire();
		}
		else
		{
			return false;
		}
	}

	return true;
}


bool UserInput::ReadJoystick(){
	HRESULT     hr;

	if (m_joystick == NULL) {
		return true;
	}


	// Poll the device to read the current state
	hr = m_joystick->Poll(); 
	if (FAILED(hr)) {
		// DInput is telling us that the input stream has been
		// interrupted. We aren't tracking any state between polls, so
		// we don't have any special reset that needs to be done. We
		// just re-acquire and try again.
		hr = m_joystick->Acquire();
		while (hr == DIERR_INPUTLOST) {
			hr = m_joystick->Acquire();
		}

		// If we encounter a fatal error, return failure.
		if ((hr == DIERR_INVALIDPARAM) || (hr == DIERR_NOTINITIALIZED)) {
			return false;
		}

		// If another application has control of this device, return successfully.
		// We'll just have to wait our turn to use the joystick.
		if (hr == DIERR_OTHERAPPHASPRIO) {
			return true;
		}
	}

	// Get the input's device state
	hr = m_joystick->GetDeviceState(sizeof(DIJOYSTATE2), &m_joyState);
	if (FAILED(hr)) {
		return false; // The device should have been acquired during the Poll()
	}

	return true;
}

void UserInput::ProcessInput()
{
	int m_screenWidth = Window::GetInstance().GetScreenWidth();
	int m_screenHeight = Window::GetInstance().GetScreenHeight();

	// Update the location of the mouse cursor based on the change of the mouse location during the frame.
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;

	// Ensure the mouse location doesn't exceed the screen width or height.
	if(m_mouseX < 0)  { m_mouseX = 0; }
	if(m_mouseY < 0)  { m_mouseY = 0; }
	
	if(m_mouseX > m_screenWidth)  { m_mouseX = m_screenWidth; }
	if(m_mouseY > m_screenHeight) { m_mouseY = m_screenHeight; }
	
	return;
}

bool UserInput::IsEscapePressed()
{
	// Do a bitwise and on the keyboard state to check if the escape key is currently being pressed.
	if(m_keyboardState[DIK_ESCAPE] & 0x80)
	{
		return true;
	}

	return false;
}

bool UserInput::IsKeyPressed(unsigned char DIK_VALUE)
{
	// Do a bitwise and on the keyboard state to check if the escape key is currently being pressed.
	if(m_keyboardState[DIK_VALUE] & 0x80)
	{
		return true;
	}

	return false;
}


void UserInput::GetMouseLocation(int& mouseX, int& mouseY)
{
	mouseX = m_mouseX;
	mouseY = m_mouseY;
	return;
}

BOOL CALLBACK
	UserInput::enumCallback(const DIDEVICEINSTANCE* instance, VOID* context)
{
	HRESULT hr;

	// Obtain an interface to the enumerated joystick.
	hr = Singleton<UserInput>::GetInstance().m_directInput->CreateDevice(instance->guidInstance, &Singleton<UserInput>::GetInstance().m_joystick, NULL);

	// If it failed, then we can't use this joystick. (Maybe the user unplugged
	// it while we were in the middle of enumerating it.)
	if (FAILED(hr)) { 
		return DIENUM_CONTINUE;
	}

	// Stop enumeration. Note: we're just taking the first joystick we get. You
	// could store all the enumerated joysticks and let the user pick.
	return DIENUM_STOP;
}

BOOL CALLBACK
	UserInput::enumAxesCallback(const DIDEVICEOBJECTINSTANCE* instance, VOID* context)
{
	HWND hDlg = (HWND)context;

	DIPROPRANGE propRange; 
	propRange.diph.dwSize       = sizeof(DIPROPRANGE); 
	propRange.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
	propRange.diph.dwHow        = DIPH_BYID; 
	propRange.diph.dwObj        = instance->dwType;
	propRange.lMin              = -1000; 
	propRange.lMax              = +1000; 

	// Set the range for the axis
	HRESULT  hr = Singleton<UserInput>::GetInstance().m_joystick->SetProperty(DIPROP_RANGE, &propRange.diph);
	if (FAILED(hr)) {
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}
