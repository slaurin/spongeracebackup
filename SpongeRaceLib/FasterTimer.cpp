#include "stdafx.h"
#include "FasterTimer.h"

class QueryPerformanceFrequencyFailed{};

LONGLONG FasterTimer::getTime()
{
    LARGE_INTEGER time;
    DWORD_PTR oldmask = ::SetThreadAffinityMask(::GetCurrentThread(), 0);
    ::QueryPerformanceCounter(&time);
    ::SetThreadAffinityMask(::GetCurrentThread(), oldmask);
    return time.QuadPart * 1000 / frequency_;
}

void FasterTimer::setFrequency()
{
    LARGE_INTEGER proc_freq;
    if (!::QueryPerformanceFrequency(&proc_freq))
        throw QueryPerformanceFrequencyFailed();
    frequency_ = proc_freq.QuadPart;
}