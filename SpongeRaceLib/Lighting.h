#pragma once

namespace GE
{
	//! Concerne l'�clairage actuel du mat�riaux
	class Lighting
	{
	public:
		Lighting(
			XMFLOAT4 ambient		= XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f),
			XMFLOAT4 diffuse		= XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f),
			XMFLOAT4 specular		= XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f),
			XMFLOAT4 emissive		= XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
			XMFLOAT4 transparent	= XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

		Lighting(const Lighting &);

		XMFLOAT4 GetAmbient()	{return ambient_;}
		XMFLOAT4 GetDiffuse()	{return diffuse_;}
		XMFLOAT4 GetSpecular()	{return specular_;}
		XMFLOAT4 GetEmissive()	{return emissive_;}

	private:
		XMFLOAT4 ambient_;
		XMFLOAT4 diffuse_;
		XMFLOAT4 specular_;
		XMFLOAT4 emissive_;
		XMFLOAT4 transparent_;
	};
}
