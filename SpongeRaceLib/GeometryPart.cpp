#include "stdafx.h"
#include "GeometryPart.h"
#include "Convert.h"
#include "Vertex.h"

using namespace std;
using namespace physx;

namespace GE
{
	void GeometryPart::loadIndexes(const aiMesh * mesh)
	{
		if(mesh->HasFaces())
		{
			unsigned int indexesPerFaces = mesh->mFaces[0].mNumIndices;

			// Pr�pare le vector
			indexes_.resize(vector<index_t>::size_type(mesh->mNumFaces * indexesPerFaces));

			for(unsigned int f = 0; f < mesh->mNumFaces; ++f)
			{
				aiFace face = mesh->mFaces[f];
				for(unsigned int i = 0; i < indexesPerFaces; ++i)
					indexes_[f*indexesPerFaces+i] = face.mIndices[i];
			}
		}
	}

	void GeometryPart::loadVertices(const aiMesh * mesh)
	{
		// Pr�pare le vector
		vertices_.resize(vector<Vertex>::size_type(mesh->mNumVertices));

		for(unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			// ------ Normals ----------------------------------------------------------
			if(mesh->HasNormals())
			{
				vertices_[i].setNormal(Convert::aiToXMFloat3(mesh->mNormals[i]));
			}
			else
			{
				vertices_[i].setNormal(XMFLOAT3(0., 0., 0.));
			}

			// ------ Positions ----------------------------------------------------------
			if(mesh->HasPositions())
			{
				vertices_[i].setPosition(Convert::aiToXMFloat3(mesh->mVertices[i]));
			}
			else
			{
				vertices_[i].setPosition(XMFLOAT3(0., 0., 0.));
			}

			// ------ Tangents and Bitangents ---------------------------------------------
			if(mesh->HasTangentsAndBitangents())
			{
				vertices_[i].setTangent(Convert::aiToXMFloat3(mesh->mTangents[i]));
				vertices_[i].setBitangent(Convert::aiToXMFloat3(mesh->mBitangents[i]));
			}
			else
			{
				vertices_[i].setTangent(XMFLOAT3(0., 0., 0.));
				vertices_[i].setBitangent(XMFLOAT3(0., 0., 0.));
			}

			// ------ Texture Coords ---------------------------------------------
			if(mesh->HasTextureCoords(0))
			{
				vertices_[i].addTexCoord(Convert::aiToXMFloat2(mesh->mTextureCoords[0][i]));
			}
			else
			{
				vertices_[i].addTexCoord(XMFLOAT2(0., 0.));
			}

			// ------ Vertex Colors ---------------------------------------------
			if(mesh->HasVertexColors(0))
			{
				vertices_[i].addColor(Convert::aiToXMFloat4(mesh->mColors[0][i]));
			}
			else
			{
				vertices_[i].addColor(XMFLOAT4(0., 0., 0., 0.));
			}
		}
	}

	GeometryPart::GeometryPart(const string &name, Material* mat, const aiMesh * mesh)
		: name_(name), material_(mat)
	{
		if(!mesh)
			throw GivenMeshIsInvalid();

		// Charge les indexes
		loadIndexes(mesh);

		// Charge les vertices
		loadVertices(mesh);
	}
	
	string & GeometryPart::name() throw()
	{
		return name_;
	}

	vector<Vertex> & GeometryPart::vertices() throw()
	{
		return vertices_;
	}

	vector<GeometryPart::index_t>& GeometryPart::indexes() throw()
	{
		return indexes_;
	}

	Material* GeometryPart::material()
	{
		return material_;
	}
}