//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "stdafx.h"
#include "PhysicsTask.h"
#include "SpawnManager.h"

#include "PxPhysicsAPI.h"
#include "SpongeRace.h"
#include "Actor.h"

using namespace physx;

//=============================================================================
// PRIVATE FUNCTIONS
//=============================================================================
class SimulationEventCallback
	: public PxSimulationEventCallback
{
public:
	virtual void onConstraintBreak(PxConstraintInfo* constraints, PxU32 count) override
	{}

	virtual void onWake(PxActor** actors, PxU32 count) override
	{}

	virtual void onSleep(PxActor** actors, PxU32 count) override
	{}

	virtual void onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs) override
	{
		IActor *actor0 = nullptr;
		if (!(pairHeader.flags & PxContactPairHeaderFlag::eDELETED_ACTOR_0))
			actor0 = static_cast<IActor*>(pairHeader.actors[0]->userData);

		IActor *actor1 = nullptr;
		if (!(pairHeader.flags & PxContactPairHeaderFlag::eDELETED_ACTOR_1))
			actor1 = static_cast<IActor*>(pairHeader.actors[1]->userData);


		for (PxU32 i = 0; i < nbPairs; ++i)
		{
			if (actor0)
				actor0->onContact(pairs[i]);

			if (actor1)
				actor1->onContact(pairs[i]);
		}
	}

	virtual void onTrigger(PxTriggerPair* pairs, PxU32 count) override
	{
		for (PxU32 i = 0; i < count; ++i)
		{
			bool triggerEnter = false;
			if (pairs->status == PxPairFlag::eNOTIFY_TOUCH_FOUND)
				triggerEnter = true;
			else if (pairs->status == PxPairFlag::eNOTIFY_TOUCH_LOST)
				triggerEnter = false;
			else
				continue;

			IActor *trigger = nullptr;
			if (!(pairs->flags & PxTriggerPairFlag::eDELETED_SHAPE_TRIGGER))
				trigger = static_cast<IActor*>(pairs[i].triggerShape->getActor().userData);

			IActor *other = nullptr;
			if (!(pairs->flags & PxTriggerPairFlag::eDELETED_SHAPE_OTHER))
				other = static_cast<IActor*>(pairs[i].otherShape->getActor().userData);

			if (trigger)
				trigger->onTrigger(triggerEnter, pairs[i].triggerShape, other ? pairs[i].otherShape : nullptr);

			if (other)
				other->onTrigger(triggerEnter, pairs[i].otherShape, trigger ? pairs[i].triggerShape : nullptr);
		}
	}

};


static PxDefaultErrorCallback gDefaultErrorCallback;
static PxDefaultAllocator gDefaultAllocatorCallback;
static SimulationEventCallback gDefaultSimulationCallback;


//=============================================================================
// CLASS PhysicsTask::PhysicsTaskImp
//=============================================================================
class PhysicsTask::PhysicsTaskImp
{
public:
	PhysicsTask::PhysicsTaskImp()
		: _foundation(nullptr), _profileZoneManager(nullptr), _physics(nullptr), _scene(nullptr), _cpuDispatcher(nullptr), _cudaContextManager(nullptr), _visualDebuggerConnection(nullptr)
	{

	}

	void init()
	{
		_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
		if(!_foundation)
			fatalError("PxCreateFoundation failed!");

		bool recordMemoryAllocations = true;
		_profileZoneManager = &PxProfileZoneManager::createProfileZoneManager(_foundation);
		if(!_profileZoneManager)
			fatalError("PxProfileZoneManager::createProfileZoneManager failed!");

#ifdef PX_WINDOWS
		pxtask::CudaContextManagerDesc cudaContextManagerDesc;
		_cudaContextManager = pxtask::createCudaContextManager(*_foundation, cudaContextManagerDesc, _profileZoneManager);
		if( _cudaContextManager )
		{
			if( !_cudaContextManager->contextIsValid() )
			{
				_cudaContextManager->release();
				_cudaContextManager = NULL;
			}
		}
#endif

		_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *_foundation,
			PxTolerancesScale(), recordMemoryAllocations, _profileZoneManager );
		if(!_physics)
			fatalError("PxCreatePhysics failed!");


		if (!PxInitExtensions(*_physics))
			fatalError("PxInitExtensions failed!");


		if (_physics->getPvdConnectionManager() != nullptr)
		{
			PxVisualDebuggerConnectionFlags connectionFlags(PxVisualDebuggerExt::getAllConnectionFlags());
			_visualDebuggerConnection = PxVisualDebuggerExt::createConnection(_physics->getPvdConnectionManager(), 
				"localhost", 5425, 100, connectionFlags);
			if (_visualDebuggerConnection == nullptr)
				printf("    NOT CONNECTED!\n");
			else
				printf("    CONNECTED!\n");
		}


		//-------------------------------------------------------------
		// create the scene
		PxSceneDesc sceneDesc(_physics->getTolerancesScale());
		customizeSceneDesc(&sceneDesc);

		if(!sceneDesc.cpuDispatcher)
		{
			_cpuDispatcher = PxDefaultCpuDispatcherCreate(1);
			if(!_cpuDispatcher)
				fatalError("PxDefaultCpuDispatcherCreate failed!");
			sceneDesc.cpuDispatcher    = _cpuDispatcher;
		}
		if(!sceneDesc.filterShader)
			sceneDesc.filterShader    = PxDefaultSimulationFilterShader;

#ifdef PX_WINDOWS
		if(!sceneDesc.gpuDispatcher && _cudaContextManager)
		{
			sceneDesc.gpuDispatcher = _cudaContextManager->getGpuDispatcher();
		}
#endif

		_scene = _physics->createScene(sceneDesc);
		if (!_scene)
			fatalError("createScene failed!");

		_scene->setSimulationEventCallback(&gDefaultSimulationCallback);
		GAME->setScene(_scene);
	}

	//-------------------------------------------------------------------------
	//
	static PxFilterFlags SimulationFilterShader(	
		PxFilterObjectAttributes attributes0, PxFilterData filterData0, 
		PxFilterObjectAttributes attributes1, PxFilterData filterData1,
		PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize)
	{
		// let triggers through
		if(PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
		{
			pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
			return PxFilterFlag::eDEFAULT;
		}
		// generate contacts for all that were not filtered above
		pairFlags = PxPairFlag::eCONTACT_DEFAULT;
		//pairFlags = PxPairFlag::eRESOLVE_CONTACTS;
		//pairFlags |= PxPairFlag::eSWEPT_INTEGRATION_LINEAR;

		// trigger the contact callback for pairs (A,B) where 
		// the filtermask of A contains the ID of B and vice versa.
		if((filterData0.word0 & filterData1.word1) || (filterData1.word0 & filterData0.word1))
		{
			pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND;
			pairFlags |= PxPairFlag::eNOTIFY_TOUCH_LOST;
			return PxFilterFlag::eDEFAULT;
		}

		return PxFilterFlag::eSUPPRESS;
	}

	//-------------------------------------------------------------------------
	//
	void customizeSceneDesc(PxSceneDesc *aSceneDesc)
	{
		aSceneDesc->gravity = PxVec3(0.0f, -20.f/*9.81f*/, 0.0f);
		//aSceneDesc->gravity = PxVec3(0.0f, 0.0f, 0.0f);
		aSceneDesc->filterShader = &PhysicsTaskImp::SimulationFilterShader;
		//aSceneDesc->flags |= PxSceneFlag::eENABLE_SWEPT_INTEGRATION;

	}

	//-------------------------------------------------------------------------
	//
	void fatalError(const char *str)
	{
		printf("%s", str);
		GAME->requestExit();
	}

	//-------------------------------------------------------------------------
	//
	void cleanup()
	{
		GAME->setScene(nullptr);

		if (_scene)
		{
			_scene->release();
			_scene = nullptr;
		}

		if (_cpuDispatcher)
		{
			_cpuDispatcher->release();
			_cpuDispatcher = nullptr;
		}

		if (_visualDebuggerConnection)
		{
			_visualDebuggerConnection->release();
			_visualDebuggerConnection = nullptr;
		}

		if (_physics)
		{
			PxCloseExtensions();

			_physics->release();
			_physics = nullptr;
		}

		if (_cudaContextManager)
		{
			_cudaContextManager->release();
			_cudaContextManager = nullptr;
		}

		if (_profileZoneManager)
		{
			_profileZoneManager->release();
			_profileZoneManager = nullptr;
		}

		if (_foundation)
		{
			_foundation->release();
			_foundation = nullptr;
		}
	}

	//-------------------------------------------------------------------------
	//
	void update()
	{
		if(!GAME->isPause())
		{
			if(LoopTime::GetInstance().Frame() > 1)
				_scene->simulate(PxReal(LoopTime::GetInstance().ElapsedTimeSeconds()));
			_scene->fetchResults(true);

			// allow objects to be unspawned
			GAME->spawnManager()->update();
		}
	}

private:
	PxFoundation *_foundation;
	PxProfileZoneManager *_profileZoneManager;
	PxPhysics *_physics;
	PxScene* _scene;
	PxDefaultCpuDispatcher *_cpuDispatcher;
	pxtask::CudaContextManager *_cudaContextManager;
	debugger::comm::PvdConnection *_visualDebuggerConnection;

};

//-----------------------------------------------------------------------------
//
PhysicsTask::PhysicsTask()
	: _imp(new PhysicsTaskImp)
{}

//-----------------------------------------------------------------------------
//
PhysicsTask::~PhysicsTask()
{
	delete _imp;
}

//-----------------------------------------------------------------------------
//
void PhysicsTask::init()
{
	_imp->init();
}

//-----------------------------------------------------------------------------
//
void PhysicsTask::cleanup()
{
	_imp->cleanup();
}

//-----------------------------------------------------------------------------
//
void PhysicsTask::update()
{
	_imp->update();
}
