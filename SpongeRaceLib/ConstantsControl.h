#ifndef CONSTANTS_CONTROLE_H
#define CONSTANTS_CONTROLE_H


typedef enum 
{
	MOVE_FORWARD,
	MOVE_BACKWARD,
	STRAFE_RIGHT,
	STRAFE_LEFT,
	MOVE_UP,
	MOVE_DOWN,
	TURN_LEFT,
	TURN_RIGHT,
	SPACE_BUTTON,
	BUTTON2,
	BUTTON3,
	BUTTON4,
	ACTION_COUNT,
	KEY_NONE = ACTION_COUNT
} ACTION;

#endif