#include "stdafx.h"
#include "PNVertex.h"

namespace GE
{
	D3D11_INPUT_ELEMENT_DESC PNVertex::Layout_[] =
	{
		{"POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},  
		{"NORMAL",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	12,	D3D11_INPUT_PER_VERTEX_DATA,	0}
	};

	D3D11_INPUT_ELEMENT_DESC * PNVertex::Begin()
	{return Layout_;}

	UINT PNVertex::Size()
	{return ARRAYSIZE(Layout_);}

	PNVertex::PNVertex(XMFLOAT3 position, XMFLOAT3 normal)
		: position_(position), normal_(normal)
	{}

	XMFLOAT3 PNVertex::GetPosition() const throw()
	{return position_;}

	XMFLOAT3 PNVertex::GetNormal() const throw()
	{return normal_;}

	void PNVertex::SetPosition(const XMFLOAT3 position) throw()
	{position_ = position;}

	void PNVertex::SetNormal(const XMFLOAT3 normal) throw()
	{normal_ = normal;}

	PNVertex::PNVertex(const Vertex & vertex)
		: position_(vertex.position()), normal_(vertex.normal())
	{
	}
}