#pragma once

#include "Object3D.h"
#include "ColorMat.h"
#include "Effect.h"
#include "SimpleVertex4.h"

#include "BSpline.h"

namespace GE
{
	class DrawableBSpline
		: public Object3D<GE::SimpleVertex4, GE::ColorMat>
	{
    private:
        BSpline spline_;
        std::vector<SimpleVertex4> knotsInterpolated_;
        int intervals_;

        XMVECTOR buildTimeVector(float t) const throw();

	public:
		DrawableBSpline(Effect<SimpleVertex4, ColorMat>* effect,
                        ColorMat* pMaterial,
			            const BSpline &spline,
                        int intervals = 10);

		virtual void Init();
		virtual void Draw();
		virtual void Anime(float timeElapsed);

		XMFLOAT4 getPositionByInterpolate(float val);
        XMFLOAT4 FindTimeFromPoint(XMVECTOR point,float offset);
	};
}
