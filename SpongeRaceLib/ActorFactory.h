#pragma once
//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "IActor.h"

//=============================================================================
// CLASS ActorFactory
//=============================================================================
class ActorFactory
{
public:
	typedef const char *ActorID;
	typedef IActor* (*CreateInstanceProc)(const IActorDataRef &aDataRef);
	typedef IActorData* (*CreateDataProc)();

public:
	static void registerActorType(const ActorID &aActorID, CreateInstanceProc aCreateActorProc, CreateDataProc aCreateActorDataProc);
	static void unregisterActorType(const ActorID &aActorID);

	static IActor* createInstance(const ActorID &aActorID);
};

//=============================================================================
// CLASS RegisterActorType<T>
//=============================================================================
template<class T>
class RegisterActorType
{
public:
	RegisterActorType()
	{
		ActorFactory::registerActorType(T::typeId(), &T::createInstance, &T::loadData);
	}

	~RegisterActorType()
	{
		ActorFactory::unregisterActorType(T::typeId());
	}
};
