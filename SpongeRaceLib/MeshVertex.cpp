#include "stdafx.h"
#include "MeshVertex.h"
#include "AssimpLoader.h"

using namespace std;
using namespace Assimp;

namespace GE
{
	D3D11_INPUT_ELEMENT_DESC MeshVertex::Layout_[] =
	{
		{"POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},  
		{"NORMAL",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	12,	D3D11_INPUT_PER_VERTEX_DATA,	0},
		{"TEXCOORD",	0,	DXGI_FORMAT_R32G32_FLOAT,		0,	24,	D3D11_INPUT_PER_VERTEX_DATA,	0}
	};

	D3D11_INPUT_ELEMENT_DESC * MeshVertex::Begin()
	{return Layout_;}

	UINT MeshVertex::Size()
	{return ARRAYSIZE(Layout_);}

	MeshVertex::MeshVertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT2 coordTex)
		: position_(position), normal_(normal), coordTex_(coordTex)
	{}

	XMFLOAT3 MeshVertex::GetPosition() const throw()
	{return position_;}

	XMFLOAT3 MeshVertex::GetNormal() const throw()
	{return normal_;}

	XMFLOAT2 MeshVertex::GetTexCoord() const throw()
	{return coordTex_;}

	void MeshVertex::SetPosition(const XMFLOAT3 position) throw()
	{position_ = position;}

	void MeshVertex::SetNormal(const XMFLOAT3 normal) throw()
	{normal_ = normal;}

	void MeshVertex::SetTexCoord(const XMFLOAT2 coordTex) throw()
	{coordTex_ = coordTex;}

	MeshVertex::MeshVertex(const Vertex & vertex)
		: position_(vertex.position()), normal_(vertex.normal()), coordTex_(XMFLOAT2(0.f, 0.f))
	{
		if(!vertex.textureCoords().empty())
			coordTex_ = vertex.textureCoords().front();
	}
}