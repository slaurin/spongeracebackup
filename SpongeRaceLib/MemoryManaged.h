#ifndef HEAPELEMENT_H
#define HEAPELEMENT_H

#include "Heap.h"
#include "HeapFactory.h"

struct NotSupported{};

template <class T, class HeapType>
class MemoryManaged
{
public:
	static Heap * pHeap;

public:
	
	MemoryManaged()
	{};
	
	~MemoryManaged(){};

	void * operator new(size_t n) 
	{
		assert(pHeap);
		void* p =  pHeap->Allocate(HeapType::getSize());
		if (p==nullptr) throw std::bad_alloc();
		return p;
	};

	void operator delete(void *pMem)
	{
		if (pMem)
			Heap::Deallocate (pMem);   
	};

	void * operator new[](size_t n) 
	{
		throw NotSupported();
	};

	void operator delete[](void *p)
	{
		throw NotSupported();
	};


	void * operator new(size_t n, const std::nothrow_t& nothrow)
	{
		assert(pHeap);
		return pHeap->Allocate(HeapType::getSize());
	};

	void operator delete(void *pMem, const std::nothrow_t& nothrow)
	{
		if (pMem)
			Heap::Deallocate (pMem);   
	};

	void * operator new[](size_t n, const std::nothrow_t& nothrow)
	{
		return nullptr;
	};

	void operator delete[](void *p, const std::nothrow_t& nothrow)
	{
		return nullptr;
	};
};

template <class T, class HeapType>
Heap* MemoryManaged<T, HeapType>::pHeap=HeapFactory::CreateHeap(HeapType::getName());



#endif