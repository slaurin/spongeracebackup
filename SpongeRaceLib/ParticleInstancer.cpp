#include "stdafx.h"
#include "ParticleInstancer.h"

#include "GraphicEngine.h"
#include "Device.h"
#include "DxUtilities.h"

using namespace DxUtilities;

namespace GE
{
    ParticleInstancer::ParticleInstancer(Effect<ParticleVertex, ParticleMat> *effect,
                                         ParticleMat *pMaterial,
                                         int maxParticles)
        : Object3D<ParticleVertex, ParticleMat>(effect, pMaterial),
          maxParticles_(maxParticles),
          particles_(std::vector<Quad*>())
    {
        InitializeParticleSystem();
    }

    ParticleInstancer::~ParticleInstancer()
    {
        ShutdownParticleSystem();
    }

    void ParticleInstancer::Anime(float timeElapsed)
    {
        // Release old particles.
        KillParticles();

        // Emit new particles.
        EmitParticles(timeElapsed);

        // Update the position of the particles.
        UpdateParticles(timeElapsed);

        AnimeParticles(timeElapsed);
    }
	void ParticleInstancer::Draw()
    {
        if(currentParticleCount_ == 0)
            return;

        
        DrawParticles();
    }

    void ParticleInstancer::InitializeParticleSystem()
    {
        // Set the random deviation of where the particles can be located when emitted.
        particleDeviationX_ = 8.0f;
        particleDeviationY_ = 2.5f;
        particleDeviationZ_ = 8.0f;

        // Set the speed and speed variation of particles.
        particleVelocity_ = 50.0f;
        particleVelocityVariation_ = 5.0f;

        // Set the physical size of the particles.
        particleSize_ = 0.2f;

        // Set the number of particles to emit per second.
        particlesPerSecond_ = 250;

        particles_.reserve(maxParticles_);

        // Initialize the particle list.
        for(int i = 0; i < maxParticles_; ++i)
        {
            particles_.push_back(new Quad(effect_, material_, XMFLOAT3(), XMFLOAT3(7.0f, 7.0f, 1.0f)));
        }

        // Initialize the current particle count to zero since none are emitted yet.
        currentParticleCount_ = 0;

        // Clear the initial accumulated time for the particle per second emission rate.
        accumulatedTime_ = 0.0f;
    }
    void ParticleInstancer::ShutdownParticleSystem()
    {
        std::for_each(std::begin(particles_), std::end(particles_), [&](Quad* quad)
        {
            delete quad;
        });
    }

    void ParticleInstancer::EmitParticles(float timeElapsed)
    {
        bool emitParticle=false;
        bool found=false;
        float positionX, positionY, positionZ, velocity, red, green, blue=0.f;
        int index, i, j=0;

        

        // Increment the frame time.
        accumulatedTime_ += timeElapsed;

        int nmParticlesToEmit = static_cast<float>(particlesPerSecond_ * accumulatedTime_ + 0.5f);
        float timePerParticle = 1.0f/particlesPerSecond_;

        // Set emit particle to false for now.
        if(nmParticlesToEmit > 0)
        {
            accumulatedTime_ -= static_cast<float>(nmParticlesToEmit) * timePerParticle;
            emitParticle = true;
        }

        //// Check if it is time to emit a new particle or not.
        //if(accumulatedTime_ > (1000.0f / particlesPerSecond_))
        //{
        //    accumulatedTime_ = 0.0f;
        //    emitParticle = true;
        //}

        // If there are particles to emit then emit one per frame.
        if((emitParticle == true) && (currentParticleCount_ < (maxParticles_ - nmParticlesToEmit)))
        {
            currentParticleCount_ += nmParticlesToEmit;

            for(int iParticle = 0; iParticle < nmParticlesToEmit; ++iParticle)
            {

                // Now generate the randomized particle properties.
                positionX = (((float)rand()-(float)rand())/RAND_MAX) * particleDeviationX_ + 0.0f;
                positionY = (((float)rand()-(float)rand())/RAND_MAX) * particleDeviationY_ + 250.0f;
                positionZ = (((float)rand()-(float)rand())/RAND_MAX) * particleDeviationZ_ + 0.0f;

                velocity = particleVelocity_ + (((float)rand()-(float)rand())/RAND_MAX) * particleVelocityVariation_;

                red   = (((float)rand()-(float)rand())/RAND_MAX) + 0.5f;
                green = (((float)rand()-(float)rand())/RAND_MAX) + 0.5f;
                blue  = (((float)rand()-(float)rand())/RAND_MAX) + 0.5f;

                // Now since the particles need to be rendered from back to front for blending we have to sort the particle array.
                // We will sort using Z depth so we need to find where in the list the particle should be inserted.
                index = 0;
                found = false;
                while(!found)
                {
                    if((particles_[index]->active_ == false) || (particles_[index]->position_.z < positionZ))
                    {
                        found = true;
                    }
                    else
                    {
                        ++index;
                    }
                }

                // Now that we know the location to insert into we need to copy the array over by one position from the index to make room for the new particle.
                i = currentParticleCount_;
                j = i - 1;

                while(i != index)
                {
                    particles_[i]->position_.x = particles_[j]->position_.x;
                    particles_[i]->position_.y = particles_[j]->position_.y;
                    particles_[i]->position_.z = particles_[j]->position_.z;
                    particles_[i]->velocity_   = particles_[j]->velocity_;
                    particles_[i]->active_     = particles_[j]->active_;
                    --i;
                    --j;
                }

                // Now insert it into the particle array in the correct depth order.
                particles_[index]->position_.x = positionX;
                particles_[index]->position_.y = positionY;
                particles_[index]->position_.z = positionZ;
                particles_[index]->velocity_ = velocity;
                particles_[index]->active_ = true;
            }
        }
    }
    void ParticleInstancer::UpdateParticles(float timeElapsed)
    {
        // Each frame we update all the particles by making them move downwards using their position, velocity, and the frame time.
        for(int i = 0; i < currentParticleCount_; ++i)
        {
            particles_[i]->position_.y = particles_[i]->position_.y - (particles_[i]->velocity_ * timeElapsed);
        }
    }
    void ParticleInstancer::KillParticles()
    {
        // Kill all the particles that have gone below a certain height range.
        for(int i = 0; i < maxParticles_; ++i)
        {
            if((particles_[i]->active_ != false) && (particles_[i]->position_.y < -40.0f))
            {
                particles_[i]->active_ = false;
                --currentParticleCount_;

                // Now shift all the live particles back up the array to erase the destroyed particle and keep the array sorted correctly.
                for(int j = i; j < maxParticles_ - 1; ++j)
                {
                    particles_[j]->position_.x = particles_[j+1]->position_.x;
                    particles_[j]->position_.y = particles_[j+1]->position_.y;
                    particles_[j]->position_.z = particles_[j+1]->position_.z;
                    particles_[j]->velocity_ = particles_[j+1]->velocity_;
                    particles_[j]->active_ = particles_[j+1]->active_;
                }
            }
        }
    }

    void ParticleInstancer::AnimeParticles(float timeElapsed)
    {
        std::for_each(std::begin(particles_), std::end(particles_), [&](Quad* quad)
        {
            quad->Anime(timeElapsed);
        });
    }
    void ParticleInstancer::DrawParticles()
    {
        std::for_each(std::begin(particles_), std::end(particles_), [&](Quad* quad)
        {
            quad->Draw();
            quad->SendToPipeline();
        });
    }

}