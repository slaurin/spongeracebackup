#pragma once

#include "StaticActor.h"
#include "ActorFactory.h"

class GE::GraphicObject;
class Controller;

class ObstacleActor : public StaticActor
{
public:
	ObstacleActor(const ActorFactory::ActorID &aActorId)
		: StaticActor(aActorId)
	{}


	static ActorFactory::ActorID typeId();
};

