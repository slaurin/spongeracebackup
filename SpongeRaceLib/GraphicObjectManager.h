#ifndef GRAPHICOBJECT_MANAGER_H
#define GRAPHICOBJECT_MANAGER_H

#include "Instancer.h"
#include "IRessourceManager.h"
#include "GraphicObject.h"
#include "Singleton.h"
#include "AssimpLoader.h"

class GraphicObjectManager
	: public IRessourceManager<GraphicObjectManager, std::string, GE::GraphicObject *>
{
	friend class Singleton<GraphicObjectManager>;
public:

	template<class VertexT, class MaterialT>
	GE::GraphicObject* Request(key_t name, 
		Effect<VertexT, MaterialT>* effect,
		GE::Geometry * geometry)
	{
		if(!Contains(name))
		{			
			GE::GraphicObject * obj = Instancer::Create(effect, geometry);
			add(name, obj);
			return obj;
		}
	}

};

#endif