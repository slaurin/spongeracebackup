#include "stdafx.h"
#include "Scene.h"

using namespace std;

namespace GE
{
	Scene::Scene(const string & name, bool enablePostEffect, const XMFLOAT4 & ambientLighting)
		: name_(name), ambientLighting_(ambientLighting), enablePostEffect_(enablePostEffect)
	{
	}

	Scene::~Scene(void)
	{
		Cleanup();
	}

	bool Scene::AddBubbles(GraphicObject *object)
	{
		if(!object)
			return false;

		bubbles_.push_back(object);
		return true;
	}

	bool Scene::AddObject(GraphicObject * object)
	{
		if(!object)
			return false;
		
		objects_.push_back(object);
		return true;
	}


	bool Scene::AddLight(const PointLight & pointLight)
	{
		pointLights_.push_back(new PointLight(pointLight));
		return true;
	}

	bool Scene::AddLight(const DirectionalLight & directionalLight)
	{
		directionalLights_.push_back(new DirectionalLight(directionalLight));
		return true;
	}

	bool Scene::AddLight(const SpotLight & spotLight)
	{
		spotLights_.push_back(new SpotLight(spotLight));
		return true;
	}

	bool Scene::AddLight(PointLight * pointLight)
	{
		pointLights_.push_back(pointLight);
		return true;
	}

	bool Scene::AddLight(DirectionalLight * directionalLight)
	{
		directionalLights_.push_back(directionalLight);
		return true;
	}

	bool Scene::AddLight(SpotLight * spotLight)
	{
		spotLights_.push_back(spotLight);
		return true;
	}

	XMFLOAT4 Scene::GetAmbientLighting() const
	{
		return ambientLighting_;
	}

	void Scene::SetAmbientLighting(const XMFLOAT4 & ambientLighting)
	{
		ambientLighting_ = ambientLighting;
	}

	vector<PointLight *> & Scene::GetPointLights()
	{
		return pointLights_;
	}

	vector<DirectionalLight *> & Scene::GetDirectionalLights()
	{
		return directionalLights_;
	}

	vector<SpotLight *> & Scene::GetSpotLights()
	{
		return spotLights_;
	}

	void Scene::RemoveBubble(GraphicObject *object)
	{
		bubbles_.erase(
			std::remove_if(bubbles_.begin(), bubbles_.end(), 
			[&](GraphicObject* obj) -> bool{ 				
				return (obj==object);
		}), bubbles_.end());
	}

	void Scene::Cleanup()
	{
		// D�truit les objets de la sc�ne
		/*for_each(objects_.begin(), objects_.end(), [](GraphicObject * object){
			delete object;
		});*/

		for_each(bubbles_.begin(), bubbles_.end(), [](GraphicObject * object){
			delete object;
		});


		objects_.clear();
		bubbles_.clear();
		pointLights_.clear();
		directionalLights_.clear();
		spotLights_.clear();
	}

	void Scene::Anime(float timeElapsed)
	{
		for_each(
			objects_.begin(), 
			objects_.end(), 
			[&](GraphicObject * object)
			{object->Anime(timeElapsed);}
		);
	}

	bool Scene::IsPostEffectEnable()
	{
		return enablePostEffect_;
	}

	void Scene::SetPostEffect(bool enable)
	{
		enablePostEffect_ = enable;
	}

	void Scene::Render()
	{
		// Appelle les fonctions de dessin de chaque objet de la sc�ne
		for_each(objects_.begin(), objects_.end(), mem_fun(&GraphicObject::Draw));

	}

	void Scene::RenderBubbles()
	{

		// Appelle les fonctions de dessin de chaque objet du HUD de la sc�ne (doit etre dessin� en dernier)
		for_each(bubbles_.begin(), bubbles_.end(), mem_fun(&GraphicObject::Draw));
	}
}
