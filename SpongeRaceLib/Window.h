#pragma once

#include "Singleton.h"
#include "resource.h"

class Window
	: public Singleton<Window>
{
	friend class Singleton<Window>;

	#define MAX_LOADSTRING 100

	//! Handle Windows de la table des acc�l�rateurs
	HACCEL accelTable_;

	// Handle Windows de l'instance actuelle de l'application
	static HINSTANCE appInstance_;

	// Handle Windows de la fen�tre principale
	static HWND mainWindow_;

	// Le nom de la classe de fen�tre principale
	TCHAR windowName_[MAX_LOADSTRING];

	//! Largeur de l'�cran
	int screenWidth_;

	//! Hauteur de l'�cran
	int screenHeight_;

public:
	Window(void);
	~Window(void);

	void SetScreenDimensions(int width, int height);

	//! Retourne le HINSTANCE, Bug le pointeur est toujours 0.0 ??
	static HINSTANCE getHAppInstance(){return appInstance_;};

	//!Retourne le HWND
	static HWND getHMainWnd() {return mainWindow_;};

	//! Effectue des op�rations d'initialisation de l'application
	/*!
		Dans cette fonction, nous enregistrons le handle de l'instance dans une variable globale, puis
		cr�ons et affichons la fen�tre principale du programme.
	*/
	bool init(HINSTANCE hInstance);

	//! Affiche la fen�tre windows
	int show();

	//! Retourne la largeur de l'�cran
	int GetScreenWidth() {return screenWidth_;}

	//! Retourne la hauteur de l'�cran
	int GetScreenHeight() {return screenHeight_;}

	//! Retourne le ratio de l'�cran
	float GetRatio() {return screenWidth_ / static_cast<float>(screenHeight_);}

		
	static int resizeWindow(LPARAM lParam);
private:
	//! Traite les messages pour la fen�tre principale.
	/*!
		WM_COMMAND	- traite le menu de l'application
		WM_PAINT	- dessine la fen�tre principale
		WM_DESTROY	- g�n�re un message d'arr�t et retourne
	*/
	static LRESULT CALLBACK windowProcess(HWND, UINT, WPARAM, LPARAM);

	//! Gestionnaire de messages pour la bo�te de dialogue "� propos de"
	static INT_PTR CALLBACK about(HWND, UINT, WPARAM, LPARAM);
	//! Inscrit la classe de fen�tre.
	/*!
		Cette fonction et son utilisation sont n�cessaires uniquement si vous souhaitez que ce code
		soit compatible avec les syst�mes Win32 avant la fonction 'RegisterClassEx'
		qui a �t� ajout�e � Windows�95. Il est important d'appeler cette fonction
		afin que l'application dispose des petites ic�nes correctes qui lui sont
		associ�es.
	*/
	ATOM registerClass(HINSTANCE hInstance);
};

