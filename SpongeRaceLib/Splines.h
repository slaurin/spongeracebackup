#pragma once

#include "Singleton.h"
#include "DrawableBSpline.h"

class Splines 
	: public Singleton<Splines>
{
public:
	typedef GE::DrawableBSpline spline_type;
	typedef std::pair<int,spline_type*> pair_type;

private:
	friend Singleton<Splines>;

	std::vector<pair_type> listSpline_;

protected:
	Splines(void):
		 listSpline_(std::vector<pair_type>())
	{
	}
	~Splines(void)
	{
	}
public:

	void addSpline(int index, GE::DrawableBSpline * spline)
	{
		assert(spline);
		assert(index >= 0 && index < 1000);
		listSpline_.push_back(pair_type(index,spline));
	}
	spline_type* getSpline(int index)
	{
		spline_type* result = findSpline(index);
		assert(result);
		return result;
	}

private:
	spline_type* findSpline(int index)
	{
		for (auto itt = listSpline_.begin();itt != listSpline_.end();++itt)
		{
			if(itt->first == index)
				return itt->second;
		}
		return nullptr;
	}
};

