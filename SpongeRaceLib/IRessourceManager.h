#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include "IOReader.h"
#include "Singleton.h"

//! Classe g�n�rique d�finissant un manager d'assets
template<class TChild, class TKey, class TData>
class IRessourceManager 
	: public Singleton<TChild>
{
public:
	//! Type utilis� pour les cl�s
	typedef TKey key_t;

	//! Type utilis� pour les assets
	typedef TData data_t;

private:
	std::map<TKey, data_t> map_;

protected:
	//! Ajoute dans la map sans contr�ler la pr�sence
	void add(TKey key, data_t data)
	{
		map_.insert(std::pair<TKey, data_t>(key, data));
	}
	
	IRessourceManager()
		: map_(std::map<TKey, data_t>())
	{}

	~IRessourceManager()
	{
		map_.clear();
	}

public:
	//! Ajoute dans la map en v�rifiant la pr�sence
	void Add(TKey key, data_t data)
	{
		if(!Contains(key))
			add(key, data);
	}

	//! V�rifie si la map contient l'occurence donn�e
	bool Contains(TKey key)
	{
		return map_.find(key) != map_.end();
	}

	//! Retire la cl� fournie (si elle est pr�sente)
	void Remove(TKey key)
	{
		if(Contains(key))
			map_.erase(key);
	}

	/* Access the data from the cache */
	TData& operator[](const TKey key)
	{
		return map_[key];
	}

	unsigned int size() const throw()
	{
		return map_.size();
	}
};

#endif