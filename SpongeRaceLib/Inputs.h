#ifndef INPUTS_H_
#define INPUTS_H_

#include "Singleton.h"
#include "ConstantsControl.h"

class Inputs : public Singleton<Inputs>
{
	std::bitset<ACTION_COUNT> keys_;
protected:
	Inputs()
	{

	}
public:

	friend Singleton<Inputs>;
	bool isPressed(ACTION key) const
	{
		if (key != KEY_NONE)
			return keys_[key];
		return false;
	}
	void setKey(ACTION key, bool isPressed)
	{
		if (key != KEY_NONE)
			keys_[key] = isPressed;
	}
};

class KeyBoardController : public Singleton<KeyBoardController>
{
	ACTION TranslateKey(int aVirtKey)
	{
		switch(aVirtKey)
		{
		case(VK_UP):
		case('W'):
			return MOVE_FORWARD;
		case(VK_DOWN):
		case('S'):
			return MOVE_BACKWARD;
		case('A'):
		case(VK_LEFT):
			return TURN_LEFT;
		case('D'):
		case(VK_RIGHT):
			return TURN_RIGHT;
		case('Q'):
			return MOVE_UP;
		case('E'):
			return MOVE_DOWN;
		case('F'):
			return BUTTON2;
		case(VK_SPACE):
			return SPACE_BUTTON;
			//	return KEY_CTRLBUTTON;
		}
		return KEY_NONE;
	}
	KeyBoardController(){}
public:
	friend class Singleton<KeyBoardController>;
	void onKeyDown(int key)
	{
		Inputs::GetInstance().setKey(TranslateKey(key),true);
	}
	void onKeyUp(int key)
	{
		Inputs::GetInstance().setKey(TranslateKey(key),false);
	}
};
#endif