#include "stdafx.h"

#include "GraphicEngine.h"
#include "SpotLight.h"
#include "GraphicObject.h"
#include "SpongeRace.h"
#include "ConstantsControl.h"
#include "BubbleActor.h"
#include "Controller.h"
#include "PxPhysicsAPI.h"
#include "Convert.h"
#include "GenerateurNombresAleatoires.h"

using namespace physx;
using namespace GE;

using std::cout;
using std::endl;


//=============================================================================
// CLASS BubbleActorData
//=============================================================================
class BubbleActorData: public IActorData
{
public:
	// physics
	PxMaterial *material_;
	PxShape* actorShape_;	

	// rendering
	GraphicObject* model_;

public:
	BubbleActorData()
		: material_(nullptr)
		, model_(nullptr)
	{}

	int load()
	{
		//---------------------------------------------------------------------
		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		//static friction, dynamic friction, restitution
		material_ = physics.createMaterial(0.0001f, 0.0001f, 0.0001f); 

		return 0;
	}

};



class BubbleActorImp : public Actor<BubbleActorData, BubbleActor>
{
public:

	GraphicObject* graphicObj_;

public:
	BubbleActorImp(const IActorDataRef &aDataRef)
		: Actor(aDataRef, typeId())
	{
	}


public:

	void setGraphicObj(GraphicObject* obj) override
	{
		graphicObj_=obj;
	}

	//-------------------------------------------------------------------------
	//
	static ActorFactory::ActorID typeId()
	{
		return BubbleActor::typeId();
	}

	//-------------------------------------------------------------------------
	//
	static IActorData* loadData()
	{
		BubbleActorData *data = new BubbleActorData();
		data->load();
		return data;
	}

	//-------------------------------------------------------------------------
	//
	static IActor* createInstance(const IActorDataRef &aDataRef)
	{
		BubbleActorImp *actor = new BubbleActorImp(aDataRef);
		return actor;
	}

	//-------------------------------------------------------------------------
	//
	virtual void onSpawn(const PxTransform &aPose, PxGeometry * geometry = nullptr) override
	{
		// create the physic object
		DynamicActor::setSpeed(1);
		PxPhysics &physics = GAME->scene()->getPhysics();

		pxActor_ = GAME->scene()->getPhysics().createRigidDynamic(aPose);
		pxActor_->userData = this;
		pxActor_->setName("Bubble");

		actorShape_ = pxActor_->createShape(PxSphereGeometry(1.5f), *_data->material_);
		actorShape_->setName("Bubble");

		PxVec3 *a = new PxVec3(0.f, 0.f, 0.f);
		bool b = PxRigidBodyExt::setMassAndUpdateInertia(*pxActor_, 0.0001f, a);
		//pxActor_->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
		GAME->scene()->addActor(*pxActor_);

		PxFilterData filterData;
		filterData.word0 = eACTOR_BUBBLE;
		filterData.word1 = eACTOR_PLAYER | eACTOR_TERRAIN | eACTOR_OBSTACLE | eACTOR_BUBBLEBARRIER | eACTOR_BUBBLE;
		actorShape_->setSimulationFilterData(filterData);

		graphicObj_->SetWorldMatrix(GetWorldTransform());

		moveLeftRight_ = GenerateurNombresAleatoires::get().generer(-3.f,3.f);
		moveBackForward_ = GenerateurNombresAleatoires::get().generer(-3.f,3.f);

	}

	//-------------------------------------------------------------------------
	//
	virtual void onUnspawn() override
	{
		//GAME->camera()->removeFromRenderList(this);

		// cleanup physics objects
		if (pxActor_)
		{
			actorShape_->release();
			actorShape_=nullptr;
			pxActor_->release();
			pxActor_= nullptr;

			GraphicEngine::GetInstance().GetCurrentScene()->RemoveBubble(graphicObj_);

			delete graphicObj_;
		}
	}

	void onContact(const physx::PxContactPair &aContactPair)
	{

		if ((aContactPair.flags & PxContactPairFlag::eDELETED_SHAPE_0) || (aContactPair.flags & PxContactPairFlag::eDELETED_SHAPE_1))
		{
			return;
		}

		if (aContactPair.shapes[0] ) {
			std::string name0 = std::string(aContactPair.shapes[0]->getName());
			if(name0=="Player" )
			{
				GAME->player()->addOneBoost();
				dead_=true;
			}
		}

		if ( aContactPair.shapes[1]) {
			std::string name1 = std::string(aContactPair.shapes[1]->getName());
			if(name1=="Player")
			{
				GAME->player()->addOneBoost();
				dead_=true;
			}
		}
	}


	//-------------------------------------------------------------------------
	//
	virtual void update(float timeElapsed) override
	{	
		PxTransform pose = pxActor_->getGlobalPose();

		if(pose.p.y>=300) 
		{
			moveUpDown_=-1;
		}

		if(!(yaw_ == 0 && pitch_ == 0 && roll_ == 0 &&	moveBackForward_ == 0 && moveUpDown_ == 0 && moveLeftRight_ == 0))
		{

			float dirSpeed = SpeedRelated(moveBackForward_) * timeElapsed;// * timeElapsed;
			float vertSpeed = SpeedRelated(moveUpDown_) * timeElapsed;// * timeElapsed;
			float rightSpeed = SpeedRelated(moveLeftRight_) * timeElapsed;// * timeElapsed;

			PxVec3 force = (getPxDirection()* dirSpeed +  getPxUp()*vertSpeed +  getPxRight()*rightSpeed);

			pxActor_->addForce(force, PxForceMode::eFORCE);

			moveLeftRight_ = 0.0f;
			moveBackForward_ = 0.0f;
			moveUpDown_ = 0.0f;
		}
		

		graphicObj_->SetWorldMatrix(GetWorldTransform());
	}


};


//-----------------------------------------------------------------------------
//
ActorFactory::ActorID BubbleActor::typeId()
{	return "BubbleActor"; }

RegisterActorType<BubbleActorImp> gRegisterActor;