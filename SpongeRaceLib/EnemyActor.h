#pragma once
#include "Actor.h"

class Controller;

class EnemyActor: public DynamicActor
{
public:
	EnemyActor();
	~EnemyActor();

	virtual void spawn() override;
	virtual void unspawn() override;
	virtual void update() override;
	virtual void render() override;

	Controller* controller();

private:
	class EnemyActorImp;
	EnemyActorImp *imp_;
};
