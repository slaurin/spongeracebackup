#include "stdafx.h"
#include "BillboardVertex.h"

namespace GE
{
    D3D11_INPUT_ELEMENT_DESC BillboardVertex::Layout_[] =
    {
        {"POSITION",	0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	0,	                            D3D11_INPUT_PER_VERTEX_DATA,	0},
        {"TEXCOORD",	0,	DXGI_FORMAT_R32G32_FLOAT,		0,	D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,	0}
    };

    D3D11_INPUT_ELEMENT_DESC * BillboardVertex::Begin()
    {return Layout_;}

    UINT BillboardVertex::Size()
    {return ARRAYSIZE(Layout_);}



    BillboardVertex::BillboardVertex(XMFLOAT4 position, XMFLOAT2 textureCoord)
        : position_(position), textureCoord_(textureCoord)
    {}

}
