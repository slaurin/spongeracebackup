#ifndef EFFECT_MANAGER_H
#define EFFECT_MANAGER_H

#include "IRessourceManager.h"
#include "IEffect.h"
#include "Singleton.h"
#include "Effect.h"
#include "string_to_wstring.h"

//! Manager d'effets, g�re leur acc�s, leur unicit� et leur dur�e de vie
class EffectManager 
	: public IRessourceManager<EffectManager, std::string, GE::IEffect *>
{
	friend class Singleton<EffectManager>;

public:
	class EffectIsNoCompatibleWithTemplates {};

	GE::IEffect * getEffect(key_t effectName)
	{
		if(Contains(effectName))
			return this->operator[](effectName);
		else
			return nullptr;
	}

	template<class VertexT, class MaterialT>
	GE::Effect<VertexT, MaterialT> * getEffect(key_t effectName)
	{
		typedef GE::Effect<VertexT, MaterialT> * effect_t;
		if(Contains(effectName))
		{
			auto * effect = dynamic_cast<effect_t>(this->operator[](effectName));
			if(!effect)
				throw EffectIsNoCompatibleWithTemplates();
			return effect;
		}
		else
			return nullptr;
	}

	template<class VertexT, class MaterialT>
	GE::Effect<VertexT, MaterialT> * Request(key_t effectName, std::string filename)
	{
		
		if(!Contains(effectName))
		{
			GE::Effect<VertexT, MaterialT> * effect = new GE::Effect<VertexT, MaterialT>(s2ws(filename));
			add(effectName, effect);
			return effect;
		}
	}

};

#endif