#include "stdafx.h"
#include "FlagWaveMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace DxUtilities;

namespace GE
{
	FlagWaveMat::FlagWaveMat(const Material * material)
		: Material(*material),phi_(0)
	{
		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ShadersParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &constantBuffer);
	}

	FlagWaveMat::~FlagWaveMat(void)
	{
		DXRelacher(constantBuffer);
	}
}