#pragma once

#include "d3dx11effect.h"
#include "Pass.h"
#include "GraphicObject.h"

namespace GE
{
	class Technique
	{
	public:
		Technique(ID3DX11EffectTechnique *technique, D3D11_INPUT_ELEMENT_DESC * begin, UINT size);
		~Technique();

		void Apply(const GraphicObject & object);

	private:
		std::vector<Pass> passes_;
		ID3DX11EffectTechnique *technique_;
	};
}