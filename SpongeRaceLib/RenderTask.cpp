#include "stdafx.h"
#include "RenderTask.h"
#include "SpongeRace.h"
#include "GraphicEngine.h"
#include "CameraActor.h"
#include "Singleton.h"
#include "Effect.h"
#include "IEffect.h"
#include "EffectManager.h"
#include "GraphicObject.h"
//#include "FileManager.h"
#include "Geometry.h"
#include "GeometryManager.h"
#include "Instancer.h"

// Devront etre enleve quand MeshMng et EffectMng ok
#include "MeshVertex.h"
#include "PhongMat.h"
#include "Cube.h"
#include "BubbleMat.h"

using namespace std;
using namespace GE;



class RenderTask::RenderTaskImp
{
	GraphicEngine &ge_;

public:
	RenderTaskImp() :
	  ge_(Singleton<GraphicEngine>::GetInstance())
	{}

	void init()
	{
		/*if(!InitGraphicObjets())
			throw FailedToInitGraphicObjects();*/
	}

	void cleanup()
	{
		ge_.Cleanup();
	}

	void update()
	{
		ge_.Present();

		if(!GAME->isPause())
		{
			ge_.AnimeScene(LoopTime::GetInstance().ElapsedTimeSeconds());
		}
		ge_.RenderPostEffect();
		ge_.RenderScene();
	}
};

void RenderTask::update()
{
	imp_->update();
}

void RenderTask::init()
{
	imp_->init();
}

void RenderTask::cleanup()
{
	imp_->cleanup();
}

RenderTask::RenderTask()
	: imp_(new RenderTaskImp())
{}

RenderTask::~RenderTask()
{
	delete imp_;
}
