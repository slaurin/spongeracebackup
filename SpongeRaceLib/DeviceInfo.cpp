#include "stdafx.h"
#include "DeviceInfo.h"
#include "DxUtilities.h"

using namespace DxUtilities;
using std::vector;
using std::begin;
using std::end;
using std::for_each;

namespace GE
{
	DeviceInfo::~DeviceInfo(void)
	{
	}

	// Pour obtenir les informations � partir d'un num�ro d'adaptateur
	// 0 = d�faut = ADAPTATEUR_DE_DEFAUT
	DeviceInfo::DeviceInfo(int NoAdaptateur)
	{
		IDXGIFactory* pFactory = NULL;
		IDXGIAdapter* pAdapter = NULL;
		IDXGIOutput* pOutput = NULL;	
	
		valide = false;

		// Cr�er un IDXGIFactory.
		CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory);

		if(FAILED(pFactory->EnumAdapters(NoAdaptateur, &pAdapter))) return;

		// Obtenir les informations de la premi�re sortie de l'adaptateur 
		// (Le moniteur)
		if(FAILED(pAdapter->EnumOutputs(0, &pOutput))) return;

		// Obtenir la description de l'�tat courant
		DXGI_OUTPUT_DESC outDesc;
		pOutput->GetDesc(&outDesc);

		largeur = outDesc.DesktopCoordinates.right - outDesc.DesktopCoordinates.left;
		hauteur = outDesc.DesktopCoordinates.bottom - outDesc.DesktopCoordinates.top;
		valide = true;

		DXGI_ADAPTER_DESC Desc;
		pAdapter->GetDesc(&Desc);

		// M�moire d�di�e (en megabytes).
		memoire = (int)(Desc.DedicatedVideoMemory / 1024 / 1024);

		// Nom de la carte video.
		wcscpy_s(nomcarte, 100, Desc.Description);
	
		// Faire le m�nage pour �viter les �memory leaks�
		DXRelacher(pOutput);
		DXRelacher(pAdapter);
		DXRelacher(pFactory);
	}


	DeviceInfo::DeviceInfo(DXGI_MODE_DESC modeDesc)
	{
		// �num�ration des adaptateurs
		IDXGIFactory *pFactory = NULL;

		CreateDXGIFactory(__uuidof(IDXGIFactory) , (void**)&pFactory);

		IDXGIAdapter *pAdapter; 
		vector<IDXGIAdapter*> vAdapters;

		for(UINT i = 0; pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND; ++i)
		{
			vAdapters.push_back(pAdapter); 
		} 

		// On travaille presque toujours avec vAdapters[0] 
		// � moins d'avoir plusieurs cartes non-collaboratives
		*this = DeviceInfo(0);

		// Obtenir la sortie 0 - le moniteur principal
		IDXGIOutput* pOutput = NULL;
		vAdapters[0]->EnumOutputs(0,&pOutput);
		
		ID3D11Device * device = nullptr;
		if(GraphicEngine::GetInstance().GetDevice() != nullptr)
		{
			modeDesc.Format = DXGI_FORMAT_UNKNOWN;
			modeDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			modeDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			modeDesc.RefreshRate.Denominator = 0;
			modeDesc.RefreshRate.Numerator = 0;
			device = GraphicEngine::GetInstance().GetDevice()->GetDXDevice();
		}

		// Obtenir le mode le plus int�ressant
		pOutput->FindClosestMatchingMode(&modeDesc, &mode, device);
  
		// Faire le m�nage pour �viter les �memory leaks�
		DXRelacher(pOutput);

		for(auto it = vAdapters.begin(); it != vAdapters.end(); ++it)
		{
			DXRelacher(*it);
		}

		DXRelacher(pFactory);
	}
}