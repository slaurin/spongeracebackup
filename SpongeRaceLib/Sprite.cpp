#include "StdAfx.h"
#include "Sprite.h"

using namespace std;

namespace GE
{
	SpriteVertex Sprite::spriteVertices_[] = {
		SpriteVertex(XMFLOAT3(-1.f, +1.f, 0.0f), XMFLOAT2(0.0f, 0.0f)),
		SpriteVertex(XMFLOAT3(+1.f, +1.f, 0.0f), XMFLOAT2(1.0f, 0.0f)),
		SpriteVertex(XMFLOAT3(+1.f, -1.f, 0.0f), XMFLOAT2(1.0f, 1.0f)),
		SpriteVertex(XMFLOAT3(-1.f, -1.f, 0.0f), XMFLOAT2(0.0f, 1.0f))
	};

	Sprite::index_t Sprite::spriteIndices_[] = {
		0, 1, 3,
		1, 2, 3
	};

	Sprite::Sprite(Effect<SpriteVertex, Pannel2DMat> *effect, Pannel2DMat *pMaterial, const XMFLOAT2 & topLeftCorner, const XMFLOAT2 & bottomRightCorner)
		: Object3D<SpriteVertex, Pannel2DMat>(effect, pMaterial)
	{
		// On cr�� les vertices
		float x1 = (topLeftCorner.x * 2.f) - 1;
		float x2 = (bottomRightCorner.x * 2.f) - 1;
		float y1 = (topLeftCorner.y * -2.f) + 1;
		float y2 = (bottomRightCorner.y * -2.f) + 1;

		vertices_.push_back(SpriteVertex(XMFLOAT3(
			x1,
			y1, 
			spriteVertices_[0].GetPosition().z), 
			spriteVertices_[0].GetTexCoords()));

		vertices_.push_back(SpriteVertex(XMFLOAT3(
			x2, 
			y1, 
			spriteVertices_[1].GetPosition().z), 
			spriteVertices_[1].GetTexCoords()));

		vertices_.push_back(SpriteVertex(XMFLOAT3(
			x2, 
			y2, 
			spriteVertices_[2].GetPosition().z), 
			spriteVertices_[2].GetTexCoords()));

		vertices_.push_back(SpriteVertex(XMFLOAT3(
			x1, 
			y2, 
			spriteVertices_[3].GetPosition().z), 
			spriteVertices_[3].GetTexCoords()));
		/*float topLeftCornerX = topLeftCorner.x * 2.f;
		float bottomRightCornerX = bottomRightCorner.x - topLeftCorner.x;
		float topLeftCornerY = topLeftCorner.y * 2.f;
		float bottomRightCornerY = bottomRightCorner.y - topLeftCorner.y;

		for_each(begin(spriteVertices_), end(spriteVertices_), [&](const SpriteVertex & sprite)
		{
			vertices_.push_back(
				SpriteVertex(
					XMFLOAT3(
						(sprite.GetPosition().x + topLeftCornerX) * bottomRightCornerX, 
						(sprite.GetPosition().y - topLeftCornerY) * bottomRightCornerY, 
						sprite.GetPosition().z), 
					sprite.GetTexCoords()));
		});*/
		//vertices_ = vector<vertex_t>(begin(spriteVertices_), end(spriteVertices_));
	
		CreateVertexBuffer();
		
		// On cr�� les indexes
		indexes_ = vector<index_t>(begin(spriteIndices_), end(spriteIndices_));
		CreateIndexBuffer();
	}
}

