#include "stdafx.h"
#include "Lighting.h"

namespace GE
{
	Lighting::Lighting(XMFLOAT4 ambient, XMFLOAT4 diffuse, XMFLOAT4 specular, XMFLOAT4 emissive, XMFLOAT4 transparent)
		: ambient_(ambient), diffuse_(diffuse), specular_(specular),  emissive_(emissive), transparent_(transparent)
	{
	}

	Lighting::Lighting(const Lighting & lighting)
		: ambient_(lighting.ambient_), diffuse_(lighting.diffuse_), specular_(lighting.specular_),  emissive_(lighting.emissive_), transparent_(lighting.transparent_)
	{

	}
}