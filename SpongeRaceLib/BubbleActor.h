#pragma once

#include "DynamicActor.h"
#include "ActorFactory.h"
#include "Actor.h"

class GE::GraphicObject;

class BubbleActor : public DynamicActor
{
public:
	static const unsigned char BubbleActor::DefaultPlayerKeyMapping[];

	BubbleActor(const ActorFactory::ActorID &aActorId)
		: DynamicActor(aActorId)
	{}

	static ActorFactory::ActorID typeId();
};