////////////////////////////////////////////////////////////////////////////////
// Filename: inputclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef USERINPUT_H
#define USERINPUT_H

//////////////
// INCLUDES //
//////////////
#include "Singleton.h"

enum {
	KEYBOARD,
	MOUSE,
	JOYSTICK,
	DEVICE_COUNT
};


enum {
	MOUSE_DIRECTION,
	KEYBOARD_DIRECTION,
	MOUSE_CLICK_DIRECTION,
	JOYSTICK_DIRECTION
};

class UserInput : Singleton<UserInput>
{
public:
	

	bool Initialize(DWORD, std::bitset<DEVICE_COUNT> devices);
	void Shutdown();
	bool Frame();
	bool IsKeyPressed(unsigned char);
	bool IsEscapePressed();
	void GetMouseLocation(int&, int&);
	void GetMouseState(DIMOUSESTATE& mouseState) {mouseState = m_mouseState;};
	void GetJoyState(DIJOYSTATE2& joyState) {joyState=m_joyState;};
	static BOOL CALLBACK enumCallback(const DIDEVICEINSTANCE* instance, VOID* context);
	static BOOL CALLBACK enumAxesCallback(const DIDEVICEOBJECTINSTANCE* instance, VOID* context);

private:
	UserInput();
	bool ReadKeyboard();
	bool ReadMouse();
	bool ReadJoystick();
	void ProcessInput();
	
	friend Singleton<UserInput>;

	LPDIRECTINPUT8 m_directInput;
	IDirectInputDevice8* m_keyboard;
	IDirectInputDevice8* m_mouse;
	IDirectInputDevice8* m_joystick;

	DIDEVCAPS capabilities;

	unsigned char m_keyboardState[256];
	DIMOUSESTATE m_mouseState;
	DIJOYSTATE2  m_joyState;

	int m_mouseX, m_mouseY;
};

#endif