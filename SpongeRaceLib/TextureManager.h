#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include "IRessourceManager.h"
#include "Texture.h"
#include "Singleton.h"

class TextureManager
	: public IRessourceManager<TextureManager, std::wstring, GE::Texture*>
{
public:
	friend class Singleton<TextureManager>;
	static std::wstring texturePath_;// = L"..\\SpongeRace\\Resources\\Textures\\";

	data_t GetTexture(key_t texName)
	{
		if(!Contains(texName))
			add(texName, new GE::Texture(texturePath_ + texName));

		return this->operator[](texName);
	}

	void AddTexture(key_t texName, data_t texture)
	{
		if(!Contains(texName))
			add(texName, texture);
	}
};

#endif