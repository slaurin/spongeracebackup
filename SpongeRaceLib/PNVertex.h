#pragma once
#include "Vertex.h"

namespace GE
{
	class PNVertex
	{
	private:
		XMFLOAT3 position_;
		XMFLOAT3 normal_;
		static D3D11_INPUT_ELEMENT_DESC Layout_[];

	public:
		PNVertex(
			XMFLOAT3 position = XMFLOAT3(0.f, 0.f, 0.f),
			XMFLOAT3 normal = XMFLOAT3(0.f, 1.f, 0.f));

		PNVertex(const Vertex & vertex);

		static D3D11_INPUT_ELEMENT_DESC * Begin();
		static UINT Size();

		XMFLOAT3 GetPosition() const throw();

		XMFLOAT3 GetNormal() const throw();

		void SetPosition(const XMFLOAT3 val) throw();

		void SetNormal(const XMFLOAT3 val) throw();
	};
}