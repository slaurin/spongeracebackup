#include "stdafx.h"
#include "TransparentMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	TransparentMat::TransparentMat(const Material * material)
		: Material(*material)
	{
		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(GlobalParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &globalBuffer_);
	}
	TransparentMat::~TransparentMat()
	{
	}
}