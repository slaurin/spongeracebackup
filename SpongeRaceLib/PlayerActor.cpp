#include "stdafx.h"

#include "GraphicEngine.h"
#include "SpotLight.h"
#include "GraphicObject.h"
#include "SpongeRace.h"
#include "ConstantsControl.h"
#include "PlayerActor.h"
#include "Controller.h"
#include "PxPhysicsAPI.h"
#include "Convert.h"

using namespace physx;
using namespace GE;

using std::cout;
using std::endl;

#define TIME_TO_BOOST 100
#define BOOST_MAX_POWER 100
#define MAXBOOST 6
const unsigned char PlayerActor::DefaultPlayerKeyMapping[] = {
	DIK_W, 
	DIK_S,
	0x0,
	0x0,
	0x0, 
	0x0,
	DIK_A,     //turn left
	DIK_D,     //turn right
	DIK_SPACE, //boost
	DIK_F      //lumiere
};

//=============================================================================
// CLASS PlayerActorData
//=============================================================================
class PlayerActorData: public IActorData
{
public:
	// physics
	PxMaterial *material_;
	PxShape* actorShape_;	

	// rendering
	GraphicObject* model_;

public:
	PlayerActorData()
		: material_(nullptr)
		, model_(nullptr)
	{}

	int load()
	{
		//---------------------------------------------------------------------
		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		//static friction, dynamic friction, restitution
		material_ = physics.createMaterial(0.1f, 0.01f, 0.1f); 

		return 0;
	}

};


class PlayerActorState
{
public:
	PlayerActorState()
		: _lastFireTime(0)
	{}

	double _lastFireTime;
};


class PlayerActorImp : public Actor<PlayerActorData, PlayerActor>
{
public:

	bool light_;
	int NbBoostLeft_;
	float boostPower_;
	float timeLeftToBoost_;
	GraphicObject* graphicObj_;
	PlayerActorState state_;
	SpotLight * rightLight_;
	PxVec3 rightLightShift_;
	SpotLight * leftLight_;
	PxVec3 leftLightShift_;
	bool onTheFloor_;

public:
	PlayerActorImp(const IActorDataRef &aDataRef)
		: Actor(aDataRef, typeId()), boostPower_(1.f),timeLeftToBoost_(0.f),NbBoostLeft_(0),onTheFloor_(false), light_(false)
	{
		std::copy ( DefaultPlayerKeyMapping, DefaultPlayerKeyMapping + ACTION_COUNT, std::back_inserter(keyMapping_));
	}

private:
	SpotLight * GetRightLight()
	{
		return rightLight_;
	}

	SpotLight * GetLeftLight()
	{
		return leftLight_;
	}

	XMFLOAT4 getRightLightPos(const PxVec3 & pose, const PxVec3 & dir, const PxVec3 & up, const PxVec3 & right) const
	{
		PxVec3 pos(pose);
		pos += rightLightShift_.x * dir;
		pos += rightLightShift_.y * up;
		pos += rightLightShift_.z * right;
		return std::move(Convert::PxVec3ToXMFloat4(pos));
	}

	XMFLOAT4 getLeftLightPos(const PxVec3 & pose, const PxVec3 & dir, const PxVec3 & up, const PxVec3 & right) const
	{
		PxVec3 pos(pose);
		pos += leftLightShift_.x * dir;
		pos += leftLightShift_.y * up;
		pos += leftLightShift_.z * right;
		return std::move(Convert::PxVec3ToXMFloat4(pos));
	}

public:

	void activateButton1(float) override
	{
 		UseBoost();
	}
	int getNumberBoost()
	{
		return NbBoostLeft_;
	}

	void activateButton2(float) override
	{
		//Front light
		light_= (!light_);

		leftLight_->Enable(light_);
		rightLight_->Enable(light_);

	}

	void setGraphicObj(GraphicObject* obj) override
	{
		graphicObj_=obj;
	}

	GraphicObject* getGraphicObj() override
	{
		return graphicObj_;
	}
	void UseBoost()
	{
		if(NbBoostLeft_ > 0)
		{
			boostPower_ = BOOST_MAX_POWER;
			NbBoostLeft_--;
		}
	}
	void addOneBoost()
	{
        if(NbBoostLeft_ < MAXBOOST)
		    NbBoostLeft_++;
	}
	/*float GetBoostPourcent()
	{
		return (timeLeftToBoost_/TIME_TO_BOOST * 100.f);
	}*/


	//-------------------------------------------------------------------------
	//
	static ActorFactory::ActorID typeId()
	{
		return PlayerActor::typeId();
	}

	//-------------------------------------------------------------------------
	//
	static IActorData* loadData()
	{
		PlayerActorData *data = new PlayerActorData();
		data->load();
		return data;
	}

	//-------------------------------------------------------------------------
	//
	static IActor* createInstance(const IActorDataRef &aDataRef)
	{
		PlayerActorImp *actor = new PlayerActorImp(aDataRef);
		return actor;
	}

	//-------------------------------------------------------------------------
	//
	virtual void onSpawn(const PxTransform &aPose, PxGeometry * geometry = nullptr) override
	{
		if(!geometry)
			geometry = new PxSphereGeometry(0.2f);

		setSpeed(1.f);
		rightLightShift_ = PxVec3(1.5f, 1.f, 0.7f);
		leftLightShift_ = PxVec3(1.5f, 1.f, -0.7f);

		// create the physic object
		DynamicActor::setSpeed(getSpeed()*3);
		PxPhysics &physics = GAME->scene()->getPhysics();

		pxActor_ = GAME->scene()->getPhysics().createRigidDynamic(aPose);
		pxActor_->userData = this;
		pxActor_->setName("Player");

		if(geometry)
			actorShape_ = pxActor_->createShape(*geometry, *_data->material_);
		else
			actorShape_ = pxActor_->createShape(PxSphereGeometry(0.2f), *_data->material_);

		actorShape_->setName("Player");

		rightLight_ = new SpotLight(
			getRightLightPos(getPxPosition(), getPxDirection(), getPxUp(), getPxRight()),
			Convert::PxVec3ToXMFloat4(getPxDirection()), 
			4.f,
			1.f,
			XMFLOAT4(0.35f, 0.65f, 0.81f, 1.f), 
			XMFLOAT4(1., 1., 1., 1.));
		rightLight_->SetAttenuation(0., 0.f, 0.2f, 0.f);
		leftLight_ = new SpotLight(*rightLight_);
		leftLight_->SetPosition(getLeftLightPos(getPxPosition(), getPxDirection(), getPxUp(), getPxRight()));

		PxVec3 *a = new PxVec3(0.f, 0.f, 0.f);
		bool b = PxRigidBodyExt::setMassAndUpdateInertia(*pxActor_, 0.8f, a);
		//pxActor_->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
		GAME->scene()->addActor(*pxActor_);

		PxFilterData filterData;
		filterData.word0 = eACTOR_PLAYER;
		filterData.word1 = eACTOR_ENEMY | eACTOR_TERRAIN | eACTOR_OBSTACLE | eACTOR_BUBBLE ;
		actorShape_->setSimulationFilterData(filterData);

		state_._lastFireTime = 0;

		graphicObj_->SetWorldMatrix(GetWorldTransform());
	}

	//-------------------------------------------------------------------------
	//
	virtual void onUnspawn() override
	{
		//GAME->camera()->removeFromRenderList(this);

		// cleanup physics objects
		if (pxActor_)
		{
			actorShape_->release();
			actorShape_=nullptr;
			pxActor_->release();
			pxActor_= nullptr;
		}
	}

	void onContact(const physx::PxContactPair &aContactPair)
	{		
		if ((aContactPair.flags & PxContactPairFlag::eDELETED_SHAPE_0) || (aContactPair.flags & PxContactPairFlag::eDELETED_SHAPE_1))
		{
			return;
		}

		if (aContactPair.events & PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
				std::string name0 = std::string(aContactPair.shapes[0]->getName());
				if(name0=="RaceTrack" )
				{
					onTheFloor_=true;
				}


				std::string name1 = std::string(aContactPair.shapes[1]->getName());
				if(name1=="RaceTrack")
				{
					onTheFloor_=true;
				}
			
		}

		if(aContactPair.events & PxPairFlag::eNOTIFY_TOUCH_LOST)
		{

				std::string name0 = std::string(aContactPair.shapes[0]->getName());
				if(name0=="RaceTrack" )
				{
					onTheFloor_=false;
				}
			


				std::string name1 = std::string(aContactPair.shapes[1]->getName());
				if(name1=="RaceTrack")
				{
					onTheFloor_=false;
				}
			
		}


	}

	//-------------------------------------------------------------------------
	//
	virtual void update(float timeElapsed) override
	{

		if(!onTheFloor_)
		{
			yaw_   = 0.0f;
			pitch_ = 0.0f;
			roll_  = 0.0f;
			moveLeftRight_ = 0.0f;
			moveBackForward_ = 0.0f;
			moveUpDown_ = 0.0f;
		}

		//PxTransform pose = pxActor_->getGlobalPose();
		//TODO COmplete
		//actorShape_->setLocalPose(PxTransform(PxVec3(position_.x, position_.y, position_.z)));
		
		if(!(yaw_ == 0 && pitch_ == 0 && roll_ == 0 &&	moveBackForward_ == 0 && moveUpDown_ == 0 && moveLeftRight_ == 0))
		{
			/*yaw_/=50; 
			pitch_/=50; 
			roll_/=50;*/ 
			Rotate();
			//Move(timeElapsed);

			//PxVec3 velocity = pxActor_->getLinearVelocity();
			//PxReal mass = pxActor_->getMass();
		
			float dirSpeed = SpeedRelated(moveBackForward_);// * timeElapsed;
			//float upSpeed = SpeedRelated(moveUpDown_) * timeElapsed;
			//float rightSpeed = SpeedRelated(moveLeftRight_) * timeElapsed;

			
		
			PxVec3 force = getPxDirection() * dirSpeed;
			/*force += getPxUp() * upSpeed;
			force += getPxRight() * upSpeed;*/
			if(force.y > 0)
				pxActor_->addForce(force.multiply(PxVec3(8., 2., 8.)*boostPower_), PxForceMode::eFORCE);
			else
				pxActor_->addForce(force.multiply(PxVec3(8., 8., 8.)*boostPower_), PxForceMode::eFORCE);
			boostPower_ = 1;
			/*PxVec3 dir = getPxDirection();
			*/

			moveLeftRight_ = 0.0f;
			moveBackForward_ = 0.0f;
			moveUpDown_ = 0.0f;
		}
		
		rightLight_->SetPosition(getRightLightPos(getPxPosition(), getPxDirection(), getPxUp(), getPxRight()));
		rightLight_->SetDirection(Convert::PxVec3ToXMFloat4(-getPxDirection()));
		leftLight_->SetPosition(getLeftLightPos(getPxPosition(), getPxDirection(), getPxUp(), getPxRight()));
		leftLight_->SetDirection(Convert::PxVec3ToXMFloat4(-getPxDirection()));
		graphicObj_->SetWorldMatrix(GetWorldTransform());
	}


};


//-----------------------------------------------------------------------------
//
ActorFactory::ActorID PlayerActor::typeId()
{	return "PlayerActor"; }

RegisterActorType<PlayerActorImp> gRegisterActor;