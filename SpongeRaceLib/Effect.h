#pragma once

#include "IEffect.h"
#include "MeshVertex.h"
#include "MiniPhongMat.h"
#include "PhongMat.h"
#include "SimpleVertex.h"
#include "ColorMat.h"

namespace GE
{




	//! Classe représentant un effect instanciable de l'architecture FX
	/*! 
		Le template VertexT représente le type de vertex utilisé (utile notamment pour construire l'input layout de chaque passe)
		Le template MaterialT permet de vérifier la compatibilité avec un Object3D
	*/
	template<class VertexT, class MaterialT>
	class Effect
		: public IEffect
	{
	public:
		/*! Type des vertices */
		typedef VertexT vertex_t;

		/*! Type des materiaux */
		typedef MaterialT material_t;
		
		Effect(const std::wstring & EffectFileName, bool preCompiled = false,  const std::string & EffectVersion = "fx_5_0")
			: IEffect(EffectFileName, preCompiled, EffectVersion)
		{
			// On charge les techniques
			loadTechniques<VertexT>();
		}
	};

}