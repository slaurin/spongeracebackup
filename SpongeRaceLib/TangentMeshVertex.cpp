#include "stdafx.h"
#include "TangentMeshVertex.h"
#include "AssimpLoader.h"

using namespace std;
using namespace Assimp;

namespace GE
{
	D3D11_INPUT_ELEMENT_DESC TangentMeshVertex::Layout_[] =
	{
		{"POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},  
		{"NORMAL",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA,	0},
		{"TANGENT",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA,	0},
		{"TEXCOORD",	0,	DXGI_FORMAT_R32G32_FLOAT,		0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA,	0}
	};

	D3D11_INPUT_ELEMENT_DESC * TangentMeshVertex::Begin()
	{return Layout_;}

	UINT TangentMeshVertex::Size()
	{return ARRAYSIZE(Layout_);}

	TangentMeshVertex::TangentMeshVertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT3 tangent,XMFLOAT2 coordTex)
		: position_(position), normal_(normal), coordTex_(coordTex), tangent_(tangent)
	{}

	XMFLOAT3 TangentMeshVertex::GetPosition() const throw()
	{return position_;}

	XMFLOAT3 TangentMeshVertex::GetNormal() const throw()
	{return normal_;}

	XMFLOAT2 TangentMeshVertex::GetTexCoord() const throw()
	{return coordTex_;}

	void TangentMeshVertex::SetPosition(const XMFLOAT3 position) throw()
	{position_ = position;}

	void TangentMeshVertex::SetNormal(const XMFLOAT3 normal) throw()
	{normal_ = normal;}

	void TangentMeshVertex::SetTexCoord(const XMFLOAT2 coordTex) throw()
	{coordTex_ = coordTex;}

	TangentMeshVertex::TangentMeshVertex(const Vertex & vertex)
		: position_(vertex.position()), normal_(vertex.normal()), coordTex_(XMFLOAT2(0.f, 0.f)),  tangent_(vertex.tangent())
	{
		if(!vertex.textureCoords().empty())
			coordTex_ = vertex.textureCoords().front();
	}
	
}
