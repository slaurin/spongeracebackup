//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "stdafx.h"
#include "ActorFactory.h"

//=============================================================================
// CLASS ActorFactoryEntry
//=============================================================================
struct ActorFactoryEntry
{
	ActorFactoryEntry(){}

	ActorFactoryEntry(ActorFactory::CreateInstanceProc aCreateActorProc, ActorFactory::CreateDataProc aCreateActorDataProc)
		: _createActorProc(aCreateActorProc)
		, _createActorDataProc(aCreateActorDataProc)
	{}

	ActorFactory::CreateInstanceProc _createActorProc;
	ActorFactory::CreateDataProc _createActorDataProc;
	std::weak_ptr<IActorData> _data;
} ;

//-----------------------------------------------------------------------------
//
typedef std::map<ActorFactory::ActorID, ActorFactoryEntry> ActorFactoryRegistry;

//=============================================================================
// CLASS ActorFactoryRegistry
//=============================================================================
static ActorFactoryRegistry& GetActorFactoryRegistry()
{
	static ActorFactoryRegistry sRegistry;
	return sRegistry;
}

//=============================================================================
// CLASS ActorFactory
//=============================================================================
void ActorFactory::registerActorType(const ActorID &aActorID, CreateInstanceProc aCreateActorProc, CreateDataProc aCreateActorDataProc)
{
	ActorFactoryRegistry &reg = GetActorFactoryRegistry();
	reg[aActorID] = ActorFactoryEntry(aCreateActorProc, aCreateActorDataProc);
}

//-----------------------------------------------------------------------------
//
void ActorFactory::unregisterActorType(const ActorID &aActorID)
{
	ActorFactoryRegistry &reg = GetActorFactoryRegistry();
	auto found = reg.find(aActorID);
	if (found != reg.end())
		reg.erase(found);
}

//-----------------------------------------------------------------------------
//
IActor* ActorFactory::createInstance(const ActorID &aActorID)
{
	ActorFactoryRegistry &reg = GetActorFactoryRegistry();
	auto found = reg.find(aActorID);
	if (found == reg.end())
		return nullptr;

	IActorDataRef dataRef = found->second._data.lock();
	if (!dataRef)
	{
		dataRef.reset(found->second._createActorDataProc());
		found->second._data = dataRef;
	}
	
	IActor *newActor = found->second._createActorProc(dataRef);
	return newActor;
}

