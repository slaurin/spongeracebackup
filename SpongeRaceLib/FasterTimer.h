#ifndef FASTER_TIMER_H
#define FASTER_TIMER_H

class FasterTimer
{
private:
    LONGLONG frequency_;
public:
    FasterTimer()
        : frequency_(LONGLONG())
    {
        setFrequency();
    }

    LONGLONG getTime();

private:
    void setFrequency();
};

#endif