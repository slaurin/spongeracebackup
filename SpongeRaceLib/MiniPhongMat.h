#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"

namespace GE
{
	class MiniPhongMat
		: public Material
	{
		struct ShadersParams // toujours un multiple de 16 pour les constantes
		{ 
			XMMATRIX matWorldViewProj;	// la matrice totale 
			XMMATRIX matWorld;			// matrice de transformation dans le monde 
			XMVECTOR vLumiere; 			// la position_ de la source d'�clairage (Point)
			XMVECTOR vCamera; 			// la position_ de la cam�ra
			XMVECTOR vAEcl; 			// la valeur ambiante de l'�clairage
			XMVECTOR vAMat; 			// la valeur ambiante du mat�riau
			XMVECTOR vDEcl; 			// la valeur diffuse de l'�clairage 
			XMVECTOR vDMat; 			// la valeur diffuse du mat�riau 
			XMVECTOR vSEcl; 			// la valeur sp�culaire de l'�clairage 
			XMVECTOR vSMat; 			// la valeur sp�culaire du mat�riau 
			float puissance;
			int bTex;					// Texture ou materiau 
			XMFLOAT2 remplissage;
		};

		ID3D11Buffer *constantBuffer;
		float power_;

	public:
		MiniPhongMat(const Material *);

		~MiniPhongMat(void);

		template<class VertexT>
		void Apply(Effect<VertexT, MiniPhongMat> * effect, const Object3D<VertexT, MiniPhongMat> & object)
		{
			// Initialiser et s�lectionner les �constantes� de l'effet
			ShadersParams sp;
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			sp.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj );
			sp.matWorld = XMMatrixTranspose(object.GetWorldMatrix());
			sp.vLumiere = XMVectorSet( 0.0f, 3.0f, -5.0f, 1.0f  );//XMVectorSet( -10.0f, 10.0f, -15.0f, 1.0f );
			sp.vCamera = XMVectorSet( 0.0f, 3.0f, -5.0f, 1.0f  );
			sp.vAEcl =  XMVectorSet( 0.2f, 0.2f, 0.2f, 1.0f ) ;
			sp.vDEcl = XMVectorSet( 1.0f, 1.0f, 1.0f, 1.0f );
			sp.vSEcl = XMVectorSet( 0.6f, 0.6f, 0.6f, 1.0f );
			sp.vAMat = XMLoadFloat4(&lighting_.GetAmbient());
			sp.vDMat = XMLoadFloat4(&lighting_.GetDiffuse());
			sp.vSMat = XMLoadFloat4(&lighting_.GetSpecular());
			sp.puissance = power_;

			// Activation de la texture ou non
			auto it = textures_.find(aiTextureType(aiTextureType_DIFFUSE));
			if(it != textures_.end())
			{
				ID3DX11EffectShaderResourceVariable* variableTexture;
				variableTexture = effect->GetVariableByName("textureEntree")->AsShaderResource();
				variableTexture->SetResource(it->second->GetTexture());
				sp.bTex = 1;

				// On charge le sampler state associ�
				ID3DX11EffectSamplerVariable* variableSampler;
				variableSampler = effect->GetVariableByName("SampleState")->AsSampler();
				variableSampler->SetSampler(0, it->second->GetSamplerState());
			}
			else
			{
				sp.bTex = 0;
			}

			// On charge le buffer
			ID3DX11EffectConstantBuffer* pCB = effect->GetConstantBufferByName("param");
			pCB->SetConstantBuffer(constantBuffer);

			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer, 0, NULL, &sp, 0, 0);

			effect->Apply(object);
		}
	};
}