#ifndef PATHING_H
#define PATHING_H

class Pathing
{
public:
    static boost::filesystem::path Path()
    {
        return boost::filesystem::current_path();
    }
};

#endif