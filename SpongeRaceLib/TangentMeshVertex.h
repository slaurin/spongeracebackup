#pragma once

#include "Vertex.h"

namespace GE
{
	class TangentMeshVertex
	{
	private:
		XMFLOAT3 position_;
		XMFLOAT3 normal_;
		XMFLOAT3 tangent_;
		XMFLOAT2 coordTex_;
		static D3D11_INPUT_ELEMENT_DESC Layout_[];

	public:
		TangentMeshVertex(
			XMFLOAT3 position	= XMFLOAT3(0.f, 0.f, 0.f),
			XMFLOAT3 normal		= XMFLOAT3(0.f, 1.f, 0.f),
			XMFLOAT3 tangent	= XMFLOAT3(0.f, 1.f, 0.f),
			XMFLOAT2 coordTex	= XMFLOAT2(0.f, 0.f));

		static D3D11_INPUT_ELEMENT_DESC * Begin();
		static UINT Size();

		XMFLOAT3 GetPosition() const throw();

		XMFLOAT3 GetNormal() const throw();

		XMFLOAT2 GetTexCoord() const throw();

		void SetPosition(const XMFLOAT3 position) throw();

		void SetNormal(const XMFLOAT3 normal) throw();

		void SetTexCoord(const XMFLOAT2 coordTex) throw();

		TangentMeshVertex(const Vertex &);
	};
}