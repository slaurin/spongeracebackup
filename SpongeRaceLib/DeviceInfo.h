#pragma once

#include "GraphicEngine.h"

namespace GE
{
	enum INFODISPO_TYPE
	{
		ADAPTATEUR_COURANT
	};

	class DeviceInfo
	{
	public:
		DeviceInfo(int NoAdaptateur);
		DeviceInfo(DXGI_MODE_DESC modeDesc);

		~DeviceInfo(void);

		void GetDesc(DXGI_MODE_DESC & modeDesc)
		{modeDesc = mode;}


	private:
		DeviceInfo();

		bool valide;
		int largeur;
		int hauteur;
		int memoire;
		wchar_t nomcarte[100];
		DXGI_MODE_DESC mode;
	};
}