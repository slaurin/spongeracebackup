#pragma once

#include "Light.h"

namespace GE
{
	//! Repr�sente une lumi�re ponctuelle
	class PointLight
		: public Light
	{
		// Exception lev�e lorsque tous les coefficients d'att�nuation sont nuls
		class AttenuationsCoefficientsCantBeAllNull{};

		//! Position de la source lumineuse 
		XMFLOAT4 position_;

		//! Distance maximale d'�clairage de la lampe
		float range_;

		//! Coefficient constant d'att�nuation d'�clairage
		float constantCoef_;

		//! Coefficient lin�aire d'att�nuation d'�clairage
		float linearCoef_;

		//! Coefficient quadratique d'att�nuation d'�clairage
		float quadraticCoef_;

	public:
		static float INFINITE_RANGE;

		PointLight(XMFLOAT4 position, XMFLOAT4 diffuse, XMFLOAT4 specular);

		//! Retourne la position de la source lumineuse
		XMFLOAT4 GetPosition() const;

		//! Fixe la position de la source lumineuse
		void SetPosition(const XMFLOAT4 &);

		//! Fixe les valeurs d'att�nuation avec la distance
		void SetAttenuation(const float range, const float constant, const float linear, const float quadratic);

		//! Retourne la distance maximale d'�clairage de la lumi�re
		float GetAttenuationRange() const;

		//! Retourne le coefficient constant d'att�nuation
		float GetAttenuationConstant() const;

		//! Retourne le coefficient lin�aire d'att�nuation
		float GetAttenuationLinear() const;

		//! Retourne le coefficient quadratic d'att�nuation
		float GetAttenuationQuadratic() const;
	};
}