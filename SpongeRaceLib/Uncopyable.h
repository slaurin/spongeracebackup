/*
 *  Author : Stephane Laurin
 *  Verifier : L�o
 */

#ifndef UNCOPYABLE_H
#define UNCOPYABLE_H

//! Classe prot�geant ses enfants priv�s de la copie
class Uncopyable
{
protected:
    Uncopyable(){}
    ~Uncopyable(){}
private:
    Uncopyable(const Uncopyable &);
    const Uncopyable & operator=(const Uncopyable &);
};

#endif