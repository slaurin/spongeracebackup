#pragma once

namespace GE
{

	class SimpleVertex
	{

	private:
		XMFLOAT3 position_;
		XMFLOAT3 color_;
		static D3D11_INPUT_ELEMENT_DESC Layout_[];

	public:
		SimpleVertex(XMFLOAT3 position = XMFLOAT3(0.f, 0.f, 0.f),
			XMFLOAT3 color = XMFLOAT3(0.f, 0.f, 0.f));

		static D3D11_INPUT_ELEMENT_DESC * Begin();
		static UINT Size();

		XMFLOAT3 GetPosition() const throw();

		XMFLOAT3 GetColor() const throw();

		void SetPosition(const XMFLOAT3 val) throw();

		void SetColor(const XMFLOAT3 val) throw();
	};
}

