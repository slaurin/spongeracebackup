#pragma once

#include "Material.h"

namespace GE
{
	class Pannel2DMat
		: public Material
	{
	public:
		// Type d'affichage 2D disponibles
		enum Type2D {
			SPRITE,
			BILLBOARD
		};

	private:
		//! Type d'affichage 2D utilis�
		Type2D type_;

		//! Param�tres de superposition des sprites
		float zIndex_;

		//! Utilise le m�lange alpha
		bool useAlphaBlending_;

	public:
		Pannel2DMat(const Material * material = new Material(), Type2D type = SPRITE, float zIndex = 0.f, bool useAlphaBlending = false);

		//! Retourne le nom de la classe
		static std::string GetClassName();

		//! Applique l'effet sur l'object � partir des valeurs de ce mat�riau
		template<class VertexT>
		void Apply(Effect<VertexT, Pannel2DMat> * effect, const Object3D<VertexT, Pannel2DMat> & object)
		{
			switch(type_)
			{
			case SPRITE:
				effect->GetVariableByName("zIndex")->AsScalar()->SetFloat(zIndex_);
				break;
			case BILLBOARD:// TODO make it running if you want to use
				//effect->GetVariableByName("matWorldViewProj")->AsMatrix()->SetMatrix(&XMMatrixTranspose(object.GetWorldMatrix() * GraphicEngine::GetInstance().GetMatViewProj()));
				break;
			}

			// On charge la texture AMBIENT si elle est d�finie
			auto it = textures_.find(aiTextureType(aiTextureType_AMBIENT));
			if(it != textures_.end())
			{
				effect->GetVariableByName("Texture")->AsShaderResource()->SetResource(it->second->GetTexture());
				effect->GetVariableByName("Sampler")->AsSampler()->SetSampler(0, it->second->GetSamplerState());
			}

			if(useAlphaBlending_)
			{
				GraphicEngine::GetInstance().GetDevice()->EnableAlphaBlend();
				effect->Apply(object);
				GraphicEngine::GetInstance().GetDevice()->DisableAlphaBlend();
			}
			else
			{
				effect->Apply(object);
			}
		}

		~Pannel2DMat();
	};
}
