#include "stdafx.h"

#include "PlayerTask.h"
#include "SpongeRace.h"
#include "PlayerActor.h"

void PlayerTask::init()
{

}

void PlayerTask::cleanup()
{

}

void PlayerTask::update()
{
	if(!GAME->isPause() && GAME->player())
	{
		GAME->player()->update(LoopTime::GetInstance().ElapsedTimeSeconds());
	}
}
