#pragma once

#include "CameraActor.h"
#include "Uncopyable.h"
#include "GameTask.h"
#include "PlayerActor.h"
#include "SpawnManager.h"
#include "LoopTime.h"
#include "GameMenu.h"
#include "Object3D.h"
#include "HUDTask.h"

namespace GE{
	class BubbleMat;
	class TangentMeshVertex;
	class Scene;
};

class BubbleActor;

class SpongeRace
	: Uncopyable
{

	//XMLAssetManager assetManager_;
	std::string currentScene_;

	bool ready_;
	bool _shouldExit;
	PlayerActor* player_;
	CameraActor* camera_;
	GE::Object3D<GE::TangentMeshVertex, GE::BubbleMat>* mainBulle;
	std::vector<BubbleActor*> bubbles_;
	GameUI::GameMenu * menu_;
	HUDTask * hudTask_;
	bool gameStarted_;


	physx::PxScene *scene_;
	physx::PxPhysics *physics_;
	physx::PxCooking *cooking_;
	std::vector<GameTask *> tasks_;
	
	int numThread_;
	bool gameover_;
	bool gamepaused_;
	SpawnManager spawnManager_;
	
	//GameTimer _systemTime;
	//GameTimer _gameTime;
	//double _frameTime;
	//double _lastFrameTime;

public:
	SpongeRace(HINSTANCE);
	~SpongeRace();

	SpawnManager* spawnManager(){return &spawnManager_;};

	void CreateBubbles(int nb, GE::Scene * scene);
	void startGame();
	bool ready(){return ready_;};
	void Init();
	void update();
	void cleanup();

	Controller& mainController();

	//-------------------------------------------------------------------------
	//
	/*void onBeginFrame()
	{
		_lastFrameTime = _frameTime;
		_frameTime = _gameTime.now();
	};*/
	//-------------------------------------------------------------------------
	//
	/*void onStartGameSession()
	{
		_gameTime.reset();
		_frameTime = 0;
		_lastFrameTime = 0;
	};*/
	//-----------------------------------------------------------------------------
	//
	/*double currentTime()
	{
		return _frameTime;
	};*/
	//-----------------------------------------------------------------------------
	//
	/*void setGameRate(float aTimeRate)
	{
		return _gameTime.setRate(aTimeRate);
	};*/


	//-----------------------------------------------------------------------------
	//
	/*double lastFrameTime()
	{
		return _lastFrameTime;
	}*/

	/*float timeDiff()
	{
		return static_cast<float>(currentTime()-lastFrameTime());
	}*/

	void setScene(physx::PxScene *aScene);
	physx::PxScene* scene();

	void setPhysics(physx::PxPhysics * aPhysics);
	physx::PxPhysics *physics();

	void setCooking(physx::PxCooking *aCooking);
	physx::PxCooking* cooking();

	void switchPause(){ 
		gamepaused_^=1;
		LoopTime::GetInstance().SetPause(gamepaused_);
		
		hudTask_->SetPause(gamepaused_);

		if(gamepaused_)
			menu_->Show();
		else
			menu_->Hide();
	};
	bool isPause(){ return gamepaused_ || gameover_;};

	void requestExit();
	bool shouldExit() const;

	void loadGameAssets();
	void loadLevel(std::string id);
	void unloadLevel(std::string id);

	void loadNextLevel();
	void loadPreviousevel();
	void unloadCurrentLevel();
	void EndOfRace(time_t raceTime);

	PlayerActor* player();
	CameraActor* camera();
	GameUI::GameMenu* menu(){return menu_;};

	void setPlayer(PlayerActor* player) {player_ = player;};
	void setCamera(CameraActor* camera) {camera_ = camera;};
};

extern SpongeRace * GAME;