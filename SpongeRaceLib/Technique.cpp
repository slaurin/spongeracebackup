#include "stdafx.h"
#include "Technique.h"
#include "GraphicEngine.h"

using namespace std;

namespace GE
{
	Technique::Technique(ID3DX11EffectTechnique *technique, D3D11_INPUT_ELEMENT_DESC * begin, UINT size)
		: technique_(technique), passes_(vector<Pass>())
	{
		// On charge les passes
		for(UINT p = 0; technique->GetPassByIndex(p)->IsValid(); ++p)
			passes_.push_back(move(Pass(technique->GetPassByIndex(p), begin, size)));
	}

	Technique::~Technique(void)
	{
	}

	void Technique::Apply(const GraphicObject & object)
	{
		//... Apply technique

		for(auto it = passes_.begin(); it != passes_.end(); ++it)
		{
			// On d�finit la passe
			it->Apply();

			// On dessine l'objet
			object.SendToPipeline();
		}
	}
}