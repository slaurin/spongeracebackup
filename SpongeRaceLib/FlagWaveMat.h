#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"

#include "LoopTime.h"

namespace GE
{
	class FlagWaveMat
		: public Material
	{
		struct ShadersParams // toujours un multiple de 16 pour les constantes
		{ 
			XMMATRIX matViewProj;
			XMMATRIX matWorld;
			XMFLOAT4 variables; //x  phi
		};

		ID3D11Buffer *constantBuffer;
		float phi_;

	public:
		FlagWaveMat(const Material *);

		~FlagWaveMat(void);

		template<class VertexT>
		void Apply(Effect<VertexT, FlagWaveMat> * effect, const Object3D<VertexT, FlagWaveMat> & object)
		{
			// Initialiser et sélectionner les «constantes» de l'effet
			ShadersParams sp;
			sp.matViewProj =XMMatrixTranspose(GraphicEngine::GetInstance().GetMatViewProj());
			sp.matWorld = XMMatrixTranspose(object.GetWorldMatrix());

			
			phi_ = phi_+ 2*LoopTime::GetInstance().ElapsedTimeSeconds();
			if(phi_ > 360)
				phi_ -= 360;
			sp.variables = XMFLOAT4(phi_,0.0f,0.0f,0.0f);
			auto it = textures_.find(aiTextureType(aiTextureType_AMBIENT));
			if(it != textures_.end())
			{
				effect->GetVariableByName("textureEntree")->AsShaderResource()->SetResource(it->second->GetTexture());
				effect->GetVariableByName("SampleState")->AsSampler()->SetSampler(0, it->second->GetSamplerState());
			}

			// On charge le buffer
			ID3DX11EffectConstantBuffer* pCB = effect->GetConstantBufferByName("param");
			pCB->SetConstantBuffer(constantBuffer);

			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer, 0, NULL, &sp, 0, 0);

			effect->Apply(object);
		}
	};
}