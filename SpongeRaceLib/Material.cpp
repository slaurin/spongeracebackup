#include "stdafx.h"
#include "Material.h"

using namespace std;

namespace GE
{
	Material::Material(const string & name, const Lighting & lighting, const map<aiTextureType, Texture *> & textures)
		: name_(name), lighting_(lighting), textures_(textures)
	{}

	Material::Material(const Material & material)
		: name_(material.name_), lighting_(material.lighting_), textures_(material.textures_)
	{
	}

	Material::~Material()
	{
	}

	string Material::GetClassName()
	{
		return "Material";
	}

	bool Material::InsertTexture(aiTextureType type, Texture * texture)
	{
		auto result = textures_.insert(pair<aiTextureType, Texture *>(type, texture));
		return result.second;
	}

	void Material::ReplaceTexture(aiTextureType type, Texture * texture)
	{
		textures_.erase(type);
		textures_.insert(pair<aiTextureType, Texture *>(type, texture));
	}

	void Material::SetLighting(const Lighting & lighting)
	{
		lighting_ = lighting;
	}
}