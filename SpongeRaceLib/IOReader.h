/*
 *  Author : Stephane Laurin
 *  Verifier : 
 */

#ifndef IO_READER_H
#define IO_READER_H

#include "Uncopyable.h"

/* Reads a file with a structure. */
template<class TData>
class IOReader : Uncopyable
{
private :
    std::vector<TData> data_;
public :

    IOReader(const std::string &fileName)
        : data_(std::istream_iterator<TData>(std::ifstream (fileName)),
                std::istream_iterator<TData>())
    {}
    
    std::vector<TData> &getData()
    {
        return data_;
    }
};

/* char specialization  */
template <>
class IOReader<char> : Uncopyable
{
private :
    std::vector<char> data_;
public :
    IOReader(const std::string &fileName)
        : data_(std::istreambuf_iterator<char>(std::ifstream(fileName, std::ios::binary)),
                std::istreambuf_iterator<char>())
    {}

    std::vector<char> &getData()
    {
        return data_;
    }
};

/* unsigned char specialization  */
template <>
class IOReader<unsigned char> : Uncopyable
{
private :
    std::vector<unsigned char> data_;
public :
    IOReader(const std::string &fileName)
        : data_(std::istreambuf_iterator<char>(std::ifstream(fileName, std::ios::binary)),
                std::istreambuf_iterator<char>())
    {}

    std::vector<unsigned char> &getData()
    {
        return data_;
    }
};

template <>
class IOReader<std::stringstream> : Uncopyable
{
private:
    std::stringstream& stream_;
public:
    IOReader(const std::string &fileName, std::stringstream &stream)
        : stream_(stream)
    {
        std::for_each(std::istreambuf_iterator<char>(std::ifstream(fileName, std::ios::binary)),
                      std::istreambuf_iterator<char>(),
                      [&](char c)
                      {
                          stream_ << c;
                      });
    }
};

#endif