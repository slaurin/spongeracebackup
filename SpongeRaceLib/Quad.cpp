#include "stdafx.h"
#include "Quad.h"

#include <algorithm>
#include <vector>

namespace GE
{
    ParticleVertex Quad::quadVertices_[] = {
        ParticleVertex(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f)),
        ParticleVertex(XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 0.0f)),
        ParticleVertex(XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 0.0f)),
        ParticleVertex(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 1.0f)),
        ParticleVertex(XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 0.0f)),
        ParticleVertex(XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f, 1.0f))
    };

    Quad::Quad(Effect<ParticleVertex, ParticleMat> *effect,
               ParticleMat *pMaterial,
               XMFLOAT3 position,
               XMFLOAT3 scale,
               float velocity,
               bool active)
      : Object3D<ParticleVertex, ParticleMat>(effect, pMaterial),
        position_(position), scale_(scale), velocity_(velocity), active_(active)
    {
        vertices_ = std::vector<ParticleVertex>(std::begin(quadVertices_), std::end(quadVertices_));
        CreateVertexBuffer();
    }

    void Quad::Anime(float timeElapsed)
    {
        XMMATRIX scale = XMMatrixScaling(scale_.x, scale_.y, scale_.z);
        XMVECTOR camPos = GAME->camera()->getPosition();
        float angle = atan2 (position_.x - camPos.x, position_.z - camPos.z);
        XMMATRIX rotation = XMMatrixRotationY(angle);
        XMMATRIX translation = XMMatrixTranslation(position_.x, position_.y, position_.z);
        SetWorldMatrix(XMMatrixMultiply(XMMatrixMultiply(scale, rotation), translation));
    }

    void Quad::Draw()
    {
        ID3D11DeviceContext * context = GraphicEngine::GetInstance().GetDevice()->GetDeviceContext();

        // Choisir la topologie des primitives
        context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        // Vertex buffer
        UINT stride = sizeof(vertex_t);
        UINT offset = 0;
        context->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);

        material_->Apply(effect_, *this);
    }

    void Quad::SendToPipeline() const
    {
        GraphicEngine::GetInstance().GetDevice()->EnableAlphaBlend();
        GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->Draw(6, 0);
        GraphicEngine::GetInstance().GetDevice()->DisableAlphaBlend();
    }
}