#include "stdafx.h"
#include "Pass.h"
#include "GraphicEngine.h"
#include "Resource.h"
#include "DxUtilities.h"

using namespace DxUtilities;

namespace GE
{
	Pass::Pass(ID3DX11EffectPass *pass, D3D11_INPUT_ELEMENT_DESC * begin, UINT size, UINT flags)
		: pass_(pass), flags_(flags), vertexLayout_(nullptr)
	{	
		// Cr�er l'organisation des sommets pour le VS de notre effet
		D3DX11_PASS_SHADER_DESC effectVSDesc;
		pass_->GetVertexShaderDesc(&effectVSDesc);

		D3DX11_EFFECT_SHADER_DESC effectVSDesc2;
		effectVSDesc.pShaderVariable->GetShaderDesc(effectVSDesc.ShaderIndex, &effectVSDesc2);

		const void *vsCodePtr = effectVSDesc2.pBytecode;
		unsigned vsCodeLen = effectVSDesc2.BytecodeLength;

		DXEssayer(
			GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateInputLayout(
			begin,
			size,
			vsCodePtr,
			vsCodeLen,
			&vertexLayout_),
			DXE_CREATIONLAYOUT);
	}

	Pass::~Pass(void)
	{
		//DXRelacher(vertexLayout_);
	}

	void Pass::Apply()
	{
		auto context = GraphicEngine::GetInstance().GetDevice()->GetDeviceContext();

		// input Layout_ des sommets
		context->IASetInputLayout(vertexLayout_);

		// IMPORTANT pour ajuster les param.
		pass_->Apply(flags_, context);
	}
}
/*
void Pass::Apply(UINT flags, ID3DX11EffectConstantBuffer* pCB, const void * parameters)
{

	foreach(pass)
	{
	// IMPORTANT pour ajuster les param.
	pass_->Apply(flags, );

	pCB->SetConstantBuffer(pConstantBuffer);
	context->UpdateSubresource(pConstantBuffer, 0, NULL, parameters, 0, 0);
	
	object->SendToPipelin();
	}
}*/