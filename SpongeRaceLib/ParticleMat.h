#ifndef PARTICLE_MAT_H
#define PARTICLE_MAT_H

#include "Material.h"
#include <string>

namespace GE
{
    class ParticleMat : public Material
    {
        struct GlobalParams // toujours un multiple de 16 pour les constantes
	    {
		    XMMATRIX matWorldViewProj;	// Matrice WVP
	    };

	    //! Param�tres globaux
	    GlobalParams globalParams_;

	    //! Buffer correspondant aux param�tres globaux
	    ID3D11Buffer * globalBuffer_;

    public:

        ParticleMat(const Material * material = new Material());

        //! Retourne le nom de la classe
        static std::string GetClassName();

        //! Applique l'effet sur l'object � partir des valeurs de ce mat�riau
		template<class VertexT>
		void Apply(Effect<VertexT, ParticleMat> * effect, const Object3D<VertexT, ParticleMat> & object)
		{
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			globalParams_.matWorldViewProj =  XMMatrixTranspose(object.GetWorldMatrix() * viewProj);
			
			// On charge les constantes globales
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(globalBuffer_, 0, NULL, &globalParams_, 0, 0);
			effect->GetConstantBufferByName("GlobalParams")->SetConstantBuffer(globalBuffer_);

            // On charge la texture AMBIENT si elle est d�finie
            auto it = textures_.find(aiTextureType(aiTextureType_AMBIENT));
            if(it != textures_.end())
            {
                effect->GetVariableByName("shaderTexture")->AsShaderResource()->SetResource(it->second->GetTexture());
                effect->GetVariableByName("SamplerType")->AsSampler()->SetSampler(0, it->second->GetSamplerState());
            }

			effect->Apply(object);
		}

		~ParticleMat();

	    private:
		    void initGlobalParams();
    };
}

#endif