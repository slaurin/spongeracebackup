#pragma once

namespace GE
{
	class VertexConstructorNotImplementedException{};

	class Vertex
	{
		// Position du vertex
		XMFLOAT3 position_;

		// Normal du vertex
		XMFLOAT3 normal_;

		// Vertex tangents
		XMFLOAT3 tangent_;

		// Vertex bitangent
		XMFLOAT3 bitangent_;

		// Vertex texture coords, also known as UV channels
		std::vector<XMFLOAT2> textureCoords_;

		// Vertex color sets
		std::vector<XMFLOAT4> colors_;

	public:
		Vertex()
			: textureCoords_(std::vector<XMFLOAT2>()),
			  colors_(std::vector<XMFLOAT4>())
		{
		
		}

		Vertex(
			const aiVector3D * position, 
			const aiVector3D * normal, 
			const aiVector3D * tangent, 
			const aiVector3D * bitangent, 
			const aiVector3D * textureCoords,
			const aiColor4D * colors);

		XMFLOAT3 position() const throw()
		{
			return position_;
		}

		XMFLOAT3 normal() const throw()
		{
			return normal_;
		}

		XMFLOAT3 tangent() const throw()
		{
			return tangent_;
		}

		XMFLOAT3 bitangent() const throw()
		{
			return bitangent_;
		}

		std::vector<XMFLOAT2> textureCoords() const throw()
		{
			return textureCoords_;
		}

		std::vector<XMFLOAT4> colors() const throw()
		{
			return colors_;
		}

		void setPosition(const XMFLOAT3 & val) throw()
		{
			position_ = val;
		}

		void setNormal(const XMFLOAT3 & val) throw()
		{
			normal_ = val;
		}

		void setTangent(const XMFLOAT3 & val) throw()
		{
			tangent_ = val;
		}

		void setBitangent(const XMFLOAT3 & val) throw()
		{
			bitangent_ = val;
		}

		void addTexCoord(const XMFLOAT2 & val) throw()
		{
			textureCoords_.push_back(val);
		}

		void addColor(const XMFLOAT4 & val) throw()
		{
			colors_.push_back(val);
		}
	};
}
