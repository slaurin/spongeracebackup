#ifndef MATERIAL_MANAGER_H
#define MATERIAL_MANAGER_H

#include "IRessourceManager.h"
#include "Material.h"
#include "Singleton.h"

//! Manager de Mat�riaux, g�re leur acc�s, leur unicit� et leur dur�e de vie
class MaterialManager
	: public IRessourceManager<MaterialManager, std::string, GE::Material>
{
	friend class Singleton<MaterialManager>;

public:
	template<class MaterialT>
	MaterialT * getMaterial(key_t materialName)
	{
		if(Contains(materialName))
			return dynamic_cast<MaterialT*>(&this->operator[](materialName));
		else
			return nullptr;
	}

	template<class MaterialT>
	MaterialT * getDefault()
	{
		std::string defaultName = "Default" + MaterialT::GetClassName();
		MaterialT * defaultMaterial;

		if(Contains(defaultName))
			defaultMaterial = getMaterial<MaterialT>(defaultName);
		else
		{
			defaultMaterial = new MaterialT(Material(defaultName));
			add(defaultName, defaultMaterial);
		}

		return defaultMaterial;
	}
};

#endif