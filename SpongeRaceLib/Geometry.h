#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "Vertex.h"
#include "GeometryPart.h"

namespace GE
{
	class Geometry
	{
		//! Nom du Mesh
		std::string name_;

		// Inner meshes of a file such as head and body.
		std::vector<GeometryPart> parts_;

	public:
		Geometry(){}

		Geometry(const std::string name)
			: name_(name),
			  parts_(std::vector<GeometryPart>())
		{}

		std::string& name() throw()
		{
			return name_;
		}

		std::vector<GeometryPart>& parts() throw()
		{
			return parts_;
		}
	};
}

#endif