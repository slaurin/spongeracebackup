#pragma once

#include "Device.h"

namespace GE
{
	//! Objet repr�sentant une texture quelconque
	/*!
		Une texture s'occupe de charger en m�moire le fichier fournis avec son �tat de sampler associ�.
	*/
	class Texture
	{
		std::wstring				filename_;
		ID3D11ShaderResourceView	*texture_;
		ID3D11SamplerState			*sampleState_;

	public:
		//! Sampler par d�faut appliqu� � toutes les Textures (Attention si vous jouez avec :p !)
		static D3D11_SAMPLER_DESC defaultSamplerDesc;

		//! Lib�re les ressources associ�es � la texture
		~Texture();

		//! Charge le fichier en m�moire et cr�er un �tat de sampling associ�
		Texture(const std::wstring & filename, const D3D11_SAMPLER_DESC & samplerDesc = defaultSamplerDesc);

		//! 
		Texture(ID3D11ShaderResourceView * shaderResource, const D3D11_SAMPLER_DESC & samplerDesc = defaultSamplerDesc);

		//! Retourne le nom du fichier associ� � la texture
		std::wstring GetFilename();

		//! Retourne l'objet charg� en m�moire associ� � la texture
		ID3D11ShaderResourceView * GetTexture();

		//! Retourne l'�tat de sampler associ� � la texture
		ID3D11SamplerState * GetSamplerState();
	};
}