#ifndef GENERATEURNOMBRESALEATOIRES_H
#define GENERATEURNOMBRESALEATOIRES_H

#include "Uncopyable.h"

class GenerateurNombresAleatoires
   : Uncopyable
{
   GenerateurNombresAleatoires () throw();
   static GenerateurNombresAleatoires singleton;
public:
   typedef float value_type;
   class BornesInvalides {};
   static GenerateurNombresAleatoires &get() throw()
      { return singleton; }
   value_type generer() const throw();
   //
   // Bornes inclusives
   //
   value_type generer(const value_type b_min, const value_type b_max) const; // throw(BornesInvalides)
};

#endif