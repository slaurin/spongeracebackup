#include "stdafx.h"

#include "CameraTask.h"
#include "SpongeRace.h"
#include "CameraActor.h"

void CameraTask::init()
{
}

void CameraTask::cleanup()
{

}

void CameraTask::update()
{
	if(!GAME->isPause() && GAME->camera())
	{
		GAME->camera()->update(LoopTime::GetInstance().ElapsedTimeSeconds());
	}
	
}
