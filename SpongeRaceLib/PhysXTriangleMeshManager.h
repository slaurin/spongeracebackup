#pragma once

#include "SpongeRace.h"
#include "PhysXConverter.h"
#include "IRessourceManager.h"

class PhysXTriangleMeshManager : public IRessourceManager<PhysXTriangleMeshManager, std::string, physx::PxTriangleMesh*>
{

	friend class Singleton<PhysXTriangleMeshManager>;
public:

	physx::PxTriangleMesh * Request(const std::string &resourceName, GeometryPart* mesh)
	{
		if(!Contains(resourceName))
			add(resourceName, GenerateTriangleMeshFromGeometry(GAME->scene()->getPhysics(), mesh));
		return operator[](resourceName);
	}
};
