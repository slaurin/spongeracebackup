#ifndef GEOMETRY_MANAGER_H
#define GEOMETRY_MANAGER_H

#include "IRessourceManager.h"
#include "Geometry.h"
#include "Singleton.h"
#include "AssimpLoader.h"

class GeometryManager
	: public IRessourceManager<GeometryManager, std::string, GE::Geometry *>
{
	friend class Singleton<GeometryManager>;
public:

	GE::Geometry * Request(const std::string &resourceName,
						  const std::string &fileName,
						  bool flipUV = false,
						  bool genUV = false,
						  bool flipWindingOrder = false,
						  bool genSmoothNormal = false)
	{
		if(!Contains(resourceName))
			add(resourceName, GE::AssimpLoader::Import(fileName, flipUV, genUV, flipWindingOrder, genSmoothNormal));
		return operator[](resourceName);
	}
};

#endif