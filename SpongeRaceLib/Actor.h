#ifndef ACTOR_H
#define ACTOR_H

#include "ActorFactory.h"
#include "IActor.h"
#include "GraphicObject.h"
#include "Geometry.h"

enum
{
	eACTOR_PLAYER  = 1 << 0,
	eACTOR_ENEMY   = 1 << 1,
	eACTOR_BULLET  = 1 << 2,
	eACTOR_TERRAIN = 1 << 3,
	eACTOR_OBSTACLE = 1 << 4,
	eACTOR_CAMERA = 1 << 5,
	eACTOR_BUBBLE = 1 << 6,
	eACTOR_BUBBLEBARRIER = 1 << 7
};

class UnsupportedMethodException{};

class ActorBase: public IActor
{
protected:
	ActorFactory::ActorID id_;
	physx::PxShape *actorShape_;

public:

	ActorBase(const ActorFactory::ActorID &aId)
		:id_(aId),
		actorShape_(nullptr)
	{};



	/************************************************************************/
	/* PhysX                                                                     */
	/************************************************************************/
	virtual void update() override {};
	virtual void onContact(const physx::PxContactPair &aContactPair) override {};
	virtual void onTrigger(bool triggerEnter, physx::PxShape *actorShape, physx::PxShape *contactShape) override {};
	virtual void onSpawn(const physx::PxTransform &aPose, physx::PxGeometry * geometry = nullptr) override {};
	virtual void onUnspawn() override {};
	virtual physx::PxTransform pose()=0;

	virtual void EnablePhysX(){
		actorShape_->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE , true);
	}

	virtual void DisablePhysX(){
		actorShape_->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE , false);
	}

	virtual XMMATRIX GetWorldTransform()
	{
		physx::PxMat44 transfo(pose());
		return *(XMMATRIX*)&transfo;
	}

	virtual const char *debugName() const override
	{
		static char name[256] = {0};
		sprintf_s(name, "%s-0x%08X", id_, (int)this);
		return name;
	};



};

//=============================================================================
// CLASS Actor
//=============================================================================
template<class _DATATYPE, class _PARENT=ActorBase>
class Actor: public _PARENT
{
public:
	Actor(const IActorDataRef &aDataRef, const ActorFactory::ActorID &aId)
		: _PARENT(aId)
		, _data(std::static_pointer_cast<_DATATYPE>(aDataRef))
	{}

	virtual ~Actor() {}

protected:
	std::shared_ptr<_DATATYPE> _data;
};

#endif