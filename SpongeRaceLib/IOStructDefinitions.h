/*
 *  Author : Stephane Laurin
 *  Verifier : 
 *
 *  In the file, you declare both operator << and >> for each structure that you want
 *  to put in a file.
 */

#include "stdafx.h"
#include "PNVertex.h"

#include <iosfwd>

// XMFLOAT3
std::ostream& operator<<(std::ostream& os, const XMFLOAT3& obj);
std::istream& operator>>(std::istream& is, XMFLOAT3& v);

// Vertex
std::ostream& operator<<(std::ostream& os, const GE::PNVertex& obj);
std::istream& operator>>(std::istream& is, GE::PNVertex& v);