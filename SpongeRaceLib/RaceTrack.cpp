#include "stdafx.h"
#include "RaceTrack.h"


#include "stdafx.h"

#include "GraphicObject.h"
#include "SpongeRace.h"
#include "ConstantsControl.h"
#include "RaceTrack.h"
#include "Controller.h"
#include "PxPhysicsAPI.h"

using namespace physx;
using namespace GE;

using std::cout;
using std::endl;


//=============================================================================
// CLASS RaceTrackData
//=============================================================================
class RaceTrackData: public IActorData
{
public:
	// physics
	PxMaterial *material_;
	PxShape* actorShape_;


	// rendering
	GraphicObject* model_;

public:
	RaceTrackData()
		: material_(nullptr)
		, model_(nullptr)
	{}

	int load()
	{
		//---------------------------------------------------------------------
		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		//static friction, dynamic friction, restitution
		material_ = physics.createMaterial(0.5f, 0.5f, 0.1f); 

		return 0;
	}

};


class RaceTrackState
{
public:
	RaceTrackState()
		: _lastFireTime(0)
	{}

	double _lastFireTime;
};


class RaceTrackImp : public Actor<RaceTrackData, RaceTrack>
{
public:
	
	// physics	
	GraphicObject* graphicObj_;

	RaceTrackState state_;

public:
	RaceTrackImp(const IActorDataRef &aDataRef)
		: Actor(aDataRef, typeId())
	{}

public:

	void setGraphicObj(GraphicObject* obj) override
	{
		graphicObj_=obj;
	}

	//-------------------------------------------------------------------------
	//
	static ActorFactory::ActorID typeId()
	{
		return RaceTrack::typeId();
	}

	//-------------------------------------------------------------------------
	//
	static IActorData* loadData()
	{
		RaceTrackData *data = new RaceTrackData();
		data->load();
		return data;
	}

	//-------------------------------------------------------------------------
	//
	static IActor* createInstance(const IActorDataRef &aDataRef)
	{
		RaceTrackImp *actor = new RaceTrackImp(aDataRef);
		return actor;
	}

	//-------------------------------------------------------------------------
	//
	virtual void onSpawn(const PxTransform &aPose, PxGeometry * geometry) override
	{
		if(!geometry)
			throw GivenGeometryIsInvalid();

		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		pxActor_ = GAME->scene()->getPhysics().createRigidStatic(aPose);
		pxActor_->userData = this;
		pxActor_->setName("RaceTrack");

		actorShape_ = pxActor_->createShape(*geometry, *_data->material_);
		actorShape_->setName("RaceTrack");

		pxActor_->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
		GAME->scene()->addActor(*pxActor_);

		
		PxFilterData filterData;
		filterData.word0 = eACTOR_TERRAIN;
		filterData.word1 = 0;
		actorShape_->setSimulationFilterData(filterData);
		
		state_._lastFireTime = 0;

		graphicObj_->SetWorldMatrix(GetWorldTransform());
	}

	//-------------------------------------------------------------------------
	//
	virtual void onUnspawn() override
	{
		// cleanup physics objects
		if (pxActor_)
		{
			pxActor_->release();
			pxActor_= nullptr;
		}
	}


};


//-----------------------------------------------------------------------------
//
ActorFactory::ActorID RaceTrack::typeId()
{	return "RaceTrack"; }

RegisterActorType<RaceTrackImp> gRegisterActor;