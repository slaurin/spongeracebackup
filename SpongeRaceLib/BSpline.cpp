#include "stdafx.h"
#include "BSpline.h"
#include "DxUtilities.h"
#include "resource.h"
#include "GraphicEngine.h"
#include <algorithm>

XMMATRIX bSplineMatrix = XMMATRIX(-1,  3, -3,  1,
                                   3, -6,  3,  0,
                                  -3,  0,  3,  0,
                                   1,  4,  1,  0);

XMVECTOR BSpline::buildTimeVector(float t) const throw()
{
    return XMVectorSet(t*t*t, t*t, t, 1.0f);
}

XMVECTOR BSpline::buildTimeVectorWithFactor(float t) const throw()
{
	return XMVectorSet(t*t*t*factor_, t*t*factor_, t*factor_, factor_);
}

BSpline::BSpline(const std::vector<XMFLOAT4> &knots)
    : knots_(std::vector<XMFLOAT4>(knots.size())),factor_(1.0f/6),
      pointsSize_(knots.size()),
      innerCurvesSize_(knots.size()-1)
{
    assert(pointsSize_ > 1);
    knots_.push_back(knots[0]);
    knots_.insert(std::end(knots_), std::begin(knots), std::end(knots));
    knots_.push_back(knots[knots.size()-1]);
    knots_.push_back(knots[knots.size()-1]);
}
BSpline::BSpline(GE::GeometryPart *geometry)
	:	knots_(),
		pointsSize_(geometry->vertices().size()),
		innerCurvesSize_(geometry->vertices().size()-1),factor_(1.0f/6)
{
	assert(pointsSize_ > 1);
	auto begin = geometry->vertices().begin();
	knots_.push_back(XMFLOAT4(begin->position().x,begin->position().y,begin->position().z,0.0f));
	for(auto i = geometry->vertices().begin(); i != geometry->vertices().end(); ++i)
		knots_.push_back(XMFLOAT4(i->position().x,i->position().y,i->position().z,0.0f));
	auto end = geometry->vertices().end()-1;
	knots_.push_back(XMFLOAT4(end->position().x,end->position().y,end->position().z,0.0f));
	knots_.push_back(XMFLOAT4(end->position().x,end->position().y,end->position().z,0.0f));
	/*XMVECTOR hey =XMMatrixDeterminant(bSplineMatrix);
	knots_.push_back(XMFLOAT4(3.43f,8.87f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(-10.53f,-20.62f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(-39.83f,-29.79f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(-131.94f,-30.32f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(3.43f,8.87f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(-10.53f,-20.62f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(-39.83f,-29.79f,0.0f,0.0f));
	knots_.push_back(XMFLOAT4(-131.94f,-30.32f,0.0f,0.0f));*/

}

XMFLOAT4 BSpline::Interpolate(float generalTime)
{
    float innerCurves = static_cast<float>(InnerCurvesSize());
    int pointIndex = static_cast<int>(generalTime * innerCurves + 0.001f);
    float localTime = generalTime * innerCurves - static_cast<float>(pointIndex);
    return Interpolate(pointIndex, localTime);
}

XMFLOAT4 BSpline::Interpolate(int pointIndex, float localTime)
{
    XMMATRIX points = XMMATRIX(XMLoadFloat4(&knots_[pointIndex]), 
                               XMLoadFloat4(&knots_[pointIndex+1]),
                               XMLoadFloat4(&knots_[pointIndex+2]),
                               XMLoadFloat4(&knots_[pointIndex+3]));
    XMMATRIX prod = XMMatrixMultiply(bSplineMatrix, points);
    XMVECTOR timeVector = buildTimeVectorWithFactor(localTime);
    XMFLOAT4 sm;
    XMStoreFloat4(&sm, XMVector4Transform(timeVector, prod));
    return sm;
}

float BSpline::FindTimeFromPoint(XMVECTOR point)
{
    float approx = 0.5f;
    float step = 0.5f;
    XMVECTOR best = point - XMLoadFloat4(&knots_[1]);
    double bestNorm = best.x * best.x + best.y * best.y + best.z * best.z;
    int bestIndex = 1;

    int legnth = pointsSize_ + 1;
    for(int i = 2; i < legnth; ++i)
    {
        XMVECTOR prospect = point - XMLoadFloat4(&knots_[i]);
        double prospectNorm = prospect.x * prospect.x + prospect.y * prospect.y + prospect.z * prospect.z;
        if(prospectNorm < bestNorm)
        {
            bestNorm = prospectNorm;
            best = prospect;
            bestIndex = i;
        }
    }
    return static_cast<float>(bestIndex) / static_cast<float>(innerCurvesSize_);

	//XMVECTOR best = XMVectorSet(10000,10000,10000,0);//!\ Dangereux
	//float result = 0.0f;
 //   for(float i = 0.0000f; i< 1.0f;i+= precision)
	//{
	//	auto inter = Interpolate(i);
	//	XMVECTOR interVec;
	//	interVec.x = inter.x;
	//	interVec.y = inter.y;
	//	interVec.z = inter.z;
	//	XMVECTOR dist = point-interVec;
	//	if(best.x*best.x* + best.y*best.y* + best.z*best.z > dist.x*dist.x + dist.y*dist.y + dist.z*dist.z)
	//	{
	//		best = dist;
	//		result = i;
	//	}

	//}
 //   return result;
}