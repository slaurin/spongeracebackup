#pragma once

#include "Uncopyable.h"

namespace GE
{
	//! Device est le dispositif permettant le rendu graphique
	/*!
		Classe servant � construire un objet Dispositif 
		qui implantera les aspects "g�n�riques" du dispositif de 
		rendu
	*/
	class Device
		: Uncopyable
	{
	public:
		/*! Constantes pour mode d�finissant le mode d'affichage courant */
		enum WINDOW_MODE
		{
			 WINDOWED, //!< Mode Fenetr�
			 FULLSCREEN //!< Mode plein Ecran
		};

		void Resize(int width, int height);

		//! Constructeur
		/*!
			\param windowMode Mode d'affichage
			\param hWnd Handle sur la fen�tre Windows de l'application,	n�cessaire pour la fonction de cr�ation du dispositif
		*/
		Device(const WINDOW_MODE windowMode);
		
		/*! Destructeur */
		~Device();

		/*! Fonction permettant de rendre � l'�cran le buffer graphique en cours */
		void Present();

		/*! Active la transparence (M�lange Alpha) */
		void EnableAlphaBlend();

		/*! D�sactive la transparence (M�lange Alpha) */
		void DisableAlphaBlend();

		void SwitchRasterizer();

		// Fonction d'acc�s aux membres prot�g�s
		ID3D11Device *				GetDXDevice()			{return device_;}
		ID3D11DeviceContext *		GetDeviceContext()		{return deviceContext_;}
		IDXGISwapChain *			GetSwapChain()			{return swapChain_;}
		ID3D11RenderTargetView *	GetRenderTargetView()	{return renderTargetView_;}
		ID3D11DepthStencilView *	GetDepthStencilView()	{return depthStencilView_;}

		void SetRenderTargetView(ID3D11RenderTargetView* pRenderTargetView_in,ID3D11DepthStencilView* pDepthStencilView_in);

	private:
		/*! Initialise le buffer de profondeur */
		void createDepthBuffer();

		/*! Initialise les BlendStates (pour le m�lange Alpha)*/
		void initBlendStates();

		void createRenderTargetView();

		ID3D11Device *				device_;
		ID3D11DeviceContext *		deviceContext_;
		IDXGISwapChain *			swapChain_;
		ID3D11RenderTargetView *	renderTargetView_;
		UINT						numViewPorts_;
		DXGI_MODE_DESC				desc_;

		// Pour le tampon de profondeur
		ID3D11Texture2D *			depthTexture_;
		ID3D11DepthStencilView *	depthStencilView_;

		// Variables d'�tat
		bool wireFrame_;
		ID3D11RasterizerState *	solidCullBackRS_;
		ID3D11RasterizerState *	wireCullBackRS_;

		// Pour le m�lange alpha (transparence)
		ID3D11BlendState *	alphaBlendEnable_;
		ID3D11BlendState *	alphaBlendDisable_;
	};
}
