#pragma once
//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "GameTask.h"

//=============================================================================
// CLASS PhysicsTask
//=============================================================================
class PhysicsTask: public GameTask
{
public:
	PhysicsTask();
	~PhysicsTask();

public:
	virtual void init() override;
	virtual void cleanup() override;
	virtual void update() override;
private:
	class PhysicsTaskImp;
	PhysicsTaskImp *_imp;
};