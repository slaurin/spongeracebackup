#ifndef BUBBLE_H
#define BUBBLE_H

#include "StdAfx.h"


#include "Object3D.h"
#include "PhongMat.h"
#include "Effect.h"
#include "TangentMeshVertex.h"

namespace GE
{
class Bubble
	: public Object3D<MeshVertex, PhongMat>
{
	public:
		/*! Constructeur */
		Bubble(ID3D11Device*, Effect<MeshVertex> *effect, PhongMat *pMaterial, float dx = 1.f, float dy = 1.f, float dz = 1.f, unsigned int ui_VCount = 5000);

		void Draw();
		
		/*! Surchage de la m�thode d'animation */
		void Anime(float timeElapsed);

	private:
		/*! Liste d'index utilis�s pour dessiner le cube */
		ModelClass *m_SphereModel_;
		ID3D11Device* m_device_;
		static const unsigned int indexList_[];

};
}

#endif

