#pragma once
#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"

namespace GE
{
	class SimpleFogMat
		: public Material
	{
		struct ShadersParams // toujours un multiple de 16 pour les constantes
		{ 
			XMMATRIX matWorldViewProj;	// la matrice totale 
			XMMATRIX ViewIXf;  //matrice inverse de la veiwProj
			XMMATRIX WorldPos; // PositionWorld
			XMFLOAT4 FogColor; //couleur du fog
		};

		ID3D11Buffer *constantBuffer;

	public:
		SimpleFogMat(const Material *);

		~SimpleFogMat(void);

		template<class VertexT>
		void Apply(Effect<VertexT, SimpleFogMat> * effect, const Object3D<VertexT, SimpleFogMat> & object)
		{
			// Initialiser et s�lectionner les �constantes� de l'effet
			ShadersParams sp;
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			sp.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj );
			sp.ViewIXf = XMMatrixTranspose(XMMatrixInverse(&XMMatrixDeterminant(GAME->camera()->getViewMatrix()),GAME->camera()->getViewMatrix()));
			sp.WorldPos = XMMatrixTranspose(object.GetWorldMatrix());
			sp.FogColor = XMFLOAT4(255,255,255,1);

			// Activation de la texture ou non
			auto it = textures_.find(aiTextureType(aiTextureType_DIFFUSE));
			if(it != textures_.end())
			{
				ID3DX11EffectShaderResourceVariable* variableTexture;
				variableTexture = effect->GetVariableByName("thetexture")->AsShaderResource();
				variableTexture->SetResource(it->second->GetTexture());

				// On charge le sampler state associ�
				ID3DX11EffectSamplerVariable* variableSampler;
				variableSampler = effect->GetVariableByName("samplers")->AsSampler();
				variableSampler->SetSampler(0, it->second->GetSamplerState());
			}

			// On charge le buffer
			ID3DX11EffectConstantBuffer* pCB = effect->GetConstantBufferByName("GlobalParams");
			pCB->SetConstantBuffer(constantBuffer);

			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer, 0, NULL, &sp, 0, 0);

			effect->Apply(object);
		}
	};
}

