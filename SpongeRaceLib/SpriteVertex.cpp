#include "stdafx.h"
#include "SpriteVertex.h"

namespace GE
{
	D3D11_INPUT_ELEMENT_DESC SpriteVertex::Layout_[] =
	{
		{"POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},  
		{"TEXCOORD",	0,	DXGI_FORMAT_R32G32_FLOAT,		0,	12,	D3D11_INPUT_PER_VERTEX_DATA,	0}
	};

	D3D11_INPUT_ELEMENT_DESC * SpriteVertex::Begin()
	{return Layout_;}

	UINT SpriteVertex::Size()
	{return ARRAYSIZE(Layout_);}

	SpriteVertex::SpriteVertex(XMFLOAT3 position, XMFLOAT2 coordTex)
		: position_(position), coordTex_(coordTex)
	{}

	XMFLOAT3 SpriteVertex::GetPosition() const
	{
		return position_;
	}

	XMFLOAT2 SpriteVertex::GetTexCoords() const
	{
		return coordTex_;
	}

	SpriteVertex::SpriteVertex(const Vertex & vertex)
		: position_(vertex.position()), coordTex_(XMFLOAT2(0.f, 0.f))
	{
		if(!vertex.textureCoords().empty())
			coordTex_ = vertex.textureCoords().front();
	}
}