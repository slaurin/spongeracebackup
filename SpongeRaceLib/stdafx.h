// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

// Modifiez les d�finitions suivantes si vous devez cibler une plate-forme avant celles sp�cifi�es ci-dessous.
// Reportez-vous � MSDN pour obtenir les derni�res informations sur les valeurs correspondantes pour les diff�rentes plates-formes.
#ifndef WINVER				// Autorise l'utilisation des fonctionnalit�s sp�cifiques � Windows�XP ou version ult�rieure.
#define WINVER 0x0501		// Attribuez la valeur appropri�e � cet �l�ment pour cibler d'autres versions de Windows.
#endif

#ifndef _WIN32_WINNT		// Autorise l'utilisation des fonctionnalit�s sp�cifiques � Windows�XP ou version ult�rieure.                   
#define _WIN32_WINNT 0x0501	// Attribuez la valeur appropri�e � cet �l�ment pour cibler d'autres versions de Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Autorise l'utilisation des fonctionnalit�s sp�cifiques � Windows�98 ou version ult�rieure.
#define _WIN32_WINDOWS 0x0410 // Attribuez la valeur appropri�e � cet �l�ment pour cibler Windows�Me ou version ult�rieure.
#endif

#ifndef _WIN32_IE			// Autorise l'utilisation des fonctionnalit�s sp�cifiques � Internet Explorer�6.0 ou version ult�rieure.
#define _WIN32_IE 0x0600	// Attribuez la valeur appropri�e � cet �l�ment pour cibler d'autres versions d'Internet Explorer.
#endif

#define WIN32_LEAN_AND_MEAN		// Exclure les en-t�tes Windows rarement utilis�s

// Fichiers d'en-t�te Windows�:
#include <windows.h>
#include <mmsystem.h>
#include <WinUser.h>

// Fichiers d'en-t�te C RunTime
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <malloc.h>

// STL includes
#include <vector>
#include <hash_map>
#include <bitset>
#include <vector>
#include <iostream>
#include <iterator>
#include <memory>
#include <map>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <list>
#include <set>
#include <cassert>
#include <new>
#include <cstddef>

// Boost includes
#include <boost/thread.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/version.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/convenience.hpp>

// XNAMath includes
#define _XM_NO_INTRINSICS_
#include <xnamath.h>

// DirectX includes
#include <d3dx11core.h>
#include <dxerr.h>
#include <d3d9.h>
#include <D3DX11.h>
#include <d3dcompiler.h>
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11tex.h>
#include <d3dx11async.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

// PhysX includes
#include <PxPhysicsAPI.h>
#include <cooking/PxCooking.h>
#include <PxRigidActor.h> 
#include <PxFiltering.h> 
#include <PxShape.h> 
#include <PxRigidDynamic.h>
#include <PxSimulationEventCallback.h> 
#include <PxPhysics.h> 
#include <PxMaterial.h>
#include <PxPhysX.h>
#include <geometry/PxTriangleMesh.h>
#include <geometry/PxConvexMesh.h>
#include <cooking/PxTriangleMeshDesc.h>
#include <extensions/PxDefaultStreams.h>
#include <PxScene.h>

// Assimp includes
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include <assimp/types.h>
#include <assimp/mesh.h>
