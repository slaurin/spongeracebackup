#pragma once

#include "Singleton.h"
#include "helper-classes.h"



#include<xercesc/parsers/XercesDOMParser.hpp>
#include<xercesc/dom/DOMElement.hpp>
#include<xercesc/dom/DOMDocument.hpp>
#include<xercesc/dom/DOMNodeList.hpp>

/*


#include<xercesc/util/PlatformUtils.hpp>
#include<xercesc/dom/DOM.hpp>

#include<xercesc/dom/DOMDocumentType.hpp>

#include<xercesc/dom/DOMImplementation.hpp>
#include<xercesc/dom/DOMImplementationLS.hpp>
#include<xercesc/dom/DOMNodeIterator.hpp>
#include<xercesc/dom/DOMNodeList.hpp>
#include<xercesc/dom/DOMText.hpp>


#include<xercesc/util/XMLUni.hpp>
*/


enum {
	ERROR_ARGS = 1 ,
	ERROR_XERCES_INIT ,
	ERROR_PARSE ,
	ERROR_EMPTY_DOCUMENT
} ;

using namespace xercesc;

/************************************************************************/
/* Exceptions                                                                     */
/************************************************************************/
struct FileNotFoundException{};
struct UnmanagedPhysxTypeException{};
struct UnmanagedEffectTypeComboException{};
struct ReferenceNotFoundException{};
struct UnhandledTagException{};

struct UnknonwSceneException{};

struct UnimplementedTagException{};

class ElementFinder {


public:


	const DualString ATTR_NAME;
	const DualString ATTR_FILENAME;

	const DualString TAG_FLOAT3;
	const DualString ATTR_X;
	const DualString ATTR_Y;
	const DualString ATTR_Z;
	const DualString TAG_FLOAT4;
	const DualString ATTR_W;

	const DualString TAG_FLOAT_VALUE;
	const DualString ATTR_VALUE;
	const DualString TAG_INT_VALUE;
	const DualString TAG_STRING_VALUE;
	const DualString TAG_POSITION;
	const DualString TAG_DIRECTION;
	const DualString TAG_ANGLE_X;
	const DualString TAG_ANGLE_Y;
	const DualString TAG_ANGLE_Z;

	//CAMERA
	const DualString TAG_CAMERA;
	const DualString ATTR_PHYSXMESHTYPE;
	const DualString ATTR_ENTITYTYPE;
	const DualString ATTR_CAMERATYPE;

	//Effect
	const DualString TAG_EFFECT;
	const DualString ATTR_VERSION;
	const DualString ATTR_VERTEXTYPE;
	const DualString ATTR_MATERIALTYPE;

	//Lightning
	const DualString TAG_LIGHTNING;
	const DualString TAG_AMBIENT;
	const DualString TAG_DIFFUSE;
	const DualString TAG_SPECULAR;
	const DualString TAG_EMISSIVE;
	const DualString TAG_TRANSPARENT;

	//Material
	const DualString TAG_MATERIAL;
	const DualString TAG_TEXTURE_REF;
	const DualString TAG_PARAM;

	//TEXTURE
	const DualString TAG_TEXTURE;

	//Geometry
	const DualString TAG_GEOMETRY;
	const DualString ATTR_FLIPUV;
	const DualString ATTR_GENUV;
	const DualString ATTR_FLIPWINDINGORDER;
	const DualString ATTR_GENSMOOTHNORMAL;

	//Mesh
	const DualString TAG_MESH;
	const DualString ATTR_GEOMETRY_REF;
	const DualString ATTR_EFFECT_REF;
	const DualString ATTR_MATERIAL_REF;

	//PhysX Mesh
	const DualString TAG_PHYSXMESH;

	//Light
	const DualString TAG_LIGHT;
	const DualString TAG_DIRECTIONALLIGHT;
	const DualString TAG_POINTLIGHT;
	const DualString TAG_SPOTLIGHT;
	const DualString ATTR_ANGLE;
	const DualString ATTR_EXPONENT;

	//Entity
	const DualString TAG_ENTITY;
	const DualString ATTR_PHYSXMESH_REF;
	const DualString ATTR_ENTITYTYPEF;
	const DualString ATTR_MESH_REF;

	//SCENE
	const DualString TAG_SCENE;
	const DualString TAG_RACE_TRACK;
	const DualString TAG_PHYSX_MESH_LIST;
	const DualString TAG_GEOMETRY_LIST;
	const DualString TAG_TEXTURE_LIST;
	const DualString TAG_MATERIAL_LIST;
	const DualString TAG_MESH_LIST;
	const DualString TAG_EFFECT_LIST;
	const DualString TAG_DIRECTIONALLIGHT_LIST;
	const DualString TAG_POINTLIGHT_LIST;
	const DualString TAG_SPOTLIGHT_LIST;
	const DualString TAG_OBSTACLE_LIST;
	const DualString ATTR_ID;
	const DualString ATTR_PREVIOUS_SCENE;
	const DualString ATTR_NEXT_SCENE;

	//Game
	const DualString TAG_GAME;
	const DualString TAG_PLAYER;
	const DualString TAG_SCENE_LIST;
	const DualString ATTR_FIRSTSCENE;


	ElementFinder()
		:

	// we could have made these static members; but
	// DualString can't call XMLString::transcode() until
	// we've initialized Xerces...	
		ATTR_NAME(  "name" ),
		ATTR_FILENAME(  "filename" ),

		TAG_FLOAT3(  "float3" ),
		ATTR_X(  "x" ),
		ATTR_Y(  "y" ),
		ATTR_Z(  "z" ),
		TAG_FLOAT4(  "float4" ),
		ATTR_W(  "w" ),

		TAG_FLOAT_VALUE(  "float_namevalue" ),
		ATTR_VALUE(  "value" ),
		TAG_INT_VALUE(  "int_namevalue" ),
		TAG_STRING_VALUE(  "string_namevalue" ),
		TAG_POSITION(  "position" ),
		TAG_DIRECTION(  "direction" ),
		TAG_ANGLE_X(  "angleX" ),
		TAG_ANGLE_Y(  "angleY" ),
		TAG_ANGLE_Z(  "angleZ" ),

		//CAMERA
		TAG_CAMERA(  "camera" ),
		ATTR_PHYSXMESHTYPE(  "physxMeshType" ),
		ATTR_ENTITYTYPE(  "entityType" ),
		ATTR_CAMERATYPE(  "cameraType" ),

		//Effect
		TAG_EFFECT(  "effect" ),
		ATTR_VERSION(  "version" ),
		ATTR_VERTEXTYPE(  "vertexType" ),
		ATTR_MATERIALTYPE(  "materialType" ),

		//Lightning
		TAG_LIGHTNING(  "lighting" ),
		TAG_AMBIENT(  "ambient" ),
		TAG_DIFFUSE(  "diffuse" ),
		TAG_SPECULAR(  "specular" ),
		TAG_EMISSIVE(  "emissive" ),
		TAG_TRANSPARENT(  "transparent" ),

		//Material
		TAG_MATERIAL(  "material" ),
		TAG_PARAM(  "param" ),
		TAG_TEXTURE_REF(  "textureRef" ),

		//TEXTURE
		TAG_TEXTURE(  "texture" ),

		//Geometry
		TAG_GEOMETRY(  "geometry" ),
		ATTR_FLIPUV(  "flipUV" ),
		ATTR_GENUV(  "genUV" ),
		ATTR_FLIPWINDINGORDER(  "flipWindingOrder" ),
		ATTR_GENSMOOTHNORMAL(  "genSmoothNormal" ),

		//Mesh
		TAG_MESH(  "mesh" ),
		ATTR_GEOMETRY_REF(  "geometryRef" ),
		ATTR_EFFECT_REF(  "effectRef" ),
		ATTR_MATERIAL_REF(  "materialRef" ),

		//PhysX Mesh
		TAG_PHYSXMESH(  "physxMesh" ),

		//Light
		TAG_LIGHT(  "light" ),
		TAG_DIRECTIONALLIGHT(  "DirectionalLight" ),
		TAG_POINTLIGHT(  "PointLight" ),
		TAG_SPOTLIGHT(  "SpotLight" ),
		ATTR_ANGLE(  "angle" ),
		ATTR_EXPONENT(  "exponent" ),

		//Entity
		TAG_ENTITY(  "entity" ),
		ATTR_PHYSXMESH_REF(  "physxMeshRef" ),
		ATTR_ENTITYTYPEF(  "entityType" ),
		ATTR_MESH_REF(  "meshRef" ),

		//SCENE
		TAG_SCENE(  "scene" ),
		TAG_RACE_TRACK(  "raceTrack" ),
		TAG_PHYSX_MESH_LIST(  "physxMeshes" ),
		TAG_GEOMETRY_LIST(  "geometries" ),
		TAG_TEXTURE_LIST(  "textures" ),
		TAG_MATERIAL_LIST(  "materials" ),
		TAG_MESH_LIST(  "meshes" ),
		TAG_EFFECT_LIST(  "effects" ),
		TAG_DIRECTIONALLIGHT_LIST(  "DirectionalLights" ),
		TAG_POINTLIGHT_LIST(  "PointLights" ),
		TAG_SPOTLIGHT_LIST(  "SpotLights" ),
		TAG_OBSTACLE_LIST(  "obstacles" ),
		ATTR_ID(  "id" ),
		ATTR_PREVIOUS_SCENE("previousScene"),
		ATTR_NEXT_SCENE("nextScene"),

		//Game
		TAG_GAME(  "game" ),
		TAG_PLAYER(  "player" ),
		TAG_SCENE_LIST(  "scenes" ),
		ATTR_FIRSTSCENE(  "firstSceneId" ),
		xmlDoc_( NULL )
	{
		return ;
	}


	xercesc::DOMElement* getGameElement(){

		// We could also have called xml->getFirstChild() and worked
		// with DOMNode objects (DOMDocument is also a DOMNode); but
		// DOMNode only lets us get at the tree using other abstract
		// DOMNodes.  In turn, that would require us to walk the tree
		// and query each node for its name before we do anything with
		// the data.

		// <config/> element
		xercesc::DOMElement* result = xmlDoc_->getDocumentElement() ;

		return( result ) ;	
	}

	xercesc::DOMElement* getScenesElement(){

		return( getElement( TAG_SCENE_LIST.asXMLString() ) ) ;
	}

	xercesc::DOMElement* getSceneElement(std::string id){
		

		xercesc::DOMElement* result = NULL ;

		xercesc::DOMNodeList* list = xmlDoc_->getElementsByTagName( TAG_SCENE_LIST.asXMLString() ) ;		

		const XMLSize_t count = list->getLength() ;
		
		StringManager sm ;

		for( XMLSize_t index = 0 ; index < count ; ++index ){

			xercesc::DOMNode* node = list->item( index ) ;

			xercesc::DOMElement* element = dynamic_cast< xercesc::DOMElement* >( node ) ;
			
			std::string elementId =  sm.convert(element->getAttribute( ATTR_ID.asXMLString() ));
			
			if (elementId == id) return element;
			
		}

		return nullptr;
	}


	xercesc::DOMElement* getElement( xercesc::DOMElement* element, const XMLCh* name ){

		xercesc::DOMElement* result = nullptr ;

		xercesc::DOMNodeList* list = element->getElementsByTagName( name ) ;

		if (list->getLength()==0) return nullptr;

		xercesc::DOMNode* node = list->item( 0 ) ;

		if( xercesc::DOMNode::ELEMENT_NODE == node->getNodeType() ){
			result = dynamic_cast< xercesc::DOMElement* >( node ) ;
		}

		return( result ) ;

	}


	xercesc::DOMElement* getElementByName( const XMLCh* type, std::string name ){

		xercesc::DOMElement* result = NULL ;

		xercesc::DOMNodeList* list = xmlDoc_->getElementsByTagName( type ) ;

		const XMLSize_t count = list->getLength() ;

		StringManager sm ;

		for( XMLSize_t index = 0 ; index < count ; ++index ){

			xercesc::DOMNode* node = list->item( index ) ;

			xercesc::DOMElement* element = dynamic_cast< xercesc::DOMElement* >( node ) ;

			std::string elementId =  sm.convert(element->getAttribute( ATTR_NAME.asXMLString() ));

			if (elementId ==name) return element;

		}

		return( result ) ;

	}

	xercesc::DOMElement* getElement( const XMLCh* name ){

		xercesc::DOMElement* result = NULL ;

		xercesc::DOMNodeList* list = xmlDoc_->getElementsByTagName( name ) ;

		if (list->getLength()==0) return nullptr;

		xercesc::DOMNode* node = list->item( 0 ) ;

		if( xercesc::DOMNode::ELEMENT_NODE == node->getNodeType() ){
			result = dynamic_cast< xercesc::DOMElement* >( node ) ;
		}

		return( result ) ;

	}

	void setDocument( xercesc::DOMDocument* const doc ){
		xmlDoc_ = doc ;
	}


protected:
	// empty


private:
	xercesc::DOMDocument* xmlDoc_ ;

} ; // class ElementFinder



// - - - - - - - - - - - - - - - - - - - -

class XMLAssetManager
{

private:

	StringManager sm ;	
	xercesc::XercesDOMParser parser_ ;
	ElementFinder finder_ ;

	void handleElement( xercesc::DOMElement* element,  bool load);

	bool fileExist(std::string filePath)
	{ return !!(std::ifstream(filePath)); }

	static const std::string getDefaultFileName()
	{
		return "spongerace_assets.xml";
	};

	void handleElementList(xercesc::DOMElement* element, const XMLCh* name,  bool load );

	XMFLOAT4 elemToXMFLAOT4(xercesc::DOMElement* element);
	XMFLOAT3 elemToXMFLAOT3(xercesc::DOMElement* element);

	physx::PxVec4 elemToPxVec4(xercesc::DOMElement* element);
	physx::PxVec3 elemToPxVec3(xercesc::DOMElement* element);

	std::string getVertexType(xercesc::DOMElement* element);
	std::string getMaterialType(xercesc::DOMElement* element);



public:

	XMLAssetManager()
		:
			parser_() ,
			finder_()
	{
	} // ctor

	~XMLAssetManager() throw(){

		// notice, the calls to XMLString::release are gone --
		// that's handled in each DualString's destructor

	} // dtor


	void load(const std::string &filePath);

	void loadGameAssets() ;

	void unloadGameAssets();

	void loadSceneAssets(std::string id);

	std::string getGameFirstSceneId();
	std::string getNextSceneId(std::string currentScene);
	std::string getPreviousSceneId(std::string currentScene);

	void unloadSceneAssets(std::string id);

} ; // class XMLAssetManager
