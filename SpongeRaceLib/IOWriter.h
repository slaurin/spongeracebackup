/*
 *  Author : Stephane Laurin
 *  Verifier : 
 */

#ifndef IO_WRITER_H
#define IO_WRITER_H

#include "Uncopyable.h"

/* Writes a vector of data to a file */
class IOWriter : Uncopyable
{
public:
    /* Wrtie a binary file. Do no use this method to write structs into files */
    static void write(const std::string &fileName, const std::vector<unsigned char> &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostreambuf_iterator<char>(std::ofstream(fileName, std::ios::binary)));
    }

    /* Wrtie at the end of a binary file. Do no use this method to write structs into files */
    static void writeAtTheEnd(const std::string &fileName, const std::vector<unsigned char> &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostreambuf_iterator<char>(std::ofstream(fileName, std::ios::binary, std::ios::app)));
    }

    /* Wrtie a binary file. Do no use this method to write structs into files */
    static void write(const std::string &fileName, const std::vector<char> &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostreambuf_iterator<char>(std::ofstream(fileName, std::ios::binary)));
    }

    /* Wrtie at the end of a binary file. Do no use this method to write structs into files */
    static void writeAtTheEnd(const std::string &fileName, const std::vector<char> &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostreambuf_iterator<char>(std::ofstream(fileName, std::ios::binary, std::ios::app)));
    }

    /* Wrtie a text file. Do no use this method to write structs into files */
    static void write(const std::string &fileName, const std::string &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostreambuf_iterator<char>(std::ofstream(fileName)));
    }

    /* Wrtie at the end of a text file. Do no use this method to write structs into files */
    static void writeAtTheEnd(const std::string &fileName, const std::string &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostreambuf_iterator<char>(std::ofstream(fileName, std::ios::app)));
    }

    /* Writes a structure to a file. The << operator must be implemented in the struct class */
    template<class TData>
    static void write(const std::string &fileName, const std::vector<TData> &data)
    {
        std::copy(std::begin(data),
                  std::end(data),
                  std::ostream_iterator<TData>(std::ofstream(fileName)));
    }
};
#endif