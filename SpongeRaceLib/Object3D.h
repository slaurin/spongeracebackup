#pragma once

#include "Convert.h"
#include "GraphicObject.h"
#include "Effect.h"
#include "GeometryPart.h"
#include "Resource.h"
#include "DxUtilities.h"
#include "GraphicEngine.h"

namespace GE
{
	/*! Toute classe h�ritant de Object3D pourra �tre rendue par le GraphicEngine (en impl�mentant les m�thodes abstraites) */
	template<class VertexT, class MaterialT>
	class Object3D
		: public GraphicObject
	{
        friend class ParticleInstancer;

	public:
		/*! Type des vertices */
		typedef VertexT vertex_t;

		/*! Type des vertices */
		typedef MaterialT material_t;

		/*! Constructeur */
		Object3D(
			Effect<vertex_t, material_t> * effect,
			GeometryPart * part,
			const XMMATRIX & matWorld = XMMatrixIdentity())
			: 
		name_(part->name()),
		GraphicObject(matWorld), 
		effect_(effect), 
		material_(new material_t(part->material())), 
		indexes_(part->indexes()),
		vertexBuffer_(nullptr),
		indexBuffer_(nullptr),
		nbIndices_(0)
		{
			assert(effect);
			for(auto it = part->vertices().begin(); it != part->vertices().end(); ++it)
				vertices_.push_back(vertex_t(*it));

			CreateVertexBuffer();
			CreateIndexBuffer();
		}

		/*! Constructeur par copie */
		Object3D(const Object3D & object)
		{
			effect_ = object.effect_;
            // Does not compile ???
			//matWorld_ = XMMATRIX(object.matWorld_);
			vertices_ = std::vector<vertex_t>(object.vertices_);
			indexes_ = std::vector<index_t>(object.indexes_);
			CreateVertexBuffer();
			CreateIndexBuffer();
		}

		//! Clone l'objet en r�cup�rant les pointeurs
		Object3D * Clone()
		{
			auto cloneObj = new Object3D<vertex_t, material_t>(effect_, material_, name_);
			vertexBuffer_->AddRef();
			cloneObj->vertexBuffer_ = vertexBuffer_;
			indexBuffer_->AddRef();
			cloneObj->indexBuffer_ = indexBuffer_;
			cloneObj->nbIndices_ = nbIndices_;

			return cloneObj;
		}

		/*! Destructeur surchargeable (ne fait rien) */
		virtual ~Object3D()
		{
			DxUtilities::DXRelacher(indexBuffer_);
			DxUtilities::DXRelacher(vertexBuffer_);
		}

		/*! M�thode virtuelle permettant de calculer les animations en fonction du temps �coul� depuis la derni�re boucle du GraphicEngine (ne fait rien) */
		virtual void Anime(float timeElapsed)
		{}

		/*! M�thode virtuelle appel�e � chaque boucle du GraphicEngine pour afficher l'objet (Affichage de base) */
		virtual void Draw()
		{
			ID3D11DeviceContext * context = GraphicEngine::GetInstance().GetDevice()->GetDeviceContext();

			// Choisir la topologie des primitives
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			// Index buffer
			context->IASetIndexBuffer(indexBuffer_, DXGI_FORMAT_R32_UINT, 0);

			// Vertex buffer
			UINT stride = sizeof(vertex_t);
			UINT offset = 0;
			context->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);

			material_->Apply(effect_, *this);
		}

		/*! Lance l'execution du pipeline une fois celui-ci pr�par� */
		virtual void SendToPipeline() const
		{
			// On envoie notre g�om�trie dans le pipeline !
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->DrawIndexed(nbIndices_, 0, 0);
		}

	protected:
		/*! Constructeur */
		Object3D(
			Effect<vertex_t, material_t> * effect,
			material_t * material,
			const std::string & name = "UnNamed",
			const XMMATRIX & matWorld = XMMatrixIdentity())
			: 
		GraphicObject(matWorld), 
			effect_(effect), 
			material_(material), 
			vertices_(std::vector<vertex_t>()), 
			indexes_(std::vector<index_t>()), 
			vertexBuffer_(nullptr), 
			indexBuffer_(nullptr), 
			name_(name),
			nbIndices_(0)
		{
			assert(effect);
		}

		/*! Cr�ation du vertex buffer et copie des sommets */
		void CreateVertexBuffer()
		{
			ID3D11Device* pD3DDevice = GraphicEngine::GetInstance().GetDevice()->GetDXDevice();

			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));

			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = vertices_.size() * sizeof(vertices_.front());
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = 0;

			D3D11_SUBRESOURCE_DATA InitData;
			ZeroMemory(&InitData, sizeof(InitData));
			InitData.pSysMem = &vertices_[0];

			DxUtilities::DXEssayer(
				pD3DDevice->CreateBuffer(
					&bd,
					&InitData,
					&vertexBuffer_),
				DXE_CREATIONVERTEXBUFFER);
			DxUtilities::DXSetDebugObjectName(vertexBuffer_, name_);
		}

		/*! Cr�ation de l'index buffer et copie des indices */
		void CreateIndexBuffer()
		{
			ID3D11Device* pD3DDevice = GraphicEngine::GetInstance().GetDevice()->GetDXDevice();

			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));

			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = indexes_.size() * sizeof(indexes_.front());
			bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			bd.CPUAccessFlags = 0;

			D3D11_SUBRESOURCE_DATA InitData;
			ZeroMemory(&InitData, sizeof(InitData));
			InitData.pSysMem = &indexes_[0];
			indexBuffer_ = NULL;

			DxUtilities::DXEssayer(
				pD3DDevice->CreateBuffer(
					&bd,
					&InitData,
					&indexBuffer_),
				DXE_CREATIONINDEXBUFFER);
			DxUtilities::DXSetDebugObjectName(indexBuffer_, name_);

			nbIndices_ = indexes_.size();
		}

		ID3D11Buffer *					vertexBuffer_;
		ID3D11Buffer *					indexBuffer_;
		std::vector<index_t>			indexes_;
		std::vector<vertex_t>			vertices_;
		Effect<vertex_t, material_t> *	effect_;
		material_t *					material_;
		std::string						name_;
		size_t							nbIndices_;
	};
}
