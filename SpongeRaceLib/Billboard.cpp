#include "stdafx.h"
#include "Billboard.h"

namespace GE
{
    MeshVertex Billboard::billboardVertices_[] = {
        MeshVertex(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 1.0f)),
        MeshVertex(XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.0f)),
        MeshVertex(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 0.0f)),
        MeshVertex(XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 1.0f))
    };

    Billboard::index_t Billboard::billboardIndices_[] = {
        0, 1, 2,
        0, 2, 3
    };

    Billboard::Billboard(
        Effect<MeshVertex, PhongMat> *effect, 
        PhongMat *pMaterial,
        XMFLOAT3 scale)
        : Object3D<MeshVertex, PhongMat>(effect, pMaterial),
          scale_(scale)
    {
        vertices_ = std::vector<MeshVertex>(std::begin(billboardVertices_), std::end(billboardVertices_));
        CreateVertexBuffer();
        indexes_ = std::vector<Billboard::index_t>(std::begin(billboardIndices_), std::end(billboardIndices_));
        CreateIndexBuffer();
    }

    void Billboard::Anime(float timeElapsed)
    {
        XMMATRIX scale = XMMatrixScaling(scale_.x, scale_.y, scale_.z);

        XMVECTOR camPos = GAME->camera()->getPosition();
        XMVECTOR pos = XMVectorSet(GetWorldMatrix()._14, GetWorldMatrix()._24, GetWorldMatrix()._34, 1.0f);
        float angle = atan2 (pos.x - camPos.x, pos.z - camPos.z);
        
        XMMATRIX rotation = XMMatrixRotationY(angle);

        SetWorldMatrix(XMMatrixMultiply(scale, rotation));
    }
}