#pragma once

#include "IRessourceManager.h"

class LightManager : public IRessourceManager<LightManager, std::string, GE::Light*>
{

	friend class Singleton<LightManager>;
public:

	GE::DirectionalLight * RequestDirectionalLight(const std::string &resourceName, const XMFLOAT4 & direction, XMFLOAT4 diffuse, XMFLOAT4 specular)
	{
		if(!Contains(resourceName))
			add(resourceName, new DirectionalLight(direction, diffuse,  specular));
		return operator[](resourceName);
	}


	GE::Light * Request(const std::string &resourceName, XMFLOAT4 diffuse, XMFLOAT4 specular, bool enable)
	{
		if(!Contains(resourceName))
			add(resourceName, new Light(diffuse, specular,  enable));
		return operator[](resourceName);
	}

	GE::Light * Request(const std::string &resourceName, XMFLOAT4 diffuse, XMFLOAT4 specular, bool enable)
	{
		if(!Contains(resourceName))
			add(resourceName, new Light(diffuse, specular,  enable));
		return operator[](resourceName);
	}
};