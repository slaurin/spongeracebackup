#include "stdafx.h"
#include "DrawableBSpline.h"
#include "DxUtilities.h"
#include "resource.h"
#include "GraphicEngine.h"

namespace GE
{
    DrawableBSpline::DrawableBSpline(Effect<SimpleVertex4, ColorMat>* effect,
                                     ColorMat* pMaterial,
			                         const BSpline &spline,
                                     int intervals)
        : Object3D<SimpleVertex4, ColorMat>(effect, pMaterial),
          spline_(spline),
          knotsInterpolated_(std::vector<SimpleVertex4>()),
          intervals_(intervals)
	{
        float step = 1.0f/static_cast<float>(intervals_);

        for(int i = 0; i < spline_.InnerCurvesSize(); ++i)
        {
            for(int j = 0; j < intervals_; ++j)
            {
                knotsInterpolated_.push_back(SimpleVertex4(
                    spline_.Interpolate(i, step * static_cast<float>(j))
                    ));
            }
        }

		Init();
	}

	void DrawableBSpline::Init()
	{	
		ID3D11Device* pD3DDevice = GraphicEngine::GetInstance().GetDevice()->GetDXDevice();

		D3D11_BUFFER_DESC bd;
		
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
        bd.ByteWidth = sizeof( SimpleVertex4 ) * knotsInterpolated_.size(); 
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
        InitData.pSysMem = &(*knotsInterpolated_.cbegin());

		DxUtilities::DXEssayer(
			pD3DDevice->CreateBuffer(
			&bd,
			&InitData,
			&vertexBuffer_),
			DXE_CREATIONVERTEXBUFFER);
	}

	void DrawableBSpline::Draw()
	{
		ID3D11DeviceContext * g_pImmediateContext = GraphicEngine::GetInstance().GetDevice()->GetDeviceContext();

		// Vertex buffer
		UINT stride = sizeof(SimpleVertex4);
		UINT offset = 0;

		// pr�pare le shader fx (configuration du pipeline graphique puis appelle SendToPipeline par d�faut, ici c'est pas ce qu'on veut)
		material_->Apply(effect_, *this);

		// config perso
		g_pImmediateContext->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);
        g_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP );
		
		// draw
        g_pImmediateContext->Draw(knotsInterpolated_.size(), 0);

		/*g_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
		g_pImmediateContext->Draw(8, 6);*/

		// Ne surtout pas appeler la config du pipeline apr�s vouloir dessiner!
		//material_->Apply(effect_, *this);
	}

	void DrawableBSpline::Anime(float timeElapsed)
	{		
		
	}
	XMFLOAT4  DrawableBSpline::getPositionByInterpolate(float val)
	{
		return spline_.Interpolate(val);
	}


    XMFLOAT4 DrawableBSpline::FindTimeFromPoint(XMVECTOR point,float offset)
    {
        float bestTime = spline_.FindTimeFromPoint(point);
		bestTime = max(0,(bestTime+offset));
		bestTime = min(bestTime,1.0f);
        return spline_.Interpolate(bestTime);
    }
}