#include "stdafx.h"
#include "Window.h"
#include "GraphicEngine.h"

using namespace GE;

HINSTANCE Window::appInstance_;
HWND Window::mainWindow_;

Window::Window()
	: screenWidth_(1024), screenHeight_(768)
{
}

bool Window::init(HINSTANCE hInstance)
{
	appInstance_ = hInstance;// Stocke le handle d'instance de l'application, plusieurs fonctions sp�cifiques en auront besoin
	TCHAR szTitle[MAX_LOADSTRING];					// Le texte de la barre de titre

	// Initialise les cha�nes globales
	LoadString(appInstance_, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(appInstance_, IDC_SPONGERACE, windowName_, MAX_LOADSTRING);
	registerClass(appInstance_);

	mainWindow_ = CreateWindow(
		windowName_, 
		szTitle, 
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 
		0, 
		CW_USEDEFAULT, 
		0, 
		NULL, 
		NULL, 
		appInstance_, 
		NULL);

	if(!mainWindow_)
		return false;

	accelTable_ = LoadAccelerators(appInstance_, MAKEINTRESOURCE(IDC_SPONGERACE));

	show();

	return true;
}

void Window::SetScreenDimensions(int width, int height)
{
	screenWidth_ = width;
	screenHeight_ = height;
}

ATOM Window::registerClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= &windowProcess;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SPONGERACE));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SPONGERACE);
	wcex.lpszClassName	= windowName_;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

LRESULT CALLBACK Window::windowProcess(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message) 
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Analyse les s�lections de menu�:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(appInstance_, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)about);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// �vitez d'ajouter du code ici...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_SIZE:
		resizeWindow(lParam);				
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

int Window::show()
{
	ShowWindow(mainWindow_, SW_SHOWNORMAL);
	UpdateWindow(mainWindow_);

	return 0;
}


int Window::resizeWindow(LPARAM lParam)
{
	GraphicEngine::GetInstance().ResizeDevice(LOWORD(lParam), HIWORD(lParam));

	return 1;
}

INT_PTR CALLBACK Window::about(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

Window::~Window(void)
{
}