#pragma once

#include "Vertex.h"
#include "Material.h"

namespace GE
{
	class GeometryPart
	{
		//! Exception lev�e si le aiMesh fournis est invalide
		class GivenMeshIsInvalid {};

	public:
		//! Type des indexes
		typedef unsigned int index_t;
		typedef Vertex vertex_t;
	private:
		//! Nom du Mesh
		std::string name_;

		//! Material
		Material * material_;

		//! Vertices du Mesh
		std::vector<Vertex> vertices_;

		//! Indexes du Mesh
		std::vector<index_t> indexes_;

		void GeometryPart::loadIndexes(const aiMesh * mesh);
			void GeometryPart::loadVertices(const aiMesh * mesh);

	public:
		GeometryPart(const std::string & name, Material * mat, const aiMesh * mesh);

		std::string& name() throw();

		std::vector<Vertex>& vertices() throw();

		std::vector<index_t>& indexes() throw();

		std::vector<Vertex>& faces() throw();

		Material* material();
	};
}


