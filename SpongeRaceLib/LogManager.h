#ifndef LOG_MANAGER_H
#define LOG_MANAGER_H

#include "IOReader.h"
#include "IOWriter.h"

class LogManager
{
private:
    std::string fileName_;
    std::stringstream stream_;
public:
    LogManager(const std::string fileName)
        : fileName_(fileName),
          stream_(std::stringstream())
    {
       
    }

    /* Clears both the inner stream and the file. */
    void emptyLog()
    {
        stream_.clear();
        stream_.str("");
        IOWriter::write(fileName_, "");
    }

    /* 
     *  Reads the content of the stream. It corresponds to everything that has been 
     *  logged since the beginning of the process.
     */
    std::string readStream()
    {
        return stream_.str();
    }

    /* This method reads the log and returns its content. Expansive to use.*/
    std::string readFile()
    {
        std::stringstream s;
        IOReader<std::stringstream> reader(fileName_, s);
        return s.str();
    }

    /* Write the message in the stream and at the end of the log. */
    void write(const std::string &message)
    {
        stream_ << message;
        IOWriter::writeAtTheEnd(fileName_, message);
    }
};

#endif