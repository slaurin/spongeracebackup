#include "stdafx.h"

#include "Window.h"

/************************************************************************/
/* SpongeRace                                                                     */
/************************************************************************/
#include "SpongeRace.h"

/************************************************************************/
/* Utils                                                                     */
/************************************************************************/
#include "Singleton.h"
#include "PhysXConverter.h"
#include "GenerateurNombresAleatoires.h"

/************************************************************************/
/* Include Tasks                                                       */
/************************************************************************/
#include "GameTask.h"
#include "InputTask.h"
#include "CameraTask.h"
#include "AITask.h"
#include "PhysicsTask.h"
#include "GameRulesTask.h"
#include "LoadingTask.h"
#include "RenderTask.h"
#include "PlayerTask.h"
#include "HUDTask.h"

/************************************************************************/
/* Include Actors Controllers                                            */
/************************************************************************/
#include "PlayerActor.h"
#include "BubbleActor.h"
#include "CameraActor.h"
#include "TriggerActor.h"
#include "ObstacleActor.h"
#include "RaceTrack.h"
#include "UserController.h"

/************************************************************************/
/* Include Managers                                                     */
/************************************************************************/
#include "ConfigManager.h"
#include "LoopTime.h"

/************************************************************************/
/* Graphic Engine                                                       */
/************************************************************************/
#include "GraphicEngine.h"
#include "Sprite.h"


#include "LoopTime.h"
#include "IEffect.h"
#include "Effect.h"
#include "EffectManager.h"
#include "GeometryManager.h"
#include "Instancer.h"
#include "MeshVertex.h"
#include "PNVertex.h"
#include "PhongMat.h"
#include "MiniPhongMat.h"
//#include "SimpleFogMat.h"
#include "Axes.h"
#include "ColorMat.h"
#include "BSpline.h"
#include "DrawableBSpline.h"
#include "SimpleVertex4.h"
#include "TangentMeshVertex.h"
#include "TransparentMat.h"
#include "BubbleMat.h"
#include "SpriteVertex.h"
#include "Pannel2DMat.h"
#include "SimpleMat.h"
#include "FlagWaveMat.h"

#include "Splines.h"

#include "ParticleInstancer.h"
#include "ParticleMat.h"
#include "ParticleVertex.h"
#include "TextureManager.h"
#include "Quad.h"

#include <GdiPlus.h>

SpongeRace * GAME;

using namespace GE;
using namespace GameUI;
using namespace Gdiplus;
namespace fs = boost::filesystem;
using namespace std;

SpongeRace::SpongeRace(HINSTANCE hInstance)
	: _shouldExit(false), scene_(nullptr), numThread_(0), gameover_(false), physics_(nullptr), cooking_(nullptr), gamepaused_(false), camera_(nullptr), player_(nullptr), currentScene_(), menu_(nullptr), gameStarted_(false)
{


	numThread_ = boost::thread::hardware_concurrency();

	// Initialise la fenetre
	Window::GetInstance().init(hInstance);


}

void SpongeRace::Init()
{
	//Initialise Configs
	ConfigManager::GetInstance().load(ConfigManager::GetInstance());

	fs::path full_path( fs::initial_path<fs::path>() );
	//assetManager_.load(full_path.string() + "\\resources\\spongerace_assets.xml");

	// Initialise le GE
	GraphicEngine::GetInstance().Initialisations();

	//GAME->onStartGameSession();
	// init the tasks
	tasks_.push_back(new LoadingTask);
	tasks_.push_back(new AITask);
	tasks_.push_back(new PhysicsTask);
	tasks_.push_back(new InputTask);
	tasks_.push_back(new PlayerTask);	
	tasks_.push_back(new CameraTask);
	//tasks_.push_back(new GameRulesTask);	
	tasks_.push_back(new RenderTask);
	hudTask_ = new HUDTask;
	tasks_.push_back(hudTask_);


	for(auto iter = tasks_.begin(); iter != tasks_.end(); ++iter)
	{
		(*iter)->init();
	}

	// load a level
	loadGameAssets();
	loadLevel("0");//assetManager_.getGameFirstSceneId());
}

SpongeRace::~SpongeRace()
{
	cleanup();
	//xercesc::XMLPlatformUtils::Terminate();
}

void SpongeRace::EndOfRace(time_t raceTime)
{
	int height = Window::GetInstance().GetScreenHeight();
	int width = Window::GetInstance().GetScreenWidth();

	TextDrawer * result_ = new TextDrawer(width, height, new Font(new FontFamily(L"Bauhaus 93", nullptr), 70.f, FontStyleBold, UnitPixel), Color(255, 241, 194, 50), Color(0, 0, 0, 0));
	
	wstringstream wss;
	wss << setw(2) << setfill(L'0') <<  raceTime / 60 << "\"" << setw(2) << setfill(L'0') << raceTime % 60;
	wstring timer = wss.str();
	result_->Write(timer, PointF(((width - 215)/ 2.f) , ((height - 52) / 2.f)), new Image(L"..\\SpongeRace\\Resources\\Textures\\UI\\Resultat.png"));

	menu_->GetCurrentPage()->SetNavigation(NEXT, new MenuPage(result_->GetTexture()));
	menu_->Naviguate(NEXT);
	menu_->Show();

	gameover_ = true;
}

void SpongeRace::loadGameAssets()
{
	//assetManager_.loadGameAssets();
}

void SpongeRace::loadNextLevel()
{
	//string nextId = assetManager_.getNextSceneId(currentScene_);
	//if (nextId.empty()) return;
	//loadLevel(nextId);
}

void SpongeRace::loadPreviousevel()
{
	//string prevId = assetManager_.getPreviousSceneId(currentScene_);
	//if (prevId.empty()) return;
	//loadLevel(prevId);
}

void SpongeRace::unloadCurrentLevel()
{
	//unloadLevel(currentScene_);
}

void SpongeRace::loadLevel(std::string id)
{
	auto phong = EffectManager::GetInstance().getEffect<MeshVertex, PhongMat>("Phong");
	auto bubbleEffect = EffectManager::GetInstance().getEffect<TangentMeshVertex, BubbleMat>("BubbleFx");

	// Graphique Bulle
	auto bulleGeo = GeometryManager::GetInstance().Request("Bulle", "..\\SpongeRace\\Resources\\Models\\Bulle.obj", true, false, false, true);
	bulleGeo->parts().front().material()->InsertTexture(aiTextureType_DIFFUSE, new Texture(L"..\\SpongeRace\\Resources\\Textures\\rainbowfilm_smooth.bmp"));
	bulleGeo->parts().front().material()->InsertTexture(aiTextureType_EMISSIVE, new Texture(GraphicEngine::GetInstance().GetPostEffectShaderResourceView()));
	mainBulle = Instancer::Create(bubbleEffect, &(bulleGeo->parts().front()));
	
	// Affichage du menu (page de loading seulement pour l'instant)
	// Doit se faire apr�s le chargement de l'effet sprite
	auto root = new MenuPage("loading.png");
	auto newGame = new MenuPage("newGame.png");
	root->SetNavigation(NEXT, newGame);
	newGame->SetNavigation(NEXT, new MenuPage("Pause.png"));
	menu_ = new GameMenu(root);
	menu_->Show();// On indique que l'on veut afficher le menu
	
	//--------- Hack -----------\\
	// Force l'affichage du loading (car le graphique engine est pas updat�)
	tasks_[6]->update();// On attends la fin du rendu pour changer de scene (menu)
	tasks_[6]->update();// On affiche la nouvelle scene (menu)

	// La suite du code fonctionne sur la sc�ne par d�faut (AddObjectToScene etc) mais rien n'est rendu !
	// C'est un hack temporaire car il est tard et que j'aime bien le r�sultat :) Bonne nuit
	

	camera_ = GAME->spawnManager()->spawn<CameraActor>(PxTransform(PxVec3(0, 100, 0))); 

	// Graphique Eponge
	auto epongeGeo = GeometryManager::GetInstance().Request("Eponge", "..\\SpongeRace\\Resources\\Models\\Eponge\\eponge.obj", true, false, false, true);
	auto eponge = Instancer::Create(phong, &(epongeGeo->parts().front()));

	// Physique Player
	auto epongeGeoPhysx = GeometryManager::GetInstance().Request("EpongePhysX", "..\\SpongeRace\\Resources\\Models\\Eponge\\spongeBox_PHY_sphere.obj", true, false, false, true);
	physx::PxConvexMesh* physxPlayerMesh = GenerateConvexFromGeometry(GAME->scene()->getPhysics(), &(epongeGeoPhysx->parts().front()));
	player_ = GAME->spawnManager()->spawn<PlayerActor>(PxTransform(PxVec3(340, 1, -2/*325, 1, -6*//*-262, 245, -9*/)), physxPlayerMesh, eponge);

	// Trigger plane d�part
	physx::PxBoxGeometry* boxGeo = new physx::PxBoxGeometry(10,10,10);
	TriggerActor* depart = GAME->spawnManager()->spawn<TriggerActor>(PxTransform(PxVec3(328, 1, -2)), boxGeo);
	depart->SetTriggerWith("Player");
	depart->SetCallbackObj(dynamic_cast<HUDTask *>(tasks_[7])->GetTriggerStart());

	// Trigger transition scene
	physx::PxBoxGeometry* boxGeo3 = new physx::PxBoxGeometry(20,1,20);
	TriggerActor* transit = GAME->spawnManager()->spawn<TriggerActor>(PxTransform(PxVec3(0, -5, 0)), boxGeo3);
	transit->SetTriggerWith("Player");
	transit->SetCallbackObj(dynamic_cast<LoadingTask *>(tasks_[0]));

	// Trigger plane arriv�e
	physx::PxBoxGeometry* boxGeo2 = new physx::PxBoxGeometry(20,20,20);
	TriggerActor* arrivee = GAME->spawnManager()->spawn<TriggerActor>(PxTransform(PxVec3(-583, 51, 44)), boxGeo2);
	arrivee->SetTriggerWith("Player");
	arrivee->SetCallbackObj(dynamic_cast<HUDTask *>(tasks_[7])->GetTriggerEnd());

	// roofTOp
	physx::PxBoxGeometry* roofTop = new physx::PxBoxGeometry(500,1,500);
	StaticActor* roofCage = GAME->spawnManager()->spawn<ObstacleActor>(PxTransform(PxVec3(0, 50, 0)), roofTop);

	camera_->switchMode(THIRD_PERSON);

	// Load Scene 1
	tasks_[0]->update();

	// Load Scene 2
	tasks_[0]->update();

	menu_->SetOldScene(dynamic_cast<LoadingTask *>(tasks_[0])->GetSceneIndex(0));
	menu_->Naviguate(NEXT);
}

void SpongeRace::startGame()
{
	if(!gameStarted_)
	{
		gameStarted_ = true;
		menu_->Hide();// On indique que l'on veut cacher le menu et revenir � la scene par defaut
		menu_->Naviguate(NEXT);
	}
};

void SpongeRace::CreateBubbles(int nb, Scene * scene)
{
	auto color4 = EffectManager::GetInstance().getEffect<SimpleVertex4, ColorMat>("Color4");
	auto splineBulleGeo = GeometryManager::GetInstance().Request("splineBubble", "..\\SpongeRace\\Resources\\Models\\Splinebubble.obj", true, false, false, true);
	DrawableBSpline * splineBulle = Instancer::CreateBSpline(color4, &(splineBulleGeo->parts().front()),new ColorMat(new Material));
	Splines::GetInstance().addSpline(2,splineBulle);

	for(int i = 0; i < nb; ++i)
	{
		auto bulle = mainBulle->Clone();
		scene->AddBubbles(bulle);

		float ranVal = GenerateurNombresAleatoires::get().generer(0.f,1.f);

		auto spline = Splines::GetInstance().getSpline(2);
		XMFLOAT4 randomPos = spline->getPositionByInterpolate(ranVal);

		////bulle physxActor
		GAME->spawnManager()->spawn<BubbleActor>(PxTransform(PxVec3(randomPos.x, randomPos.y, randomPos.z)), bulle);
	}
}

void SpongeRace::unloadLevel(string id)
{
	//assetManager_.unloadSceneAssets(id);
}
	
void SpongeRace::cleanup()
{
	//unloadCurrentLevel();
	
	UserController::GetInstance().cleanup();

	for(auto iter = tasks_.begin(); iter != tasks_.end(); ++iter)
	{
		(*iter)->cleanup();
		delete *iter;
	}

	//assetManager_.unloadGameAssets();

	tasks_.clear();
}


Controller &SpongeRace::mainController()
{
	return UserController::GetInstance();
}

bool SpongeRace::shouldExit() const
{
	return _shouldExit;
}

void SpongeRace::update()
{
	
	for (auto iter = tasks_.begin(); iter != tasks_.end(); ++iter)
	{
		(*iter)->update();
	}
}

void SpongeRace::requestExit()
{
	_shouldExit = true;
}

void SpongeRace::setScene(physx::PxScene *aScene)
{
	scene_ = aScene;
}

physx::PxScene *SpongeRace::scene()
{
	return scene_;
}


void SpongeRace::setPhysics(physx::PxPhysics * aPhysics)
{
	physics_ = aPhysics;
}

physx::PxPhysics *SpongeRace::physics()
{
	return physics_;
}

void SpongeRace::setCooking(physx::PxCooking *aCooking)
{
	cooking_ = aCooking;
}

physx::PxCooking *SpongeRace::cooking()
{
	return cooking_;
}

PlayerActor* SpongeRace::player()
{
	return player_;
}

CameraActor* SpongeRace::camera()
{
	return camera_;
}
