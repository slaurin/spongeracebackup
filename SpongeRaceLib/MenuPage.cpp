#include "StdAfx.h"
#include "MenuPage.h"

using namespace std;
using namespace GE;

namespace GameUI
{
	MenuPage::MenuPage(const string & filename)
		: fileName_(filename), texture_(nullptr)
	{
		wstring wpath(L"..\\SpongeRace\\Resources\\Textures\\UI\\");
		copy(filename.begin(), filename.end(), back_inserter(wpath));

		texture_ = new Texture(wpath);
	}

	MenuPage::MenuPage(Texture * texture)
		: texture_(texture)
	{
		assert(texture_);
	}

	MenuPage * MenuPage::Naviguate(PageEvent event)
	{
		if(navigation_.find(event) != navigation_.end())
			return navigation_[event];
		return this;
	}

	void MenuPage::SetNavigation(PageEvent event, MenuPage * page)
	{
		navigation_.insert(make_pair(event, page));
	}

	Texture * MenuPage::GetTexture()
	{
		return texture_;
	}

	MenuPage::~MenuPage()
	{
		delete texture_;
	}
}
