#pragma once

namespace GE
{
	//! Repr�sente une source d'illumination
	class Light
	{
		//! Facteur d'illumination diffuse
		XMFLOAT4 diffuse_;

		//! Facteur d'illumination sp�culaire
		XMFLOAT4 specular_;

		//! Etat de la lampe (activ� ou non)
		bool enabled_;

	public:
		//! Active ou d�sactive la lampe
		void Enable(bool);

		//! Retourne l'�tat actuel de la lampe
		bool IsEnabled();

		//! Retourne la valeur actuelle de l'illumination diffuse
		XMFLOAT4 GetDiffuse() const;

		//! Retourne la valeur actuelle de l'illumination sp�culaire
		XMFLOAT4 GetSpecular() const;

		//! Fixe la valeur de l'illumination diffuse
		void SetDiffuse(const XMFLOAT4 &);

		//! Fixe la valeur de l'illumination sp�culaire
		void SetSpecular(const XMFLOAT4 &);

	protected:
		//! Constructeur param�tr� & d�faut prot�g� pour forcer l'instanciations d'enfants
		Light(XMFLOAT4 diffuse = XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4 specular = XMFLOAT4(0.f, 0.f, 0.f, 1.f), bool enable = true);
	};
}