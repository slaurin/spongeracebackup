#include "stdafx.h"
#include "LoopTime.h"

LoopTime::LoopTime()
    : gapTime_(1000/FPS), // To change back to FPS
      timer_(FasterTimer()),
      rate_(1.0),
      frame_(0),
      active_(1.0)
{
    previousTime_ = timer_.getTime();
    nextTime_ = previousTime_;
}

bool LoopTime::CanUpdate() const throw() {return canUpdate_;}

float LoopTime::ElapsedTimeMilliseconds()
{
    return static_cast<float>(elapsedTime_ * rate_ * active_);
}
float LoopTime::ElapsedTimeSeconds()
{
    return static_cast<float>(elapsedTimeSeconds_ * rate_ * active_);
}

unsigned long LoopTime::Frame() const throw() {return frame_;}

double LoopTime::GetRate() const throw() {return rate_;}
void LoopTime::SetRate(double rate) {rate_ = rate;}

void LoopTime::Start() throw()
{
    active_ = 1.0;
}

void LoopTime::Stop() throw()
{
    active_ = 0.0;
}

void LoopTime::SetPause(bool isPaused) throw()
{
    if(isPaused)
        Stop();
    else
        Start();
}

void LoopTime::Update() throw()
{
	canUpdate_ = false;
	LONGLONG currentTime;

	currentTime = timer_.getTime();

	if (currentTime > nextTime_)
	{
        elapsedTime_ = static_cast<double>(currentTime-previousTime_);
        bool overGap = elapsedTime_ > gapTime_*1.5;

        if(overGap)
        {
            elapsedTime_ = gapTime_;
            previousTime_ = currentTime;
            nextTime_ = currentTime + gapTime_;
        }
        else
        {
            elapsedTimeSeconds_ = elapsedTime_ * 0.001;
            previousTime_ = currentTime;
            nextTime_ = previousTime_ + gapTime_;
        }
        canUpdate_ = true;
        ++frame_;
	}
	else
	{
		LONGLONG remainingTime = (nextTime_ - currentTime) / 2;
		//Sleep(static_cast<DWORD>(remainingTime));
	}
}