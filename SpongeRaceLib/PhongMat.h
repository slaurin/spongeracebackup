#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"
#include "PointLight.h"
#include "SpongeRace.h"
#include "CameraActor.h"

namespace GE
{
	class PhongMat
		: public Material
	{
		static const unsigned int MAX_SPOTLIGHTS = 8;
		static const unsigned int MAX_POINTLIGHTS = 8;
		static const unsigned int MAX_DIRECTIONALLIGHTS = 8;
		static const unsigned int MAX_TEXTURES = aiTextureType_UNKNOWN - aiTextureType_DIFFUSE;

		// Lumi�res ponctuelles
		struct PointLightStruct
		{
			XMVECTOR Pos;	// Position
			XMVECTOR Dif;	// Diffusion
			XMVECTOR Spec;	// Sp�culaire
			XMFLOAT3 Att;	// Facteurs d'att�nuation
			float padding;
		};

		// Lumi�res directionnelles
		struct DirLightStruct
		{
			XMVECTOR Dir;	// Direction 
			XMVECTOR Dif;	// Diffusion
			XMVECTOR Spec;	// Sp�culaire
		};

		// Lumi�res Spot
		struct SpotLightStruct
		{
			XMVECTOR Pos;	// Position
			XMVECTOR Dir;	// Direction
			XMVECTOR Dif;	// Diffusion
			XMVECTOR Spec;	// Sp�culaire
			XMFLOAT3 Att;	// Facteurs d'att�nuation
			float AngleCos;	// Cosinus du demi-angle du c�ne de lumi�re
			float Exp;		// Exposant d'att�nuation
			XMFLOAT3 padding;
		};

		struct GlobalParams // toujours un multiple de 16 pour les constantes
		{
			XMMATRIX matWorldViewProj;							// Matrice WVP
			XMMATRIX matWorld;									// Matrice de transformation dans le monde 
			XMVECTOR vCamera; 									// Position de la cam�ra
			XMVECTOR vAmbientLight;								// Valeur ambiante d'illumination de la sc�ne
			int numPointLights;									// Nombre de lampes ponctuelles
			int numDirLights;									// Nombre de lampes directionnelles
			int numSpotLights;									// Nombre de lampes spot
			float exponent;										// Exposant Sp�culaire
			PointLightStruct PointLights[MAX_POINTLIGHTS];		// Lumi�res ponctuelles
			DirLightStruct DirLights[MAX_DIRECTIONALLIGHTS];	// Lumi�res directionnelles
			SpotLightStruct SpotLights[MAX_SPOTLIGHTS];			// Lumi�res spot
		};

		struct TexLessParams
		{
			XMVECTOR vAmbient; 		// Valeur ambiante du mat�riau
			XMVECTOR vDiffusion;	// Valeur diffuse du mat�riau 
			XMVECTOR vSpecular;		// Valeur sp�culaire du mat�riau
			XMVECTOR vEmissive;		// Valeur emissive du mat�riau
		};

		//! Param�tres globaux
		GlobalParams globalParams_;

		//! Buffer correspondant aux param�tres globaux
		ID3D11Buffer * globalBuffer_;

		//! Param�tres d'illumination du mat�riau
		TexLessParams texLessParams_;

		//! Buffer correspondant aux param�tres d'illumination du mat�riau
		ID3D11Buffer *	texLessBuffer_;

		//! Exposant sp�culaire par d�faut
		float specularExponent_;

		//! Tableau indiquant � l'effet quelles textures sont disponibles
		BOOL loadedTex_[MAX_TEXTURES];

		//! Tableau contenant les textures disponibles
		ID3D11ShaderResourceView * texArray_[MAX_TEXTURES];
		
		//! Tableau contenant les �tats de sampling
		ID3D11SamplerState * samplerArray_[MAX_TEXTURES];

	public:
		PhongMat(const Material *);

		~PhongMat();

		//! Surcharge de Material
		bool InsertTexture(aiTextureType type, Texture * texture);

		//! Surcharge de Material
		void ReplaceTexture(aiTextureType type, Texture * texture);

		//! Surcharge de Material
		void SetLighting(const Lighting &);

		//! Retourne le nom de la classe
		static std::string GetClassName();

		//! Applique l'effet sur l'object � partir des valeurs de ce mat�riau
		template<class VertexT>
		void Apply(Effect<VertexT, PhongMat> * effect, const Object3D<VertexT, PhongMat> & object)
		{
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			globalParams_.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj);
			globalParams_.matWorld = XMMatrixTranspose(object.GetWorldMatrix());
			globalParams_.vCamera = (GAME->camera()->getPosition());
			globalParams_.vAmbientLight = XMLoadFloat4(&GraphicEngine::GetInstance().GetCurrentScene()->GetAmbientLighting());

			// Charge les lumi�res ponctuelles
			loadPointLights();

			// Charge les lumi�res directionnelles
			loadDirrectionalLights();
			
			// Charge les lumi�res spot
			loadSpotLights();

			// On charge les constantes globales
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(globalBuffer_, 0, NULL, &globalParams_, 0, 0);
			effect->GetConstantBufferByName("GlobalParams")->SetConstantBuffer(globalBuffer_);

			// On charge les constantes du mat�riau
			effect->GetConstantBufferByName("TexLessParams")->SetConstantBuffer(texLessBuffer_);

			// S'il y a des textures, on les charge
			if(!textures_.empty())
			{
				// On charge le tableau de bool�ens indiquant les textures charg�es
				effect->GetVariableByName("loadedTextures")->AsScalar()->SetBoolArray(loadedTex_, 0, MAX_TEXTURES);

				// On charge le tableau de textures
				effect->GetVariableByName("textures")->AsShaderResource()->SetResourceArray(texArray_, NULL, MAX_TEXTURES);

				// On charge le tableau de samplers
				//for(unsigned int i = 0; i < MAX_TEXTURES; ++i)
				//	effect->GetVariableByName("samplers")->AsSampler()->SetSampler(i, samplerArray_[i]);

				// On applique l'effet prenant en compte les textures
				effect->Apply(object, "PhongTex");
			}
			else
			{
				// On applique l'effet ne prenant pas en compte les textures
				effect->Apply(object, "PhongTexLess");
			}
		}

	private:
		void initGlobalParams();
		void initTexParams();

		void updateTexLessParams();
		void updateTexParams();
		void updateTexParam(unsigned int key, Texture * texture);

		void loadPointLights();
		void loadDirrectionalLights();
		void loadSpotLights();
	};
}