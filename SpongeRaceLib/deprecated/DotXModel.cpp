//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "stdafx.h"
#include "DotXModel.h"
#include "PxPhysicsAPI.h"
#include <vector>

//=============================================================================
// class MemoryStream
//=============================================================================
class MemoryStream: public PxOutputStream, public PxInputStream
{
public:
	virtual PxU32 write(const void* src, PxU32 count)
	{
		int oldSize = _data.size();
		_data.resize(_data.size() + count);
		memcpy(&_data[oldSize], src, count);
		return count;
	}

	virtual PxU32 read(void* dest, PxU32 count)
	{
		if (count > _data.size())
			count = _data.size();
		memcpy(dest, &_data[0], count);
		_data.erase(_data.begin(), _data.begin()+count);
		return count;
	}

public:
	std::vector<unsigned char> _data;
};

//=============================================================================
// PRIVATE FUNCTIONS
//=============================================================================
static PxConvexMesh* GenerateConvexFromDXMesh(PxPhysics &physics, ID3DXMesh* &Mesh)
{


	int NumVerticies = Mesh->GetNumVertices();
	DWORD FVFSize = D3DXGetFVFVertexSize(Mesh->GetFVF());

	//Create pointer for vertices
	PxVec3* verts = new PxVec3[NumVerticies];

	char *DXMeshPtr;
	Mesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&DXMeshPtr);
	for(int i = 0; i < NumVerticies; i++)
	{
		Mesh_FVF *DXMeshFVF = (Mesh_FVF*)DXMeshPtr;
		verts[i] = PxVec3(DXMeshFVF->VertexPos.x, DXMeshFVF->VertexPos.y, DXMeshFVF->VertexPos.z);
		DXMeshPtr += FVFSize;
	}
	Mesh->UnlockVertexBuffer();

	// Create descriptor for convex mesh
	PxConvexMeshDesc convexDesc;
	convexDesc.points.count		= NumVerticies;
	convexDesc.points.stride	= sizeof(PxVec3);
	convexDesc.points.data		= verts;
	convexDesc.flags			= PxConvexFlag::eCOMPUTE_CONVEX;
	
	PxCooking *cooker = PxCreateCooking(PX_PHYSICS_VERSION, physics.getFoundation(), PxCookingParams());

	// Cooking from memory
	MemoryStream buf;
	PxConvexMesh *convexMesh = nullptr;
	if(cooker->cookConvexMesh(convexDesc, buf))
	{
		convexMesh = physics.createConvexMesh(buf);
	}	
	cooker->release();

	delete[] verts;

	return convexMesh;
}

//=============================================================================
// CLASS DotXModel::DotXModelImp
//=============================================================================
class DotXModel::DotXModelImp
{
public:
	LPDIRECT3DDEVICE9 _d3ddev;
	D3DMATERIAL9 *_meshMaterials;
	LPDIRECT3DTEXTURE9 *_meshTextures;
	DWORD _materialCount;
	LPD3DXMESH _d3dMesh;


public:
	DotXModelImp(LPDIRECT3DDEVICE9 aD3dDev, const TCHAR *aPathToDotX, const char *aPathToTextures)
		: _d3ddev(aD3dDev)
		, _meshMaterials(nullptr)
		, _meshTextures(nullptr)
		, _materialCount(0)
		, _d3dMesh(nullptr)
	{
		LPD3DXBUFFER _materials;
		HRESULT hr = D3DXLoadMeshFromX( aPathToDotX, D3DXMESH_MANAGED, _d3ddev, NULL, &_materials, NULL, &_materialCount, &_d3dMesh); 

		D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)_materials->GetBufferPointer();

		_meshMaterials = new D3DMATERIAL9[_materialCount];
		_meshTextures  = new LPDIRECT3DTEXTURE9[_materialCount];

		// load the associated materials
		for (int i = 0; i < _materialCount; ++i)
		{
			// Copy the material
			_meshMaterials[i] = d3dxMaterials[i].MatD3D;

			// Set the ambient color for the material (D3DX does not do this)
			_meshMaterials[i].Ambient = _meshMaterials[i].Diffuse;
     
			// Create the texture if it exists - it may not
			_meshTextures[i] = NULL;
			if (d3dxMaterials[i].pTextureFilename)
			{
				char path[1024];
				sprintf(path, "%s%s", aPathToTextures, d3dxMaterials[i].pTextureFilename);
				D3DXCreateTextureFromFileA(_d3ddev, path, &_meshTextures[i]);
				printf("loaded %s\n", path);
			}
		}
	}

	//-------------------------------------------------------------------------
	//
	~DotXModelImp()
	{
		if (_meshTextures)
		{
			for (int i = 0; i < _materialCount; ++i)
			{
				if (_meshTextures[i])
					_meshTextures[i]->Release();
			}
			delete [] _meshTextures;
			_meshTextures = nullptr;
		}
		
		if (_meshMaterials)
		{
			delete [] _meshMaterials;
			_meshMaterials = nullptr;
		}
	}

	//-------------------------------------------------------------------------
	//
	void render()
	{
		D3DMATERIAL9 prevMaterial;
		_d3ddev->GetMaterial(&prevMaterial);

		for (int i = 0; i < _materialCount; ++i)
		{
		   // Set the material and texture for this subset
		  _d3ddev->SetMaterial(&_meshMaterials[i]);
		  _d3ddev->SetTexture(0, _meshTextures[i]);
        
		  // Draw the mesh subset
		  _d3dMesh->DrawSubset( i );
		}

		_d3ddev->GetMaterial(&prevMaterial);
		_d3ddev->SetTexture(0, 0);
	}
};

//=============================================================================
// CLASS DotXModel
//=============================================================================
DotXModel::DotXModel(LPDIRECT3DDEVICE9 aD3dDev, const TCHAR *aPathToDotX, const char *aPathToTextures)
	: _imp(new DotXModelImp(aD3dDev, aPathToDotX, aPathToTextures))
{}

//-----------------------------------------------------------------------------
//
DotXModel::~DotXModel()
{
	delete _imp;
}

//-----------------------------------------------------------------------------
//
void DotXModel::render()
{
	_imp->render();
}

//-----------------------------------------------------------------------------
//
PxConvexMesh* DotXModel::convexMeshFromModel()
{
	return GenerateConvexFromDXMesh(GAME->scene()->getPhysics(), _imp->_d3dMesh);
}
