#ifndef OBJMODELLOADER_H
#define OBJMODELLOADER_H

/*
Static Util Class
Load Maya Obj models
*/


typedef struct
{
	float x, y, z;
}VertexType;

typedef struct
{
	int vIndex1, vIndex2, vIndex3;
	int tIndex1, tIndex2, tIndex3;
	int nIndex1, nIndex2, nIndex3;
}FaceType;

class CannotOpenFile{};

class ObjModelLoader
{
public:
	static bool ReadFileCounts(char*, int&, int&, int&, int&);
	static bool LoadDataStructures(char*, int, int, int, int);
};

#endif