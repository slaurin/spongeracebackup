#pragma once

class RenderPrimitive
{
public:
	virtual void render() = 0;
};