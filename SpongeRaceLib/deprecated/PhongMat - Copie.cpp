#include "stdafx.h"
#include "PhongMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	PhongMat::PhongMat(const Material & material)
		: Material(material), power_(1)
	{
		int i = sizeof(ShadersParams);

		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		initGlobalParams();

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ShadersParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &constantBuffer);


		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(TexLessParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &constantBuffer2);
	}

	void PhongMat::initGlobalParams()
	{
		// Initialisation de la structure des param�tres
		sp_.matWorldViewProj = XMMATRIX();
		sp_.matWorld = XMMATRIX(); 
		sp_.vCamera = XMVECTOR();
		sp_.vAmbientLight = XMVECTOR();
		sp_.pointLights = 0;
		sp_.dirLights = 0;		
		sp_.spotLights = 0;
		sp_.puissance = power_;

		// Lumi�res ponctuelles
		for(unsigned int i = 0; i < MAX_POINTLIGHTS; ++i)
		{
			sp_.vPointLightsPos[i] = XMVECTOR();
			sp_.vPointLightsDif[i] = XMVECTOR();
			sp_.vPointLightsSpec[i] = XMVECTOR();
			sp_.vPointLightsAtt[i] = XMVECTOR();
		}

		// Lumi�res directionnelles
		for(unsigned int i = 0; i < MAX_DIRECTIONALLIGHTS; ++i)
		{
			sp_.vDirLightsDir[i] = XMVECTOR();
			sp_.vDirLightsDif[i] = XMVECTOR();
			sp_.vDirLightsSpec[i] = XMVECTOR();
		}

		// Lumi�res spot
		for(unsigned int i = 0; i < MAX_SPOTLIGHTS; ++i)
		{
			sp_.vSpotLightsPos[i] = XMVECTOR();
			sp_.vSpotLightsDir[i] = XMVECTOR();
			sp_.vSpotLightsDif[i] = XMVECTOR();
			sp_.vSpotLightsSpec[i] = XMVECTOR();
			sp_.vSpotLightsAtt[i] = XMVECTOR();
			sp_.vSpotLightsInner[i] = 0.f;
			sp_.vSpotLightsOuter[i] = 0.f;
		}
	}

	void PhongMat::loadPointLights()
	{
		vector<PointLight> & lights = GraphicEngine::GetInstance().GetCurrentScene().GetPointLights();
		sp_.pointLights = 0;
		for(auto it = lights.begin(); it != lights.end() && sp_.pointLights < MAX_POINTLIGHTS; ++it)
		{
			if(it->IsEnabled() && true)//todo LOD : check range et objectboudingbox
			{
				sp_.vPointLightsPos[sp_.pointLights] = XMLoadFloat4(&it->GetPosition());
				sp_.vPointLightsDif[sp_.pointLights] = XMLoadFloat4(&it->GetDiffuse());
				sp_.vPointLightsSpec[sp_.pointLights] = XMLoadFloat4(&it->GetSpecular());
				sp_.vPointLightsAtt[sp_.pointLights] = XMVectorSet(
					it->GetAttenuationRange(),
					it->GetAttenuationConstant(),
					it->GetAttenuationLinear(),
					it->GetAttenuationQuadric());
				++sp_.pointLights;
			}
		}
	}

	void PhongMat::loadDirrectionalLights()
	{
		vector<DirectionalLight> & lights = GraphicEngine::GetInstance().GetCurrentScene().GetDirectionalLights();
		sp_.dirLights = 0;
		for(auto it = lights.begin(); it != lights.end() && sp_.dirLights < MAX_DIRECTIONALLIGHTS; ++it)
		{
			if(it->IsEnabled() && true)//todo LOD : check range et objectboudingbox
			{
				sp_.vDirLightsDir[sp_.dirLights] = XMLoadFloat4(&it->GetDirection());
				sp_.vDirLightsDif[sp_.dirLights] = XMLoadFloat4(&it->GetDiffuse());
				sp_.vDirLightsSpec[sp_.dirLights] = XMLoadFloat4(&it->GetSpecular());
				++sp_.dirLights;
			}
		}
	}

	void PhongMat::loadSpotLights()
	{
		vector<SpotLight> & lights = GraphicEngine::GetInstance().GetCurrentScene().GetSpotLights();
		sp_.spotLights = 0;
		for(auto it = lights.begin(); it != lights.end() && sp_.spotLights < MAX_SPOTLIGHTS; ++it)
		{
			if(it->IsEnabled() && true)//todo LOD : check range et objectboudingbox
			{
				sp_.vSpotLightsPos[sp_.spotLights] = XMLoadFloat4(&it->GetPosition());
				sp_.vSpotLightsDir[sp_.spotLights] = XMLoadFloat4(&it->GetDirection());
				sp_.vSpotLightsDif[sp_.spotLights] = XMLoadFloat4(&it->GetDiffuse());
				sp_.vSpotLightsSpec[sp_.spotLights] = XMLoadFloat4(&it->GetSpecular());
				sp_.vSpotLightsInner[sp_.spotLights] = cos(it->GetInnerAngle());
				sp_.vSpotLightsOuter[sp_.spotLights] = cos(it->GetOuterAngle());
				sp_.vSpotLightsAtt[sp_.spotLights] = XMVectorSet(
					it->GetAttenuationRange(),
					it->GetAttenuationConstant(),
					it->GetAttenuationLinear(),
					it->GetAttenuationQuadric());
				++sp_.spotLights;
			}
		}
	}

	PhongMat::~PhongMat(void)
	{
		DXRelacher(constantBuffer);
		DXRelacher(constantBuffer2);
	}
}