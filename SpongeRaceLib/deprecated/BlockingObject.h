#pragma once
#include "Actor.h"


class BlockingObject//: public ActorBase
{
public:
	BlockingObject();
	~BlockingObject();


private:
	class BlockingObjectImp;
	BlockingObjectImp *imp_;
};
