#include "stdafx.h"
#include "MiniPhongMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace DxUtilities;

namespace GE
{
	MiniPhongMat::MiniPhongMat(const Material & material)
		: Material(material), power_(1)
	{
		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ShadersParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &constantBuffer);
	}

	MiniPhongMat::~MiniPhongMat(void)
	{
		DXRelacher(constantBuffer);
	}
}