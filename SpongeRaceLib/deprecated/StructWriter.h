/*
 *  Author : Stephane Laurin
 *  Verifier : 
 *
 *  Currently unused
 */

#ifndef STRUCT_WRITER_H
#define STRUCT_WRITER_H

#include <ostream>

template<class DataType>
class StructWriter
{
private:
    std::ofstream& stream_;
public:
    StructWriter(std::ofstream &stream)
        : stream_(stream)
    {}
    void operator()(DataType val)
    {
        char* s = reinterpret_cast<char *>(&val);
        stream_.write(s, sizeof(val));
    }
};

#endif