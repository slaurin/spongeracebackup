#include "StdAfx.h"
#include "Object3D.h"
#include "Resource.h"
#include "util.h"
#include "GraphicEngine.h"

#include <iterator>

using namespace UtilitairesDX;
using std::vector;

namespace GE
{
	Object3D::Object3D(Material *pMaterial, const XMMATRIX & matWorld, const std::vector<MeshVertex> & vertices, const std::vector<index_t> & indexes)
		: mat_(pMaterial), matWorld_(matWorld), vertices_(vertices), indexes_(indexes)
	{
		//material_->newUse();
		effect_ = new Effect<MeshVertex>();
	}

	Object3D::Object3D(const Object3D & object)
	{
		//material_ = object.material_;
		matWorld_ = XMMATRIX(object.matWorld_);
		vertices_ = vector<MeshVertex>(object.vertices_);
		indexes_ = vector<index_t>(object.indexes_);
		CreateVertexBuffer();
		CreateIndexBuffer();
	}

	Object3D::~Object3D()
	{
		//material_->deleteUse();
		DXRelacher(indexBuffer_);
		DXRelacher(vertexBuffer_);
	}

	void Object3D::Draw()
	{
		// On applique le material et on rend le cube
		//material_->Apply(matWorld_, vertexBuffer_, indexBuffer_)->DrawIndexed(indexes_.size(), 0, 0);	
		


		//// On charge la structure de param�tres initialis�e par l'effet
		//Effect::Parameters parameters = effect_->GetParameters();

		//// On choisit notre technique
		//Technique technique = effect_->GetTechnique(0);

		//// On applique les param�tres li�s � la Technique utilis�e
		//technique.SetParameters(parameters);

		//// Pour chaque passe de la technique
		//unsigned int passesNb = technique.GetPassesNumber();
		//for(unsigned int i = 0; i < passesNb; ++i)
		//{
		//	// On configure le pipeline avec les param�tres de la passe (et ceux h�rit�s de l'effet et de la technique)
		//	technique.GetPass(i).Apply(parameters);
		//	
		//	// On envoie notre g�om�trie dans le pipeline !
		//	GraphicEngine::GetInstance().GetDevice->DrawIndexed(indexes_.size(), 0, 0);
		//}




		ID3D11DeviceContext * context = GraphicEngine::GetInstance().GetDevice()->GetImmediateContext();

		// Choisir la topologie des primitives
		context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		// input Layout_ des sommets
		//context->IASetInputLayout(vertexLayout_);

		// Index buffer
		context->IASetIndexBuffer(indexBuffer_, DXGI_FORMAT_R32_UINT, 0);

		// Vertex buffer
		UINT stride = sizeof(MeshVertex);
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);

		effect_->Apply(mat_->GetBuffer(*this), *this);
	}

	void Object3D::SendToPipeline() const
	{
		// On envoie notre g�om�trie dans le pipeline !
		GraphicEngine::GetInstance().GetDevice()->GetImmediateContext()->DrawIndexed(indexes_.size(), 0, 0);
	}

	XMMATRIX Object3D::GetWorldMatrix() const
	{
		return matWorld_;
	}

	void Object3D::CreateVertexBuffer()
	{
		ID3D11Device* pD3DDevice = GraphicEngine::GetInstance().GetDevice()->GetDevice();

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = vertices_.size() * sizeof(vertices_.front());
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = &vertices_[0];
		vertexBuffer_ = NULL;
		
		DXEssayer(
			pD3DDevice->CreateBuffer(
				&bd,
				&InitData,
				&vertexBuffer_),
			DXE_CREATIONVERTEXBUFFER);
	}

	void Object3D::CreateIndexBuffer()
	{
		ID3D11Device* pD3DDevice = GraphicEngine::GetInstance().GetDevice()->GetDevice();

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = indexes_.size() * sizeof(indexes_.front());
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = &indexes_[0];
		indexBuffer_ = NULL;

		DXEssayer(
			pD3DDevice->CreateBuffer(
				&bd,
				&InitData,
				&indexBuffer_),
			DXE_CREATIONINDEXBUFFER);
	}
}