/*
 *  Author : Stephane Laurin
 *  Verifier : 
 *
 *  Currently unused
 */

#ifndef SHOW_H
#define SHOW_H

template<class T>
class Show
{
    std::ostream &os_;
public:

    Show(std::ostream &os) throw()
         : os_(os)
    {}

    void operator()(const T &val)
    {
        os_ << val << ' ';
    }
};

#endif