#include "StdAfx.h"
#include "Device.h"
#include "GestionnaireDeTextures.h"

namespace GE
{

CGestionnaireDeTextures::CGestionnaireDeTextures(void)
{
}

CGestionnaireDeTextures::~CGestionnaireDeTextures(void)
{
        Cleanup();
}

Texture* const CGestionnaireDeTextures::GetNewTexture(const wchar_t* filename_in, Device* pDispositif)
{       
        Texture* pTexture;

        // On v�rifie si la texture est d�jr dans notre liste
        pTexture = GetTexture(filename_in);

        // Si non, on la cr�e
        if (!pTexture)
        {
                pTexture = new Texture(filename_in, pDispositif);

                // Puis, il est ajout� r la sccne
                ListeTextures.push_back(pTexture);
        }

        return pTexture;
}

Texture* const CGestionnaireDeTextures::GetTexture(const wchar_t* filename_in)
{
Texture* pTexture = 0;
std::vector<Texture*>::iterator It;
        
        It = ListeTextures.begin();

        while ( pTexture==0 && It != ListeTextures.end() )
        {
                //if ( wcscmp( (*It)->GetFilename(), filename_in)==0)
                //{
                //      pTexture = *It;
                //}

                It++;
        }

        return pTexture;
}

void CGestionnaireDeTextures::Cleanup()
{
// d�truire les objets
std::vector<Texture*>::iterator It;

        for (It = ListeTextures.begin(); It != ListeTextures.end();It++)
        {
                delete (*It);
        }

        ListeTextures.clear();
}

}