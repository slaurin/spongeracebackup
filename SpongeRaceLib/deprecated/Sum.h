/*
 *  Author : Stephane Laurin
 *  Verifier : 
 *
 *  Currently unused
 */

#ifndef SUM_H
#define SUM_H

template<class T>
class Sum
{
public:
    typedef T value_t;
private:
    int &sum_;
public:
    Sum(value_t& initValue)
        : sum_(initValue)
    {}

    void operator()(const value_t &val)
    {
        sum_ += val;
    }
};

#endif