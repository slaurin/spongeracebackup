#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"
#include "PointLight.h"

#include <string>
#include <vector>

namespace GE
{
	class PhongMat
		: public Material
	{
		static const unsigned int MAX_SPOTLIGHTS = 8;
		static const unsigned int MAX_POINTLIGHTS = 8;
		static const unsigned int MAX_DIRECTIONALLIGHTS = 8;

		struct ShadersParams // toujours un multiple de 16 pour les constantes
		{
			XMMATRIX matWorldViewProj;	// Matrice WVP
			XMMATRIX matWorld;			// Matrice de transformation dans le monde 
			XMVECTOR vCamera; 			// Position de la cam�ra
			XMVECTOR vAmbientLight;		// Valeur ambiante d'illumination de la sc�ne
			int pointLights;			// Nombre de lampes ponctuelles
			int dirLights;				// Nombre de lampes directionnelles
			int spotLights;				// Nombre de lampes spot
			float puissance;			// Puissance Sp�culaire

			// Lumi�res ponctuelles
			XMVECTOR vPointLightsPos[MAX_POINTLIGHTS];		// Position
			XMVECTOR vPointLightsDif[MAX_POINTLIGHTS];		// Diffusion
			XMVECTOR vPointLightsSpec[MAX_POINTLIGHTS];		// Speculaire
			XMVECTOR vPointLightsAtt[MAX_POINTLIGHTS];		// Facteurs d'att�nuation

			// Lumi�res directionnelles
			XMVECTOR vDirLightsDir[MAX_DIRECTIONALLIGHTS];	// Direction 
			XMVECTOR vDirLightsDif[MAX_DIRECTIONALLIGHTS];	// Diffusion
			XMVECTOR vDirLightsSpec[MAX_DIRECTIONALLIGHTS];	// Speculaire
			
			// Lumi�res spot
			XMVECTOR vSpotLightsPos[MAX_SPOTLIGHTS];	// Position
			XMVECTOR vSpotLightsDir[MAX_SPOTLIGHTS];	// Direction
			XMVECTOR vSpotLightsDif[MAX_SPOTLIGHTS];	// Diffusion
			XMVECTOR vSpotLightsSpec[MAX_SPOTLIGHTS];	// Speculaire
			XMVECTOR vSpotLightsAtt[MAX_SPOTLIGHTS];	// Facteurs d'att�nuation
			float vSpotLightsInner[MAX_SPOTLIGHTS];		// Inner Angle
			float vSpotLightsOuter[MAX_SPOTLIGHTS];		// Outer Angle
		};

		struct TexLessParams
		{
			XMVECTOR vAmbient; 		// Valeur ambiante du mat�riau
			XMVECTOR vDiffusion;	// Valeur diffuse du mat�riau 
			XMVECTOR vSpecular;		// Valeur sp�culaire du mat�riau
			XMVECTOR vEmissive;		// Valeur emissive du mat�riau
		};

		ID3D11Buffer *constantBuffer;
		ID3D11Buffer *constantBuffer2;
		float power_;
		ShadersParams sp_;

	public:
		PhongMat(const Material & material);

		~PhongMat();

		template<class VertexT>
		void Apply(Effect<VertexT, PhongMat> * effect, const Object3D<VertexT, PhongMat> & object)
		{
			TexLessParams tlp;

			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			sp_.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj );
			sp_.matWorld = XMMatrixTranspose(object.GetWorldMatrix());
			sp_.vCamera = XMVectorSet(0.0f, 3.0f, -5.0f, 1.0f);
			sp_.vAmbientLight = XMVectorSet(0.2f, 0.2f, 0.2f, 1.0f);

			// Charge les lumi�res ponctuelles
			loadPointLights();

			// Charge les lumi�res directionnelles
			loadDirrectionalLights();
			
			// Charge les lumi�res spot
			loadSpotLights();

			tlp.vAmbient = XMLoadFloat4(&lighting_.GetAmbient());
			tlp.vDiffusion = XMLoadFloat4(&lighting_.GetDiffuse());
			tlp.vSpecular = XMLoadFloat4(&lighting_.GetSpecular());
			tlp.vEmissive = XMLoadFloat4(&lighting_.GetEmissive());

			// Activation de la texture ou non
			//auto it = textures_.find(aiTextureType(aiTextureType_DIFFUSE));
			//if(it->second)
			//{
			//	ID3DX11EffectShaderResourceVariable* variableTexture;
			//	variableTexture = effect->GetVariableByName("textureEntree")->AsShaderResource();
			//	variableTexture->SetResource(it->second->GetTexture());
			//	sp.bTex = 1;

			//	// On charge le sampler state associ�
			//	ID3DX11EffectSamplerVariable* variableSampler;
			//	variableSampler = effect->GetVariableByName("SampleState")->AsSampler();
			//	variableSampler->SetSampler(0, it->second->GetSamplerState());
			//}
			//else
			//{
			//	sp.bTex = 0;
			//}

			// On charge les buffers
			effect->GetConstantBufferByName("GlobalParams")->SetConstantBuffer(constantBuffer);
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer, 0, NULL, &sp_, 0, 0);

			effect->GetConstantBufferByName("TexLessParams")->SetConstantBuffer(constantBuffer2);
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer2, 0, NULL, &tlp, 0, 0);
		}

	private:
		void initGlobalParams();
		void loadPointLights();
		void loadDirrectionalLights();
		void loadSpotLights();
	};
}