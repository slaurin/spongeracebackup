#pragma once

#include "Object3D.h"
#include "MiniPhongMat.h"
#include "Effect.h"
#include "MeshVertex.h"

//
//namespace GE
//{
//	/*! Cube2 en 3D (est un Object3D) */
//	class Cube2
//		: public Object3D<MeshVertex, MiniPhongMat>
//	{
//	public:
//		/*! Constructeur */
//		Cube2(IEffect *effect, MiniPhongMat *pMaterial, float dx = 1.f, float dy = 1.f, float dz = 1.f);
//
//		/*! Surchage de la m�thode d'animation */
//		void Anime(float timeElapsed);
//
//	private:
//		/*! Liste d'index utilis�s pour dessiner le Cube2 */
//		static const unsigned int indexList_[];
//
//		/*! D�finit la rotation actuelle du Cube2 */
//		float rotation_;
//	};
//}

	