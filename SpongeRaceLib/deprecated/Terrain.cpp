#include "stdafx.h"
#include "Terrain.h"

GE::Terrain::Terrain(std::vector<unsigned char> &data)
{
	const float heightFactor = 0.1f;
	unsigned int nbColRow = static_cast<unsigned int>(std::sqrt(static_cast<double>(data.size())));

	int x = 0;
	std::for_each(std::begin(data), std::end(data), [&] (unsigned char c)
	{
		Vertex vertex (XMFLOAT3(static_cast<float>(x % nbColRow),
								static_cast<float>(c * heightFactor),
								static_cast<float>(x / nbColRow)));
		GetVertices().push_back(vertex);
		++x;
	});

	std::vector<XMFLOAT3> trianglesNormales;
	Vertex * currentVertex = &GetVertices()[0];

	unsigned int nbSquare = nbColRow - 1;
	unsigned int maxSqares = nbSquare * nbSquare;
	for(unsigned int i = 0; i < nbSquare; ++i)
	{
		for(unsigned int  j = 0; j < nbSquare; ++j)
		{
			// 2 vectors for each square
			XMFLOAT3 v1;
			XMStoreFloat3(&v1, XMVector3Normalize( XMVector3Cross(
							 (XMLoadFloat3(&currentVertex->GetPosition()) - XMLoadFloat3(&(currentVertex+1)->GetPosition())),
							 (XMLoadFloat3(&currentVertex->GetPosition()) - XMLoadFloat3(&(currentVertex+nbColRow)->GetPosition()))
						 )) * -1
						 );
			XMFLOAT3 v2;
			XMStoreFloat3(&v2, XMVector3Normalize( XMVector3Cross(
							 (XMLoadFloat3(&(currentVertex+1)->GetPosition()) - XMLoadFloat3(&(currentVertex + nbColRow)->GetPosition())),
							 (XMLoadFloat3(&(currentVertex+1)->GetPosition()) - XMLoadFloat3(&(currentVertex+nbColRow-1)->GetPosition()))
						 )) * -1
						 );
			trianglesNormales.push_back(v1);
			trianglesNormales.push_back(v2);
			++currentVertex;
		}
		++currentVertex;
	}

	unsigned int  nbTriangles = nbSquare * 2;

	// Normal for each corner, 2 of them need not to be done since there is only 1 triangle
	GetVertices()[nbSquare].SetNormal(XMFLOAT3( (trianglesNormales[nbTriangles-2].x + trianglesNormales[nbTriangles-1].x) / 2,
												(trianglesNormales[nbTriangles-2].y + trianglesNormales[nbTriangles-1].y) / 2,
												(trianglesNormales[nbTriangles-2].z + trianglesNormales[nbTriangles-1].z) / 2 ));
	GetVertices()[nbSquare*(nbSquare+1)].SetNormal(XMFLOAT3( (trianglesNormales[nbTriangles*(nbTriangles-1)].x + trianglesNormales[nbTriangles*(nbTriangles-1)+1].x) / 2,
															 (trianglesNormales[nbTriangles*(nbTriangles-1)].y + trianglesNormales[nbTriangles*(nbTriangles-1)+1].y) / 2,
															 (trianglesNormales[nbTriangles*(nbTriangles-1)].z + trianglesNormales[nbTriangles*(nbTriangles-1)+1].z) / 2 ));

	// Normal for north side
	currentVertex = &GetVertices()[1];
	for(unsigned int  i = 1; i < nbSquare; ++i)
	{
		unsigned int  a1 = i*2-2;
		unsigned int  a2 = i*2-1;
		unsigned int  a3 = i*2;
		XMFLOAT3 v1 = trianglesNormales[a1];
		XMFLOAT3 v2 = trianglesNormales[a2];
		XMFLOAT3 v3 = trianglesNormales[a3];
		currentVertex->SetNormal(XMFLOAT3( (v1.x + v2.x + v3.x) / 3 ,
										   (v1.y + v2.y + v3.y) / 3 ,
										   (v1.z + v2.z + v3.z) / 3 ));
		++currentVertex;
	}

	// Normal for west side
	currentVertex = &GetVertices()[nbColRow];
	for(unsigned int  i = nbColRow; i < nbSquare; i += nbColRow)
	{
		unsigned int  a1 = i-nbColRow;
		unsigned int  a2 = i-nbColRow+1;
		unsigned int  a3 = i*nbTriangles;
		XMFLOAT3 v1 = trianglesNormales[a1];
		XMFLOAT3 v2 = trianglesNormales[a2];
		XMFLOAT3 v3 = trianglesNormales[a3];
		currentVertex->SetNormal(XMFLOAT3( (v1.x + v2.x + v3.x) / 3 ,
										   (v1.y + v2.y + v3.y) / 3 ,
										   (v1.z + v2.z + v3.z) / 3 ));
		++currentVertex;
	}

	// Normal for east side
	currentVertex = &GetVertices()[nbColRow + nbSquare];
	for(unsigned int  i = 1; i < nbSquare; i += nbColRow)
	{
		unsigned int  a1 = (i-1)*nbTriangles;
		unsigned int  a2 = (i-1)*nbTriangles+1;
		unsigned int  a3 = (i-1)*nbTriangles;
		XMFLOAT3 v1 = trianglesNormales[a1];
		XMFLOAT3 v2 = trianglesNormales[a2];
		XMFLOAT3 v3 = trianglesNormales[a3];
		currentVertex->SetNormal(XMFLOAT3( (v1.x + v2.x + v3.x) / 3 ,
										   (v1.y + v2.y + v3.y) / 3 ,
										   (v1.z + v2.z + v3.z) / 3 ));
		++currentVertex;
	}

	// Normal for all inner squares
	currentVertex = &GetVertices()[nbColRow+1];
	for(unsigned int i = 1; i < nbSquare; ++i)
	{
		for(unsigned int  j = 1; j < nbSquare; ++j)
		{
			unsigned int  a1 = (i-1)*nbTriangles+j*2-1;
			unsigned int  a2 = (i-1)*nbTriangles+j*2;
			unsigned int  a3 = (i-1)*nbTriangles+j*2+1;
			unsigned int  a4 = i*nbTriangles+j*2-2;
			unsigned int  a5 = i*nbTriangles+j*2-1;
			unsigned int  a6 = i*nbTriangles+j*2;
			XMFLOAT3 v1 = trianglesNormales[a1];
			XMFLOAT3 v2 = trianglesNormales[a2];
			XMFLOAT3 v3 = trianglesNormales[a3];
			XMFLOAT3 v4 = trianglesNormales[a4];
			XMFLOAT3 v5 = trianglesNormales[a5];
			XMFLOAT3 v6 = trianglesNormales[a6];

			currentVertex->SetNormal(XMFLOAT3( (v1.x + v2.x + v3.x + v4.x + v5.x + v6.x) / 6 ,
											   (v1.y + v2.y + v3.y + v4.y + v5.y + v6.y) / 6 ,
											   (v1.z + v2.z + v3.z + v4.z + v5.z + v6.z) / 6 ));
			++currentVertex;
		}
		++currentVertex;
	}
}