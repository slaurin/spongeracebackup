#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "Singleton.h"

class FileManager : public Singleton<FileManager>
{
	friend class Singleton<FileManager>;
	std::list<std::string> keys_;

	FileManager()
		: keys_(std::list<std::string>())
	{}
public:
	void Add(std::string key)
	{
		if(!Contains(key))
			keys_.push_back(key);
	}

	bool Contains(std::string key)
	{
		return std::find(keys_.begin(), keys_.end(), key) != keys_.end();
	}

	void Remove(std::string key)
	{
		if(Contains(key))
			keys_.remove(key);
	}

	std::string& operator[](const std::string key)
	{
		return *std::find(keys_.begin(), keys_.end(), key);
	}
};

#endif