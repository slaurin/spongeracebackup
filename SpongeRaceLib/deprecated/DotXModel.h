#pragma once
#include "Model.h"

class DotXModel: public Model
{
public:
	DotXModel(LPDIRECT3DDEVICE9 aD3dDev, const TCHAR *aPathToDotX, const char *aPathToTextures);
	~DotXModel();

	virtual void render() override;
	PxConvexMesh* convexMeshFromModel();

private:
	class DotXModelImp;
	DotXModelImp *_imp;
};