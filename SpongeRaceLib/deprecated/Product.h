/*
 *  Author : Stephane Laurin
 *  Verifier : 
 *
 *  Currently unused
 */

#ifndef PRODUCT_H
#define PRODUCT_H

template<class T>
class Product
{
public:
    typedef T value_t;
private:
    int &product_;
public:
    Product(value_t& initValue)
        : product_(initValue)
    {}

    void operator()(const value_t &val)
    {
        product_ *= val;
    }
};

#endif