#pragma once

#include "Geometry.h"
#include "GeometryPart.h"

class ShapeTest
{
public:
	ShapeTest(void);
	~ShapeTest(void);

	physx::PxConvexMesh * CreateConvexMesh(GE::Geometry *);
	static physx::PxTriangleMesh * CreateTriangleMesh(GE::GeometryPart *);
};

