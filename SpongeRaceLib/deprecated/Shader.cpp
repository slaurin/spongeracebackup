#include "StdAfx.h"
#include "Shader.h"
#include "Vertex.h"
#include "GraphicEngine.h"
#include "Resource.h"
#include "util.h"
#include "MeshVertex.h"

using namespace UtilitairesDX;

namespace GE
{

	Shader::Shader(Device* pDevice,	const std::wstring & VSFileName, const std::wstring & PSFileName, D3D11_PRIMITIVE_TOPOLOGY topology)
		: uses_(0), device_(pDevice), topology_(topology)
	{
		ID3D11Device *pD3DDevice = device_->GetDXDevice();

		ID3DBlob* pVSBlob = NULL;
		DXEssayer(
			D3DX11CompileFromFile(
				VSFileName.c_str(),
				NULL,
				NULL, 
				"VS1", 
				"vs_4_0", 
				D3DCOMPILE_ENABLE_STRICTNESS, 
				0, 
				NULL, 
				&pVSBlob, 
				NULL,
				0),
			DXE_FICHIER_VS); 

		DXEssayer(
			pD3DDevice->CreateVertexShader(
				pVSBlob->GetBufferPointer(), 
				pVSBlob->GetBufferSize(), 
				NULL, 
				&vertexShader_),
			DXE_CREATION_VS);

		// Cr�er l'organisation des sommets
		vertexLayout_ = NULL;
		DXEssayer(
			pD3DDevice->CreateInputLayout(
				MeshVertex::Begin(), 
				MeshVertex::Size(), 
				pVSBlob->GetBufferPointer(), 
				pVSBlob->GetBufferSize(), 
				&vertexLayout_),
			DXE_CREATIONLAYOUT);

		pVSBlob->Release(); //  On n'a plus besoin du blob

		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(XMMATRIX);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		pD3DDevice->CreateBuffer(&bd, NULL, &constantBuffer_);  

		// Compilation et chargement du pixel shader
		ID3DBlob* pPSBlob = NULL;
		DXEssayer(
			D3DX11CompileFromFile(
				PSFileName.c_str(), 
				NULL,
				NULL,
				"PS1", 
				"ps_4_0", 
				D3DCOMPILE_ENABLE_STRICTNESS, 
				0, 
				NULL, 
				&pPSBlob, 
				NULL, 
				0),
			DXE_FICHIER_PS); 

		DXEssayer(
			pD3DDevice->CreatePixelShader(
				pPSBlob->GetBufferPointer(), 
				pPSBlob->GetBufferSize(), 
				NULL, 
				&pixelShader_),
			DXE_CREATION_PS);

		pPSBlob->Release(); //  On n'a plus besoin du blob
	}

	ID3D11DeviceContext * Shader::Apply(XMMATRIX matWorld, ID3D11Buffer *pVertexBuffer, ID3D11Buffer *pIndexBuffer, UINT offsetVBuf, UINT offsetIBuf, DXGI_FORMAT formatIBuf) const
	{
		// Obtenir le contexte
		ID3D11DeviceContext *pImmediateContext = device_->GetDeviceContext();

		// Choisir la topologie des primitives
		pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// Source des sommets
		UINT stride = sizeof(Vertex);
		pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offsetVBuf);

		// Source des index
		pImmediateContext->IASetIndexBuffer(pIndexBuffer, formatIBuf, offsetIBuf);

		// Input layout des sommets
		pImmediateContext->IASetInputLayout(vertexLayout_);

		// Activer le VS
		pImmediateContext->VSSetShader(vertexShader_, NULL, 0);

		// Initialiser et s�lectionner les �constantes� du VS
		XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();
		XMMATRIX matWorldViewProj = XMMatrixTranspose(matWorld * viewProj);
		pImmediateContext->UpdateSubresource(constantBuffer_, 0, NULL, &matWorldViewProj, 0, 0);
		pImmediateContext->VSSetConstantBuffers(0, 1, &constantBuffer_);
		
		// Activer le PS
		pImmediateContext->PSSetShader(pixelShader_, NULL, 0);

		return pImmediateContext;
	}

	Shader::~Shader(void)
	{
		DXRelacher(pixelShader_);
		DXRelacher(constantBuffer_);
		DXRelacher(vertexLayout_);
		DXRelacher(vertexShader_);
	}

	void Shader::newUse()
	{
		uses_++;
	}

	void Shader::deleteUse()
	{
		// Auto-destruction si plus personne n'utilise ce material
		if(--uses_ < 1)
			delete this;
	}
}