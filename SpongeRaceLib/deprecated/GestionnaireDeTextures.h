#pragma once
#include <vector>
#include "texture.h"
#include "Device.h"

namespace GE
{

class CGestionnaireDeTextures
{
public:
        CGestionnaireDeTextures(void);
        ~CGestionnaireDeTextures(void);
        Texture* const GetNewTexture(const wchar_t* filename_in, Device* pDispositif);
        Texture* const GetTexture(const wchar_t* filename_in);
        void Cleanup();

protected:
        // Le tableau de textures
        std::vector<Texture*> ListeTextures;

};

}