/*
 *  Author : Stephane Laurin
 *  Verifier : 
 *
 *  Currently unused
 */

#ifndef CONCAT_H
#define CONCAT_H

#include <string>
#include <sstream>

class Concat
{
    std::stringstream& stream_;
    std::string delimiter_;
public:
    Concat(std::stringstream& stream,
           const std::string delimiter)
        : stream_(stream),
          delimiter_(delimiter)
    {}

    void operator()(const std::string &val)
    {
        stream_ << val << delimiter_;
    }
};

#endif