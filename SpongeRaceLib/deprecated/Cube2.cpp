#include "stdafx.h"
#include "Cube2.h"
#include "Vertex.h"
#include "DxUtilities.h"
#include "resource.h"
#include "GraphicEngine.h"
#include "Effect.h"
#include <vector>

using namespace DxUtilities;
using std::vector;
using std::begin;
using std::end;
/*
namespace GE
{
	const unsigned int Cube2::indexList_[] = {
		 0,  1,  2,		// devant
		 0,  2,  3,		// devant
		 5,  6,  7,		// arri�re
		 5,  7,  4,		// arri�re
		 8,  9, 10,		// dessous
		 8, 10, 11,		// dessous
		13, 14, 15,		// dessus
		13, 15, 12,		// dessus
		19, 16, 17,		// gauche
		19, 17, 18,		// gauche
		20, 21, 22,		// droite
		20, 22, 23		// droite
	};


	//  FONCTION : Cube2, constructeur param�tr� 
	//  BUT :	Constructeur d'une classe de bloc
	//  PARAM�TRES:		
	//		dx, dy, dz:	dimension en x, y, et z
	//		device_: pointeur sur notre objet dispositif
	Cube2::Cube2(IEffect *effect, MiniPhongMat *pMaterial, float dx, float dy, float dz)
		: Object3D<MeshVertex, MiniPhongMat>(effect, pMaterial), rotation_(float())
	{
		// Les points
		float hx(dx / 2.f), hy(dy / 2.f), hz(dz / 2.f);
		XMFLOAT3 point[] = {
			XMFLOAT3(-hx,  hy, -hz),
			XMFLOAT3( hx,  hy, -hz),
			XMFLOAT3( hx, -hy, -hz),
			XMFLOAT3(-hx, -hy, -hz),
			XMFLOAT3(-hx,  hy,  hz),
			XMFLOAT3(-hx, -hy,  hz),
			XMFLOAT3( hx, -hy,  hz),
			XMFLOAT3( hx,  hy,  hz)
		};

		// Calculer les normales
		XMFLOAT3 n0( 0.0f, 0.0f,-1.0f); // devant
		XMFLOAT3 n1( 0.0f, 0.0f, 1.0f); // arri�re
		XMFLOAT3 n2( 0.0f,-1.0f, 0.0f); // dessous
		XMFLOAT3 n3( 0.0f, 1.0f, 0.0f); // dessus
		XMFLOAT3 n4(-1.0f, 0.0f, 0.0f); // face gauche
		XMFLOAT3 n5( 1.0f, 0.0f, 0.0f); // face droite

		// Vertices
		// Le devant du bloc (0 - 3)
		vertices_.push_back(MeshVertex(point[0], n0, XMFLOAT2(0, 0)));
		vertices_.push_back(MeshVertex(point[1], n0, XMFLOAT2(1, 0)));
		vertices_.push_back(MeshVertex(point[2], n0, XMFLOAT2(1, 1)));
		vertices_.push_back(MeshVertex(point[3], n0, XMFLOAT2(0, 1)));

		// L'arri�re du bloc (5 - 8)
		vertices_.push_back(MeshVertex(point[4], n1, XMFLOAT2(0, 0)));
		vertices_.push_back(MeshVertex(point[5], n1, XMFLOAT2(1, 0)));
		vertices_.push_back(MeshVertex(point[6], n1, XMFLOAT2(1, 1)));
		vertices_.push_back(MeshVertex(point[7], n1, XMFLOAT2(0, 1)));

		// Le dessous du bloc (9 - 12)
		vertices_.push_back(MeshVertex(point[3], n2, XMFLOAT2(0, 0)));
		vertices_.push_back(MeshVertex(point[2], n2, XMFLOAT2(1, 0)));
		vertices_.push_back(MeshVertex(point[6], n2, XMFLOAT2(1, 1)));
		vertices_.push_back(MeshVertex(point[5], n2, XMFLOAT2(0, 1)));

		// Le dessus du bloc (13 - 16)
		vertices_.push_back(MeshVertex(point[0], n3, XMFLOAT2(0, 0)));
		vertices_.push_back(MeshVertex(point[4], n3, XMFLOAT2(1, 0)));
		vertices_.push_back(MeshVertex(point[7], n3, XMFLOAT2(1, 1)));
		vertices_.push_back(MeshVertex(point[1], n3, XMFLOAT2(0, 1)));

		// La face gauche (17 - 20)
		vertices_.push_back(MeshVertex(point[0], n4, XMFLOAT2(0, 0)));
		vertices_.push_back(MeshVertex(point[3], n4, XMFLOAT2(1, 0)));
		vertices_.push_back(MeshVertex(point[5], n4, XMFLOAT2(1, 1)));
		vertices_.push_back(MeshVertex(point[4], n4, XMFLOAT2(0, 1)));

		// La face droite (21 - 24)
		vertices_.push_back(MeshVertex(point[1], n5, XMFLOAT2(0, 0)));
		vertices_.push_back(MeshVertex(point[7], n5, XMFLOAT2(1, 0)));
		vertices_.push_back(MeshVertex(point[6], n5, XMFLOAT2(1, 1)));
		vertices_.push_back(MeshVertex(point[2], n5, XMFLOAT2(0, 1)));

		CreateVertexBuffer();

		indexes_ = vector<index_t>(begin(indexList_), end(indexList_));
		CreateIndexBuffer();
	}

	void Cube2::Anime(float timeElapsed)
	{
		rotation_ +=  ((XM_PI * 2.0f) / 3.0f * timeElapsed);

		worldMatrix_._11 = 1;
		worldMatrix_._12 = 0;
		worldMatrix_._13 = 0;

		worldMatrix_._21 = 0;
		worldMatrix_._22 = 1;
		worldMatrix_._23 = 0;

		worldMatrix_._31 = 0;
		worldMatrix_._32 = 0;
		worldMatrix_._33 = 1;

		worldMatrix_ *=  XMMatrixRotationX(rotation_);
	}
}*/