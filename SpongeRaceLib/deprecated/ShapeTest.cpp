﻿#include "stdafx.h"
#include "ShapeTest.h"
#include "SpongeRace.h"
#include "Convert.h"

using namespace std;
using namespace physx;
using namespace GE;

ShapeTest::ShapeTest(void)
{
}


ShapeTest::~ShapeTest(void)
{
}

PxConvexMesh* ShapeTest::CreateConvexMesh(Geometry * geometry)
{
	/*
	unsigned int   mVertexCount = 0; 
	unsigned int   mIndexCount  = 0; 
	size_t         vertex_count; 
	Ogre::Vector3*   vertices; 
	size_t         index_count; 
	unsigned long*   indices; 
	bool added_shared = false; 
	vertex_count = 0; 
	index_count = 0; 
	size_t current_offset = 0; 
	size_t shared_offset = 0; 
	size_t next_offset = 0; 
	size_t index_offset = 0; 
	for ( unsigned short i = 0; i < pEntity->getMesh()->getNumSubMeshes(); ++i) { 
		Ogre::SubMesh* submesh = pEntity->getMesh()->getSubMesh( i ); 
		if(submesh->useSharedVertices) { 
			if( !added_shared ) { 
				mVertexCount += pEntity->getMesh()->sharedVertexData->vertexCount; 
				added_shared = true; 
			} 
		} 
		else { 
			mVertexCount += submesh->vertexData->vertexCount; 
		} 
		mIndexCount += submesh->indexData->indexCount; 
	} 

	vertices = new Ogre::Vector3[mVertexCount]; 
	indices = new unsigned long[mIndexCount]; 

	physx::PxVec3* mMeshVertices = new physx::PxVec3[mVertexCount]; 
	physx::PxU32* mMeshFaces = new physx::PxU32[mIndexCount]; 

	added_shared = false; 

	for ( unsigned short i = 0; i < pEntity->getMesh()->getNumSubMeshes();i++) { 

		Ogre::SubMesh* submesh = pEntity->getMesh()->getSubMesh(i); 
		Ogre::VertexData* vertex_data=submesh->useSharedVertices ? pEntity->getMesh()->sharedVertexData:submesh->vertexData; 

		if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared)) 
		{ 
			if(submesh->useSharedVertices) 
			{ 
				added_shared = true; 
				shared_offset = current_offset; 
			} 

			const Ogre::VertexElement* posElem = vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION); 
			Ogre::HardwareVertexBufferSharedPtr vbuf = vertex_data->vertexBufferBinding->getBuffer(posElem->getSource()); 
			unsigned char* vertex = static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY)); 

			float* pReal; 

			for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize()) { 
				posElem->baseVertexPointerToElement(vertex, &pReal); 
				mMeshVertices[current_offset + j] = physx::PxVec3(pReal[0],pReal[1],pReal[2]); 
			} 

			vbuf->unlock(); 
			next_offset += vertex_data->vertexCount; 
		} 

		Ogre::IndexData* index_data = submesh->indexData; 

		size_t numTris = index_data->indexCount / 3; 
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer; 

		bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT); 


		if ( use32bitindexes )   { 

			unsigned int*  pInt = static_cast<unsigned int*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY)); 

			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset; 


			for ( size_t k = 0; k < numTris*3; ++k) { 
				mMeshFaces[index_offset] = pInt[k] + static_cast<unsigned int>(offset); 

			} 
		} else { 
			unsigned short* pShort = reinterpret_cast<unsigned short*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY)); 
			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset; 

			for ( size_t k = 0; k < numTris*3; ++k) { 
				mMeshFaces[index_offset] = static_cast<unsigned int>(pShort[k]) + static_cast<unsigned int>(offset); 


			} 

		} 

		ibuf->unlock(); 
		current_offset = next_offset; 
	} 
	physx::PxConvexMeshDesc convexDesc;

	convexDesc.points.count            = mVertexCount; 
	convexDesc.points.data             = mMeshVertices; 
	convexDesc.points.stride           = sizeof(PxVec3); 

	convexDesc.triangles.count          = mIndexCount / 3; 
	convexDesc.triangles.stride         = 3 * sizeof(PxU32); 
	convexDesc.triangles.data           = mMeshFaces; 
	convexDesc.flags                    = PxConvexFlag::eCOMPUTE_CONVEX;



	PxCooking *cooking = PxCreateCooking(PX_PHYSICS_VERSION, &mSDK->getFoundation(), PxCookingParams());
	MemoryWriteBuffer outputStream;
	bool status = cooking->cookConvexMesh(convexDesc, outputStream);
	physx::PxConvexMesh* convexMesh = mSDK->createConvexMesh(MemoryReadBuffer(outputStream.data));
	cooking->release();


	if (!actor2)
		MessageBox(FindWindow("OgreD3D9Wnd","OGRE"),"Actor не загрузилось!","Ошибка!",MB_OK | MB_ICONERROR | MB_TASKMODAL);
	delete []vertices;
	delete []indices;
	delete []mMeshVertices;
	delete []mMeshFaces;
	return convexMesh;*/
	return nullptr;
}

PxTriangleMesh * ShapeTest::CreateTriangleMesh(GE::GeometryPart * geometryPart) 
{
	/*
	unsigned int mVertexCount = 0; 
	unsigned long mIndexCount = 0; 
	size_t vertex_count; 
	Ogre::Vector3* vertices; 
	size_t index_count; 
	unsigned long* indices; 
	bool added_shared = false; 
	//vertex_count = 0; 
	//index_count = 0; 
	size_t current_offset = 0; 
	size_t shared_offset = 0; 
	size_t next_offset = 0; 
	size_t index_offset = 0; 

	for(unsigned short i = 0; i < geometry->getMesh()->getNumSubMeshes(); ++i) { 
		Ogre::SubMesh* submesh = geometry->getMesh()->getSubMesh( i ); 

		if(submesh->useSharedVertices) { 
			if( !added_shared ) { 
				mVertexCount += geometry->getMesh()->sharedVertexData->vertexCount; 
				added_shared = true; 
			} 
		} 
		else { 
			mVertexCount += submesh->vertexData->vertexCount; 
		} 

		mIndexCount += submesh->indexData->indexCount; 
	}

	

	vertices = new Ogre::Vector3[mVertexCount]; 
	indices = new unsigned long[mIndexCount]; 

	PxVec3* mMeshVertices = new PxVec3[mVertexCount]; 
	PxU32* mMeshFaces = new PxU32[mIndexCount]; 

	added_shared = false; 

	for ( unsigned short i = 0; i < geometry->getMesh()->getNumSubMeshes();i++) { 

		Ogre::SubMesh* submesh = geometry->getMesh()->getSubMesh(i); 
		Ogre::VertexData* vertex_data=submesh->useSharedVertices ? geometry->getMesh()->sharedVertexData:submesh->vertexData; 

		if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared)) 
		{ 
			if(submesh->useSharedVertices) 
			{ 
				added_shared = true; 
				shared_offset = current_offset; 
			} 

			const Ogre::VertexElement* posElem = vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION); 
			Ogre::HardwareVertexBufferSharedPtr vbuf = vertex_data->vertexBufferBinding->getBuffer(posElem->getSource()); 
			unsigned char* vertex = static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY)); 

			float* pReal; 

			for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize()) { 
				posElem->baseVertexPointerToElement(vertex, &pReal); 
				mMeshVertices[current_offset + j] = PxVec3(pReal[0],pReal[1],pReal[2]); 
			} 

			vbuf->unlock(); 
			next_offset += vertex_data->vertexCount; 
		} 

		Ogre::IndexData* index_data = submesh->indexData; 

		size_t numTris = index_data->indexCount / 3; 
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer; 

		bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT); 


		if ( use32bitindexes ) { 

			unsigned int* pInt = static_cast<unsigned int*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY)); 

			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset; 


			for ( size_t k = 0; k < numTris*3; ++k) { 
				mMeshFaces[index_offset] = pInt[k] + static_cast<unsigned int>(offset); 

			} 
		} 
		else { 
			unsigned short* pShort = reinterpret_cast<unsigned short*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY)); 
			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset; 

			for ( size_t k = 0; k < numTris*3; ++k) { 
				mMeshFaces[index_offset] = static_cast<unsigned int>(pShort[k]) + static_cast<unsigned int>(offset); 


			} 

		} 

		ibuf->unlock(); 
		current_offset = next_offset; 
	} 

	PxTriangleMeshDesc meshDesc;

	meshDesc.points.count      = mVertexCount; 
	meshDesc.points.data       = mMeshVertices; 
	meshDesc.points.stride     = 4*3; 

	meshDesc.triangles.count   = mIndexCount / 3; 
	meshDesc.triangles.stride  = 4*3; 
	//meshDesc.triangles.data    = mMeshFaces;

	delete []vertices;
	delete []indices;
	delete []mMeshVertices;
	delete []mMeshFaces;
	*/

	auto vertices = geometryPart->vertices();
	PxU32 nbVertices = vertices.size();

	vector<PxVec3> mMeshVertices(nbVertices); 
	vector<PxVec3>::size_type i = 0;
	for(auto it = vertices.begin(); it != vertices.end(); ++it, ++i)
	{
		mMeshVertices[i] = Convert::XMFloatToPxVec3(it->position());
	}
	
	auto indexes = geometryPart->indexes();
	PxU32 nbIndexes = indexes.size();
	PxU32 nbFaces = nbIndexes / 3;
	/*vector<PxU32> mMeshFaces(nbFaces);
	vector<PxU32>::size_type j = 0;
	for(auto it = indexes.begin(); it != indexes.end(); ++it, ++j)
	{
		mMeshFaces[j] = *it
	}*/

	PxTriangleMeshDesc meshDesc;

	meshDesc.points.count      = nbVertices; 
	meshDesc.points.data       = &mMeshVertices.front();
	meshDesc.points.stride     = 4*3; 

	meshDesc.triangles.count   = nbFaces; 
	meshDesc.triangles.stride  = 4*3;
	meshDesc.triangles.data    = &indexes.front();//&mMeshFaces.front();


	// Export
	PxDefaultMemoryOutputStream outputStream;
	bool status = GAME->cooking()->cookTriangleMesh(meshDesc, outputStream);

	// Import
	PxDefaultMemoryInputData inputData(outputStream.getData(), outputStream.getSize());
	PxTriangleMesh* triangleMesh = GAME->physics()->createTriangleMesh(inputData);

	return triangleMesh;
}
