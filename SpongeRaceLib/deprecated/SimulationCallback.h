#ifndef SIMULATIONCALLBACK_H
#define SIMULATIONCALLBACK_H

#include "PxPhysicsAPI.h"
#include "Actor.h"
#include "PlayerActor.h"

class SimulationCallback
	: public physx::PxSimulationEventCallback
{
	/*
	std::vector<Actor> &objLst_;

	SimulationCallback(std::vector<Actor> &objLst) :
	objLst_(objLst)
	{}
	*/

	virtual void onConstraintBreak( physx::PxConstraintInfo* constraints, physx::PxU32 count ) 
	{
		printf("onConstraintBreak\n");
	}

	virtual void onWake( physx::PxActor** actors, physx::PxU32 count ) 
	{
		printf("onWake\n");
	}

	virtual void onSleep( physx::PxActor** actors, physx::PxU32 count ) 
	{
		printf("onSleep\n");
	}

	virtual void onContact( const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs ) 
	{

		printf("onContact\n");
		ActorBase* a1 = static_cast<ActorBase*>(pairHeader.actors[0]->userData);
		ActorBase* a2 = static_cast<ActorBase*>(pairHeader.actors[1]->userData);
		/*
		if( a1->isBullet() && a2->isAlien())
		{
			GAME->gameover = true;
			AlienActor* alien = static_cast<AlienActor*>(getAlien()->userData);
			alien->die();
		}
		else if( a1->isAlien() && a2->isBullet() )
		{
			GAME->gameover = true;
			AlienActor* alien = static_cast<AlienActor*>(getAlien()->userData);
			alien->die();
		}
		else if( a1->isPlayer() && a2->isAlien())
		{
			GAME->gameover = true;
			PlayerActor* player = static_cast<PlayerActor*>(getPlayer()->userData);
			player->die();
		}
		else if( a1->isAlien() && a2->isPlayer())
		{
			GAME->gameover = true;
			PlayerActor* player = static_cast<PlayerActor*>(getPlayer()->userData);
			player->die();
		}
		*/
	}

	virtual void onTrigger( physx::PxTriggerPair* pairs, physx::PxU32 count ) 
	{
		printf("onTrigger\n");
	}
	
};
#endif