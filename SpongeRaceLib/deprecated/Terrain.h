#ifndef TERRAIN_H
#define TERRAIN_H

#include "Vertex.h"
#include "Object3D.h"

#include <vector>
#include <algorithm>

namespace GE
{
    class Terrain// : public GE::Object3D
    {
    private:
        std::vector<Vertex> vertices_;

    public:
        Terrain()
            : vertices_(std::vector<Vertex>())
        {}
        Terrain(std::vector<unsigned char> &data);

        // virtual void Anime(float timeElapsed) {}

        //void Draw() override{}

        std::vector<Vertex> &GetVertices()
        {
            return vertices_;
        }
    };
}
#endif