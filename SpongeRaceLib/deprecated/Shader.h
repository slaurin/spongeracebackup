#pragma once

#include "Device.h"
#include <string>

namespace GE
{
	class Shader
	{
	public:
		Shader(
			Device *pDispositif,
			const std::wstring & VSFileName = L"..\\SpongeRace\\Ressources\\Shaders\\vs1.vhl",
			const std::wstring & PSFileName = L"..\\SpongeRace\\Ressources\\Shaders\\ps1.phl",
			D3D11_PRIMITIVE_TOPOLOGY topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
		);

		~Shader(void);

		ID3D11DeviceContext * Apply(
			XMMATRIX matWorld,
			ID3D11Buffer *pVertexBuffer,
			ID3D11Buffer *pIndexBuffer,
			UINT offsetVBuf = 0,
			UINT offsetIBuf = 0,
			DXGI_FORMAT formatIBuf = DXGI_FORMAT_R32_UINT
		) const;

		void newUse();
		void deleteUse();

	private:
		unsigned int				uses_;
		Device						*device_;
		D3D11_PRIMITIVE_TOPOLOGY	topology_;
		ID3D11VertexShader			*vertexShader_;
		ID3D11PixelShader			*pixelShader_;
		ID3D11InputLayout			*vertexLayout_;
		ID3D11Buffer				*constantBuffer_;
	};

}