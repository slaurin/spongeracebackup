#include "stdafx.h"
#include "Resource.h"
#include "Device.h"
#include "DxUtilities.h"
#include "DeviceInfo.h"
#include "Window.h"
#include "LogManager.h"

using namespace DxUtilities;

namespace GE
{
	Device::~Device(void)
	{
		if(deviceContext_)
			deviceContext_->ClearState();

		DXRelacher(solidCullBackRS_);
		DXRelacher(wireCullBackRS_);
		DXRelacher(depthStencilView_);
		DXRelacher(depthTexture_);
		DXRelacher(renderTargetView_);		
		DXRelacher(deviceContext_);
		DXRelacher(swapChain_);
		DXRelacher(device_);
	}

	Device::Device(const WINDOW_MODE windowMode)
		: deviceContext_(NULL), swapChain_(NULL), renderTargetView_(NULL), wireFrame_(false), numViewPorts_(1)
	{
		UINT createDeviceFlags = 0;
		HWND hWnd = Window::getHMainWnd();

		#ifdef _DEBUG
			createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
		#endif

		D3D_FEATURE_LEVEL featureLevels[] =	{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};

		UINT numFeatureLevels = ARRAYSIZE(featureLevels);

		// R�cup�rer les informations sur la dispo voulue
		desc_.Format					= DXGI_FORMAT_R8G8B8A8_UNORM;
		desc_.Height					= static_cast<UINT>(Window::GetInstance().GetScreenHeight());
		desc_.Width						= static_cast<UINT>(Window::GetInstance().GetScreenWidth());
		desc_.RefreshRate.Numerator		= 60;
		desc_.RefreshRate.Denominator	= 1;
		desc_.ScanlineOrdering			= DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		desc_.Scaling					= DXGI_MODE_SCALING_UNSPECIFIED;

		DeviceInfo DispoVoulu(desc_);
		DispoVoulu.GetDesc(desc_);
		Window::GetInstance().SetScreenDimensions(desc_.Width, desc_.Height);

		// Configuration du SwapChain
		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));

		sd.BufferCount = 1;
		sd.BufferDesc.Width = desc_.Width;
		sd.BufferDesc.Height = desc_.Height;
		sd.BufferDesc.Format = desc_.Format;
		sd.BufferDesc.RefreshRate.Numerator = desc_.RefreshRate.Numerator;
		sd.BufferDesc.RefreshRate.Denominator = desc_.RefreshRate.Denominator;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH; // Permettre l'�change plein �cran

		switch(windowMode)
		{
		case WINDOWED:
			sd.Windowed = TRUE;
			break;

		case FULLSCREEN:
			sd.Windowed = FALSE;
			break;
		}	

		// R�gler le probl�me no 1 du passage en mode fen�tr�
		RECT rcClient, rcWindow;
		POINT ptDiff;
		GetClientRect(hWnd, &rcClient);
		GetWindowRect(hWnd, &rcWindow);
		ptDiff.x = (rcWindow.right - rcWindow.left) - rcClient.right;
		ptDiff.y = (rcWindow.bottom - rcWindow.top) - rcClient.bottom;
		MoveWindow(hWnd,rcWindow.left, rcWindow.top, desc_.Width + ptDiff.x, desc_.Height + ptDiff.y, TRUE);

		DXEssayer(
			D3D11CreateDeviceAndSwapChain(
				0, 
				D3D_DRIVER_TYPE_HARDWARE, 
				NULL, 
				createDeviceFlags, 
				featureLevels,
				numFeatureLevels,
				D3D11_SDK_VERSION, 
				&sd, 
				&swapChain_, 
				&device_, 
				NULL, 
				&deviceContext_),
			DXE_ERREURCREATIONDEVICE);

		// Initialiser le tampon de profondeur
		createDepthBuffer();

		// Cr�ation d'un �render target view�
		createRenderTargetView();
		
		// Initialiser les �tats de m�lange (blending states)
		initBlendStates();

		deviceContext_->OMSetRenderTargets(1, &renderTargetView_, depthStencilView_);

		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)desc_.Width;
		vp.Height = (FLOAT)desc_.Height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		deviceContext_->RSSetViewports(numViewPorts_, &vp);

		// Cr�ation et initialisation des �tats
		D3D11_RASTERIZER_DESC rsDesc;
		ZeroMemory(&rsDesc, sizeof(D3D11_RASTERIZER_DESC));
		rsDesc.FillMode = D3D11_FILL_SOLID;
		rsDesc.CullMode = D3D11_CULL_BACK;// FRONT, BACK ou NONE
		rsDesc.FrontCounterClockwise = false;
		device_->CreateRasterizerState(&rsDesc, &solidCullBackRS_);
		rsDesc.FillMode = D3D11_FILL_WIREFRAME;
		device_->CreateRasterizerState(&rsDesc, &wireCullBackRS_);

		deviceContext_->RSSetState(solidCullBackRS_);
	}

	void Device::SwitchRasterizer()
	{
		if(wireFrame_)
			deviceContext_->RSSetState(solidCullBackRS_);
		else
			deviceContext_->RSSetState(wireCullBackRS_);
		wireFrame_ = !wireFrame_;
	}

	void Device::Resize(int width, int height)
	{
		if(swapChain_)
		{
			// R�cup�rer les informations sur la dispo voulue
			desc_.Height	= static_cast<UINT>(height);
			desc_.Width		= static_cast<UINT>(width);
			DeviceInfo DispoVoulu(desc_);
			DispoVoulu.GetDesc(desc_);
			
			// Release all outstanding references to the swap chain's buffers.
			deviceContext_->OMSetRenderTargets(0, 0, 0);
			renderTargetView_->Release();

			// Preserve the existing buffer count and format.
			// Automatically choose the width and height to match the client rect for HWNDs.
			DXEssayer(
				swapChain_->ResizeBuffers(0, desc_.Width, desc_.Height, DXGI_FORMAT_UNKNOWN, 0),
				DXE_ERREURCREATIONDEVICE
				);

			// Initialiser le tampon de profondeur
			createDepthBuffer();

			// Cr�ation d'un �render target view�
			createRenderTargetView();

			// Mise � jour du viewport
			D3D11_VIEWPORT vp;
			deviceContext_->RSGetViewports(&numViewPorts_, &vp);
			vp.Height = static_cast<float>(desc_.Height);
			vp.Width = static_cast<float>(desc_.Width);
			deviceContext_->RSSetViewports(numViewPorts_, &vp);

			Window::GetInstance().SetScreenDimensions(desc_.Width, desc_.Height);
		}
	}

	void Device::Present()
	{
		swapChain_->Present(0, 0);
	}

	void Device::SetRenderTargetView(ID3D11RenderTargetView* pRenderTargetView_in,
		ID3D11DepthStencilView* pDepthStencilView_in)
	{
		renderTargetView_ = pRenderTargetView_in;
		depthStencilView_ = pDepthStencilView_in;
		ID3D11RenderTargetView* tabRTV[1];
		tabRTV[0] = renderTargetView_;
		deviceContext_->OMSetRenderTargets( 1,
			tabRTV,
			depthStencilView_);
	}

	void Device::createRenderTargetView()
	{
		// Cr�ation d'un �render target view�
		ID3D11Texture2D *pBackBuffer;
		DXEssayer(
			swapChain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer),
			DXE_ERREUROBTENTIONBUFFER) ;

		DXEssayer(
			device_->CreateRenderTargetView(pBackBuffer, NULL, &renderTargetView_), 
			DXE_ERREURCREATIONRENDERTARGET);
		pBackBuffer->Release();

		deviceContext_->OMSetRenderTargets(numViewPorts_, &renderTargetView_, depthStencilView_);
	}

	void Device::createDepthBuffer()
	{
		D3D11_TEXTURE2D_DESC depthTextureDesc;
		ZeroMemory(&depthTextureDesc, sizeof(depthTextureDesc));
	
		depthTextureDesc.Width				= static_cast<UINT>(desc_.Width);
		depthTextureDesc.Height				= static_cast<UINT>(desc_.Height);
		depthTextureDesc.MipLevels			= 1;
		depthTextureDesc.ArraySize			= 1;
		depthTextureDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthTextureDesc.SampleDesc.Count	= 1;
		depthTextureDesc.SampleDesc.Quality	= 0;
		depthTextureDesc.Usage				= D3D11_USAGE_DEFAULT;
		depthTextureDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
		depthTextureDesc.CPUAccessFlags		= 0;
		depthTextureDesc.MiscFlags			= 0;

		DXEssayer(
			device_->CreateTexture2D(&depthTextureDesc, NULL, &depthTexture_),
			DXE_ERREURCREATIONTEXTURE);

		// Cr�ation de la vue du tampon de profondeur (ou de stencil)
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSView;
		ZeroMemory(&descDSView, sizeof(descDSView));
		
		descDSView.Format				= depthTextureDesc.Format;
		descDSView.ViewDimension		= D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSView.Texture2D.MipSlice	= 0;
		
		DXEssayer(
			device_->CreateDepthStencilView(depthTexture_, &descDSView, &depthStencilView_), 
			DXE_ERREURCREATIONDEPTHSTENCILTARGET);
	}

	void Device::initBlendStates()
	{
		D3D11_BLEND_DESC blendDesc;

		// Effacer la description -  rappel 0 = valeur de d�faut
		ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));

		// On initialise la description pour un m�lange alpha classique
		blendDesc.RenderTarget[0].BlendEnable			= TRUE;
		blendDesc.RenderTarget[0].SrcBlend				= D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend				= D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp				= D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha			= D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha		= D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha			= D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;

		// On cr�� l'�tat alphaBlendEnable_
		DXEssayer(
			device_->CreateBlendState(&blendDesc, &alphaBlendEnable_),
			DXE_ERREURCREATION_BLENDSTATE);

		// Seul le booleen BlendEnable n�cessite d'�tre modifi�
		blendDesc.RenderTarget[0].BlendEnable = FALSE;

		// On cr�� l'�tat alphaBlendDisable_ 
		DXEssayer(
			device_->CreateBlendState(&blendDesc, &alphaBlendDisable_),
			DXE_ERREURCREATION_BLENDSTATE);
	}

	void Device::EnableAlphaBlend()
	{
		float factor[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	
		// Activer le m�lange - alpha blending.
		deviceContext_->OMSetBlendState(alphaBlendEnable_, factor, 0xffffffff);	
	}

	void Device::DisableAlphaBlend()
	{
		float factor[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	
		// D�sactiver le m�lange - alpha blending.
		deviceContext_->OMSetBlendState(alphaBlendDisable_, factor, 0xffffffff);
	}
}

