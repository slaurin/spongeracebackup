#include "stdafx.h"
#include "AITask.h"
#include "SpongeRace.h"
#include "DynamicActor.h"


using namespace physx;

void AITask::update()
{
	

	if(GAME->isPause()) return;

	PxActorTypeSelectionFlags desiredTypes = PxActorTypeSelectionFlag::eRIGID_DYNAMIC;
	PxU32 count = GAME->scene()->getNbActors(desiredTypes);
	PxActor** buffer = new PxActor*[count];
	GAME->scene()->getActors(desiredTypes, buffer, count);
	for(PxU32 i = 0; i < count; i++)
	{

		if (buffer[i]->getName()) {
			std::string tmp = std::string(buffer[i]->getName());
			if (tmp=="Bubble")   //skip actors owned by foreign clients
			{		
				// further process this actor
				DynamicActor* actor = static_cast<DynamicActor*>(buffer[i]->userData);
				if(actor->IsDead()) {
					GAME->spawnManager()->unspawn(actor);
				} else {
					actor->update(LoopTime::GetInstance().ElapsedTimeSeconds());
				}
			}
		}
	}
	delete buffer;
}
