#pragma once

//=============================================================================
// CLASS GameTask
//=============================================================================
class GameTask
{
public:
	virtual ~GameTask() {}
public:
	virtual void init()  {}
	virtual void cleanup() = 0;
	virtual void update() {}
};