#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include "SpongeRace.h"
#include "IRessourceManager.h"
#include "Actor.h"
#include "ObstacleActor.h"
#include "RaceTrack.h"
#include "Singleton.h"

#include <string>

class EntityManager
	: public IRessourceManager<EntityManager, std::string, ActorBase*>
{
	friend class Singleton<EntityManager>;
public:

	ActorBase * RequestObstacleActor(const std::string &resourceName,
		PxVec3 position,
		physx::PxTriangleMesh *physxStaticMesh,
		GE::GraphicObject* graphicObj)
	{
		if(!Contains(resourceName))
			add(resourceName, GAME->spawnManager()->spawn<ObstacleActor>(PxTransform(position), physxStaticMesh, graphicObj));
		return operator[](resourceName);
	}

	ActorBase * RequestRaceTrack(const std::string &resourceName,
		PxVec3 position,
		physx::PxTriangleMesh *physxStaticMesh,
		GE::GraphicObject* graphicObj)
	{
		if(!Contains(resourceName))
			add(resourceName, GAME->spawnManager()->spawn<RaceTrack>(PxTransform(position), physxStaticMesh, graphicObj));
		return operator[](resourceName);
	}

	ActorBase* Request(const std::string &resourceName)
	{
	}

	//! Retire la cl� fournie (si elle est pr�sente)
	void Remove(std::string key) 
	{
		if(Contains(key)){
			GAME->spawnManager()->unspawn(operator[](key));
			IRessourceManager::Remove(key);
		}
	}

};

#endif