#pragma once

#include "Texture.h"
#include <Gdiplus.h>

namespace GE
{
	class TextDrawer
	{
		unsigned int texWidth_;
		unsigned int texHeight_;
		unsigned int pitchMem_;
		Texture * texture_;

		Gdiplus::Color bgColor_;
		Gdiplus::Font * font_;
		Gdiplus::Bitmap * bitmap_;
		Gdiplus::Graphics * graphics_;
		Gdiplus::SolidBrush * brush_;

	public:
		TextDrawer(
			unsigned int width, 
			unsigned int height,
			Gdiplus::Font * font,
			const Gdiplus::Color & brushColor = Gdiplus::Color(255, 0, 0, 0),// Noir opaque
			const Gdiplus::Color & bgColor = Gdiplus::Color(255, 255, 255, 255));// Blanc opaque
		~TextDrawer();

		void Write(const std::wstring & text, const Gdiplus::PointF & origin = Gdiplus::PointF(0.0f, 0.0f), Gdiplus::Image * image = nullptr);
		Texture * GetTexture();
	};
}

