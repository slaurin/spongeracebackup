//=============================================================================
// EXTERNAL DECLARATIONS
//=============================================================================
#include "stdafx.h"
#include "SpawnManager.h"
#include "ActorFactory.h"

using namespace physx;
using namespace GE;

//=============================================================================
// CLASS SpawnManager::SpawnManagerImp
//=============================================================================
class SpawnManager::SpawnManagerImp
{
public:
	std::set<IActor*> _unspawnList;
	std::set<IActor*> _existingActors;

};

//=============================================================================
// CLASS SpawnManager
//=============================================================================
SpawnManager::SpawnManager()
	: _imp(new SpawnManagerImp)
{}

//-----------------------------------------------------------------------------
//
SpawnManager::~SpawnManager()
{
	delete _imp;
}

//-----------------------------------------------------------------------------
//
void SpawnManager::update()
{
	// copy, in case something is unspawned while we unspawn...
	std::set<IActor*> unspawnList = _imp->_unspawnList;
	_imp->_unspawnList.clear();

	for (auto iter = unspawnList.begin(); iter != unspawnList.end(); ++iter)
	{
		(*iter)->onUnspawn();
		delete (*iter);
	}
}

//-----------------------------------------------------------------------------
//
void SpawnManager::unspawn(IActor *aActorToUnspawn)
{
	_imp->_unspawnList.insert(aActorToUnspawn);
	auto foundIter = _imp->_existingActors.find(aActorToUnspawn);
	if (foundIter != _imp->_existingActors.end())
		_imp->_existingActors.erase(foundIter);
}

//-----------------------------------------------------------------------------
//
void SpawnManager::unspawnAll()
{
	_imp->_unspawnList.insert(_imp->_existingActors.begin(), _imp->_existingActors.end());
	_imp->_existingActors.clear();
}

//-----------------------------------------------------------------------------
//
IActor* SpawnManager::spawn(const ActorFactory::ActorID &aActorID, const PxTransform &aPose, GraphicObject* obj)
{
	IActor *actor = ActorFactory::createInstance(aActorID);
	if(obj)
		actor->setGraphicObj(obj);
	actor->onSpawn(aPose);
	_imp->_existingActors.insert(actor);
	return actor;
}


//-----------------------------------------------------------------------------
//
IActor* SpawnManager::spawn(const ActorFactory::ActorID &aActorID, const physx::PxTransform &aPose, physx::PxGeometry* mesh)
{
	IActor *actor = ActorFactory::createInstance(aActorID);
	
	actor->onSpawn(aPose, mesh);
	_imp->_existingActors.insert(actor);
	return actor;
}
//-----------------------------------------------------------------------------
//
IActor* SpawnManager::spawn(const ActorFactory::ActorID &aActorID, const PxTransform &aPose, PxConvexMesh* convexMesh, GraphicObject* obj, const PxMeshScale & scaling)
{
	IActor *actor = ActorFactory::createInstance(aActorID);	
	if(obj)
		actor->setGraphicObj(obj);
	actor->onSpawn(aPose, new PxConvexMeshGeometry(convexMesh, scaling));
	_imp->_existingActors.insert(actor);
	return actor;
}

//-----------------------------------------------------------------------------
//
IActor* SpawnManager::spawn(const ActorFactory::ActorID &aActorID, const PxTransform &aPose, PxTriangleMesh* triangleMesh, GraphicObject* obj, const PxMeshScale & scaling)
{
	IActor *actor = ActorFactory::createInstance(aActorID);	
	if(obj)
		actor->setGraphicObj(obj);
	actor->onSpawn(aPose, new PxTriangleMeshGeometry(triangleMesh, scaling));
	_imp->_existingActors.insert(actor);
	return actor;
}