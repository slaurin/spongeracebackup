#pragma once

class ITriggerCallback
{

public:
	virtual void Notify()=0;

};