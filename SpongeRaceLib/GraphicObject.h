#pragma once

namespace GE
{
	// R�f�rence crois�e
	template<class VertexT, class MaterialT>
	class Object3D;

	template<class VertexT, class MaterialT>
	class Effect;

	class GeometryPart;

	class GraphicObject
	{
	protected:
		XMMATRIX worldMatrix_;

		GraphicObject(const XMMATRIX & worldMatrix = XMMatrixIdentity())
			: worldMatrix_(worldMatrix)
		{}

	public:
		/*! Type des indexes */
		typedef unsigned int index_t;

		~GraphicObject(void){}

		/*! M�thode virtuelle permettant de calculer les animations en fonction du temps �coul� depuis la derni�re boucle du GraphicEngine (ne fait rien) */
		virtual void Anime(float timeElapsed) = 0;

		/*! M�thode virtuelle appel�e � chaque boucle du GraphicEngine pour afficher l'objet (Affichage de base) */
		virtual void Draw() = 0;

		virtual void SendToPipeline() const = 0;

		/*! Retourne la world matrice de l'objet */
		virtual XMMATRIX GetWorldMatrix() const	{return worldMatrix_;}
		
		//!
		virtual void SetWorldMatrix(const XMMATRIX & worldMatrix) {worldMatrix_ = worldMatrix;}
	};
}

