#include "stdafx.h"
#include "ColorMat.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	ColorMat::ColorMat(const Material * material)
		: Material(*material)
	{
		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		// Initialise la structure des param�tres globaux
		initGlobalParams();

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(GlobalParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		HRESULT hr = GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateBuffer(&bd, NULL, &globalBuffer_);
	}

	void ColorMat::initGlobalParams()
	{
		// Initialisation de la structure des param�tres
		globalParams_.matWorldViewProj = XMMatrixIdentity();
	}

	string ColorMat::GetClassName()
	{
		return "ColorMat";
	}

	ColorMat::~ColorMat()
	{
		DXRelacher(globalBuffer_);
	}
}