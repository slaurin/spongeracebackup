#pragma once

#include "GraphicObject.h"
#include "Lighting.h"
#include "Material.h"
#include "Effect.h"
#include "Mesh.h"
#include "Object3D.h"
#include "MaterialManager.h"
#include "Convert.h"
#include "Geometry.h"
#include "GeometryPart.h"

class AssimpException
{
private:
	std::string &msg_;
public:
	AssimpException(std::string &msg)
		: msg_(msg)
	{
	}
	std::string& getMessage()
	{
		return msg_;
	}
};

namespace GE
{
	/*! Classe statique permettant de charger des assets (mesh) */
	class AssimpLoader
	{
	public:
		~AssimpLoader(void);

		//! Importe un asset depuis un fichier et lui assigne l'effet fournis
		/*!
			Le type d'effet � assigner conditionne la structure du mesh charg�.
			En fonction de l'effet, l'objet graphique associ� aura le m�me type de vertices et de mat�riaux.
		*/
		static Geometry * Import(
			const std::string& pFile, 
			bool flipUV = false,
			bool genUV = false,
			bool flipWindingOrder = false,
			bool genSmoothNormal = false);
	
	private:
		AssimpLoader(void);
		static void DoTheErrorLogging(const char *);
		static Lighting loadLighting(const aiMaterial *);
		static void loadTextures(std::string filePath, const aiMaterial *, Material &);
		static Material loadMaterial(std::string filePath, const aiMaterial *);
	};

}