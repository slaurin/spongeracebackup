#ifndef B_SPLINE_H
#define B_SPLINE_H

#include "GeometryPart.h"

class BSpline
{
private:
    std::vector<XMFLOAT4> knots_;
    int pointsSize_;
    int innerCurvesSize_;
	float factor_;

    XMVECTOR buildTimeVector(float t) const throw();
	XMVECTOR buildTimeVectorWithFactor(float t) const throw();

public:
	BSpline(const std::vector<XMFLOAT4> &knots);
	BSpline(GE::GeometryPart *geometry);

    XMFLOAT4 Interpolate(float generalTime);
    XMFLOAT4 Interpolate(int pointIndex, float localTime);
    
    float FindTimeFromPoint(XMVECTOR point);

    int PointsSize() const throw()
    {
        return pointsSize_;
    }

    int InnerCurvesSize() const throw()
    {
        return innerCurvesSize_;
    }
};

#endif