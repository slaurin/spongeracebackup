#pragma once

#include "MenuPage.h"
#include "GraphicEngine.h"
#include "Pannel2DMat.h"

namespace GameUI
{
	class GameMenu
	{
		MenuPage * rootPage_;
		GE::GraphicEngine::sceneIndex menuScene_;
		GE::GraphicEngine::sceneIndex previousScene_;
		GE::Pannel2DMat * pageMat_;
		bool isShown_;

	public:
		GameMenu(MenuPage * rootPage);
		void Naviguate(PageEvent event);
		void SetOldScene(GE::GraphicEngine::sceneIndex index);
		void Show();
		void Hide();
		void Switch();
		MenuPage * GetCurrentPage();
		~GameMenu();
	};
}
