#ifndef MANAGER_H
#define MANAGER_H

#include "IOReader.h"
#include "Singleton.h"

#include <map>
#include <vector>

template<class TChild, class TKey, class TData>
class Manager : public Singleton<TChild>
{
private:
    std::map<TKey, TData> map_;
    TKey keyCounter_;

protected:
    Manager()
        : map_(map<TKey, TData>()),
          keyCounter_(TKey())
    {}
    ~Manager(){}

public:
    /* Load a file that has already been processed and that can be loaded
    directly as an internal representation */
    TKey LoadIntoCache(const std::string &fileName)
    {
        IOReader<TData> io(fileName);
        TKey key = ++keyCounter_;
        map_.insert(pair<TKey, TData>(key, io.getData()));
        return key;
    }

    /* This means the file is raw (such as a height map) and has not been processed to
    match our internal representation (height map = (Terrain)).
    The file is first loaded into memory, then the Process method is called */
    TKey LoadIntoCacheFromRaw(const std::string &fileName)
    {
        IOReader<unsigned char> io(fileName);
        TKey key = ++keyCounter_;
        map_.insert(pair<TKey, TData>(key, Process(io.getData())));
        return key;
    }

    /* Removes the resource from the cache */
	void RemoveFromCache(const TKey key)
    {
        map_.erase(key);
    }

    /* Access the data from the cache */
    TData& operator[](const TKey key)
    {
        return map_[key];
    }

protected:
    /* This method must be implemented when the TData is loaded from a binary file.
    When a file(resource) is loaded into memory, a proper conversion must be made. This
    is where the conversion is made. This method is called when LoadIntoCacheFromRaw is
    called. */
    virtual TData Process(std::vector<unsigned char> &rawData)
    {
        return TData();
    }
};

#endif