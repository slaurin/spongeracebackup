/*
 *  Author : Leo Lefebvre
 *  Modifier : Stephane Laurin
 *  Verifier : Stephane Laurin
 */

#ifndef SINGLETON_H
#define SINGLETON_H

#include "Uncopyable.h"

//! Template du design pattern Singleton
/*!
	Comme plusieurs de nos objets repr�senteront des �l�ments uniques 
	du syst�me (ex: le moteur lui-m�me, le lien vers 
	le dispositif Direct3D), l'utilisation d'un singleton 
	nous simplifiera plusieurs aspects.
*/
template <class T>
class Singleton : Uncopyable
{
public:
    /*! Renvoie l'instance unique de la classe */
    static T &GetInstance()
    {return instance_;}

protected :
    /*! Constructeur par d�faut */
	Singleton(void){}
	
    /*! Destructeur */
	~Singleton(void){}

private :
	/*! Instance de la classe */
    static T instance_;
};

// Instanciation du singleton
template <class T> T Singleton<T>::instance_;

#endif