#include "StdAfx.h"
#include "TextDrawer.h"
#include "GraphicEngine.h"
#include "DxUtilities.h"
#include "Window.h"

using namespace Gdiplus;
using namespace std;
using namespace DxUtilities;

namespace GE
{
	TextDrawer::TextDrawer(unsigned int width, unsigned int height, Font * font, const Color & brushColor, const Color & bgColor)
		: texWidth_(width), texHeight_(height), font_(font), pitchMem_(width * 4), bgColor_(bgColor)
	{
		// Cr�er le bitmap et un objet GRAPHICS (un dessinateur)
		bitmap_ = new Bitmap(texWidth_, texHeight_, PixelFormat32bppARGB);
		graphics_ = new Graphics(bitmap_);

		// Param�tres de l'objet Graphics
		graphics_->SetCompositingMode(CompositingModeSourceOver);
		graphics_->SetCompositingQuality(CompositingQualityHighSpeed);
		graphics_->SetInterpolationMode(InterpolationModeHighQuality);
		graphics_->SetPixelOffsetMode(PixelOffsetModeHighSpeed);
		graphics_->SetSmoothingMode(SmoothingModeNone);
		graphics_->SetPageUnit(UnitPixel);
		graphics_->SetTextRenderingHint(TextRenderingHintAntiAlias);//TextRenderingHintSystemDefault;  

		// On efface le bitmap
		graphics_->Clear(bgColor_); 

		// Un brosse pour le remplissage
		brush_ = new SolidBrush(brushColor);

		// Acc�der aux bits du bitmap
		BitmapData bmData;
		bitmap_->LockBits(&Rect(0, 0, texWidth_, texHeight_), ImageLockModeRead, PixelFormat32bppARGB, &bmData); 

		// Cr�ation d'une texture de m�me dimension sur la carte graphique
		D3D11_TEXTURE2D_DESC texDesc;
		texDesc.Width                = texWidth_;
		texDesc.Height               = texHeight_;
		texDesc.MipLevels            = 1;
		texDesc.ArraySize            = 1;
		texDesc.Format               = DXGI_FORMAT_B8G8R8A8_UNORM;
		texDesc.SampleDesc.Count     = 1;
		texDesc.SampleDesc.Quality   = 0;
		texDesc.Usage                = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags            = D3D11_BIND_SHADER_RESOURCE;
		texDesc.CPUAccessFlags       = 0;
		texDesc.MiscFlags            = 0;

		D3D11_SUBRESOURCE_DATA data;        
		data.pSysMem			= bmData.Scan0;
		data.SysMemPitch		= pitchMem_;
		data.SysMemSlicePitch	= 0;

		
		

		// Cr�ation de la texture � partir des donn�es du bitmap
		ID3D11Texture2D *pTexture;
		GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateTexture2D(&texDesc, &data, &pTexture);

		// Cr�ation d'un �resourve view� pour acc�der facilement � la texture
		//     (comme pour les sprites)
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format                      = DXGI_FORMAT_B8G8R8A8_UNORM;
		srvDesc.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels         = 1;
		srvDesc.Texture2D.MostDetailedMip   = 0;

		ID3D11ShaderResourceView* pTextureView;
		GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateShaderResourceView(pTexture,&srvDesc, &pTextureView);
		texture_ = new Texture(pTextureView);

		bitmap_->UnlockBits(&bmData);
	}

	TextDrawer::~TextDrawer()
	{
		delete brush_;
		delete graphics_;
		delete bitmap_;
		delete font_;
		delete texture_;
	}

	Texture * TextDrawer::GetTexture()
	{
		return texture_;
	}

	void TextDrawer::Write(const wstring & text, const PointF & origin, Image * image)
	{
		// Effacer
		graphics_->Clear(bgColor_);

		if(image)
		{
			graphics_->DrawImage(image, Rect(0, 0, Window::GetInstance().GetScreenWidth(), Window::GetInstance().GetScreenHeight()));
		}

		// �crire le nouveau texte
		graphics_->DrawString(text.c_str(), text.size(), font_, origin, brush_);  

		// Transf�rer
		BitmapData bmData;
		bitmap_->LockBits(&Rect(0, 0, texWidth_, texHeight_), ImageLockModeRead, PixelFormat32bppARGB, &bmData); 

		ID3D11Resource * res;
		texture_->GetTexture()->GetResource(&res);

		GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(res, 0, 0, bmData.Scan0, pitchMem_, 0);

		bitmap_->UnlockBits(&bmData);  
	}
}