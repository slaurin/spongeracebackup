#include "StdAfx.h"
#include "GameMenu.h"
#include "EffectManager.h"
#include "SpriteVertex.h"
#include "Pannel2DMat.h"
#include "Sprite.h"

using namespace GE;

namespace GameUI
{
	GameMenu::GameMenu(MenuPage * rootPage)
		: rootPage_(rootPage), menuScene_(GraphicEngine::GetInstance().NewScene("GameMenu", false)), pageMat_(nullptr), isShown_(false)
	{
		// On r�cup�re l'effet (QUI DOIT ETRE CHARGE!)
		auto spriteFX = EffectManager::GetInstance().getEffect<SpriteVertex, Pannel2DMat>("SpriteFX");

		pageMat_ = new Pannel2DMat(new Material(), Pannel2DMat::SPRITE, 0.f);
		pageMat_->InsertTexture(aiTextureType_AMBIENT, rootPage_->GetTexture());

		GraphicEngine::GetInstance().AddObjectToScene(new Sprite(spriteFX, pageMat_), menuScene_);
	}

	void GameMenu::Naviguate(PageEvent event)
	{
		rootPage_ = rootPage_->Naviguate(event);
		pageMat_->ReplaceTexture(aiTextureType_AMBIENT, rootPage_->GetTexture());
	}

	void GameMenu::SetOldScene(GE::GraphicEngine::sceneIndex index)
	{
		previousScene_ = index;
	}

	void GameMenu::Show()
	{
		if(!isShown_)
		{
			isShown_ = true;
			previousScene_ = GraphicEngine::GetInstance().SwitchScene(menuScene_);
		}
	}

	void GameMenu::Hide()
	{
		if(isShown_)
		{
			isShown_ = false;
			GraphicEngine::GetInstance().SwitchScene(previousScene_);
		}
	}

	void GameMenu::Switch()
	{
		return isShown_ ? Hide() : Show();
	}

	MenuPage * GameMenu::GetCurrentPage()
	{
		return rootPage_;
	}

	GameMenu::~GameMenu()
	{
		delete rootPage_;
	}
}