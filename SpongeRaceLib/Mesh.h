#pragma once

#include "MemoryManaged.h"
#include "GraphicObject.h"
#include "Geometry.h"
#include "GeometryPart.h"
#include "Object3D.h"

struct NameFunctor{
	static std::string getName(){return "Mesh";};
	static int getSize(){return 1000;};
};

namespace GE
{
	class Mesh :
		public GraphicObject//, public MemoryManaged<Mesh,NameFunctor>
	{
		//friend MemoryManaged<Mesh,NameFunctor>;

	public:
		/*! Constructeur */
		Mesh(const std::vector<GraphicObject *> & subMeshes = std::vector<GraphicObject *>());

		template<class VertexT, class MaterialT>
		Mesh(Effect<VertexT, MaterialT> * effect, Geometry * geometry)
		{
			std::for_each(std::begin(geometry->parts()),
						  std::end(geometry->parts()),
						  [&](GeometryPart &part)
			{
				addSubMesh(new Object3D<VertexT, MaterialT>(effect, &part));
			});
		}


		~Mesh();

		/*! M�thode virtuelle permettant de calculer les animations en fonction du temps �coul� depuis la derni�re boucle du GraphicEngine (ne fait rien) */
		void Anime(float timeElapsed){}

		/*! M�thode virtuelle appel�e � chaque boucle du GraphicEngine pour afficher l'objet (Affichage de base) */
		void Draw();

		void SendToPipeline() const {}

		void addSubMesh(GraphicObject *);

		void SetWorldMatrix(const XMMATRIX & worldMatrix);

	//private:
		std::vector<GraphicObject *> subMeshes_;
	};
}