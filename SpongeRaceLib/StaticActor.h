#ifndef STATICACTOR_H
#define STATICACTOR_H

#include "UserInput.h"
#include "Actor.h"


class StaticActor : public ActorBase
{
protected:
	PxRigidStatic *pxActor_;

public :
	StaticActor(const ActorFactory::ActorID &aActorId)
		: ActorBase(aActorId),
		pxActor_(nullptr)
	{}

	virtual PxTransform pose()
	{
		return pxActor_->getGlobalPose();
	}
	
	void SetFilterData(PxFilterData filterData)
	{
		actorShape_->setSimulationFilterData(filterData);
	}
};

#endif
