#ifndef QUAD_H
#define QUAD_H

#include "stdafx.h"
#include "Object3D.h"
#include "ParticleMat.h"
#include "ParticleVertex.h"

namespace GE
{
    class Quad : public Object3D<ParticleVertex, ParticleMat>
    {
        friend class ParticleInstancer;

        static ParticleVertex quadVertices_[];
        XMFLOAT3 position_;
        XMFLOAT3 scale_;
        float velocity_;
        bool active_;

    public:

        Quad(
            Effect<ParticleVertex, ParticleMat> *effect,
            ParticleMat *pMaterial,
            XMFLOAT3 position = XMFLOAT3(0.0f, 0.0f, 0.0f),
            XMFLOAT3 scale = XMFLOAT3(1.0f, 1.0f, 1.0f),
            float velocity = 0.0f,
            bool active = false);

        virtual void Anime(float timeElapsed);
        virtual void Draw();
        virtual void SendToPipeline() const;
    private:

    };
}
#endif