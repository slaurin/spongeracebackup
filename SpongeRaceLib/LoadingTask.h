#ifndef LOADING_TASK_H
#define LOADING_TASK_H
#include "GameTask.h"
#include "GraphicEngine.h"
#include "ITriggerCallback.h"

class FailedToLoadShaders{};
class FailedToLoadGraphicObjects{};
class FailedToLoadPhysxObjects{};
class FailedToLoadLights{};

class LoadingTask: public GameTask, public ITriggerCallback
{
	class LoadingTaskImp;
	LoadingTaskImp *imp_;

public:
	LoadingTask();
	~LoadingTask();

	virtual void init() override;
	virtual void cleanup() override;
	virtual void update() override;
	void Notify();
	GE::GraphicEngine::sceneIndex GetSceneIndex(unsigned int scene);

};

#endif