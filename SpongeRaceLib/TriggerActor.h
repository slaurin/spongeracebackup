#pragma once

#include "ITriggerCallback.h"
#include "StaticActor.h"

class TriggerActor : public StaticActor
{

public:
	TriggerActor(const ActorFactory::ActorID &aActorId)
		: StaticActor(aActorId)
	{}

	virtual void SetCallbackObj(ITriggerCallback *callbackObj)=0;	
	virtual void SetTriggerWith(std::string name)=0;

	static ActorFactory::ActorID typeId();
};

