#pragma once

#include "Object3D.h"
#include "PhongMat.h"
#include "Effect.h"
#include "TangentMeshVertex.h"
#include "FasterTimer.h"

namespace GE
{
	class BubbleMat
		: public Material
	{

		float timeVal;

		struct ShadersParams // toujours un multiple de 16 pour les constantes
		{ 
			XMMATRIX WorldViewProjection;	// la matrice totale 
			XMMATRIX WorldView;			// matrice de transformation dans le monde 
			XMMATRIX World;
			XMVECTOR   Eye; 			// la position_ de la cam�ra
			float      time;
			float _[3];//padding
		};

		ID3D11Buffer *constantBuffer;

		FasterTimer timer_;

	public:
		BubbleMat(const Material * material);

		BubbleMat(const BubbleMat & material);

		static std::string GetClassName();

		~BubbleMat();

		template<class VertexT>
		void Apply(Effect<VertexT, BubbleMat> * effect, const Object3D<VertexT, BubbleMat> & object)
		{
			// Initialiser et s�lectionner les �constantes� de l'effet
			ShadersParams sp;
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();
			XMMATRIX view = GraphicEngine::GetInstance().GetMatView();

			sp.WorldViewProjection = XMMatrixTranspose(object.GetWorldMatrix() * viewProj );
			sp.WorldView = XMMatrixTranspose(object.GetWorldMatrix() * view);
			sp.World = XMMatrixTranspose(object.GetWorldMatrix());
			sp.Eye = GAME->camera()->getPosition();
			timeVal +=LoopTime::GetInstance().ElapsedTimeSeconds();
			sp.time = static_cast<float>(timeVal);

			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer, 0, NULL, &sp, 0, 0);
			effect->GetConstantBufferByName("param")->SetConstantBuffer(constantBuffer);

			// Activation de la texture ou non
			auto it = textures_.find(aiTextureType(aiTextureType_DIFFUSE));
			if(it != textures_.end())
			{
				effect->GetVariableByName("BaseTexture")->AsShaderResource()->SetResource(it->second->GetTexture());

				// On charge le sampler state associ�
				//effect->GetVariableByName("BaseSampler")->AsSampler()->SetSampler(0, NULL);			
			}

			it = textures_.find(aiTextureType(aiTextureType_EMISSIVE));
			if(it != textures_.end())
			{
				//Post effect		
				effect->GetVariableByName("EnvTexture")->AsShaderResource()->SetResource(it->second->GetTexture());
			}

			GraphicEngine::GetInstance().GetDevice()->EnableAlphaBlend();
			effect->Apply(object);
			GraphicEngine::GetInstance().GetDevice()->DisableAlphaBlend();
		}
	};
}

