#include "stdafx.h"
#include "SimpleVertex4.h"

namespace GE
{
	D3D11_INPUT_ELEMENT_DESC SimpleVertex4::Layout_[] =
	{
		{"POSITION",	0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},  
		{"COLOR",		0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT ,	D3D11_INPUT_PER_VERTEX_DATA,	0}
	};

	D3D11_INPUT_ELEMENT_DESC * SimpleVertex4::Begin()
	{return Layout_;}

	UINT SimpleVertex4::Size()
	{return ARRAYSIZE(Layout_);}

	SimpleVertex4::SimpleVertex4(XMFLOAT4 position, XMFLOAT4 color)
		: position_(position), color_(color)
	{}

	XMFLOAT4 SimpleVertex4::GetPosition() const throw()
	{return position_;}

	XMFLOAT4 SimpleVertex4::GetColor() const throw()
	{return color_;}

	void SimpleVertex4::SetPosition(const XMFLOAT4 position) throw()
	{position_ = position;}

	void SimpleVertex4::SetColor(const XMFLOAT4 color) throw()
	{color_ = color;}

	SimpleVertex4::SimpleVertex4(const Vertex & vertex)
		: position_(vertex.position().x,vertex.position().y,vertex.position().z,1.f), color_()
	{
	}
}
