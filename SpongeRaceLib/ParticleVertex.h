#ifndef PARTICLE_VERTEX_H
#define PARTICLE_VERTEX_H

#include "stdafx.h"

namespace GE
{
    class ParticleVertex
    {
    public:
        XMFLOAT4 position_;
        XMFLOAT2 textureCoord_;
    private:
        static D3D11_INPUT_ELEMENT_DESC Layout_[];

    public:
        ParticleVertex(XMFLOAT4 position = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
                       XMFLOAT2 textureCoord = XMFLOAT2(0.0f, 0.0f));

        static D3D11_INPUT_ELEMENT_DESC * Begin();
        static UINT Size();
    };
}

#endif