#pragma once

#include "Object3D.h"
#include "PhongMat.h"
#include "Effect.h"
#include "MeshVertex.h"


namespace GE
{
	/*! Cube en 3D (est un Object3D) */
	class Cube
		: public Object3D<MeshVertex, PhongMat>
	{
	public:
		/*! Constructeur */
		Cube(Effect<MeshVertex, PhongMat> *effect, PhongMat *pMaterial, float dx = 1.f, float dy = 1.f, float dz = 1.f);

		/*! Surchage de la m�thode d'animation */
		void Anime(float timeElapsed);

	private:
		/*! Liste d'index utilis�s pour dessiner le cube */
		static const unsigned int indexList_[];

		/*! D�finit la rotation actuelle du cube */
		float rotation_;
	};
}

	