#ifndef PARTICLES_H
#define PARTICLES_H
//
//#include "Object3D.h"
//#include "ParticleMat.h"
//#include "ParticleVertex.h"
//
//namespace GE
//{
//    class Particles : public Object3D<ParticleVertex, ParticleMat>
//    {
//    private:
//        enum{MAX_NUMBER_OF_PARTICLES = 5000};
//
//        static ParticleVertex particleVertices_[];
//        static index_t particleIndices_[];
//
//
//
//        float m_particleDeviationX, m_particleDeviationY, m_particleDeviationZ;
//        float m_particleVelocity, m_particleVelocityVariation;
//        float m_particleSize, m_particlesPerSecond;
//        int m_maxParticles;
//
//        int m_currentParticleCount;
//        float m_accumulatedTime;
//
//        TextureClass* m_Texture;
//
//        ParticleType* m_particleList;
//
//        int m_vertexCount, m_indexCount;
//        VertexType* m_vertices;
//        ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
//
//
//
//
//    public:
//
//        Particles(Effect<ParticleVertex, ParticleMat> *effect, 
//                  ParticleMat *pMaterial);
//
//        ~Particles();
//
//        virtual void Anime(float timeElapsed);
//        virtual void Draw();
//
//        ID3D11ShaderResourceView* GetTexture();
//        int GetIndexCount();
//
//    private:
//        bool Initialize();
//        void Shutdown();
//
//        bool InitializeParticleSystem();
//        void ShutdownParticleSystem();
//
//        bool InitializeBuffers();
//        void ShutdownBuffers();
//
//        void EmitParticles(float timeElapsed);
//        void UpdateParticles(float timeElapsed);
//        void KillParticles();
//
//        bool UpdateBuffers();
//        void RenderBuffers();
//    };
//
//}
//
#endif