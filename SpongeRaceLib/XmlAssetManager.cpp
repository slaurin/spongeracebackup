#include "stdafx.h"


#include "XmlAssetManager.h"
#include "GraphicEngine.h"

#include "RaceTrack.h"
#include "ObstacleActor.h"
#include "Object3D.h"
#include "MeshVertex.h"
#include "MiniPhongMat.h"

#include "EntityManager.h"
#include "EffectManager.h"
#include "GeometryManager.h"
#include "MaterialManager.h"
#include "SpawnManager.h"
#include "TextureManager.h"
#include "PhysXConvexMeshManager.h"
#include "PhysXTriangleMeshManager.h"
#include "GraphicObjectManager.h"

#include "Lexical_cast.h"
#include "Convert.h"
#include "Effect.h"
#include "ColorMat.h"
#include "SimpleVertex.h"
#include "PhongMat.h"

#include<xercesc/dom/DOM.hpp>


#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"
#include "boost/lexical_cast.hpp"

using namespace GE;
using namespace std;
using namespace physx;
namespace fs = boost::filesystem;

void XMLAssetManager::handleElementList(xercesc::DOMElement* element, const XMLCh* name,  bool load )
{

	xercesc::DOMElement* result = NULL ;

	xercesc::DOMNodeList* list = element->getElementsByTagName(name) ;		

	const XMLSize_t count = list->getLength() ;

	StringManager sm ;

	for( XMLSize_t index = 0 ; index < count ; ++index ){
		
		xercesc::DOMNode* node = list->item( index ) ;		
		xercesc::DOMElement* element = dynamic_cast< xercesc::DOMElement* >( node ) ;
		xercesc::DOMElement* parent = dynamic_cast< xercesc::DOMElement* >( node->getParentNode() ) ;

		handleElement(element, load);		
	}

}



void XMLAssetManager::handleElement( xercesc::DOMElement* element,  bool load){

	if (element==nullptr) return;

	if( xercesc::XMLString::equals(  finder_.TAG_PHYSX_MESH_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Physx mesh (front part only)                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string physxMeshType = sm.convertStr( element->getAttribute( finder_.ATTR_PHYSXMESHTYPE.asXMLString() ) ) ;
		string geometryRef = sm.convertStr( element->getAttribute( finder_.ATTR_GEOMETRY_REF.asXMLString() ) ) ;

		Geometry* geometry = GeometryManager::GetInstance()[geometryRef];
		if (geometry==nullptr) throw std::runtime_error("geometryRef could not be found, name=" + geometryRef);

		//TODO Change to handle multiple parts
		if (physxMeshType=="Triangle") {
			if (load) {
				PhysXTriangleMeshManager::GetInstance().Request(name, &(geometry->parts().front()));
			} else {
				PhysXTriangleMeshManager::GetInstance().Remove(name);
			}
		} else if (physxMeshType=="Convex") {
			if (load) {
				PhysXConvexMeshManager::GetInstance().Request(name, &(geometry->parts().front()));
			} else {
				PhysXConvexMeshManager::GetInstance().Remove(name);
			}
		} else {
			throw std::runtime_error("Unknown physxMeshType, name=" + physxMeshType);
		}

	} else if( xercesc::XMLString::equals(  finder_.TAG_GEOMETRY_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Geometry                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string filename = sm.convertStr( element->getAttribute( finder_.ATTR_FILENAME.asXMLString() ) ) ;
		bool flipUV = to_bool(sm.convertStr( element->getAttribute( finder_.ATTR_FLIPUV.asXMLString() ) ));
		bool genUV = to_bool(sm.convertStr( element->getAttribute( finder_.ATTR_GENUV.asXMLString() ) ) );
		bool flipWindingOrder = to_bool(sm.convertStr( element->getAttribute( finder_.ATTR_FLIPWINDINGORDER.asXMLString() ) ) );
		bool genSmoothNormal = to_bool(sm.convertStr( element->getAttribute( finder_.ATTR_GENSMOOTHNORMAL.asXMLString() ) ) );

		if (!fileExist(filename)){
			throw( std::runtime_error( "Geometry document does not exist :" +  filename) ) ;
		}

		if(load) {
			GeometryManager::GetInstance().Request(name,filename,flipUV,genUV,flipWindingOrder,genSmoothNormal);
		} else {
			GeometryManager::GetInstance().Remove(name);
		}

	} else if( xercesc::XMLString::equals(  finder_.TAG_MESH_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Mesh (GraphicObject)                                                                    */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string geometryRef = sm.convertStr( element->getAttribute( finder_.ATTR_GEOMETRY_REF.asXMLString() ) ) ;
		string effectRef = sm.convertStr( element->getAttribute( finder_.ATTR_EFFECT_REF.asXMLString() ) ) ;

		if (geometryRef.empty()) throw std::runtime_error("Empty geometryRef, geometryRef=" + geometryRef);
		if (effectRef.empty()) throw std::runtime_error("Empty effectRef, effectRef=" + effectRef);

		//Unused ??
		//string materialRef = sm.convertStr( element->getAttribute( finder_.ATTR_MATERIAL_REF.asXMLString() ) ) ;

		xercesc::DOMElement* effectElem = finder_.getElementByName(finder_.TAG_EFFECT_LIST.asXMLString(), effectRef);
		std::string vertexType = getVertexType(effectElem);
		std::string materialType = getMaterialType(effectElem);

		IEffect* effect= EffectManager::GetInstance()[effectRef];

		if (!effect) throw std::runtime_error("Effect not found, effectRef=" + effectRef);

		GE::Geometry * geometry = GeometryManager::GetInstance()[geometryRef];

		if (!geometry) throw std::runtime_error("Geometry not found, geometryRef=" + geometryRef);

		if (load){				
			if (vertexType=="MeshVertex")
			{
				if (materialType=="MiniPhongMat"){

					GraphicObjectManager::GetInstance().Request(name, (Effect<MeshVertex,MiniPhongMat>*)effect, geometry);

				} else if (materialType=="PhongMat"){

					GraphicObjectManager::GetInstance().Request(name, (Effect<MeshVertex,PhongMat>*)effect, geometry);
				}
			} else {					
				throw std::runtime_error("Unmanaged vertexType and materialType combination, vertexType=" + vertexType + " materialType=" + materialType);
			}

		}else{
			GraphicObjectManager::GetInstance().Remove(name);
		}
	} else if( xercesc::XMLString::equals(  finder_.TAG_TEXTURE_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Texture (unimplemented)                                                                     */
		/************************************************************************/

		//TODO Complete
		throw std::runtime_error("Texture parsing is unimplemented");

		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string filename = sm.convertStr( element->getAttribute( finder_.ATTR_FILENAME.asXMLString() ) ) ;			

		if (!fileExist(filename)){
			throw( std::runtime_error( "Texture document does not exist :" +  filename) ) ;
		}


		//TextureManager::GetInstance().

	} else if( xercesc::XMLString::equals(  finder_.TAG_MATERIAL_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Material  (unimplemented)                                                                     */
		/************************************************************************/
		//TODO Complete
		throw std::runtime_error("Material parsing is unimplemented");

		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string textureRef = sm.convertStr( element->getAttribute( finder_.TAG_TEXTURE_REF.asXMLString() ) ) ;

		//Lighting
		xercesc::DOMElement* ambientElem = finder_.getElement(element, finder_.TAG_AMBIENT.asXMLString() );
		xercesc::DOMElement* diffuseElem = finder_.getElement(element, finder_.TAG_DIFFUSE.asXMLString() );
		xercesc::DOMElement* specularElem = finder_.getElement(element, finder_.TAG_SPECULAR.asXMLString() );
		xercesc::DOMElement* emissiveElem = finder_.getElement(element, finder_.TAG_EMISSIVE.asXMLString() );
		xercesc::DOMElement* transparentElem = finder_.getElement(element, finder_.TAG_TRANSPARENT.asXMLString() );

		XMFLOAT4 ambient(0.f,0.f,0.f,0.f);
		XMFLOAT4 diffuse(0.f,0.f,0.f,0.f);
		XMFLOAT4 specular(0.f,0.f,0.f,0.f);
		XMFLOAT4 emissive(0.f,0.f,0.f,0.f);
		XMFLOAT4 transparent(0.f,0.f,0.f,0.f);

		ambient = elemToXMFLAOT4(ambientElem);
		diffuse = elemToXMFLAOT4(diffuseElem);
		specular=elemToXMFLAOT4(specularElem);
		emissive = elemToXMFLAOT4(emissiveElem);
		transparent = elemToXMFLAOT4(transparentElem);
		//End of lighting



	} else if( xercesc::XMLString::equals(  finder_.TAG_DIRECTIONALLIGHT_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Directionnal light                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;			

		xercesc::DOMElement* directionElem = finder_.getElement(element, finder_.TAG_DIRECTION.asXMLString() );
		xercesc::DOMElement* diffuseElem = finder_.getElement(element, finder_.TAG_DIFFUSE.asXMLString() );
		xercesc::DOMElement* specularElem = finder_.getElement(element, finder_.TAG_SPECULAR.asXMLString() );


		XMFLOAT4 direction = elemToXMFLAOT4(directionElem);
		XMFLOAT4 diffuse= elemToXMFLAOT4(diffuseElem);
		XMFLOAT4 spéculaire = elemToXMFLAOT4(specularElem);

		DirectionalLight dl = DirectionalLight(direction, diffuse, spéculaire);
		//TODO Use managers
		if (load) {
			GraphicEngine::GetInstance().GetCurrentScene().AddLight(dl);
		} else {
			GraphicEngine::GetInstance().GetCurrentScene().GetDirectionalLights().clear();
		}

	} else if( xercesc::XMLString::equals(  finder_.TAG_POINTLIGHT_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* PointLight                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;			

		xercesc::DOMElement* diffuseElem = finder_.getElement(element, finder_.TAG_DIFFUSE.asXMLString() );
		xercesc::DOMElement* specularElem = finder_.getElement(element, finder_.TAG_SPECULAR.asXMLString() );
		xercesc::DOMElement* positionElem = finder_.getElement(element, finder_.TAG_POSITION.asXMLString() );

		XMFLOAT4 position = elemToXMFLAOT4(positionElem);
		XMFLOAT4 diffuse= elemToXMFLAOT4(diffuseElem);
		XMFLOAT4 spéculaire = elemToXMFLAOT4(specularElem);

		PointLight pl = PointLight(position, diffuse, spéculaire);
		//TODO Use managers
		if (load) {
			GraphicEngine::GetInstance().GetCurrentScene().AddLight(pl);
		} else {
			GraphicEngine::GetInstance().GetCurrentScene().GetPointLights().clear();
		}

	} else if( xercesc::XMLString::equals(  finder_.TAG_SPOTLIGHT_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* SpotLight                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;			
		float angle = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_ANGLE.asXMLString() ) )) ;			
		float exponent = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_EXPONENT.asXMLString() ) )) ;			

		xercesc::DOMElement* directionElem = finder_.getElement(element, finder_.TAG_DIRECTION.asXMLString() );
		xercesc::DOMElement* diffuseElem = finder_.getElement(element, finder_.TAG_DIFFUSE.asXMLString() );
		xercesc::DOMElement* specularElem = finder_.getElement(element, finder_.TAG_SPECULAR.asXMLString() );
		xercesc::DOMElement* positionElem = finder_.getElement(element, finder_.TAG_POSITION.asXMLString() );

		XMFLOAT4 direction = elemToXMFLAOT4(directionElem);
		XMFLOAT4 position = elemToXMFLAOT4(positionElem);
		XMFLOAT4 diffuse= elemToXMFLAOT4(diffuseElem);
		XMFLOAT4 spéculaire = elemToXMFLAOT4(specularElem);

		SpotLight sl = SpotLight(position, direction, angle, exponent, diffuse, spéculaire);
		//TODO Use managers
		if (load) {
			GraphicEngine::GetInstance().GetCurrentScene().AddLight(sl);
		} else {
			GraphicEngine::GetInstance().GetCurrentScene().GetSpotLights().clear();
		}

	}  else if( xercesc::XMLString::equals(  finder_.TAG_EFFECT_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Effects                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string filename = sm.convertStr( element->getAttribute( finder_.ATTR_FILENAME.asXMLString() ) ) ;	
		string vertexType = sm.convertStr( element->getAttribute( finder_.ATTR_VERTEXTYPE.asXMLString() ) ) ;	
		string materialType = sm.convertStr( element->getAttribute( finder_.ATTR_MATERIALTYPE.asXMLString() ) ) ;	
		string version = sm.convertStr( element->getAttribute( finder_.ATTR_VERSION.asXMLString() ) ) ;	


		if (!fileExist(filename)){
			throw( std::runtime_error( "Effects document does not exist :" +  filename) ) ;
		}

		if (load) {
			if (vertexType == "MeshVertex" && materialType=="MiniPhongMat") {
				EffectManager::GetInstance().Request<MeshVertex, MiniPhongMat>(name,filename);
			} else if (vertexType == "MeshVertex" && materialType=="PhongMat") {
				EffectManager::GetInstance().Request<MeshVertex, PhongMat>(name,filename);
			}else if (vertexType == "SimpleVertex" && materialType=="ColorMat") {
				EffectManager::GetInstance().Request<SimpleVertex, ColorMat>(name,filename);
			} else {
				throw std::runtime_error("(Effects) Unknown vertexType and materialType combination, vertexType=" + vertexType + " materialType=" + materialType);
			}
		}else {
			EffectManager::GetInstance().Remove(name);
		}

	} else if( xercesc::XMLString::equals( finder_.TAG_PLAYER.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Player                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string physxMeshRef = sm.convertStr( element->getAttribute( finder_.ATTR_PHYSXMESH_REF.asXMLString() ) ) ;
		string meshRef = sm.convertStr( element->getAttribute( finder_.ATTR_MESH_REF.asXMLString() ) ) ;
		string type = sm.convertStr( element->getAttribute( finder_.ATTR_ENTITYTYPE.asXMLString() ) ) ;

		xercesc::DOMElement* positionElem = finder_.getElement(element, finder_.TAG_POSITION.asXMLString() );		
		xercesc::DOMElement* angleXElem = finder_.getElement(element, finder_.TAG_ANGLE_X.asXMLString() );
		xercesc::DOMElement* angleYElem = finder_.getElement(element, finder_.TAG_ANGLE_Y.asXMLString() );
		xercesc::DOMElement* angleZElem = finder_.getElement(element, finder_.TAG_ANGLE_Z.asXMLString() );
		
		PxVec3 position = elemToPxVec3(positionElem);
		PxVec3 angleX = elemToPxVec3(angleXElem);
		PxVec3 angleY= elemToPxVec3(angleYElem);
		PxVec3 angleZ = elemToPxVec3(angleZElem);

		physx::PxConvexMesh *physxPlayerMesh = PhysXConvexMeshManager::GetInstance()[physxMeshRef];
		GE::GraphicObject* playerObj = GraphicObjectManager::GetInstance()[meshRef];

		if (load) {
			GAME->setPlayer(GAME->spawnManager()->spawn<PlayerActor>(PxTransform(position), physxPlayerMesh, playerObj));	
			GraphicEngine::GetInstance().AddObjectToScene(playerObj);
		}else {
			GAME->spawnManager()->unspawn(GAME->player());
		}


	}  else if( xercesc::XMLString::equals(  finder_.TAG_RACE_TRACK.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Race track                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string physxMeshRef = sm.convertStr( element->getAttribute( finder_.ATTR_PHYSXMESH_REF.asXMLString() ) ) ;
		string meshRef = sm.convertStr( element->getAttribute( finder_.ATTR_MESH_REF.asXMLString() ) ) ;
		string type = sm.convertStr( element->getAttribute( finder_.ATTR_ENTITYTYPE.asXMLString() ) ) ;

		xercesc::DOMElement* positionElem = finder_.getElement(element, finder_.TAG_POSITION.asXMLString() );
		xercesc::DOMElement* angleXElem = finder_.getElement(element, finder_.TAG_ANGLE_X.asXMLString() );
		xercesc::DOMElement* angleYElem = finder_.getElement(element, finder_.TAG_ANGLE_Y.asXMLString() );
		xercesc::DOMElement* angleZElem = finder_.getElement(element, finder_.TAG_ANGLE_Z.asXMLString() );

		PxVec3 position = elemToPxVec3(positionElem);
		PxVec3 angleX = elemToPxVec3(angleXElem);
		PxVec3 angleY= elemToPxVec3(angleYElem);
		PxVec3 angleZ = elemToPxVec3(angleZElem);

		physx::PxTriangleMesh *physxStaticMesh = PhysXTriangleMeshManager::GetInstance()[physxMeshRef];
		GE::GraphicObject* graphicObj = GraphicObjectManager::GetInstance()[meshRef];

		if (load) {
			EntityManager::GetInstance().RequestRaceTrack(name, position, physxStaticMesh, graphicObj);
			GraphicEngine::GetInstance().AddObjectToScene(graphicObj);
		}else {
			EntityManager::GetInstance().Remove(name);
		}

	}  else if( xercesc::XMLString::equals(  finder_.TAG_CAMERA.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Camera                                                                     */
		/************************************************************************/
		string camType = sm.convertStr( element->getAttribute( finder_.ATTR_CAMERATYPE.asXMLString() ) ) ;

		xercesc::DOMElement* positionElem = finder_.getElement(element, finder_.TAG_POSITION.asXMLString() );
		xercesc::DOMElement* angleXElem = finder_.getElement(element, finder_.TAG_ANGLE_X.asXMLString() );
		xercesc::DOMElement* angleYElem = finder_.getElement(element, finder_.TAG_ANGLE_Y.asXMLString() );
		xercesc::DOMElement* angleZElem = finder_.getElement(element, finder_.TAG_ANGLE_Z.asXMLString() );

		PxVec3 position = elemToPxVec3(positionElem);
		PxVec3 angleX = elemToPxVec3(angleXElem);
		PxVec3 angleY= elemToPxVec3(angleYElem);
		PxVec3 angleZ = elemToPxVec3(angleZElem);

		if(load) {
			GAME->setCamera(GAME->spawnManager()->spawn<CameraActor>(PxTransform(position)));

			if (camType=="FirstPerson") {
				GAME->camera()->switchMode(FIRST_PERSON);		
			} else if (camType=="FixedCam") {
				GAME->camera()->switchMode(FIXEDCAM);
			} else {
				//Default
				GAME->camera()->switchMode(THIRD_PERSON);
			}
		} else {
			GAME->spawnManager()->unspawn(GAME->camera());
		}

	} else if( xercesc::XMLString::equals(  finder_.TAG_OBSTACLE_LIST.asXMLString() , element->getTagName() ) ){
		/************************************************************************/
		/* Obstacle                                                                     */
		/************************************************************************/
		string name = sm.convertStr( element->getAttribute( finder_.ATTR_NAME.asXMLString() ) ) ;
		string physxMeshRef = sm.convertStr( element->getAttribute( finder_.ATTR_PHYSXMESH_REF.asXMLString() ) ) ;
		string meshRef = sm.convertStr( element->getAttribute( finder_.ATTR_MESH_REF.asXMLString() ) ) ;
		string type = sm.convertStr( element->getAttribute( finder_.ATTR_ENTITYTYPE.asXMLString() ) ) ;

		xercesc::DOMElement* positionElem = finder_.getElement(element, finder_.TAG_POSITION.asXMLString() );
		xercesc::DOMElement* angleXElem = finder_.getElement(element, finder_.TAG_ANGLE_X.asXMLString() );
		xercesc::DOMElement* angleYElem = finder_.getElement(element, finder_.TAG_ANGLE_Y.asXMLString() );
		xercesc::DOMElement* angleZElem = finder_.getElement(element, finder_.TAG_ANGLE_Z.asXMLString() );

		PxVec3 position = elemToPxVec3(positionElem);
		PxVec3 angleX = elemToPxVec3(angleXElem);
		PxVec3 angleY= elemToPxVec3(angleYElem);
		PxVec3 angleZ = elemToPxVec3(angleZElem);

		physx::PxTriangleMesh *physxStaticMesh = PhysXTriangleMeshManager::GetInstance()[physxMeshRef];
		GE::GraphicObject* graphicObj = GraphicObjectManager::GetInstance()[meshRef];

		if(load){
			EntityManager::GetInstance().RequestObstacleActor(name, position, physxStaticMesh, graphicObj);
			GraphicEngine::GetInstance().AddObjectToScene(graphicObj);
		} else {
			EntityManager::GetInstance().Remove(name);
		}

	}else {
		char* tagName = xercesc::XMLString::transcode( element->getTagName() ) ;

		std::ostringstream buf ;
		buf << "Unexpected tag: <" << tagName << "/>" << std::flush ;

		throw( std::runtime_error( buf.str() ) ) ;
	}

} // handleElement()

void XMLAssetManager::load(const std::string &filePath)
{

	parser_.setValidationScheme( xercesc::XercesDOMParser::Val_Never ) ;
	parser_.setDoNamespaces( false ) ;
	parser_.setDoSchema( false ) ;
	parser_.setLoadExternalDTD( false ) ;

	StringManager sm ;

	try{


		if (!fileExist(filePath)){
			throw( std::runtime_error( "XML document does not exist :" +  filePath) ) ;
		}

		parser_.parse( filePath.c_str() ) ;

		// there's no need to free this pointer -- it's
		// owned by the parent parser object
		xercesc::DOMDocument* xmlDoc = parser_.getDocument() ;

		if( NULL == xmlDoc->getDocumentElement() ){
			throw( std::runtime_error( "empty XML document" ) ) ;
		}

		finder_.setDocument( xmlDoc ) ;



	}catch( xercesc::XMLException& e ){

		// believe it or not, XMLException is not
		// a parent class of DOMException

		std::ostringstream buf ;
		buf << "Error parsing file: "
			<< DualString( e.getMessage() )
			<< std::flush
			;

		throw( std::runtime_error( buf.str() ) ) ;


	}catch( const xercesc::DOMException& e ){

		std::ostringstream buf ;
		buf << "Encountered DOM Exception: "
			<< DualString( e.getMessage() )
			<< std::flush
			;

		throw( std::runtime_error( buf.str() ) ) ;

	}

	return ;

} // load()


/************************************************************************/
/* Loads permanent game assets                                          */
/************************************************************************/
void XMLAssetManager::loadGameAssets()
{
	xercesc::DOMElement* game = finder_.getGameElement();
	//In proper order
	handleElementList(game,finder_.TAG_EFFECT_LIST.asXMLString(), true );
	handleElementList(game,finder_.TAG_GEOMETRY_LIST.asXMLString(), true );
	handleElementList(game,finder_.TAG_TEXTURE_LIST.asXMLString(), true );
	handleElementList(game,finder_.TAG_MATERIAL_LIST.asXMLString(), true );
	handleElementList(game,finder_.TAG_PHYSX_MESH_LIST.asXMLString(), true );
	handleElementList(game,finder_.TAG_MESH_LIST.asXMLString(), true );

	//Unique elements
	handleElement(finder_.getElement(finder_.TAG_PLAYER.asXMLString()), true );
	handleElement(finder_.getElement(finder_.TAG_CAMERA.asXMLString()), true );

}

void XMLAssetManager::unloadGameAssets()
{
	xercesc::DOMElement* game = finder_.getGameElement();
	//In proper order
	handleElementList(game,finder_.TAG_EFFECT_LIST.asXMLString(), false );
	handleElementList(game,finder_.TAG_GEOMETRY_LIST.asXMLString(), false );
	handleElementList(game,finder_.TAG_TEXTURE_LIST.asXMLString(), false );
	handleElementList(game,finder_.TAG_MATERIAL_LIST.asXMLString(), false );
	handleElementList(game,finder_.TAG_PHYSX_MESH_LIST.asXMLString(), false );
	handleElementList(game,finder_.TAG_MESH_LIST.asXMLString(), false );

	//Unique elements
	handleElement(finder_.getElement(finder_.TAG_PLAYER.asXMLString()), false );
	handleElement(finder_.getElement(finder_.TAG_CAMERA.asXMLString()), false );
}

/************************************************************************/
/* Load scenes assets                                                   */
/************************************************************************/
void XMLAssetManager::loadSceneAssets(std::string id)
{
	xercesc::DOMElement* scene = finder_.getSceneElement(id);

	if (!scene) throw std::runtime_error("Scene could not be found, id=" + id);

	//In proper order
	handleElementList(scene,finder_.TAG_EFFECT_LIST.asXMLString(), true );
	handleElementList(scene,finder_.TAG_GEOMETRY_LIST.asXMLString(), true );
	handleElementList(scene,finder_.TAG_TEXTURE_LIST.asXMLString(), true );
	handleElementList(scene,finder_.TAG_MATERIAL_LIST.asXMLString(), true );
	//Lights		
	handleElementList(scene,finder_.TAG_DIRECTIONALLIGHT_LIST.asXMLString(), true );
	handleElementList(scene,finder_.TAG_SPOTLIGHT_LIST.asXMLString(), true );
	handleElementList(scene,finder_.TAG_POINTLIGHT_LIST.asXMLString(), true );		

	//Dependent items
	handleElementList(scene,finder_.TAG_MESH_LIST.asXMLString(), true );
	handleElementList(scene,finder_.TAG_PHYSX_MESH_LIST.asXMLString(), true );

	//handleElementList(scene,finder_.TAG_OBSTACLE_LIST.asXMLString(), true );

	//Unique elements
	handleElement(finder_.getElement(finder_.TAG_LIGHTNING.asXMLString()), true );
	handleElement(finder_.getElement(finder_.TAG_RACE_TRACK.asXMLString()), true );


}

void XMLAssetManager::unloadSceneAssets(std::string id)
{
	
	xercesc::DOMElement* scene = finder_.getSceneElement(id);

	if (!scene) throw std::runtime_error("Scene could not be found, id=" + id);

	//In proper order
	handleElementList(scene,finder_.TAG_EFFECT_LIST.asXMLString(), false );
	handleElementList(scene,finder_.TAG_GEOMETRY_LIST.asXMLString(), false );
	handleElementList(scene,finder_.TAG_TEXTURE_LIST.asXMLString(), false );
	handleElementList(scene,finder_.TAG_MATERIAL_LIST.asXMLString(), false );
	//Lights		
	handleElementList(scene,finder_.TAG_DIRECTIONALLIGHT_LIST.asXMLString(), false );
	handleElementList(scene,finder_.TAG_SPOTLIGHT_LIST.asXMLString(), false );
	handleElementList(scene,finder_.TAG_POINTLIGHT_LIST.asXMLString(), false );		

	//Dependent items
	handleElementList(scene,finder_.TAG_MESH_LIST.asXMLString(), false );
	handleElementList(scene,finder_.TAG_PHYSX_MESH_LIST.asXMLString(), false );

	handleElementList(scene,finder_.TAG_OBSTACLE_LIST.asXMLString(), false );

	//Unique elements
	handleElement(finder_.getElement(finder_.TAG_LIGHTNING.asXMLString()), false );
	handleElement(finder_.getElement(finder_.TAG_RACE_TRACK.asXMLString()), false );

}

std::string XMLAssetManager::getGameFirstSceneId()
{
	xercesc::DOMElement* game = finder_.getGameElement();
	return sm.convertStr( game->getAttribute( finder_.ATTR_FIRSTSCENE.asXMLString() ) ) ;		
}

std::string XMLAssetManager::getNextSceneId(std::string currentScene)
{
	xercesc::DOMElement* scene = finder_.getSceneElement(currentScene);
	return sm.convertStr( scene->getAttribute( finder_.ATTR_NEXT_SCENE.asXMLString() ) ) ;		
}

std::string XMLAssetManager::getPreviousSceneId(std::string currentScene)
{
	xercesc::DOMElement* scene = finder_.getSceneElement(currentScene);
	return sm.convertStr( scene->getAttribute( finder_.ATTR_PREVIOUS_SCENE.asXMLString() ) ) ;		
}

XMFLOAT4 XMLAssetManager::elemToXMFLAOT4(xercesc::DOMElement* element)
{
	if (element==nullptr) return XMFLOAT4(0.f,0.f,0.f,0.f);

	XMFLOAT4 val;
	val.x = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.y = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.z = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.w = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	return val;
}

XMFLOAT3 XMLAssetManager::elemToXMFLAOT3(xercesc::DOMElement* element)
{
	if (element==nullptr) return XMFLOAT3(0.f,0.f,0.f);

	XMFLOAT3 val;
	val.x = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.y = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.z = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	return val;
}

physx::PxVec4 XMLAssetManager::elemToPxVec4(xercesc::DOMElement* element)
{
	if (element==nullptr) return PxVec4(0.f,0.f,0.f,0.f);

	PxVec4 val;
	val.x = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.y = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.z = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.w = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	return val;
}

physx::PxVec3 XMLAssetManager::elemToPxVec3(xercesc::DOMElement* element)
{
	if (element==nullptr) return PxVec3(0.f,0.f,0.f);

	PxVec3 val;
	val.x = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.y = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	val.z = lexical_cast<float>(sm.convertStr( element->getAttribute( finder_.ATTR_X.asXMLString() ) ));
	return val;
}

std::string XMLAssetManager::getVertexType(xercesc::DOMElement* element)
{
	if (!element) return "";
	return sm.convertStr( element->getAttribute( finder_.ATTR_VERTEXTYPE.asXMLString() ) ) ;	
}

std::string XMLAssetManager::getMaterialType(xercesc::DOMElement* element)
{
	if (!element) return "";
	return  sm.convertStr( element->getAttribute( finder_.ATTR_MATERIALTYPE.asXMLString() ) ) ;	
}