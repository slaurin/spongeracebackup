#pragma once

#include "Object3D.h"
#include "Effect.h"
#include "GeometryPart.h"
#include "GeometryManager.h"
#include "Mesh.h"
#include "DrawableBSpline.h"

namespace GE
{
	//! Classe publique servant de fabrique � divers objets graphiques
	/*
		La fonction principale de cette classe est d'all�ger l'�criture et de fournir un API agr�able � la cr�ation d'objets graphiques.
	*/
	struct Instancer
	{
		class EffectAndObjectAreIncompatibles {};

		//! Cr�e un Objet3D en se basant sur les templates de l'effet fourni et une geometryPart
		/*! Exemple : auto object = Create(effect, geometryPart);*/
		template<class VertexT, class MaterialT>
		static Object3D<VertexT, MaterialT> * Create(Effect<VertexT, MaterialT> * effect, GeometryPart * geometryPart)
		{
			return new Object3D<VertexT, MaterialT>(effect, geometryPart);
		}

		template<class VertexT, class MaterialT>
		static DrawableBSpline * CreateBSpline(Effect<VertexT, MaterialT> * effect, GeometryPart * geometryPart, ColorMat * colorMat)
		{
			return new DrawableBSpline(effect, colorMat, geometryPart, 100);
		}



		//! Cr�e un Objet3D en se basant sur les templates fourni avec un IEffect une geometryPart
		/*! Exemple : auto object = Create<VertexT, MaterialT>(iEffect, geometryPart);*/
		template<class VertexT, class MaterialT>
		static Object3D<VertexT, MaterialT> * Create(IEffect * iEffect, GeometryPart * geometryPart)
		{
			Effect<VertexT, MaterialT> * effect = dynamic_cast<Effect<VertexT, MaterialT> *>(iEffect);

			if(!effect)
				throw EffectAndObjectAreIncompatibles();

			return new Object3D<VertexT, MaterialT>(effect, geometryPart);
		}

		//! Cr�e un Objet3D en se basant sur les templates fourni avec un IEffect une geometryPart
		/*! Exemple : auto object = Create<VertexT, MaterialT>("effectName", geometryPart);*/
		template<class VertexT, class MaterialT>
		static Object3D<VertexT, MaterialT> * Create(const std::string & effectName, GeometryPart * geometryPart)
		{
			return Create<VertexT, MaterialT>(EffectManager::GetInstance().getEffect(effectName), geometryPart);
		}

		//! Cr�e un Mesh en se basant sur les templates de l'effet fourni et une geometry
		/*! Exemple : auto mesh = Create(effect, geometry);*/
		template<class VertexT, class MaterialT>
		static Mesh * Create(Effect<VertexT, MaterialT> * effect, Geometry * geometry)
		{
			return new Mesh(effect, geometry);
		}


		//...

	private:
		Instancer();
		Instancer(const Instancer &);
		~Instancer();
	};
}