#pragma once

#include "GameTask.h"

class FailedToInitGraphicObjects{};

class RenderTask: public GameTask
{
	class RenderTaskImp;
	RenderTaskImp *imp_;

public:
	RenderTask();
	~RenderTask();

	virtual void init() override;
	virtual void cleanup() override;
	virtual void update() override;


};