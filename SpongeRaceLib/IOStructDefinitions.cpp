/*
 *  Author : Stephane Laurin
 *  Verifier : 
 */

#include "stdafx.h"
#include "IOStructDefinitions.h"

#include <istream>
#include <ostream>

std::ostream& operator<<(std::ostream& os, const XMFLOAT3& obj)
{
	return os << obj.x << ' ' << obj.y << ' ' << obj.z << ' ';
}

std::istream& operator>>(std::istream& is, XMFLOAT3& v)
{
	if(!is)
		return is;
	float x, y, z;
	if( is >> x >> y >> z)
		v = XMFLOAT3(x, y, z);
	return is;
}

std::ostream& operator<<(std::ostream& os, const GE::PNVertex& obj)
{
	return os << obj.GetPosition() << ' ' << obj.GetNormal() << ' ';
}

std::istream& operator>>(std::istream& is, GE::PNVertex& v)
{
	if(!is)
		return is;
	XMFLOAT3 position, normal;
	if( is >> position >> normal)
		v = GE::PNVertex(position, normal);
	return is;
}