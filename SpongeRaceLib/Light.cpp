#include "stdafx.h"
#include "Light.h"

namespace GE
{
	Light::Light(XMFLOAT4 diffuse, XMFLOAT4 specular, bool enable)
		: enabled_(enable), diffuse_(diffuse), specular_(specular)
	{}

	void Light::Enable(bool enable)
	{
		enabled_ = enable;
	}

	bool Light::IsEnabled()
	{
		return enabled_;
	}

	XMFLOAT4 Light::GetDiffuse() const
	{
		return diffuse_;
	}

	XMFLOAT4 Light::GetSpecular() const
	{
		return specular_;
	}

	void Light::SetDiffuse(const XMFLOAT4 & diffuse)
	{
		diffuse_ = diffuse;
	}

	void Light::SetSpecular(const XMFLOAT4 & specular)
	{
		specular_ = specular;
	}

}