#pragma once

#include "Texture.h"

namespace GameUI
{
	enum PageEvent {
		NEXT,		// Va � la page suivante
		PREVIOUS,	// Va � la page pr�c�dente
		OK,			// Va dans la page enfant
		RETURN		// Retourne � la page parent
	};

	class MenuPage
	{
		std::string fileName_;
		std::map<PageEvent, MenuPage *> navigation_;
		GE::Texture * texture_;

	public:
		MenuPage(const std::string & filename);
		MenuPage(GE::Texture * texture);
		void SetNavigation(PageEvent event, MenuPage * page);
		MenuPage * Naviguate(PageEvent event);
		GE::Texture * GetTexture();
		~MenuPage();
	};
}