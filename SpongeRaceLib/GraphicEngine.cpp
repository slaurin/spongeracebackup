#include "stdafx.h"
#include "SpongeRace.h"
#include "CameraActor.h"
#include "GraphicEngine.h"
#include "resource.h"
#include "DxUtilities.h"
#include "Window.h"

#include <GdiPlus.h>

using namespace std;
using namespace DxUtilities;
using namespace Gdiplus;

namespace GE
{
	const float	GraphicEngine::FPRECISION	= 0.00001f;
	const double GraphicEngine::DPRECISION	= 0.000000001;
	ULONG_PTR GraphicEngine::gdiToken_ = 0;

	GraphicEngine::GraphicEngine()
		: device_(nullptr)
	{
		GdiplusStartupInput  startupInput(0, TRUE, TRUE);
		GdiplusStartupOutput startupOutput;
		GdiplusStartup(&gdiToken_, &startupInput, &startupOutput);
	}

	GraphicEngine::~GraphicEngine()
	{
		Cleanup();
		GdiplusShutdown(gdiToken_);
	}

	int GraphicEngine::Initialisations()
	{
		// Initialisation du dispositif de rendu
		device_ = new Device(Device::WINDOWED);

		// Cr�ation d'une sc�ne par d�faut
		scenes_.push_back(new Scene("Default Scene", true));
		currentScene_ = size_t();
		nextScene_ = currentScene_;

		// Initialisation des matrices
		initMatrices();

		InitPostEffect();

		return 0;
	}

	Scene * GraphicEngine::GetScene(sceneIndex index)
	{
		if(index < scenes_.size())
			return scenes_[index];
		return nullptr;
	}

	void GraphicEngine::Present()
	{
		device_->Present();

		if(nextScene_ != currentScene_)
			currentScene_ = nextScene_;
	}

	void GraphicEngine::AnimeScene(float timeElapsed)
	{
		GetCurrentScene()->Anime(timeElapsed);
	}

	bool GraphicEngine::RenderPostEffect()
	{
		if(GetCurrentScene()->IsPostEffectEnable())
		{
			StartPostEffect();
			RenderScene(true);
			EndPostEffect();
			return true;
		}
		return false;		
	}

	bool GraphicEngine::RenderScene(bool PostEffect)
	{
		ID3D11DeviceContext* pImmediateContext = device_->GetDeviceContext();
		ID3D11RenderTargetView* pRenderTargetView = device_->GetRenderTargetView();

		// On efface la surface de rendu
		float Couleur[4] = { 0.0f, 0.0f, 0.f, 1.0f };  //  RGBA - Noir pour le moment
		pImmediateContext->ClearRenderTargetView( pRenderTargetView, Couleur );	

		// On r�-initialise le tampon de profondeur
		ID3D11DepthStencilView* pDepthStencilView = device_->GetDepthStencilView();	
		pImmediateContext->ClearDepthStencilView( pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0 );

		// Appeler les fonctions de dessin de chaque objet de la sc�ne
		GetCurrentScene()->Render();

		if(!PostEffect) {
			GetCurrentScene()->RenderBubbles();
			RenderHud();
		}

		//DXRelacher(pImmediateContext);

		return true;
	}

	bool GraphicEngine::AddObjectToHud(GraphicObject * object)
	{
		if(!object)
			return false;

		hudObjects_.push_back(object);
		return true;
	}

	void GraphicEngine::RenderHud()
	{
		// Appelle les fonctions de dessin de chaque objet du HUD de la sc�ne (doit etre dessin� en dernier)
		for_each(hudObjects_.begin(), hudObjects_.end(), mem_fun(&GraphicObject::Draw));
	}


	void GraphicEngine::ResizeDevice(int width, int height)
	{
		if(device_)
		{
			device_->Resize(width, height);
			initMatrices();
		}
	}

	void GraphicEngine::SwitchRasterizer()
	{
		if(device_)
			device_->SwitchRasterizer();
	}

	bool GraphicEngine::AddObjectToScene(GraphicObject * object)
	{
		return GetCurrentScene()->AddObject(object);
	}

	bool GraphicEngine::AddObjectToScene(GraphicObject * object, sceneIndex index)
	{
		assert(index < scenes_.size());
		return scenes_[index]->AddObject(object);
	}

	void GraphicEngine::Cleanup()
	{
		DXRelacher(pResourceView);
		DXRelacher(pRenderTargetView);
		DXRelacher(pTextureScene);
		DXRelacher(pDepthStencilView);
		DXRelacher(pDepthTexture);

		// Nettoie toutes les sc�nes
		for_each(scenes_.begin(), scenes_.end(), [](Scene * scene){
			delete scene;
		});

		// D�truit les objets du HUD
		for_each(hudObjects_.begin(), hudObjects_.end(), [](GraphicObject * object){
			delete object;
		});

		scenes_.clear();
		hudObjects_.clear();

		// D�truire le dispositif
		if(device_) 
		{
			delete device_;
			device_ = nullptr;
		}
	}

	auto GraphicEngine::NewScene(const string & name, bool enablePostEffect) -> sceneIndex
	{
		scenes_.push_back(new Scene(name, enablePostEffect));
		return scenes_.size() - 1;
	}

	auto GraphicEngine::SwitchScene(sceneIndex index) -> sceneIndex
	{
		auto oldScene = currentScene_;

		if(index < scenes_.size())
		{
			nextScene_ = index;
		}

		return oldScene;
	}

	Scene * GraphicEngine::GetCurrentScene()
	{
		if(currentScene_ >= scenes_.size())
			throw InvalidCurrentScene();
		
		return scenes_[currentScene_];
	}

	auto GraphicEngine::GetCurrentSceneIndex() -> sceneIndex
	{
		return currentScene_;
	}

	int GraphicEngine::initMatrices()
	{

		float champDeVision = XM_PI/4; 	// 45 degr�s
		float ratioDAspect = Window::GetInstance().GetRatio();
		float planRapproche = 1.0;
		float planEloigne = 10000.0;
		
		projMat_ = XMMatrixPerspectiveFovLH( 
									champDeVision,
									ratioDAspect,
									planRapproche,
									planEloigne );

		return 0;
	}

	XMMATRIX GraphicEngine::GetMatView()
	{
		return GAME->camera()->getViewMatrix();
	}

	XMMATRIX GraphicEngine::GetMatProj()
	{return projMat_;}

	XMMATRIX GraphicEngine::GetMatViewProj()
	{
		// Calcul de VP � l'avance
		XMMATRIX &view = GetMatView();
		return view * projMat_;
	}

	Device * GraphicEngine::GetDevice()
	{return device_;}


	void GraphicEngine::InitPostEffect()
	{
		// *********************** POUR LE POST EFFECT **************************
		D3D11_TEXTURE2D_DESC textureDesc;
		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

		// Description de la texture
		ZeroMemory(&textureDesc, sizeof(textureDesc));

		// Cette texture sera utilis�e comme cible de rendu et
		// comme ressource de shader
		textureDesc.Width = Window::GetInstance().GetScreenWidth();
		textureDesc.Height = Window::GetInstance().GetScreenHeight();
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET|D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		// Cr�ation de la texture
		device_->GetDXDevice()->CreateTexture2D(&textureDesc, NULL, & pTextureScene);

		// VUE - Cible de rendu
		renderTargetViewDesc.Format = textureDesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		// Cr�ation de la vue.
		device_->GetDXDevice()->CreateRenderTargetView(pTextureScene,
			&renderTargetViewDesc,
			&pRenderTargetView);

		// VUE � Ressource de shader
		shaderResourceViewDesc.Format = textureDesc.Format;
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
		shaderResourceViewDesc.Texture2D.MipLevels = 1;

		// Cr�ation de la vue.
		device_->GetDXDevice()->CreateShaderResourceView( pTextureScene,
			&shaderResourceViewDesc,
			&pResourceView);

		// Au tour du tampon de profondeur
		D3D11_TEXTURE2D_DESC depthTextureDesc;
		ZeroMemory( &depthTextureDesc, sizeof( depthTextureDesc ) );
		depthTextureDesc.Width = Window::GetInstance().GetScreenWidth();
		depthTextureDesc.Height = Window::GetInstance().GetScreenHeight();
		depthTextureDesc.MipLevels = 1;
		depthTextureDesc.ArraySize = 1;
		depthTextureDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthTextureDesc.SampleDesc.Count = 1;
		depthTextureDesc.SampleDesc.Quality = 0;
		depthTextureDesc.Usage = D3D11_USAGE_DEFAULT;
		depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthTextureDesc.CPUAccessFlags = 0;
		depthTextureDesc.MiscFlags = 0;

		DXEssayer( device_->GetDXDevice()->CreateTexture2D( &depthTextureDesc, NULL, &pDepthTexture ),
			DXE_ERREURCREATIONTEXTURE );

		// Cr�ation de la vue du tampon de profondeur (ou de stencil)
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSView;
		ZeroMemory( &descDSView, sizeof( descDSView ) );
		descDSView.Format = depthTextureDesc.Format;
		descDSView.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSView.Texture2D.MipSlice = 0;
		DXEssayer( device_->GetDXDevice()->CreateDepthStencilView( pDepthTexture, &descDSView,
			&pDepthStencilView ),
			DXE_ERREURCREATIONDEPTHSTENCILTARGET );
	}

	void GraphicEngine::StartPostEffect()
	{
		// Prendre en note l'ancienne surface de rendu
		pOldRenderTargetView = device_->GetRenderTargetView();
		// Prendre en note l'ancienne surface de tampon Z
		pOldDepthStencilView = device_->GetDepthStencilView();
		// Utiliser la texture comme surface de rendu et le tampon de profondeur
		// associ�
		device_->SetRenderTargetView(pRenderTargetView, pDepthStencilView);		
	}

	void GraphicEngine::EndPostEffect()
	{
		// Restaurer l'ancienne surface de rendu et le tampon de profondeur
		// associ�
		device_->SetRenderTargetView(pOldRenderTargetView,
			pOldDepthStencilView);
	}
}