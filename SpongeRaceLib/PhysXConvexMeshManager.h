#pragma once

#include "SpongeRace.h"
#include "PhysXConverter.h"
#include "IRessourceManager.h"

class PhysXConvexMeshManager : public IRessourceManager<PhysXConvexMeshManager, std::string, physx::PxConvexMesh*>
{

	friend class Singleton<PhysXConvexMeshManager>;
public:

	physx::PxConvexMesh * Request(const std::string &resourceName, GeometryPart* mesh)
	{
		if(!Contains(resourceName))
			add(resourceName, GenerateConvexFromGeometry(GAME->scene()->getPhysics(), mesh));
		return operator[](resourceName);
	}
};
