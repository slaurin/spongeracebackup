#pragma once

#include "GraphicObject.h"
#include "Light.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "SpotLight.h"

namespace GE
{
	//! Repr�sente une sc�ne graphique contenant des objets graphiques et des lumi�res
	class Scene
	{
		//! Objets de la sc�ne
		std::vector<GraphicObject *> objects_;

		//! PostEffect Bubbles
		std::vector<GraphicObject *> bubbles_;

		//! Lumi�res ponctuelles de la sc�ne
		std::vector<PointLight *> pointLights_;

		//! Lumi�res directionnelles de la sc�ne
		std::vector<DirectionalLight *> directionalLights_;

		//! Lumi�res Spot de la sc�ne
		std::vector<SpotLight *> spotLights_;

		//! Nom de la sc�ne
		std::string name_;

		//! Valeur d'illumination ambiente de la sc�ne
		XMFLOAT4 ambientLighting_;

		//! Activation des posts effects
		bool enablePostEffect_;

	public:
		Scene(const std::string & name, bool enablePostEffect = false, const XMFLOAT4 & ambientLighting = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f));
		~Scene();

		//Ajoute une bulle � la scene
		bool AddBubbles(GraphicObject *);
		void RemoveBubble(GraphicObject *object);

		//! Ajoute un objet � la sc�ne
		bool AddObject(GraphicObject *);

		//! Ajoute une lumi�re ponctuelle � la sc�ne
		bool AddLight(const PointLight &);

		//! Ajoute une lumi�re directionnelle � la sc�ne
		bool AddLight(const DirectionalLight &);

		//! Ajoute une lumi�re spot � la sc�ne
		bool AddLight(const SpotLight &);

		//! Ajoute une lumi�re ponctuelle � la sc�ne
		bool AddLight(PointLight *);

		//! Ajoute une lumi�re directionnelle � la sc�ne
		bool AddLight(DirectionalLight *);

		//! Ajoute une lumi�re spot � la sc�ne
		bool AddLight(SpotLight *);

		//! Retourne la valeur d'illumination ambiente de la sc�ne
		XMFLOAT4 GetAmbientLighting() const;

		//! Fixe la valeur d'illumination ambiente de la sc�ne
		void SetAmbientLighting(const XMFLOAT4 &);

		//! Retourne la liste des lumi�res ponctuelles de la sc�ne
		std::vector<PointLight *> & GetPointLights();

		//! Retourne la liste des lumi�res directionnelles de la sc�ne
		std::vector<DirectionalLight *> & GetDirectionalLights();

		//! Retourne la liste des lumi�res spot de la sc�ne
		std::vector<SpotLight *> & GetSpotLights();

		//! Anime la sc�ne
		void Anime(float timeElapsed);

		//! Determine si les post effects sont activ�s
		bool IsPostEffectEnable();

		//! Active/Desactive les posts effects
		void SetPostEffect(bool enable);

		//! Rend les objets de la sc�ne
		void Render();

		//! Rend les objets post effect (bubbles)
		void RenderBubbles();

		//! Nettoie la sc�ne
		void Cleanup();
	};
}