#include "stdafx.h"
#include "Axes.h"
#include "DxUtilities.h"
#include "resource.h"
#include "GraphicEngine.h"
#include "Effect.h"

using namespace DxUtilities;

namespace GE
{


	SimpleVertex  point[] =
	{
		// line list of the axes.
		SimpleVertex( XMFLOAT3( 0,0,0 ), XMFLOAT3( 1,0,0 )), // start of first line at (0,0,0) (color = red)
		SimpleVertex( XMFLOAT3( 1,0,0  ), XMFLOAT3( 1,0,0 )), // end of first line at (1,0,0) (color = red)
		SimpleVertex( XMFLOAT3( 0,0,0  ), XMFLOAT3( 0,1,0 )), // start of second line at (0,0,0) (color = green)  
		SimpleVertex( XMFLOAT3( 0,1,0  ), XMFLOAT3( 0,1,0 )), // end of second line at (0,1,0) (color = green)
		SimpleVertex( XMFLOAT3( 0,0,0  ), XMFLOAT3( 0,0,1 )), // start of third line at (0,0,0) (color = blue)  
		SimpleVertex( XMFLOAT3( 0,0,1  ), XMFLOAT3( 0,0,1 )), // end of third line at (0,0,1) (color = blue)

		// point list for your points.
		SimpleVertex( XMFLOAT3( -1.0f, 1.0f, -1.0f ), XMFLOAT3( 0.0f, 0.0f, 1.0f)),
		SimpleVertex( XMFLOAT3( 1.0f, 1.0f, -1.0f ), XMFLOAT3( 0.0f, 1.0f, 0.0f )),
		SimpleVertex( XMFLOAT3( 1.0f, 1.0f, 1.0f ), XMFLOAT3( 0.0f, 1.0f, 1.0f )),
		SimpleVertex( XMFLOAT3( -1.0f, 1.0f, 1.0f ), XMFLOAT3( 1.0f, 0.0f, 0.0f )),
		SimpleVertex( XMFLOAT3( -1.0f, -1.0f, -1.0f ), XMFLOAT3( 1.0f, 0.0f, 1.0f )),
		SimpleVertex( XMFLOAT3( 1.0f, -1.0f, -1.0f ), XMFLOAT3( 1.0f, 1.0f, 0.0f)),
		SimpleVertex( XMFLOAT3( 1.0f, -1.0f, 1.0f ), XMFLOAT3( 1.0f, 1.0f, 1.0f)),
		SimpleVertex( XMFLOAT3( -1.0f, -1.0f, 1.0f ), XMFLOAT3( 0.0f, 0.0f, 0.0f )),
	};

	Axes::Axes(Effect<SimpleVertex, ColorMat> *effect, ColorMat *pMaterial, GraphicObject *parent)
		: Object3D<SimpleVertex, ColorMat>(effect, pMaterial), parent_(parent)
	{
		Init();
	}

	Axes::Axes(Effect<SimpleVertex, ColorMat> *effect, ColorMat *pMaterial)
		: Object3D<SimpleVertex, ColorMat>(effect, pMaterial), parent_(nullptr)
	{
		Init();
	}

	void Axes::Init()
	{	
		ID3D11Device* pD3DDevice = GraphicEngine::GetInstance().GetDevice()->GetDXDevice();

		D3D11_BUFFER_DESC bd;
		
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof( SimpleVertex  ) * 14; 
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = &point[0];

		DxUtilities::DXEssayer(
			pD3DDevice->CreateBuffer(
			&bd,
			&InitData,
			&vertexBuffer_),
			DXE_CREATIONVERTEXBUFFER);
	}

	void Axes::Draw()
	{

		ID3D11DeviceContext * g_pImmediateContext = GraphicEngine::GetInstance().GetDevice()->GetDeviceContext();

		// Vertex buffer
		UINT stride = sizeof(SimpleVertex);
		UINT offset = 0;

		// pr�pare le shader fx (configuration du pipeline graphique puis appelle SendToPipeline par d�faut, ici c'est pas ce qu'on veut)
		material_->Apply(effect_, *this);

		// config perso
		g_pImmediateContext->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);
		g_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
		
		// draw
		g_pImmediateContext->Draw(6, 0);

		g_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
		g_pImmediateContext->Draw(8, 6);

		// Ne surtout pas appeler la config du pipeline apr�s vouloir dessiner!
		//material_->Apply(effect_, *this);
	}

	void Axes::Anime(float timeElapsed)
	{		
	
		if(parent_) {
			Object3D::SetWorldMatrix(parent_->GetWorldMatrix());
		}
	}
}