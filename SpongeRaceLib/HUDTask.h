#pragma once

#include "GameTask.h"
#include "HUD.h"
#include "Chrono.h"
#include "ITriggerCallback.h"

class HUDTask
	: public GameTask
{
	struct TimeTrigger
		: public ITriggerCallback
	{
		TimeTrigger();
		bool notified_;
		void Notify();
	};

	GameUI::HUD * hud_;
	Chrono chrono_;
	TimeTrigger start_;
	TimeTrigger end_;
	bool needTimer_;

public:
	HUDTask();
	~HUDTask();

	void init();
	void cleanup();
	void update();
	void SetPause(bool pause);

	TimeTrigger * GetTriggerStart();
	TimeTrigger * GetTriggerEnd();
};

