#include "stdafx.h"
#include "DirectionalLight.h"

namespace GE
{
	DirectionalLight::DirectionalLight(const XMFLOAT4 & direction, XMFLOAT4 diffuse, XMFLOAT4 specular)
		: Light(diffuse, specular), direction_(direction)
	{}
	
	void DirectionalLight::SetDirection(const XMFLOAT4 & direction)
	{
		direction_ = direction;
	}

	XMFLOAT4 DirectionalLight::GetDirection() const
	{
		return direction_;
	}
}