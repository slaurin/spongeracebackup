#pragma once

#include "Object3D.h"
#include "ColorMat.h"
#include "Effect.h"
#include "SimpleVertex.h"

namespace GE
{


	/*! Cube en 3D (est un Object3D) */
	class Axes
		: public Object3D<SimpleVertex, ColorMat>
	{
		GraphicObject* parent_;

	public:
		/*! Constructeur */
		Axes(Effect<SimpleVertex, ColorMat> *effect, ColorMat *pMaterial);
		Axes(Effect<SimpleVertex, ColorMat> *effect, ColorMat *pMaterial, GraphicObject *);
		void Init();
		void CreateVertexBuffer();
		void Draw();

		/*! Surchage de la m�thode d'animation */
		void Anime(float timeElapsed);

	};
}

	