#include "stdafx.h"
#include "ConfigManager.h"

using std::string;

namespace fs = boost::filesystem;



ConfigManager::ConfigManager()
{
	fs::path full_path( fs::initial_path<fs::path>() );	
	configFileFullPath = full_path.string() + "\\" + ConfigManager::getDefaultConfigName();

	//Load default
	//setValue("FPS", 20);
}


ConfigManager::~ConfigManager() throw()
{
}


std::string ConfigManager::getStrValue(std::string id)
{
	return strConfigs_.find(id)->second;
}

void ConfigManager::setStrValue(std::string id, std::string value)
{
	strConfigs_[id] = value;
}

int ConfigManager::getIntValue(std::string id)
{
	return intConfigs_.find(id)->second;
}

void ConfigManager::setIntValue(std::string id, int value)
{
	intConfigs_[id] = value;
}

float ConfigManager::getFltValue(std::string id)
{
	return fltConfigs_.find(id)->second;
}

void ConfigManager::setFltValue(std::string id, float value)
{
	fltConfigs_[id] = value;
}

void ConfigManager::save()
{
	// make an archive
	std::ofstream ofs(configFileFullPath);
	assert(ofs.good());
	boost::archive::xml_oarchive oa(ofs);
	oa << BOOST_SERIALIZATION_NVP(this);

}

void ConfigManager::save(const std::string &filename)
{
	// make an archive
	std::ofstream ofs(filename);
	assert(ofs.good());
	boost::archive::xml_oarchive oa(ofs);
	oa << BOOST_SERIALIZATION_NVP(this);

}

void ConfigManager::load(ConfigManager &obj)
{
	if (!configExist()) {
		//Le fichier n'existe pas, rien � loader
		return;
	}

	// open the archive
	std::ifstream ifs(configFileFullPath);

	boost::archive::xml_iarchive ia(ifs);

	// restore the schedule from the archive
	ia >> BOOST_SERIALIZATION_NVP(obj);
}

void ConfigManager::load(ConfigManager &obj, const string &filePath)
{
	// open the archive
	std::ifstream ifs(filePath);
	
	if (!ifs.good()) throw ConfigFileNotFound();

	boost::archive::xml_iarchive ia(ifs);

	// restore the schedule from the archive
	ia >> BOOST_SERIALIZATION_NVP(obj);
}

bool ConfigManager::configExist()
			{ return !!(std::ifstream(configFileFullPath)); }

bool ConfigManager::configExist( const string &filePath)
			{ return !!(std::ifstream(filePath)); }
