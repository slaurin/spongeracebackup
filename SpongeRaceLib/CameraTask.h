#pragma once

#include "GameTask.h"

class CameraTask: public GameTask
{
public:
	virtual void init() override;

	virtual void cleanup() override;

	virtual void update() override;
};