/************************************************************************/
/* New Camera                                                                     */
/************************************************************************/


#include "stdafx.h"

#include "SpongeRace.h"
#include "ConstantsControl.h"
#include "PlayerActor.h"
#include "Controller.h"
#include "CameraActor.h"
#include "PxPhysicsAPI.h"

#include "Splines.h"

using namespace std;
using namespace physx;
using namespace GE;

using std::cout;
using std::endl;

class GE::GraphicObject;


//THIRD_PERSON
#define RANGE_TO_PLAYER 20
#define HEIGHT_TO_PLAYER 4
#define SPEED_TO_GO_TO_PLAYER 20
#define MAX_RANGE_TO_PLAYER 20

//FIRST_PERSON  OFFSET
#define OFFSET_Y 1
//=============================================================================
// CLASS CameraActorData
//=============================================================================
class CameraActorData: public IActorData
{
public:
	// physics
	PxMaterial *material_;
	PxShape* actorShape_;

public:
	CameraActorData()
		: material_(nullptr)
	{}

	int load()
	{
		//---------------------------------------------------------------------
		// create the physic object
		PxPhysics &physics = GAME->scene()->getPhysics();

		//static friction, dynamic friction, restitution
		material_ = physics.createMaterial(0.f, 0.f, 1.f);

		return 0;
	}

};


class CameraActorState
{
public:
	CameraActorState()
		: _lastFireTime(0)
	{}

	double _lastFireTime;
};

const unsigned char CameraActor::DefaultCameraKeyMapping[] = {
	DIK_W, 
	DIK_S,
	DIK_D, 
	DIK_A, 
	DIK_E, 
	DIK_Q};

	class CameraActorImp : public Actor<CameraActorData, CameraActor>
	{



	public:

		CameraActorState state_;
		XMMATRIX camView_;
		std::vector<Actor*> visibleActorList;
		int mode_;
		float camDistance_;
		
		Splines::spline_type* currentSpline_;
		bool onSpline_;

	public:
		CameraActorImp(const IActorDataRef &aDataRef)
			: Actor(aDataRef, typeId()),onSpline_(false),currentSpline_(nullptr)
		{
			std::copy ( DefaultCameraKeyMapping, DefaultCameraKeyMapping + ACTION_COUNT, std::back_inserter(keyMapping_));
		}

	public:
		
		void sweptScene()
		{

		}

		void switchMode(int newMode)
		{
			mode_=newMode;
			switch(mode_)
			{
				case FREECAM :
				case FIRST_PERSON:
					DisablePhysX();
					break;
				default:
					EnablePhysX();
					break;
			}
			

			switch(mode_)
			{
			case FREECAM :
				UserController::GetInstance().setUsingKeyboard(true);
				UserController::GetInstance().setUsingMouse(true);
				UserController::GetInstance().setUsingJoystick(false);

				UserController::GetInstance().Initialise();
				UserController::GetInstance().setActor(this);
				break;
							
			default:
				UserController::GetInstance().setUsingKeyboard(true);
				UserController::GetInstance().setUsingMouse(false);
				UserController::GetInstance().setUsingJoystick(false);

				UserController::GetInstance().Initialise();
				UserController::GetInstance().setActor(GAME->player());          
				break;

			}

		}

		
		void onContact(const physx::PxContactPair &aContactPair)
		{		
			pxActor_->clearTorque();
			pxActor_->clearForce();			
		}

		void switchPlayerCamera()
		{
			mode_^=1;
			switchMode(mode_);
		}

		void switchToFromFreeCam()
		{
			if (mode_==FREECAM)
			{
				mode_=THIRD_PERSON;                                             
			} else {
				mode_=FREECAM;                                  
			}

			switchMode(mode_);
		}


		XMMATRIX & getViewMatrix()
		{
			switch(mode_) {
			case THIRD_PERSON:
			case FIXEDCAM:
				camView_ = XMMatrixLookAtLH( getPosition(), GAME->player()->getPosition(), getUp() );
				break;
			default:
				camView_ = XMMatrixLookAtLH( getPosition(), getPosition() + getDirection(), getUp() );
			}

			return camView_;
		};

		void setGraphicObj(GraphicObject* obj) override {};

		//-------------------------------------------------------------------------
		//
		static ActorFactory::ActorID typeId()
		{
			return CameraActor::typeId();
		}


		//-------------------------------------------------------------------------
		//
		static IActorData* loadData()
		{
			CameraActorData *data = new CameraActorData();
			data->load();
			return data;
		}

		//-------------------------------------------------------------------------
		//
		static IActor* createInstance(const IActorDataRef &aDataRef)
		{
			CameraActorImp *actor = new CameraActorImp(aDataRef);

			return actor;
		}

		//-------------------------------------------------------------------------
		//
		virtual void onSpawn(const PxTransform &aPose, PxGeometry * geometry) override
		{
			// create the physic object
			PxPhysics &physics = GAME->scene()->getPhysics();

			pxActor_ = GAME->scene()->getPhysics().createRigidDynamic(aPose);
			pxActor_->userData = this;
			pxActor_->setName("Camera");

			actorShape_ = pxActor_->createShape(PxSphereGeometry(0.01f), *_data->material_);
			actorShape_->setName("Camera");
			actorShape_->setFlag(PxShapeFlag::eUSE_SWEPT_BOUNDS, true);

			pxActor_->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
			GAME->scene()->addActor(*pxActor_);
			
			pxActor_->setMaxAngularVelocity(0.);
			pxActor_->setAngularDamping(55550.f);

			PxFilterData filterData;
			filterData.word0 = eACTOR_PLAYER;
			filterData.word1 = eACTOR_ENEMY | eACTOR_TERRAIN | eACTOR_OBSTACLE;
			actorShape_->setSimulationFilterData(filterData);

			state_._lastFireTime = 0;
		}

		//-------------------------------------------------------------------------
		//
		virtual void onUnspawn() override
		{
			// cleanup physics objects
			if (pxActor_)
			{
				pxActor_->release();
				pxActor_= nullptr;
			}
		}


		//-------------------------------------------------------------------------
		//
		virtual void update(float timeElapsed)
		{
			if (mode_==FIRST_PERSON)
			{                               
				PxTransform transform = GAME->player()->pose();
				PxVec3 frontVector = transform.q.rotate(PxVec3(0, 0, 1));		
				transform.p += frontVector;
				transform.p.y += OFFSET_Y;

				pxActor_->setGlobalPose(transform);
				PxVec3 lookPos = transform.p + frontVector;
				//			LookAt(lookPos.x,lookPos.y,lookPos.z);
				return;
			} 

			else if (mode_==THIRD_PERSON)
			{
				PxTransform transform = GAME->player()->pose();
				PxTransform currentTransform =  pxActor_->getGlobalPose();				
			
				PxVec3 frontVector = transform.q.getBasisVector2();
			
				if(!onSpline_)
				{
					transform.p -= frontVector*RANGE_TO_PLAYER; //nouvelle position =  joueur + offset
					transform.p.y += HEIGHT_TO_PLAYER;
				}
				else
				{                                   //nouvelle position = point sur la spline
					assert(currentSpline_);
					auto valuePos = currentSpline_->FindTimeFromPoint(XMVectorSet(transform.p.x,transform.p.y,transform.p.z,0.f),-RANGE_TO_PLAYER*0.001f);
					transform.p = PxVec3(valuePos.x,valuePos.y,valuePos.z);
				}					

				PxVec3 range = transform.p - currentTransform.p;
				float distance = std::sqrt(range.x*range.x + range.y*range.y + range.z*range.z);

				//currentTransform.q = transform.q;
				//if(distance < MAX_RANGE_TO_PLAYER)
					currentTransform.p += range/SPEED_TO_GO_TO_PLAYER;  // � tester avec une bon lookAt
				//else currentTransform.p += range.getNormalized()*MAX_RANGE_TO_PLAYER;

				

				/*PxVec3 upVector = transform.q.rotate(PxVec3(0, -1, 0));		
				transform.p -= upVector;*/
				//TODO  lookat
				pxActor_->setGlobalPose(currentTransform);
				//LookAt(transform.p.x,transform.p.y+HEIGHT_TO_PLAYER,transform.p.z);
				return;
			}

			else if (mode_==FREECAM)
			{
				if(yaw_ == 0 && pitch_ == 0 && roll_ == 0 &&
					moveBackForward_ == 0 && moveUpDown_ == 0 && moveLeftRight_ == 0)
					return;
                yaw_/=10.0f;
				Rotate();
				Move(timeElapsed);
				//PxTransform transform = pxActor_->getGlobalPose();
				//
				//transform.q = PxQuat(pitch_,  PxVec3(0.f,0.f,1.f))*PxQuat(yaw_, PxVec3(0.f,1.f,0.f)) * transform.q ;
				
				//yaw_   = 0.0f;
				//pitch_ = 0.0f;
				//roll_  = 0.0f;

				//PxVec3 velocity (0, 0, 0);
				//float dirSpeed = SpeedRelated(moveBackForward_);// * timeElapsed;
				//float upSpeed = SpeedRelated(moveUpDown_);// * timeElapsed;
				//float rightSpeed = SpeedRelated(moveLeftRight_);// * timeElapsed;
				//velocity += getPxDirection() * dirSpeed;
				//velocity += getPxUp() * upSpeed;
				//velocity += getPxRight() * rightSpeed;
				//transform.p += velocity;

				//velocity / timeElapsed;

				//pxActor_->setGlobalPose(transform);

				//moveLeftRight_ = 0.0f;
				//moveBackForward_ = 0.0f;
				//moveUpDown_ = 0.0f;
				return;
			}
			
			//actorShape_->setLocalPose(PxTransform(PxVec3(position_.x, position_.y, position_.z)));
		}
		
		void switchToSpline(int splineIndex)
		{
			currentSpline_ = Splines::GetInstance().getSpline(splineIndex);
			onSpline_ = true;
		}
		void leaveSpline()
		{
			onSpline_ = false;
		}

	};


	//-----------------------------------------------------------------------------
	//
	ActorFactory::ActorID CameraActor::typeId()
	{       return "CameraActor"; }

	RegisterActorType<CameraActorImp> gRegisterActor;