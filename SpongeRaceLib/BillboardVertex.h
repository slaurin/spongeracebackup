#ifndef BILLBOARD_VERTEX_H
#define BILLBOARD_VERTEX_H

#include "stdafx.h"

namespace GE
{
    class BillboardVertex
    {
    public:
        XMFLOAT4 position_;
        XMFLOAT2 textureCoord_;
    private:
        static D3D11_INPUT_ELEMENT_DESC Layout_[];

    public:
        BillboardVertex(XMFLOAT4 position = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
                        XMFLOAT2 textureCoord = XMFLOAT2(0.0f, 0.0f));

        static D3D11_INPUT_ELEMENT_DESC * Begin();
        static UINT Size();
    };
}

#endif