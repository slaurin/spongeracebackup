#ifndef DYNAMICACTOR_H
#define DYNAMICACTOR_H

#include "UserInput.h"
#include "Actor.h"

class Controller;

class UnmanagedControl{};

class DynamicActor : public ActorBase
{
	static const unsigned char DefaultKeyMapping[];

protected :
	physx::PxRigidDynamic *pxActor_;

	std::vector<unsigned char> keyMapping_;

	bool dead_;

	float yawSpeed;
	float rollSpeed;
	float pitchSpeed;

	float speed_; //general speed
	float acceleration_;

	float moveLeftRight_;
	float moveBackForward_;
	float moveUpDown_;

	float yaw_;                 // rotation around the y axis
	float pitch_;               // rotation around the x axis
	float roll_;                // rotation around the z axis

	Controller* controller_;

public :
	DynamicActor(const ActorFactory::ActorID &aActorId)
		: ActorBase(aActorId),
		pxActor_(nullptr),
		speed_(15),		
		yaw_(),
		roll_(),
		pitch_(),	
		moveLeftRight_(),
		moveBackForward_(),
		moveUpDown_(),
		controller_(nullptr),
		dead_(false)
	{
		
	}

	virtual physx::PxTransform pose()
	{
		return pxActor_->getGlobalPose();
	}

	void setController(Controller* controller){
		controller_=controller;
	};

	void setKeyMapping(std::vector<unsigned char> &keys){keyMapping_=keys;};
	virtual std::vector<unsigned char> &getKeyMapping(){return keyMapping_;};

	physx::PxRigidDynamic *GetPxActor(){return pxActor_;};

	float getYaw(){return yaw_;};
	float getPitch(){return pitch_;};
	float getRoll(){return roll_;};
	float getSpeed(){return speed_;};

	void setYaw(float val){yaw_=val;};
	void setPitch(float val){pitch_=val;};
	void setRoll(float val){roll_=val;};
	void setSpeed(float speed){speed_=speed;};

	bool IsDead(){return dead_;};

	void moveBackwardForward(float movementAmount){	
		moveBackForward_+=movementAmount;
	};
	void moveLeftRight(float movementAmount){	
		moveLeftRight_+=movementAmount;
	};
	void moveUpDown(float movementAmount){	
		moveUpDown_+=movementAmount;
	};

	void moveForward(float movementAmount){
		moveBackForward_+=movementAmount;
	};
	void moveBackward(float movementAmount){	
		moveBackForward_-=movementAmount;
	};
	void strafeLeft(float movementAmount){	
		moveLeftRight_-=movementAmount;
	};
	void strafeRight(float movementAmount){		
		moveLeftRight_+=movementAmount;
	};
	void moveUp(float movementAmount){
		moveUpDown_+=movementAmount;
	};
	void moveDown(float movementAmount){
		moveUpDown_-=movementAmount;
	};

	void Yaw(float amount){yaw_=RestrictAngleTo360Range(yaw_ + amount);};
	void turnRight(float amount){yaw_=RestrictAngleTo360Range(yaw_ + amount);};
	void turnLeft(float amount){yaw_=RestrictAngleTo360Range(yaw_ - amount);};
	void Pitch(float amount){pitch_=RestrictAngleTo360Range(pitch_ + amount);};
	void Roll(float amount){roll_=RestrictAngleTo360Range(roll_ + amount);};


	/*void LookAt(XMVECTOR lookAt) 
	{
		LookAt(PxVec3(lookAt.x, lookAt.y, lookAt.z));
	}

	void LookAt(float x, float y, float z) 
	{
		LookAt(PxVec3(x, y, z));
	}

	void LookAt(PxVec3 lookAt)
	{
		PxTransform transform = pose();
		PxVec3 pos = transform.p;

		PxVec3 newDir = (lookAt-pos).getNormalized();

		PxVec3 axe = getPxDirection().cross(newDir);
		if(axe.normalizeSafe() == 0.0f)
			return;
		PxReal tmp = getPxDirection().dot(newDir);
		PxReal angle = acos(tmp);

		transform.q = PxQuat(angle, axe.getNormalized());
		pxActor_->setGlobalPose(transform);
	}*/

	void Rotate()
	{
		physx::PxTransform transform = pxActor_->getGlobalPose();
		physx::PxQuat rotation = physx::PxQuat(roll_, getPxDirection()) * physx::PxQuat(pitch_, getPxRight()) * physx::PxQuat(yaw_, getPxUp());
		transform.q = rotation * transform.q;
		pxActor_->setGlobalPose(transform);
		yaw_   = 0.0f;
		pitch_ = 0.0f;
		roll_  = 0.0f;
	}

	void Move(float timeElapsed)
	{
		physx::PxTransform transform = pxActor_->getGlobalPose();
		physx::PxVec3 velocity (0, 0, 0);
		float dirSpeed = SpeedRelated(moveBackForward_) * timeElapsed;
		float upSpeed = SpeedRelated(moveUpDown_) * timeElapsed;
		float rightSpeed = SpeedRelated(moveLeftRight_) * timeElapsed;
		velocity += getPxDirection() * dirSpeed;
		velocity += getPxUp() * upSpeed;
		velocity += getPxRight() * rightSpeed;
		transform.p += velocity;

		velocity / timeElapsed;

		pxActor_->setGlobalPose(transform);

		moveLeftRight_ = 0.0f;
		moveBackForward_ = 0.0f;
		moveUpDown_ = 0.0f;
	}


	float SpeedRelated(float data)
	{
		const float DYNAMIC_ACTOR_TIME_EPSILON = 0.001f;
		if(data > DYNAMIC_ACTOR_TIME_EPSILON)
			return speed_;
		else if(data < -DYNAMIC_ACTOR_TIME_EPSILON)
			return -speed_;
		else
			return 0;
	}


	XMVECTOR getPosition() {
		return *(XMVECTOR*)&getPxPosition();
	}

	XMVECTOR getDirection() {
		return *(XMVECTOR*)&getPxDirection();
	}

	XMVECTOR getUp() {
		return *(XMVECTOR*)&getPxUp();
	}

	XMVECTOR getRight() {
		return *(XMVECTOR*)&getPxRight();
	}

	physx::PxVec3 getPxPosition() {
		return pose().p;
	}

	physx::PxVec3 getPxDirection() {
		return pose().q.getBasisVector2().getNormalized();
	}

	physx::PxVec3 getPxUp() {
		return pose().q.getBasisVector1().getNormalized();
	}

	physx::PxVec3 getPxRight() {
		return pose().q.getBasisVector0().getNormalized();
	}

	float RestrictAngleTo360Range(float angle) const
	{
		while(angle>2*XM_PI)
			angle-=2*XM_PI;

		while(angle<-2*XM_PI)
			angle+=2*XM_PI;

		return angle;
	};


	virtual void update(float timeElapsed) = 0;

	virtual void activateButton1(float){};
	virtual void activateButton2(float){};
	virtual void activateButton3(float){};
	virtual void activateButton4(float){};
};

#endif