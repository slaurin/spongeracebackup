#include "stdafx.h"

#include "UserController.h"
#include "Inputs.h"

void UserController::setActor(DynamicActor* actor){
	Controller::setActor(actor);

	action_[MOVE_FORWARD] = &DynamicActor::moveForward;
	action_[MOVE_BACKWARD] = &DynamicActor::moveBackward;
	action_[STRAFE_RIGHT] = &DynamicActor::strafeRight;
	action_[STRAFE_LEFT] = &DynamicActor::strafeLeft;
	action_[MOVE_UP] = &DynamicActor::moveUp;
	action_[MOVE_DOWN] = &DynamicActor::moveDown;
	action_[TURN_LEFT] = &DynamicActor::turnLeft;
	action_[TURN_RIGHT] = &DynamicActor::turnRight;
	action_[SPACE_BUTTON] = &DynamicActor::activateButton1;
	action_[BUTTON2] = &DynamicActor::activateButton2;
	action_[BUTTON3] = &DynamicActor::activateButton3;
	action_[BUTTON4] = &DynamicActor::activateButton4;
};

void UserController::updateMouse(float timeTick)
{
	input_.GetMouseState(mouseCurrState_);

	if(mouseCurrState_.lZ != mouseLastState_.lZ)
	{
		(getActor()->moveBackwardForward(mouseCurrState_.lZ * timeTick));
	}
	

	if(mouseCurrState_.rgbButtons[0])
	{
		if(mouseCurrState_.lX != mouseLastState_.lX)
		{
			getActor()->Yaw(mouseCurrState_.lX * -0.002f);
		}
		if(mouseCurrState_.lY != mouseLastState_.lY)
		{
			getActor()->Pitch(mouseCurrState_.lY * -0.001f);
		}
		mouseLastState_ = mouseCurrState_;
	}
};
int test = 0;
void UserController::updateKeyboard(float timeTick)
{
	//std::vector<unsigned char> &keys = getActor()->getKeyMapping();
	test ++;
	for (int i=0; i<ACTION_COUNT; i++)
	{
		if((ACTION)i == SPACE_BUTTON)
		{
			if(Inputs::GetInstance().isPressed((ACTION)i))
			{
				if(!Space_Button_down)
				{
					Space_Button_down = true;
					(getActor()->*action_[i])(getActor()->getSpeed()*timeTick);
				}
			}
			else Space_Button_down = false;
			continue;
		}
		if(Inputs::GetInstance().isPressed((ACTION)i))
			(getActor()->*action_[i])(getActor()->getSpeed()*timeTick);
		
	}
};

void UserController::updateJoystick(float timeTick)
{
	input_.GetJoyState(jsCurrState_);

	getActor()->moveBackwardForward(jsCurrState_.lX * getActor()->getSpeed());
	getActor()->moveLeftRight(jsCurrState_.lY * getActor()->getSpeed());

	getActor()->Yaw(jsCurrState_.lRx * 0.001f);
	getActor()->Pitch(jsCurrState_.lRy * 0.001f);
	
};
