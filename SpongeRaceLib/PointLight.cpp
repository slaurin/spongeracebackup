#include "stdafx.h"
#include "PointLight.h"
#include "GraphicEngine.h"

namespace GE
{
	float PointLight::INFINITE_RANGE = 0.f;

	PointLight::PointLight(XMFLOAT4 position, XMFLOAT4 diffuse, XMFLOAT4 specular)
		: Light(diffuse, specular), position_(position), range_(INFINITE_RANGE), constantCoef_(1.f), linearCoef_(0.f), quadraticCoef_(0.f)
	{}

	XMFLOAT4 PointLight::GetPosition() const
	{
		return position_;
	}

	void PointLight::SetPosition(const XMFLOAT4 & position)
	{
		position_ = position;
	}


	void PointLight::SetAttenuation(const float range, const float constant, const float linear, const float quadratic)
	{
		if(GraphicEngine::IsNull(constant) && GraphicEngine::IsNull(linear) && GraphicEngine::IsNull(quadratic)) 
			throw AttenuationsCoefficientsCantBeAllNull();

		range_ = range;
		constantCoef_ = constant;
		linearCoef_ = linear;
		quadraticCoef_ = quadratic;
	}

	float PointLight::GetAttenuationRange() const
	{
		return range_;
	}

	float PointLight::GetAttenuationConstant() const
	{
		return constantCoef_;
	}

	float PointLight::GetAttenuationLinear() const
	{
		return linearCoef_;
	}

	float PointLight::GetAttenuationQuadratic() const
	{
		return quadraticCoef_;
	}
}