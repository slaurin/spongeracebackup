#ifndef _CAMERAACTOR_H
#define _CAMERAACTOR_H


#include "DynamicActor.h"
#include "UserController.h"

namespace GE
{
	class DrawableBSpline;
}



class Controller;

enum CAMERA_MODE {
	FIRST_PERSON,
	THIRD_PERSON,
	FREECAM,
    FIXEDCAM,
	MODE_COUNT
};


class Controller;

class CameraActor : public DynamicActor
{
	
public:
	static const unsigned char DefaultCameraKeyMapping[];

	CameraActor(const ActorFactory::ActorID &aActorId)
		: DynamicActor(aActorId)
	{}



    virtual void switchMode(int newMode)=0;
	virtual void switchPlayerCamera()=0;
	virtual void switchToFromFreeCam()=0;
	virtual XMMATRIX& getViewMatrix()=0;

	virtual void switchToSpline(int)=0;
	virtual void leaveSpline()=0;
	static ActorFactory::ActorID typeId();

};

#endif
