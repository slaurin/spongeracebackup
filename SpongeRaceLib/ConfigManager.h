#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include "Singleton.h"

// Forward declaration of class boost::serialization::access
namespace boost {
namespace serialization {
class access;
}
}

class ConfigFileNotFound{};

class ConfigManager :
	public Singleton<ConfigManager>
{
	friend class boost::serialization::access;
	friend class Singleton<ConfigManager>;

	ConfigManager();
	~ConfigManager() throw();

	std::map<std::string, std::string> strConfigs_;
	std::map<std::string, float> fltConfigs_;
	std::map<std::string, int> intConfigs_;

	bool configExist();
	bool configExist(const std::string &filePath);

	std::string configFileFullPath;

public:		
	std::string getStrValue(std::string id);
	void setStrValue(std::string id, std::string value);

	int getIntValue(std::string id);
	void setIntValue(std::string id, int value);

	float getFltValue(std::string id);
	void setFltValue(std::string id, float value);


	template<typename Archive> 
		void serialize(Archive& ar, const unsigned version)
	{
			using boost::serialization::make_nvp;
        ar & make_nvp("strConfigs_", strConfigs_);
		ar & make_nvp("fltConfigs_", fltConfigs_);
		ar & make_nvp("intConfigs_", intConfigs_);
	}
	void save(const std::string &filename);
	void save();
	void load(ConfigManager &obj);
	void load(ConfigManager &obj, const std::string &filePath);

	static const std::string getDefaultConfigName()
	{
		return "spongerace.cfg";
	}
};

#endif
