#pragma once

#include "d3dx11effect.h"
#include "Texture.h"
#include "Lighting.h"

namespace GE
{
	class TextureIndexOutOfBounds{};

	//! Passe d'une technique
	class Pass
	{
	public:
		Pass(ID3DX11EffectPass *pass, D3D11_INPUT_ELEMENT_DESC * begin, UINT size, UINT flags = 0);
		~Pass(void);
		void Apply();

	private:
		ID3DX11EffectPass *pass_;
		ID3D11InputLayout *vertexLayout_;
		UINT flags_;
	};
}