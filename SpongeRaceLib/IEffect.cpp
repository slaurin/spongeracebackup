#include "stdafx.h"
#include "IEffect.h"
#include "DxUtilities.h"
#include "Resource.h"
#include "GraphicEngine.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	IEffect::IEffect(const wstring & EffectFileName, bool preCompiled,  const string & EffectVersion)
		: name_(EffectFileName)
	{
		if(!preCompiled)
		{
			loadAndCompileEffect(EffectFileName, EffectVersion);
		}
		else
		{
			loadPrecompiledEffect(EffectFileName, EffectVersion);
		}
	}

	IEffect::IEffect(const IEffect &)
	{}

	IEffect::~IEffect()
	{
		DXRelacher(effect_);
	}

	void IEffect::Apply(const GraphicObject & object, const size_t techniqueIndex)
	{
		techniques_[techniqueIndex].Apply(object);
	}

	void IEffect::Apply(const GraphicObject & object, const string & techniqueName)
	{
		auto it = techniqueNames_.find(techniqueName);

		// Si on trouve la technique associ�e au nom, on l'applique
		if(it != techniqueNames_.end())
		{
			Apply(object, it->second);
		}
		// Sinon on applique la technique par d�faut
		else
		{
			Apply(object);
		}
	}

	ID3DX11EffectVariable * IEffect::GetVariableByName(const char * name)
	{
		return effect_->GetVariableByName(name);
	}

	ID3DX11EffectVariable * IEffect::GetVariableByIndex(const UINT index)
	{
		return effect_->GetVariableByIndex(index);
	}

	ID3DX11EffectVariable * IEffect::GetVariableBySemantic(const char * semantic)
	{
		return effect_->GetVariableBySemantic(semantic);
	}

	ID3DX11EffectConstantBuffer * IEffect::GetConstantBufferByName(const char * name)
	{
		return effect_->GetConstantBufferByName(name);
	}

	ID3DX11EffectConstantBuffer * IEffect::GetConstantBufferByIndex(const UINT index)
	{
		return effect_->GetConstantBufferByIndex(index);
	}

	void IEffect::loadPrecompiledEffect(const wstring & EffectFileName, const string & EffectVersion)
	{
		/*ifstream is(EffectFileName, ios_base::binary);
		is.seekg(0, ios_base::end);
		streampos pos = is.tellg();
		is.seekg(0, ios_base::beg);
		vector<char> effectBuffer((unsigned int)pos);
		is.read(&effectBuffer[0], pos);
		is.close();*/

		vector<char> effectBuffer;
		ifstream in(EffectFileName, ios::binary);
		for(char c; in.get(c); effectBuffer.push_back(c));

		DXEssayer(
			D3DX11CreateEffectFromMemory(
				static_cast<void *>(&effectBuffer[0]),
				effectBuffer.size(), 
				0, 
				GraphicEngine::GetInstance().GetDevice()->GetDXDevice(), 
				&effect_),
			DXE_ERREURCHARGEMENT_FX);
	}

	void IEffect::loadAndCompileEffect(const wstring & EffectFileName, const string & EffectVersion)
	{
		ID3DBlob* pFXBlob = NULL;

		DXEssayer(
			D3DX11CompileFromFile(
			EffectFileName.c_str(),
			0,
			0,
			0,
			EffectVersion.c_str(),
			D3DCOMPILE_DEBUG,
			0,
			0, 
			&pFXBlob,
			NULL,
			0),
			DXE_ERREURCREATION_FX);

		DXEssayer(
			D3DX11CreateEffectFromMemory(
				pFXBlob->GetBufferPointer(),
				pFXBlob->GetBufferSize(),
				0,
				GraphicEngine::GetInstance().GetDevice()->GetDXDevice(),
				&effect_),
			DXE_ERREURCHARGEMENT_FX);
		pFXBlob->Release();
	}
}