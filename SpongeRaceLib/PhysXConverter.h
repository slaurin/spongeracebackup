#ifndef PHYSXCONVERTER_H
#define PHYSXCONVERTER_H

#include "Convert.h"
#include "GeometryPart.h"
#include "GraphicObject.h"
#include "Vertex.h"

using namespace physx;
using namespace GE;


//=============================================================================
// class MemoryStream
//=============================================================================
class MemoryStream: public PxOutputStream, public PxInputStream
{
public:
	virtual PxU32 write(const void* src, PxU32 count)
	{
		int oldSize = _data.size();
		_data.resize(_data.size() + count);
		memcpy(&_data[oldSize], src, count);
		return count;
	}

	virtual PxU32 read(void* dest, PxU32 count)
	{
		if (count > _data.size())
			count = _data.size();
		memcpy(dest, &_data[0], count);
		_data.erase(_data.begin(), _data.begin()+count);
		return count;
	}

public:
	std::vector<unsigned char> _data;
};


//=============================================================================
// PRIVATE FUNCTIONS
//=============================================================================
static physx::PxConvexMesh* GenerateConvexFromGeometry(PxPhysics &physics, GE::GeometryPart* Mesh)
{
	int NumVerticies = Mesh->vertices().size();
	//DWORD FVFSize = sizeof(Vertex);

	//Create pointer for vertices
	PxVec3* verts = new PxVec3[NumVerticies];


	for(int i = 0; i < NumVerticies; i++)
	{
		verts[i] = Convert::XMFloatToPxVec3(Mesh->vertices()[i].position());
	}


	// Create descriptor for convex mesh
	PxConvexMeshDesc convexDesc;
	convexDesc.points.count		= NumVerticies;
	convexDesc.points.stride	= sizeof(PxVec3);
	convexDesc.points.data		= verts;
	convexDesc.flags			= PxConvexFlag::eCOMPUTE_CONVEX;

	PxCooking *cooker = PxCreateCooking(PX_PHYSICS_VERSION, physics.getFoundation(), PxCookingParams());

	// Cooking from memory

	MemoryStream buf;
	PxConvexMesh *convexMesh = nullptr;
	if(cooker->cookConvexMesh(convexDesc, buf))
	{
		convexMesh = physics.createConvexMesh(buf);
	}	
	cooker->release();

	delete[] verts;

	return convexMesh;
}


static physx::PxTriangleMesh*  GenerateTriangleMeshFromGeometry(PxPhysics &physics, GE::GeometryPart* Mesh)
{
	int nbVerts = Mesh->vertices().size();
	int nbIndexes = Mesh->indexes().size();
	int triCount =  nbIndexes / 3;

	//Create pointer for vertices
	PxVec3* verts = new PxVec3[nbVerts];
	PxU32* indices32 = new PxU32[nbIndexes];




	for(int i = 0; i < nbVerts; i++)
	{
		verts[i] = Convert::XMFloatToPxVec3(Mesh->vertices()[i].position());
	}
	
	for(int i = 0; i < nbIndexes; i++)
	{
		indices32[i] = Mesh->indexes()[i];
	}


	PxCooking *cooker = PxCreateCooking(PX_PHYSICS_VERSION, physics.getFoundation(), PxCookingParams());

	// Cooking from memory
		PxTriangleMeshDesc meshDesc;
	meshDesc.points.count           = nbVerts;
	meshDesc.points.stride          = sizeof(PxVec3);
	meshDesc.points.data            = verts;

	meshDesc.triangles.count        = triCount;
	meshDesc.triangles.stride       = 3*sizeof(PxU32);
	meshDesc.triangles.data         = indices32;


	MemoryStream buf;
	PxTriangleMesh *triangleMesh = nullptr;
	if(cooker->cookTriangleMesh(meshDesc, buf))
	{
		triangleMesh = physics.createTriangleMesh(buf);
	}	
	cooker->release();

	delete[] verts;
	delete[] indices32;

	return triangleMesh;

}

#endif