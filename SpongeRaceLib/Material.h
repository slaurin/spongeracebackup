#pragma once

#include "Lighting.h"
#include "Texture.h"
#include "IEffect.h"
#include "GraphicObject.h"

namespace GE
{
	//! Classe g�n�rique repr�sentant un mat�riau.
	/*! Un mat�riau contient des facteurs d'illumination (Lighting) et une liste de textures typ�es */
	class Material
	{
	protected:
		//! Nom du mat�riau
		std::string	name_;

		//! Facteurs d'illumination
		Lighting lighting_;

		//! Conteneur des textures typ�es
		std::map<aiTextureType, Texture *> textures_;

	public:
		Material(){}
		Material(const Material &);

		Material(
			const std::string & name, 
			const Lighting & lighting = Lighting(),
			const std::map<aiTextureType, Texture *> & textures = std::map<aiTextureType, Texture *>());

		virtual ~Material();

		//! Essaye d'ins�rer la texture au type souhait�
		bool InsertTexture(aiTextureType type, Texture * texture);

		//! Essaye d'ins�rer la texture au type souhait�
		void ReplaceTexture(aiTextureType type, Texture * texture);

		//! Fixe les facteurs d'illumination
		void SetLighting(const Lighting &);

		//! Retourne le nom d�crivant la classe
		static std::string GetClassName();
	};
}