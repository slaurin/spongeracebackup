#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"
#include "PointLight.h"
#include "SpongeRace.h"
#include "CameraActor.h"

namespace GE
{
	class TransparentMat
		: public Material
	{

		struct GlobalParams // toujours un multiple de 16 pour les constantes
		{
			XMMATRIX matWorldViewProj;							// Matrice WVP
			XMMATRIX matWorld;									// Matrice de transformation dans le monde 
			XMFLOAT4 vCamera; 									// Position de la cam�ra
			XMFLOAT4 InternColor;
			XMFLOAT4 ExternColor;
		};

		//! Param�tres globaux
		GlobalParams globalParams_;

		//! Buffer correspondant aux param�tres globaux
		ID3D11Buffer * globalBuffer_;

	public:
		TransparentMat(const Material *);

		~TransparentMat();

		//! Retourne le nom de la classe
		static std::string GetClassName();

		//! Applique l'effet sur l'object � partir des valeurs de ce mat�riau
		template<class VertexT>
		void Apply(Effect<VertexT, TransparentMat> * effect, const Object3D<VertexT, TransparentMat> & object)
		{
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			globalParams_.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj);
			globalParams_.matWorld = XMMatrixTranspose(object.GetWorldMatrix());
			auto vcam = GAME->camera()->getPosition();
			globalParams_.vCamera = XMFLOAT4(vcam.x,vcam.y,vcam.z,1);

			globalParams_.InternColor = XMFLOAT4(0.0f,0.2f,0.9f,1.f);//Bleu Clair
			//globalParams_.InternColor = XMFLOAT4(0.9f,0.2f,0.2f,1.f);//Rouge Clair
			globalParams_.ExternColor = XMFLOAT4(1.f,0.0f,0.9f,1.f);//RoseBleu
			//globalParams_.ExternColor = XMFLOAT4(1.f,0.9f,0.f,1.f);//Jaunes

			// On charge les constantes globales
			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(globalBuffer_, 0, NULL, &globalParams_, 0, 0);
			effect->GetConstantBufferByName("param")->SetConstantBuffer(globalBuffer_);

			
			// On applique l'effet ne prenant pas en compte les textures
			//CreateDepthsten
			GraphicEngine::GetInstance().GetDevice()->EnableAlphaBlend();
			effect->Apply(object, "AdvTrans");
			GraphicEngine::GetInstance().GetDevice()->DisableAlphaBlend();
		}
	};
}