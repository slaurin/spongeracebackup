#pragma once

#include "Effect.h"
#include "Object3D.h"
#include "SpriteVertex.h"
#include "Pannel2DMat.h"

namespace GE
{
	/* Sprite (Quad 2D dont les dimensions sont relatives � l'�cran) */
	class Sprite
		: public Object3D<SpriteVertex, Pannel2DMat>
	{
	public:
		/*! Constructeur */
		//! Les coordonn�es des corners vont de (0,0) en haut � gauche de l'�cran jusqu'� (1,1) en bas � droite
		//! En bas � gauche est (1,0)
		Sprite(
			Effect<SpriteVertex, Pannel2DMat> *effect, 
			Pannel2DMat *pMaterial, 
			const XMFLOAT2 & topLeftCorner = XMFLOAT2(0.f, 0.f), 
			const XMFLOAT2 & bottomRightCorner = XMFLOAT2(1.f, 1.f));
	
	protected:
		static SpriteVertex spriteVertices_[];
		static index_t spriteIndices_[];
	};
}
