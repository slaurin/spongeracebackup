#ifndef CONVERT_H
#define	CONVERT_H

#include "Material.h"

class Convert
{
public:
	template<class T>
	static std::string convertPrimitiveToString(T val)
	{
		std::stringstream ss;
		ss << val;
		return ss.str();
	}

	template<class T>
	static T convertMaterialToTemplate(GE::Material* pointer)
	{
		return dynamic_cast<T>(pointer);
	}

	static XMFLOAT2 aiToXMFloat2(const aiVector3D & vector)
	{
		return std::move(XMFLOAT2(vector.x, vector.y));
	}

	static XMFLOAT3 aiToXMFloat3(const aiVector3D & vector)
	{
		return std::move(XMFLOAT3(vector.x, vector.y, vector.z));
	}

	static XMFLOAT4 aiToXMFloat4(const aiColor3D & color)
	{
		return std::move(XMFLOAT4(color.r, color.g, color.b, 1.f));
	}

	static XMFLOAT4 aiToXMFloat4(const aiColor4D & color)
	{
		return std::move(XMFLOAT4(color.r, color.g, color.b, color.a));
	}

	static XMFLOAT4 PxVec3ToXMFloat4(const physx::PxVec3 & val)
	{
		return std::move(XMFLOAT4(val.x, val.y, val.z, 1.f));
	}

	static physx::PxVec3 XMFloatToPxVec3(const XMFLOAT3 & val)
	{
		return std::move(physx::PxVec3(val.x, val.y, val.z));
	}

};

static std::wstring ctow(const char* src)
{
	return std::wstring(src, src + strlen(src));
}

static bool to_bool(std::string str) {
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	std::istringstream is(str);
	bool b;
	is >> std::boolalpha >> b;
	return b;
}

#endif
