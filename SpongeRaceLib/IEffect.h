#ifndef IEFFECT_H
#define IEFFECT_H

#include "d3dx11effect.h"
#include "Technique.h"
#include "GraphicObject.h"

namespace GE
{
	//! Interface repr�sentant un effect g�nrique de l'architecture FX
	/*!
		IEffect est asbtrait car un effet doit conna�tre son type de Vertex et de Mat�riau associ�.
		Voir la classe Effect.
	*/
	class IEffect
	{
		//! Exception lev�e si aucune technique (compatible avec la carte graphique) n'a pu �tre charg�
		class NoTechniqueAvailableInThisEffect {};

		//! Liste des techniques utilis�es par l'effet
		std::vector<Technique> techniques_;

		//! Permet de retrouver une technique � partir de son nom plus rapidement
		std::map<std::string, size_t> techniqueNames_;

		//! Nom de l'effet
		std::wstring name_;

		//! Effet DirectX11
		ID3DX11Effect * effect_;

		void loadPrecompiledEffect(const std::wstring & EffectFileName, const std::string & EffectVersion);

		void loadAndCompileEffect(const std::wstring & EffectFileName, const std::string & EffectVersion);

	protected:
		IEffect(const std::wstring & EffectFileName, bool preCompiled,  const std::string & EffectVersion);

		IEffect(const IEffect &);

		//! Charge les techniques de l'effet bas� sur un type de Vertex
		template<class VertexT>
		void loadTechniques()
		{
			for(UINT i = 0; effect_->GetTechniqueByIndex(i)->IsValid(); ++i)
			{
				_D3DX11_TECHNIQUE_DESC desc;
				effect_->GetTechniqueByIndex(i)->GetDesc(&desc);
				std::string name(desc.Name);

				techniques_.push_back(Technique(effect_->GetTechniqueByIndex(i), VertexT::Begin(), VertexT::Size()));
				techniqueNames_.insert(std::pair<std::string, size_t>(name, (techniques_.size() - 1)));
			}

			// Si aucune technique valide n'a �t� trouv�e, on l�ve une exception
			if(techniques_.size() < 1)
				throw NoTechniqueAvailableInThisEffect();
		}

	public:
		virtual ~IEffect();

		//!
		void Apply(const GraphicObject & object, const size_t techniqueIndex = 0);

		//!
		void Apply(const GraphicObject & object, const std::string & techniqueName);

		ID3DX11EffectVariable * GetVariableByName(const char * name);

		ID3DX11EffectVariable * GetVariableByIndex(const UINT index);

		ID3DX11EffectVariable * GetVariableBySemantic(const char * semantic);

		ID3DX11EffectConstantBuffer * GetConstantBufferByName(const char * name);

		ID3DX11EffectConstantBuffer * GetConstantBufferByIndex(const UINT index);
	};
}
#endif