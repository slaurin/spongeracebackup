#pragma once

#include "Material.h"
#include "Effect.h"
#include "Object3D.h"
#include "d3dx11effect.h"

namespace GE
{
	class SimpleMat
		: public Material
	{
		struct ShadersParams // toujours un multiple de 16 pour les constantes
		{ 
			XMMATRIX matWorldViewProj;	// la matrice totale
		};

		ID3D11Buffer *constantBuffer;

	public:
		SimpleMat(const Material *);

		~SimpleMat(void);

		template<class VertexT>
		void Apply(Effect<VertexT, SimpleMat> * effect, const Object3D<VertexT, SimpleMat> & object)
		{
			// Initialiser et sélectionner les «constantes» de l'effet
			ShadersParams sp;
			XMMATRIX viewProj = GraphicEngine::GetInstance().GetMatViewProj();

			sp.matWorldViewProj = XMMatrixTranspose(object.GetWorldMatrix() * viewProj );


			auto it = textures_.find(aiTextureType(aiTextureType_AMBIENT));
			if(it != textures_.end())
			{
				effect->GetVariableByName("textureEntree")->AsShaderResource()->SetResource(it->second->GetTexture());
				effect->GetVariableByName("SampleState")->AsSampler()->SetSampler(0, it->second->GetSamplerState());
			}

			// On charge le buffer
			ID3DX11EffectConstantBuffer* pCB = effect->GetConstantBufferByName("param");
			pCB->SetConstantBuffer(constantBuffer);

			GraphicEngine::GetInstance().GetDevice()->GetDeviceContext()->UpdateSubresource(constantBuffer, 0, NULL, &sp, 0, 0);

			effect->Apply(object);
		}
	};
}