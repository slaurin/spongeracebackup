#pragma once

#include "Vertex.h"

namespace GE
{
	class SpriteVertex
	{
	private:
		XMFLOAT3 position_;
		XMFLOAT2 coordTex_;
		static D3D11_INPUT_ELEMENT_DESC Layout_[];

	public:
		SpriteVertex(
			XMFLOAT3 position = XMFLOAT3(0.f, 0.f, 0.f), 
			XMFLOAT2 coordTex = XMFLOAT2(0.f, 0.f));

		static D3D11_INPUT_ELEMENT_DESC * Begin();
		static UINT Size();

		XMFLOAT3 GetPosition() const;
		XMFLOAT2 GetTexCoords() const;
		SpriteVertex(const Vertex &);
	};
}