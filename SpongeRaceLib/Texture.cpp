#include "stdafx.h"
#include "Device.h"
#include "Texture.h"
#include "resource.h"
#include "DxUtilities.h"
#include "GraphicEngine.h"

using namespace std;
using namespace DxUtilities;

namespace GE
{
	D3D11_SAMPLER_DESC Texture::defaultSamplerDesc = {
		D3D11_FILTER_ANISOTROPIC,
		D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_WRAP,
		0.0f,
		4,
		D3D11_COMPARISON_ALWAYS,
		0,
		0,
		0,
		0,
		0,
		D3D11_FLOAT32_MAX
	};

	Texture::~Texture()
	{
		DXRelacher(texture_);
	}

	std::wstring Texture::GetFilename()
	{
		return filename_;
	}

	ID3D11ShaderResourceView * Texture::GetTexture()
	{
		return texture_;
	}

	ID3D11SamplerState * Texture::GetSamplerState()
	{
		return sampleState_;
	}

	Texture::Texture(const wstring & filename, const D3D11_SAMPLER_DESC & samplerDesc)
		: filename_(filename), texture_(nullptr)
	{
		// Charger la texture en ressource
		DXEssayer(
			D3DX11CreateShaderResourceViewFromFile(GraphicEngine::GetInstance().GetDevice()->GetDXDevice(), filename_.c_str(), NULL, NULL, &texture_, NULL), 
			DXE_FICHIERTEXTUREINTROUVABLE);
		DXSetDebugObjectName(texture_, filename);

		// Cr�ation de l'�tat de sampling
		GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateSamplerState(&samplerDesc, &sampleState_);
		DXSetDebugObjectName(sampleState_, filename);
	}

	Texture::Texture(ID3D11ShaderResourceView * shaderResource, const D3D11_SAMPLER_DESC & samplerDesc)
		: texture_(shaderResource), filename_(L"a")
	{
		GraphicEngine::GetInstance().GetDevice()->GetDXDevice()->CreateSamplerState(&samplerDesc, &sampleState_);
	}
}