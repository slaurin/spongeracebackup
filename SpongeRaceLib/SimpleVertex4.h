#pragma once

#include "stdafx.h"

#include "Vertex.h"

namespace GE
{
	class SimpleVertex4
	{

	private:
		XMFLOAT4 position_;
		XMFLOAT4 color_;
		static D3D11_INPUT_ELEMENT_DESC Layout_[];

	public:
		SimpleVertex4(XMFLOAT4 position = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f),
			          XMFLOAT4 color    = XMFLOAT4(0.3f, 0.8f, 0.3f, 1.0f));

		SimpleVertex4(const Vertex &);


		static D3D11_INPUT_ELEMENT_DESC * Begin();
		static UINT Size();

		XMFLOAT4 GetPosition() const throw();

		XMFLOAT4 GetColor() const throw();

		void SetPosition(const XMFLOAT4 val) throw();

		void SetColor(const XMFLOAT4 val) throw();
	};
}

