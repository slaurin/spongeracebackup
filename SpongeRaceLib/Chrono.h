#pragma once

class Chrono
{
	time_t startTime_;
	time_t amountOfTime_;
	bool isPaused_;
	bool isRunning_;

public:
	Chrono();
	void Start();
	void Pause();
	time_t GetTime();
	bool IsPaused();
	bool IsRunning();
	void Reset();
	~Chrono();
};

