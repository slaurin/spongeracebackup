#ifndef LEXICAL_CAST_H
#define LEXICAL_CAST_H


class implicite   {};
class texte_dest  {};
class texte_texte {};
class general     {};

template <bool, class, class>
struct static_if_else;
template <class SiVrai, class SiFaux>
struct static_if_else<true, SiVrai, SiFaux>
{
	typedef SiVrai type;
};
template <class SiVrai, class SiFaux>
struct static_if_else<false, SiVrai, SiFaux>
{
	typedef SiFaux type;
};

template <class T, class U>
struct meme_type
{
	enum { VAL = false };
};
template <class T>
struct meme_type<T,T>
{
	enum { VAL = true };
};

template <class S, class D>
class est_convertible
{
	typedef char oui_type;
	struct non_type { char _[3]; } ;
	static oui_type test(D);
	static non_type test(...);
	static S creer();
public:
	enum { VAL = sizeof(test(creer()))==sizeof(oui_type) };
};

template <class>
struct est_texte
{
	enum { VAL = false };
};
template <class C>
struct est_texte<std::basic_string<C> >
{
	enum { VAL = true };
};

template <class D, class S>
struct traits_conversion
{
	typedef typename static_if_else<
		meme_type<D,S>::VAL || est_convertible<S, D>::VAL,
		implicite,
		typename static_if_else<
		est_texte<D>::VAL,
		typename static_if_else<
		est_texte<S>::VAL,
		texte_texte,
		texte_dest
		>::type,
		general
		>::type
	>::type type;
};


template <class D, class S>
D lexical_cast(const S &src, general)
{
	stringstream sstr;
	sstr << src;
	D dest;
	sstr >> dest;
	return dest;
}
template <class D, class S>
D lexical_cast(const S &src, texte_dest)
{
	typedef typename
		D::value_type value_type;
	basic_stringstream<value_type> sstr;
	sstr << src;
	return sstr.str();
}
template <class D, class S>
D lexical_cast(const S &src, texte_texte)
{ return D(src.begin(), src.end()); }
template <class D, class S>
D lexical_cast(const S &src, implicite)
{ return src; }

#include <iostream>
using std::cout;
using std::endl;

//template <class T, class U>
//   void diag ()
//   {
//      cout << typeid(T).name () << " <-- \n\t" << typeid(U).name () << endl;
//      cout << "\tMeme type? " << meme_type<T,U>::VAL << endl;
//      cout << "\t" << typeid(traits_conversion<T, U>::type()).name() << endl;
//   }

template <class D, class S>
D lexical_cast(const S &src)
{
	//diag<D, S>();
	return lexical_cast<D, S>(src, traits_conversion<D, S>::type());
}

#include <iostream>
using std::istream;
using std::ostream;

class X {};
istream& operator>>(istream &is, X&)
{ return is; }
ostream& operator<<(ostream &os, const X&)
{ return os; }

struct Y
{
	Y()
	{ }
	Y(const X &)
	{}
};
istream& operator>>(istream & is, Y&)
{ return is; }
ostream& operator<<(ostream &os, const Y&)
{ return os; }


#endif