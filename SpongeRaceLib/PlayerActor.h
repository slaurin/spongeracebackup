#pragma once

#include "DynamicActor.h"
#include "ActorFactory.h"
#include "Actor.h"

class GE::GraphicObject;
class Controller;

class PlayerActor : public DynamicActor
{

public:
	static const unsigned char PlayerActor::DefaultPlayerKeyMapping[];
	static const float MAX_BOOST_DURATION;

	PlayerActor(const ActorFactory::ActorID &aActorId)
		: DynamicActor(aActorId)
	{}

	virtual void activateButton1(float)=0;
	virtual void activateButton2(float)=0;
	//virtual void FullBoost()=0;
	virtual void addOneBoost()=0;
	virtual void UseBoost()=0;
	virtual int getNumberBoost()=0;
	//virtual float GetBoostPourcent()=0;

	static ActorFactory::ActorID typeId();
};

