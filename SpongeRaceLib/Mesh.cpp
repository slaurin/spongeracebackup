#include "stdafx.h"
#include "Mesh.h"

using namespace std;

namespace GE
{
	Mesh::Mesh(const vector<GraphicObject *> & subMeshes)
		: subMeshes_(subMeshes)
	{}

	Mesh::~Mesh()
	{
		for_each(
			subMeshes_.begin(), 
			subMeshes_.end(), 
			[&](GraphicObject * object)
			{delete object;}
		);
	}

	void Mesh::Draw()
	{
		for_each(subMeshes_.begin(), subMeshes_.end(), mem_fun(&GraphicObject::Draw));
	}

	void Mesh::SetWorldMatrix(const XMMATRIX & worldMatrix)
	{
		worldMatrix_ = worldMatrix;

		for_each(
			subMeshes_.begin(), 
			subMeshes_.end(),
			[&](GraphicObject * object)
			{object->SetWorldMatrix(object->GetWorldMatrix() * worldMatrix);}
		);
	}

	void Mesh::addSubMesh(GraphicObject * subMesh)
	{
		subMeshes_.push_back(subMesh);
	}
}