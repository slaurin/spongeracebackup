#include <stdafx.h>
#include "ConfigManager.h"
#include "Singleton.h"
#include "gtest/gtest.h"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

namespace fs = boost::filesystem;
using namespace std;


TEST(ConfigManager, getValue_normal) {
	Singleton<ConfigManager>::GetInstance().setStrValue("id1", "value1");
	Singleton<ConfigManager>::GetInstance().setStrValue("id2", "value2");
	Singleton<ConfigManager>::GetInstance().setStrValue("id3", "value3");
	Singleton<ConfigManager>::GetInstance().setStrValue("id4", "value4");

	Singleton<ConfigManager>::GetInstance().setFltValue("id1", 20.0f);
	Singleton<ConfigManager>::GetInstance().setFltValue("id2", 1.0f);

	Singleton<ConfigManager>::GetInstance().setIntValue("int1", 1);
	Singleton<ConfigManager>::GetInstance().setIntValue("int2", 2);
	Singleton<ConfigManager>::GetInstance().setIntValue("int3", 3);

	EXPECT_EQ("value3", Singleton<ConfigManager>::GetInstance().getStrValue("id3"));
}


TEST(ConfigManager, getValue_2fois_id) {
	Singleton<ConfigManager>::GetInstance().setStrValue("id1", "value1");
	Singleton<ConfigManager>::GetInstance().setStrValue("id1", "value2");
	Singleton<ConfigManager>::GetInstance().setStrValue("id3", "value3");
	Singleton<ConfigManager>::GetInstance().setStrValue("id4", "value4");

	EXPECT_EQ("value2", Singleton<ConfigManager>::GetInstance().getStrValue("id1"));
}


TEST(ConfigManager, save_avecFileName) {
	Singleton<ConfigManager>::GetInstance().setStrValue("id1", "value1");
	Singleton<ConfigManager>::GetInstance().setStrValue("id2", "value2");
	Singleton<ConfigManager>::GetInstance().setStrValue("id3", "value3");
	Singleton<ConfigManager>::GetInstance().setStrValue("id4", "value4");

	Singleton<ConfigManager>::GetInstance().setFltValue("id1", 20.0f);
	Singleton<ConfigManager>::GetInstance().setFltValue("id2", 1.0f);

	Singleton<ConfigManager>::GetInstance().setIntValue("int1", 1);
	Singleton<ConfigManager>::GetInstance().setIntValue("int2", 2);
	Singleton<ConfigManager>::GetInstance().setIntValue("int3", 3);

	fs::path full_path( fs::initial_path<fs::path>() );
	// std::cout << full_path << std::endl;

	Singleton<ConfigManager>::GetInstance().save(full_path.string() + "\\ressources\\testSaveConfigs.out");
}


TEST(ConfigManager, load_avecFileName) {
	fs::path full_path( fs::initial_path<fs::path>() );
	try {
		ConfigManager &cfg = Singleton<ConfigManager>::GetInstance();
		std::cout << full_path << std::endl;
		cfg.load(cfg, full_path.string() + "\\ressources\\testReadConfigs.out");

		EXPECT_EQ("ReadInput5", cfg.getStrValue("NewId"));

	} catch (ConfigFileNotFound) {
		cout<<"File not found" <<endl;
		ADD_FAILURE(); 
	}
	
}

TEST(ConfigManager, save_default) {
	Singleton<ConfigManager>::GetInstance().setStrValue("id1", "value1");
	Singleton<ConfigManager>::GetInstance().setStrValue("id1", "value2");
	Singleton<ConfigManager>::GetInstance().setStrValue("id3", "value3");
	Singleton<ConfigManager>::GetInstance().setStrValue("id4", "value4");


	Singleton<ConfigManager>::GetInstance().save();
}

TEST(ConfigManager, load_default) {
		ConfigManager &cfg = Singleton<ConfigManager>::GetInstance();

		cfg.load(cfg);	
		EXPECT_EQ("value2", cfg.getStrValue("id1"));
}

