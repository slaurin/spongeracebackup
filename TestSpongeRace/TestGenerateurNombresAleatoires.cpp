#include "GenerateurNombresAleatoires.h"
#include "gtest/gtest.h"

TEST(GenerateurNombresAleatoires, generer) {
	float test= static_cast<float>(GenerateurNombresAleatoires::get().generer(0.0f,0.1f));
	EXPECT_GT(0.1f, test);
	EXPECT_LT(0.0f, test);
}

TEST(GenerateurNombresAleatoires, generer_limit_min_et_max_0) {
	float test= static_cast<float>(GenerateurNombresAleatoires::get().generer(0.0f,0.0f));
	EXPECT_EQ(0.0f, test);
}

TEST(GenerateurNombresAleatoires, generer_limit_min_et_max_0_1) {
	float test= static_cast<float>(GenerateurNombresAleatoires::get().generer(0.1f,0.1f));
	EXPECT_EQ(0.1f, test);
}