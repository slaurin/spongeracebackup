#define MAX_SPOTLIGHTS = 8;
#define MAX_POINTLIGHTS = 8;
#define MAX_DIRECTIONALLIGHTS = 8;
#define MAX_TEXTURES = 8;

cbuffer GlobalParams
{
	float4x4 matWorldViewProj;					// Matrice VP
	float4x4 matWorld;							// Matrice de transformation dans le monde 
	float4 vCamera; 							// Position de la cam�ra
	
	// Lumi�res ponctuelles
	float4 vPointLightsPos[MAX_SPOTLIGHTS];		// Position
	float4 vPointLightsDif[MAX_SPOTLIGHTS];		//  Diffusion
	float4 vPointLightsSpec[MAX_SPOTLIGHTS];	// Speculaire

	// Lumi�res directionnelles
	float4 vDirLightsDir[MAX_POINTLIGHTS];	// Direction 
	float4 vDirLightsDif[MAX_POINTLIGHTS];	// Diffusion
	float4 vDirLightsSpec[MAX_POINTLIGHTS];	// Speculaire

	// Lumi�res spot
	float4 vSpotLightsPos[MAX_DIRECTIONALLIGHTS];	// Position
	float4 vSpotLightsDif[MAX_DIRECTIONALLIGHTS];	// Diffusion
	float4 vSpotLightsSpec[MAX_DIRECTIONALLIGHTS];	// Speculaire
	float vSpotLightsInner[MAX_DIRECTIONALLIGHTS];	// Inner Angle
	float vSpotLightsOuter[MAX_DIRECTIONALLIGHTS];	// Outer Angle
	
	float puissance;
	float1 remplissage;
}
/*
cbuffer TexParams
{
	Texture2D textures[MAX_TEXTURES];			// Textures
	SamplerState sampleStates[MAX_TEXTURES];	// Etats de sampling
}
*/
cbuffer TexLessParams
{ 
	float4 vAmbient; 	// Valeur ambiante du mat�riau
	float4 vDiffusion;	// Valeur diffuse du mat�riau 
	float4 vSpecular;	// Valeur sp�culaire du mat�riau 
}

struct VS_Sortie
{
	float4 Pos : SV_Position;
	float3 Norm :    POSITION1;
	float3 vDirLum : POSITION2;
	float3 vDirCam : POSITION3;
	float2 coordTex : TEXCOORD3; 
};

// Vertex Shader avec Texture
VS_Sortie PhongVS(
	float4 Pos : POSITION,
	float3 Normale : NORMAL,
	float2 coordTex: TEXCOORD)  
{
	VS_Sortie sortie = (VS_Sortie)0;
	
	sortie.Pos = mul(Pos, matWorldViewProj); 
	sortie.Norm = mul(Normale, matWorld); 
	
 	float3 PosWorld = mul(Pos, matWorld);

	sortie.vDirLum = vLumiere - PosWorld; 
	sortie.vDirCam = vCamera - PosWorld; 

    // Coordonn�es d'application de texture
    sortie.coordTex = coordTex;

	return sortie;
}

// Vertex Shader sans Texture
VS_Sortie PhongTexLessVS(
	float4 Pos : POSITION,
	float3 Normale : NORMAL,
	float2 coordTex: TEXCOORD)
{
	VS_Sortie sortie = (VS_Sortie)0;
	
	sortie.Pos = mul(Pos, matWorldViewProj); 
	sortie.Norm = mul(Normale, matWorld); 
	
 	float3 PosWorld = mul(Pos, matWorld);

	sortie.vDirLum = vLumiere - PosWorld; 
	sortie.vDirCam = vCamera - PosWorld; 

	return sortie;
}

// Calcul global de Pixel selon Phong
float4 PhongPixel(float4 vAmbient, float4 vDiffusion, float4 vSpecular)
{
	return vAEcl * vAmbient + vDEcl * vDiffusion * diff  + vSEcl * vSMat * S;
}

// Pixel Shader avec Texture
float4 PhongTexPS(VS_Sortie vs) : SV_Target
{
	float4 couleur; 

	// Normaliser les param�tres
	float3 N = normalize(vs.Norm);
 	float3 L = normalize(vs.vDirLum);
	float3 V = normalize(vs.vDirCam);

	// Valeur de la composante diffuse
	float4 diff = saturate(dot(N, L)); 

	// R = 2 * (N.L) * N � L
	float3 R = normalize(2 * diff * N - L);
    
	// Calcul de la sp�cularit� 
	float4 S = pow(saturate(dot(R, V)), puissance); 

	// �chantillonner la couleur du pixel � partir de la texture  
	float4 couleurTexture = textureEntree.Sample(SampleState, vs.coordTex);   

	// I = A + D * N.L + (R.V)n
	couleur = 
		couleurTexture * vAEcl  + 
		couleurTexture * vDEcl * diff +
		vSEcl * vSMat * S;

	return couleur;
}

// Pixel Shader sans Texture
float4 PhongTexLessPS(VS_Sortie vs) : SV_Target
{
	float4 couleur; 

	// Normaliser les param�tres
	float3 N = normalize(vs.Norm);
 	float3 L = normalize(vs.vDirLum);
	float3 V = normalize(vs.vDirCam);

	// Valeur de la composante diffuse
	float4 diff = saturate(dot(N, L)); 

	// R = 2 * (N.L) * N � L
	float3 R = normalize(2 * diff * N - L);
    
	// Calcul de la sp�cularit� 
	float4 S = pow(saturate(dot(R, V)), puissance); 

	couleur =
		vAEcl * vAMat +
		vDEcl * vDMat * diff  + 
		vSEcl * vSMat * S;

	return couleur;
}

technique11 Phong
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, PhongVS()));
        SetPixelShader(CompileShader(ps_4_0, PhongPS()));
        SetGeometryShader(NULL);
    }
}

technique11 PhongTexLess
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, PhongTexLessVS()));
        SetPixelShader(CompileShader(ps_4_0, PhongTexLessPS()));
        SetGeometryShader(NULL);
    }
}