cbuffer param
{ 
	float4x4 WorldViewProjection;   // la matrice totale 
	float4x3 WorldView ;
	float4x3 World;		// matrice de transformation dans le monde 
	float Time; 		// la position de la source d'�clairage (Point)
	float4 Eye; 			// la position de la cam�ra	
}


texture2D BaseTexture <string name = "rainbowfilm_smooth.bmp";>;
textureCUBE EnvTexture <string name = "room-dxt5.dds"; string type = "CUBE";>;


//float4 WaveHeight = {0.05, 0.05, 0.025, 0.025};
float4 WaveHeight = {0.01, 0.01, 0.005, 0.005};
float4 WaveOffset = {0.0, 0.0, 0.0, 0.0};
float4 WaveSpeed = {0.6, 0.7, 1.2, 1.4};
float4 WaveDirX  = {0.0, 2.0, 0.0, 4.0};
float4 WaveDirY  = {2.0, 0.0, 4.0, 0.0};


sampler2D BaseSampler = sampler_state
{
    Texture = (BaseTexture);
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

samplerCUBE EnvSampler = sampler_state
{
    Texture = (EnvTexture);
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};



void BubbleVS(const float3 position : POSITION,
              const float3 normal   : NORMAL,
              const float3 tangent  : TANGENT,
              const float2 tex0     : TEXCOORD0,
              out float4 oPosition : POSITION,
              out float2 oTex0     : TEXCOORD0,
              out float3 oReflect  : TEXCOORD1,
              out float  oDot      : TEXCOORD2)
{
	//Use texture coords as inputs to sinusoidal warp
	float4 waveVec = WaveDirX * tex0.x + WaveDirY * tex0.y;

	waveVec += Time * WaveSpeed + WaveOffset; //add scaled time to move bumps according to frequency
	waveVec = frac(waveVec);

	waveVec -= 0.5;  //subtract 0.5  coords range from -0.5 to 0.5
	waveVec *= 2 * 3.1415926535897932384626433832795; //mult tex coords by 2pi  coords range from(-pi to pi)

	float3 positionDeformation = normal * dot(sin(waveVec), WaveHeight);
	float3 positionDeformedObject = position + positionDeformation;

	oPosition = mul(float4(positionDeformedObject, 1), WorldViewProjection);
	oTex0 = tex0;

	float3 binormal = cross(tangent, normal);

	float4 cosTimesWaveHeight = cos(waveVec) * WaveHeight;
	float binormalWarping = dot(-cosTimesWaveHeight, WaveDirX);
	float tangentWarping  = dot(-cosTimesWaveHeight, WaveDirY);
	float3 warping = binormal * binormalWarping + tangent * tangentWarping;
	float3 normalDeformed = normal + warping;
	float3 normalDeformedWorld = normalize(mul(normalDeformed, (float3x3)World));

	float3 view = normalize(Eye - mul(float4(positionDeformedObject, 1), (float4x3)World));

	oDot = dot(normalDeformedWorld, view);

	//oReflect = view - 2 * oDot * normalDeformedWorld;
	oReflect = 2 * oDot * normalDeformedWorld - view;  //2N(N.V)-V
}



float4 BubblePS(float2 texBase       : TEXCOORD0,
                float3 reflect       : TEXCOORD1,
                float  viewDotNormal : TEXCOORD2) : COLOR
{
    float4 baseColor = tex2D(BaseSampler, texBase);
    float4 envColor  = texCUBE(EnvSampler, reflect);
    float4 combined  = saturate(2 * envColor * baseColor);
    float glow = saturate(4 * (envColor.a * envColor.a - 0.75));
    float fresnel = (1 - abs(viewDotNormal)) * 0.6 + 0.1;  // * alpha scale + alpha bias

    return float4(lerp(envColor.rgb, combined.rgb, glow), glow + fresnel);
    //return float4(combined.rgb, glow + fresnel);
}

technique11 BubbleVsPs
{
    pass p0
    {
        VertexShader = compile vs_4_0 BubbleVS();
        PixelShader = compile ps_4_0 BubblePS();
		AlphaBlendEnable = true;
        
        SrcBlend = SrcAlpha;
        DestBlend = InvSrcAlpha;
    }
}