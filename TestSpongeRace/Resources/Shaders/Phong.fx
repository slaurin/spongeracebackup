/************************************************
 *					Shader Phong				*
 ************************************************
 * Author			: LEFEBVRE L�o				*
 * Contact			: leo.lefebvre@ymail.com	*
 * Rights			: Copyright 2012 (c)		*
 * Script Version	: 1.1						*
 * Fx Version		: 5.0						*
 * Shader Version	: 4.0						*
 ************************************************/

// Nombre maximum de sources de lumi�re de type ponctuel autoris�
#define MAX_POINTLIGHTS 8

// Nombre maximum de sources de lumi�re de type directionnel autoris�
#define MAX_DIRECTIONALLIGHTS 8

// Nombre maximum de sources de lumi�re de type spot autoris�
#define MAX_SPOTLIGHTS 8

// Nombre maximum de textures � charger
#define MAX_TEXTURES 11

/** The texture is combined with the result of the diffuse
 *  lighting equation.
 */
#define TEXTYPE_DIFFUSE 0

/** The texture is combined with the result of the specular
 *  lighting equation.
 */
#define TEXTYPE_SPECULAR 1

/** The texture is combined with the result of the ambient
 *  lighting equation.
 */
#define TEXTYPE_AMBIENT 2

/** The texture is added to the result of the lighting
 *  calculation. It isn't influenced by incoming light.
 */
#define TEXTYPE_EMISSIVE 3

/** The texture is a height map.
 *
 *  By convention higher gray-scale values stand for
 *  higher elevations from the base height.
 */
#define TEXTYPE_HEIGHT 4

/** The texture is a (tangent space) normal-map.
 *
 *  Again there are several conventions for tangent-space
 *  normal maps. Assimp does (intentionally) not 
 *  distinguish here.
 */
#define TEXTYPE_NORMALS 5

/** The texture defines the glossiness of the material.
 *
 *  The glossiness is in fact the exponent of the specular
 *  (phong) lighting equation. Usually there is a conversion
 *  function defined to map the linear color values in the
 *  texture to a suitable exponent. Have fun.
*/
#define TEXTYPE_SHININESS 6

/** The texture defines per-pixel opacity.
 *
 *  Usually 'white' means opaque and 'black' means 
 *  'transparency'. Or quite the opposite. Have fun.
*/
#define TEXTYPE_OPACITY 7

/** Displacement texture
 *
 *  The exact purpose and format is application-dependent.
 *  Higher color values stand for higher vertex displacements.
*/
#define TEXTYPE_DISPLACEMENT 8

/** Lightmap texture (aka Ambient Occlusion)
 *
 *  Both 'Lightmaps' and dedicated 'ambient occlusion maps' are
 *  covered by this material property. The texture contains a
 *  scaling value for the final color value of a pixel. Its
 *  intensity is not affected by incoming light.
*/
#define TEXTYPE_LIGHTMAP 9

/** Reflection texture
 *
 * Contains the color of a perfect mirror reflection.
 * Rarely used almost never for real-time applications.
*/
#define TEXTYPE_REFLECTION 10

// Structure d'une lumi�re ponctuelle
struct PointLight
{
	float4 Pos;	// Position
	float4 Dif;	// Diffusion - [0; 1].rgba
	float4 Spec;// Speculaire - [0; 1].rgba
	float3 Att;	// Facteurs d'att�nuation
	float1 padding;
};

// Structure d'une lumi�re directionnelle
struct DirLight
{
	float4 Dir;	// Direction - normalis�
	float4 Dif;	// Diffusion - [0; 1].rgba
	float4 Spec;// Speculaire - [0; 1].rgba
};

// Structure d'un lumi�re spot
struct SpotLight
{
	float4 Pos;		// Position
	float4 Dir;		// Direction - normalis�
	float4 Dif;		// Diffusion - [0; 1].rgba
	float4 Spec;	// Speculaire - [0; 1].rgba
	float3 Att;		// Facteurs d'att�nuation
	float AngleCos;	// Cosinus du demi-angle du c�ne de lumi�re - [0; 1]
	float Exp;		// Exposant d'att�nuation
	float3 padding;
};

// Param�tres globaux
cbuffer GlobalParams
{
	float4x4 matWorldViewProj;					// Matrice WVP
	float4x4 matWorld;							// Matrice de transformation dans le monde 
	float4 vCamera; 							// Position de la cam�ra
	float4 vAmbientLight;						// Valeur ambiante d'illumination de la sc�ne - [0; 1].rgba
	int numPointLights;							// Nombre de lampes ponctuelles - [0; MAX_POINTLIGHTS]
	int numDirLights;							// Nombre de lampes directionnelles - [0; MAX_DIRECTIONALLIGHTS]
	int numSpotLights;							// Nombre de lampes spot - [0; MAX_SPOTLIGHTS]
	float puissance;							// Puissance sp�culaire
	PointLight PointLights[MAX_POINTLIGHTS];	// Lumi�res ponctuelles
	DirLight DirLights[MAX_DIRECTIONALLIGHTS];	// Lumi�res directionnelles
	SpotLight SpotLights[MAX_SPOTLIGHTS];		// Lumi�res spot
}

// Param�tres du mat�riau (lorsque pas de texture)
cbuffer TexLessParams
{ 
	float4 vAmbient; 	// Valeur ambiante du mat�riau - [0; 1].rgba
	float4 vDiffusion;	// Valeur diffuse du mat�riau  - [0; 1].rgba
	float4 vSpecular;	// Valeur sp�culaire du mat�riau - [0; 1].rgba
	float4 vEmissive;	// Valeur emissive du mat�riau - [0; 1].rgba
}

// Param�tres des textures
bool loadedTextures[MAX_TEXTURES];		// Tableau de bool�ens notifiant la pr�sence de texture
Texture2D textures[MAX_TEXTURES];		// Tableau de textures typ�es
SamplerState samplers[MAX_TEXTURES];	// Tableau de samplers

// Sortie du Vertex Shader
struct VS_Sortie
{
	float4 Pos :		SV_Position;
	float2 CoordTex :	TEXCOORD0;
	float4 Norm :		POSITION0;
	float4 DirCam :		POSITION1;
	float4 PosWorld :	POSITION2;
};

// Vertex Shader
VS_Sortie PhongVS(
	float3 Position :	POSITION,
	float3 Normal :		NORMAL,
	float2 TexCoord :	TEXCOORD)
{
	// Initialisation de la structure de sortie
	VS_Sortie sortie = (VS_Sortie)0;
	
	// Position du vertex dans le viewport
	sortie.Pos = mul(float4(Position, 1.f), matWorldViewProj); 
	
	// Normale du vertex dans le monde
	sortie.Norm = mul(float4(Normal, 0.f), matWorld);
	
	// Position du vertex dans le monde
 	sortie.PosWorld = mul(float4(Position, 1.f), matWorld);
	
	// Direction de la camera
	sortie.DirCam = float4(vCamera.xyz - sortie.PosWorld.xyz, 1.f);
	
	// Coordonn�es d'application de texture
    sortie.CoordTex = TexCoord;

	return sortie;
}

// Calcul pour chaque pixel la nuance associ�e � chaque source de lumi�re ponctuelle
float4 PhongPSPointLights(float4 cDiff, float4 cSpec, float4 N, float4 V, float4 posWorld, float exponent)
{
	float4 cPoint = float4(0., 0., 0., 0.);

	// Calcul des lampes ponctuelles
	for(int i = 0; i < numPointLights; ++i)
	{
		float4 Dir = PointLights[i].Pos - posWorld;
		float dist = length(Dir);
		float4 L = Dir / dist;

		// Calcul de l'att�nuation en fonction de la distance lampe - pixel
		float distAtten = dot(PointLights[i].Att, float3(1.f, dist, dist * dist));
		
		// Valeur de la composante diffuse
		float diff = saturate(dot(N, L));

		// R = 2 * (N.L) * N � L
		float4 R = normalize(2 * diff * N - L);
		
		// Calcul de la sp�cularit� 
		float S = pow(saturate(dot(R, V)), exponent);
		
		cPoint +=
			(PointLights[i].Dif * cDiff * diff  + 
			PointLights[i].Spec * cSpec * S)
			/ distAtten;
	}

	return cPoint;
}

// Calcul pour chaque pixel la nuance associ�e � chaque source de lumi�re directionnelle
float4 PhongPSDirLights(float4 cDiff, float4 cSpec, float4 N, float4 V, float exponent)
{
	float4 cDir = float4(0., 0., 0., 0.);

	// Calcul des lampes directionnelles
	for(int i = 0; i < numDirLights; ++i)
	{
		// TODO : useless
		float4 Dir = DirLights[i].Dir;
		float4 L = normalize(Dir);
		
		// Valeur de la composante diffuse
		float diff = saturate(dot(N, L));

		// R = 2 * (N.L) * N � L
		float4 R = normalize(2 * diff * N - L);
		
		// Calcul de la sp�cularit� 
		float S = pow(saturate(dot(R, V)), exponent);
		
		cDir +=
			DirLights[i].Dif * cDiff * diff  + 
			DirLights[i].Spec * cSpec * S;
	}

	return cDir;
}

// Calcul pour chaque pixel la nuance associ�e � chaque source de lumi�re spot
float4 PhongPSSpotLights(float4 cDiff, float4 cSpec, float4 N, float4 V, float4 posWorld, float exponent)
{
	float4 cSpots = float4(0., 0., 0., 0.);

	// Calcul des lampes ponctuelles
	for(int i = 0; i < numSpotLights; ++i)
	{
		float4 Dir = SpotLights[i].Pos - posWorld;
		float dist = length(Dir);
		float4 L = Dir / dist;
		float4 D = normalize(SpotLights[i].Dir);

		// Calcul du cosinus de l'angle entre la direction de la lampe et le pixel
		float angleCos = saturate(dot(D, L));
		
		// Le pixel est �clair�
		if(angleCos > SpotLights[i].AngleCos)
		{
			// Calcul de l'att�nuation en fonction de la distance lampe - pixel
			float distAtten = dot(SpotLights[i].Att, float3(1.f, dist, dist * dist));
		
			// Calcul de l'att�nuation due � l'angle du spot
			float spotAtten = pow(angleCos, SpotLights[i].Exp);
									
			// Valeur de la composante diffuse
			float diff = saturate(dot(N, L));

			// R = 2 * (N.L) * N � L
			float4 R = normalize(2 * diff * N - L);
			
			// Calcul de la sp�cularit� 
			float S = pow(saturate(dot(R, V)), exponent);
			
			cSpots +=
				(SpotLights[i].Dif * cDiff * diff  + 
				SpotLights[i].Spec * cSpec * S)
				* spotAtten / distAtten;
		}
	}

	return cSpots;
}

// Pixel Shader sans Texture
float4 PhongTexLessPS(VS_Sortie vs) : SV_Target
{
	// Valeur ambiente d'illumination de la sc�ne
	float4 couleur = vEmissive + (vAmbientLight * vAmbient);
	
	// Normaliser les param�tres
	float4 N = normalize(vs.Norm);
	float4 V = normalize(vs.DirCam);
	
	// Calcul des lampes ponctuelles : OK
	couleur += PhongPSPointLights(vDiffusion, vSpecular, N, V, vs.PosWorld, puissance);
	
	// Calcul des lampes directionnelles : OK
	couleur += PhongPSDirLights(vDiffusion, vSpecular, N, V, puissance);
	
	// Calcul des lampes spot
	couleur += PhongPSSpotLights(vDiffusion, vSpecular, N, V, vs.PosWorld, puissance);

	return couleur;
}

// Pixel Shader avex Texture
float4 PhongTexPS(VS_Sortie vs) : SV_Target
{
	float4 cAmbient = vAmbient;
	float4 cEmissive = vEmissive;
	float4 cDiff = vDiffusion;
	float4 cSpec = vSpecular;
	float exponent = puissance;
	
	// Charge la texture diffuse
	if(loadedTextures[TEXTYPE_DIFFUSE])
		cDiff = textures[TEXTYPE_DIFFUSE].Sample(samplers[TEXTYPE_DIFFUSE], vs.CoordTex);
	/*
	// Charge la texture specular
	if(loadedTextures[TEXTYPE_SPECULAR])
		cSpec = textures[TEXTYPE_SPECULAR].Sample(samplers[TEXTYPE_SPECULAR], vs.CoordTex);
		
	// Charge la texture emissive
	if(loadedTextures[TEXTYPE_EMISSIVE])
		cEmissive = textures[TEXTYPE_EMISSIVE].Sample(samplers[TEXTYPE_EMISSIVE], vs.CoordTex);
		
	// Charge la texture ambient
	if(loadedTextures[TEXTYPE_AMBIENT])
		cAmbient = textures[TEXTYPE_AMBIENT].Sample(samplers[TEXTYPE_AMBIENT], vs.CoordTex);
		
	// Charge la texture de Shininess
	if(loadedTextures[TEXTYPE_SHININESS])
		exponent = length(textures[TEXTYPE_SHININESS].Sample(samplers[TEXTYPE_SHININESS], vs.CoordTex));
	*/
	float4 couleur = float4(cEmissive.xyz + (vAmbientLight.xyz * cAmbient.xyz), 1.f);
	
	// Normaliser les param�tres
	float4 N = normalize(vs.Norm);
	float4 V = normalize(vs.DirCam);
	
	// Calcul des lampes ponctuelles
	couleur += PhongPSPointLights(cDiff, cSpec, N, V, vs.PosWorld, exponent);
	
	// Calcul des lampes directionnelles
	couleur += PhongPSDirLights(cDiff, cSpec, N, V, exponent);
	
	// Calcul des lampes spot
	couleur += PhongPSSpotLights(cDiff, cSpec, N, V, vs.PosWorld, exponent);
	
	return couleur;
}

// Technique de phong avec texture
technique11 PhongTex
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, PhongVS()));
        SetPixelShader(CompileShader(ps_4_0, PhongTexPS()));
        SetGeometryShader(NULL);
    }
}

// Technique de phong sans texture
technique11 PhongTexLess
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, PhongVS()));
        SetPixelShader(CompileShader(ps_4_0, PhongTexLessPS()));
        SetGeometryShader(NULL);
    }
}