#include <stdafx.h>
#include "XmlAssetManager.h"
#include "Singleton.h"
#include "gtest/gtest.h"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"


namespace fs = boost::filesystem;
using namespace std;

namespace {

TEST(XMLAssetManager, constructor) {

	xercesc::XMLPlatformUtils::Initialize();

	XMLAssetManager* manager = nullptr;
	manager = new XMLAssetManager();

	EXPECT_NE(manager, nullptr);
	delete manager;

	xercesc::XMLPlatformUtils::Terminate();
}

TEST(XMLAssetManager, load) {
	xercesc::XMLPlatformUtils::Initialize();

	try {
		
		XMLAssetManager manager;

		fs::path full_path( fs::initial_path<fs::path>() );
		manager.load(full_path.string() + "\\resources\\spongerace_assets.xml");
		
	}  catch( const std::exception & ex ) {
		cerr << ex.what() << endl;
		FAIL();
	} catch (...){
	}
	xercesc::XMLPlatformUtils::Terminate();
}

TEST(AssetManager, load_unloadGameAssets) {

	xercesc::XMLPlatformUtils::Initialize();

	try{
		XMLAssetManager manager;

		fs::path full_path( fs::initial_path<fs::path>() );
		manager.load(full_path.string() + "\\resources\\spongerace_assets.xml");
		manager.loadGameAssets();
		manager.unloadGameAssets();
	} catch( const std::exception & ex ) {
		cerr << ex.what() << endl;
		FAIL();
	} catch (...){
	}
	xercesc::XMLPlatformUtils::Terminate();
}

TEST(AssetManager, load_unloadSceneAssets) {
	xercesc::XMLPlatformUtils::Initialize();

	try{
	XMLAssetManager manager;

	fs::path full_path( fs::initial_path<fs::path>() );
	manager.load(full_path.string() + "\\resources\\spongerace_assets.xml");

	manager.loadSceneAssets("0");
	manager.unloadSceneAssets("0");

	} catch( const std::exception & ex ) {
		cerr << ex.what() << endl;
		FAIL();
	} catch (...){
	}
	xercesc::XMLPlatformUtils::Terminate();
}




}