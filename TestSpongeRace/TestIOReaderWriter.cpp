#include <stdafx.h>
#include <IOStructDefinitions.h>
#include <IOWriter.h>
#include <IOReader.h>

#include <xnamath.h>
#include <vector>
#include <string>

#include "gtest/gtest.h"

TEST(IOWriter, write_char)
{
    IOWriter::write("TestIOReaderWriter.txt", std::vector<char>(1, 'c'));
    IOReader<char> reader("TestIOReaderWriter.txt");
    std::vector<char>& data = reader.getData();
    EXPECT_EQ('c', data[0]);
    EXPECT_EQ(1, data.size());
}

TEST(IOWriter, write_unsigned_char)
{
	IOWriter::write("TestIOReaderWriter.txt", std::vector<unsigned char>(1, (unsigned char)'c'));
	IOReader<unsigned char> reader("TestIOReaderWriter.txt");
	std::vector<unsigned char>& data = reader.getData();
	EXPECT_EQ('c', data[0]);
	EXPECT_EQ(1, data.size());
}

TEST(IOWriter, write_string)
{
	IOWriter::write("TestIOReaderWriter.txt", std::vector<std::string>(1, "test"));
	IOReader<std::string> reader("TestIOReaderWriter.txt");
	std::vector<std::string>& data = reader.getData();
	EXPECT_EQ("test", data[0]);
	EXPECT_EQ(1, data.size());
}

TEST(IOWriter, write_Vertex)
{
	std::vector<XMFLOAT3> v;
	v.push_back(XMFLOAT3(1,2,3));
	v.push_back(XMFLOAT3(4,5,6));
	v.push_back(XMFLOAT3(7,8,9));
	IOWriter::write<XMFLOAT3>("TestIOReaderWriter.txt", v);
	IOReader<XMFLOAT3> reader("TestIOReaderWriter.txt");
	std::vector<XMFLOAT3>& data = reader.getData();
    EXPECT_EQ(1, data[0].x);
    EXPECT_EQ(2, data[0].y);
    EXPECT_EQ(3, data[0].z);
    EXPECT_EQ(4, data[1].x);
    EXPECT_EQ(5, data[1].y);
    EXPECT_EQ(6, data[1].z);
    EXPECT_EQ(7, data[2].x);
    EXPECT_EQ(8, data[2].y);
    EXPECT_EQ(9, data[2].z);
	EXPECT_EQ(3, data.size());
}