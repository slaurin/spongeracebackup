#include <stdafx.h>
#include "LogManager.h"

#include "gtest/gtest.h"

TEST(LogManager, emptyLog)
{
    LogManager log("TestLogManager.log");
    log.emptyLog();
    EXPECT_EQ("", log.readFile());
    log.write("test");
    EXPECT_EQ("test", log.readFile());
    log.emptyLog();
    EXPECT_EQ("", log.readFile());
}

TEST(LogManager, write)
{
    LogManager log("TestLogManager.log");
    log.emptyLog();
    EXPECT_EQ("", log.readFile());
    log.write("test");
    EXPECT_EQ("test", log.readFile());
    log.write("test2");
    EXPECT_EQ("testtest2", log.readFile());
    log.write("test3");
    EXPECT_EQ("testtest2test3", log.readFile());
}

TEST(LogManager, readFile)
{
    LogManager log("TestLogManager.log");
    log.emptyLog();
    EXPECT_EQ("", log.readFile());
    log.write("I need to read the whole file");
    EXPECT_EQ("I need to read the whole file", log.readFile());
}

TEST(LogManager, readStream)
{
    LogManager log("TestLogManager.log");
    log.emptyLog();
    EXPECT_EQ("", log.readFile());
    log.write("Previous part of the file");
    EXPECT_EQ("Previous part of the file", log.readStream());
    LogManager log2("TestLogManager.log");
    EXPECT_EQ("Previous part of the file", log2.readFile());
    log2.write("New part of the file");
    EXPECT_EQ("New part of the file", log2.readStream());
    EXPECT_EQ("Previous part of the fileNew part of the file", log2.readFile());
}