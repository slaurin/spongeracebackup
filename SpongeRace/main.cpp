// SpongeRace.cpp : Defines the entry point for the console application.
//

#include <stdafx.h>
#include <SpongeRace.h>
#include <LoopTime.h>

// include the basic windows header files and the Direct3D header file
#include <windows.h>
#include <windowsx.h>



int _tmain(int argc, _TCHAR* argv[])
{
	return WinMain(GetModuleHandle(NULL), 0, nullptr, true);
}

 // the entry point for any Windows program
int WINAPI WinMain(HINSTANCE hInstance,	HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// set up and initialize Direct3D
	//xercesc::XMLPlatformUtils::Initialize();

	GAME = new SpongeRace(hInstance);
	GAME->Init();
	//GAME->onStartGameSession();
	// Boucle de jeu
	while(!GAME->shouldExit())
	{
		//GAME->onBeginFrame();
		LoopTime::GetInstance().Update();
		if(LoopTime::GetInstance().CanUpdate())
			GAME->update();
	}

	GAME->cleanup();
	delete GAME;
	GAME=nullptr;
	return 0;
}
