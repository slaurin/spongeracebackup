﻿========================================================================
							SpongeRace
Equipe :
	- Stéphane Laurin (stephanelaurin@hotmail.com)
	- Léo Lefebvre (leo.lefebvre@insa-lyon.fr)
	- Jonathan Paturel (paturelj@gmail.com)
	- Yannick Triqueneaux (yannick.triqueneaux@gmail.com)
========================================================================

La solution SpongeRace est décomposée en trois projets :
	- SpongeRace :
		Génère l'exécutable de la solution
		Ce projet contient le point d'entrée main
	- SpongeRaceLib
		Contient le jeu en lui-même
		Généré en librairie .lib
	- TestSpongeRace
		Permet d'éxecuter un jeu de tests sur la librairie SpongeRaceLib

/////////////////////////////////////////////////////////////////////////////
							Configurations
/////////////////////////////////////////////////////////////////////////////

Debug (Compilation : /MDd) :
	Configuration utilisée pour débugger la physique. Elle lance PhysX en
	mode CHECKED, ce qui permet de lancer PhysX visual Debugger. Les autres 
	librairies sont lancées en mode Debug. (Lent)
	
Pre-Release (Compilation : /MDd) :
	Configuration utilisée pour débugger. Elle lance PhysX en mode PROFILE, 
	PhysX visual Debugger n'est plus utilisable. Les autres librairies sont 
	lancées en mode Debug. (Plus rapide)
	
Release (Compilation : /MD) :
	Configuration utilisée pour tester. Toutes les librairies sont utilisées
	en mode Release, le tout est très rapide.
	
/////////////////////////////////////////////////////////////////////////////
						Dépendances externes
/////////////////////////////////////////////////////////////////////////////

Pour la solution :
	- Assimp (3.0.1270)
		Variable d'environnement : ASSIMP (ex: C:\Dev\Assimp)
		
	- Boost (1.51.0)
		Variable d'environnement : BOOST (ex: C:\Dev\Boost)
		Compilations : Multithreaded, Multithreaded static runtime, 
		Multithreaded debug static runtime
		
	- DirectX11 SDK (June 2010)
		Variable d'environnement : DXSDK_DIR (ex: C:\Dev\DX11SDK)
		
	- Effects11 (June 2010)
		Inclus au projet (SpongeRace/Resources/ExternalLibs)
		
	- PhysX (3.2.1)
		Variable d'environnement : PHYSX (ex: C:\Dev\PhysX)
		
	- Xerces-C (3.1.1)
		Variable d'environnement : XERCES (ex: C:\Dev\Xerces)

Pour TestSpongeRace :
	- GTest (1.6.0)
		Variable d'environnement : GTEST (ex: C:\Dev\gtest)
		
Note :
Certaines librairies sont à compiler soit même, bien penser à les compiler en 
utilisant les mêmes options de compilation (voir Configurations ci-dessus).