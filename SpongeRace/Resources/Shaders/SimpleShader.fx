cbuffer param
{ 
	float4x4 matWorldViewProj;
}

struct VS_Sortie
{
	float4 Pos : SV_Position;
	float2 coordTex : TEXCOORD0; 
};

VS_Sortie SimpleVS(float4 Pos : POSITION, float3 Normale : NORMAL, float2 coordTex: TEXCOORD)  
{
VS_Sortie sortie = (VS_Sortie)0;
	
	sortie.Pos = mul(Pos, matWorldViewProj); 
    sortie.coordTex = coordTex;

	return sortie;
}

Texture2D textureEntree;  // la texture
SamplerState SampleState;  // l'�tat de sampling

float4 SimplePS( VS_Sortie vs ) : SV_Target
{
	float4 couleur = textureEntree.Sample(SampleState, vs.coordTex);
	return couleur;
}

technique11 Simple
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, SimpleVS()));
        SetPixelShader(CompileShader(ps_4_0, SimplePS()));
        SetGeometryShader(NULL);
    }
}
