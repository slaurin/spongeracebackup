// Param�tres g�n�raux
float zIndex;				// Index pour la superposition des sprites
float4x4 matWorldViewProj;	// Matrice WVP pour les billboards

// Param�tres des textures
Texture2D Texture;		// Texture charg�e
SamplerState Sampler;	// Sampler utilis�

// Sortie du Vertex Shader
struct VS_Sortie
{
	float4 Pos :		SV_Position;
	float2 CoordTex :	TEXCOORD0;
};

// Vertex Shader Sprite
VS_Sortie SpriteVS(
	float2 Position :	POSITION,
	float2 TexCoord :	TEXCOORD)
{
	// Initialisation de la structure de sortie
	VS_Sortie sortie = (VS_Sortie)0;
	
	// Position du vertex dans le viewport
	sortie.Pos = float4(Position, zIndex, 1.f);
	
	// Coordonn�es d'application de texture
    sortie.CoordTex = TexCoord;

	return sortie;
}

// Vertex Shader Billboard
VS_Sortie BillboardVS(
	float3 Position :	POSITION,
	float2 TexCoord :	TEXCOORD)
{
	// Initialisation de la structure de sortie
	VS_Sortie sortie = (VS_Sortie)0;
	
	// Position du vertex dans le viewport
	sortie.Pos = mul(float4(Position, 1.f), matWorldViewProj); 
	
	// Coordonn�es d'application de texture
    sortie.CoordTex = TexCoord;

	return sortie;
}

// Pixel Shader Sprite
float4 SpritePS(VS_Sortie vs) : SV_Target
{
	return Texture.Sample(Sampler, vs.CoordTex);
}

// Pixel Shader Sprite
float4 BillboardPS(VS_Sortie vs) : SV_Target
{
	return Texture.Sample(Sampler, vs.CoordTex);
}

// Technique des Sprites
technique11 Sprite
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, SpriteVS()));
        SetPixelShader(CompileShader(ps_4_0, SpritePS()));
        SetGeometryShader(NULL);
    }
}

// Technique des Billboards
technique11 Billboard
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, BillboardVS()));
        SetPixelShader(CompileShader(ps_4_0, BillboardPS()));
        SetGeometryShader(NULL);
    }
}