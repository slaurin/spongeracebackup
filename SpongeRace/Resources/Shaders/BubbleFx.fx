cbuffer param
{ 
	float4x4 WorldViewProjection;
	float4x4 WorldView;
	float4x4 World;
	float4 Eye; 			// la position de la cam�ra	
	float Time; 		// la position de la source d'�clairage (Point)
}

Texture2D BaseTexture;
//TextureCube EnvTexture;
Texture2D EnvTexture;

//float4 WaveHeight = {0.05, 0.05, 0.025, 0.025};
float4 WaveHeight = float4(0.01f, 0.01f, 0.005f, 0.005f);
float4 WaveOffset = float4(0.0f, 0.0f, 0.0f, 0.0f);
float4 WaveSpeed = float4(0.6f, 0.7f, 1.2f, 1.4f);
float4 WaveDirX  = float4(0.0f, 2.0f, 0.0f, 4.0f);
float4 WaveDirY  = float4(2.0f, 0.0f, 4.0f, 0.0f);


SamplerState EnvSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
	AddressW = Wrap;
};


// Sortie du Vertex Shader
struct VS_Sortie
{
    float4 oPosition : SV_Position;
    float2 oTex0     : TEXCOORD0;
    float3 oReflect  : TEXCOORD1;
    float  oDot      : TEXCOORD2;
};


VS_Sortie BubbleVS(float3 position : POSITION,
              float3 normal   : NORMAL,
              float3 tangent  : TANGENT,
              float2 tex0     : TEXCOORD
)
{
	VS_Sortie sortie = (VS_Sortie)0;
	
	//Use texture coords as inputs to sinusoidal warp
	float4 waveVec = WaveDirX * tex0.x + WaveDirY * tex0.y;

	waveVec += Time * WaveSpeed + WaveOffset; //add scaled time to move bumps according to frequency
	waveVec = frac(waveVec);

	waveVec -= 0.5;  //subtract 0.5  coords range from -0.5 to 0.5
	waveVec *= 2 * 3.1415926535897932384626433832795; //mult tex coords by 2pi  coords range from(-pi to pi)

	float3 positionDeformation = normal * dot(sin(waveVec), WaveHeight);
	float3 positionDeformedObject = position + positionDeformation;

	sortie.oPosition = mul(float4(positionDeformedObject, 1), WorldViewProjection);
	sortie.oTex0 = tex0;

	float3 binormal = cross(tangent, normal);

	float4 cosTimesWaveHeight = cos(waveVec) * WaveHeight;
	float binormalWarping = dot(-cosTimesWaveHeight, WaveDirX);
	float tangentWarping  = dot(-cosTimesWaveHeight, WaveDirY);
	float3 warping = binormal * binormalWarping + tangent * tangentWarping;
	float3 normalDeformed = normal + warping;
	float3 normalDeformedWorld = normalize(mul(normalDeformed, (float3x3)World));

	float3 view = normalize(Eye - mul(float4(positionDeformedObject, 1), (float4x3)World));

	sortie.oDot = dot(normalDeformedWorld, view);

	//oReflect = view - 2 * oDot * normalDeformedWorld;
	sortie.oReflect = 2 * sortie.oDot * normalDeformedWorld - view;  //2N(N.V)-V
	
	return sortie;
}



float4 BubblePS(VS_Sortie vs) : SV_Target
{	
    float4 baseColor = BaseTexture.Sample(EnvSampler, vs.oTex0);
	baseColor.a = 0.85;	
	float4 envColor  = EnvTexture.Sample(EnvSampler, vs.oReflect)*1.1;
	envColor.a = 0.85;	
	envColor.rgb = lerp(envColor.rgb, float3(1,1,1), 0.80);
	
    float4 combined  = saturate(2 * envColor * baseColor);
    float glow = saturate(4 * (envColor.a * envColor.a - 0.75));    
	float fresnel = (1 - abs(vs.oDot)) * 0.6 + 0.1;  // * alpha scale + alpha bias

    return float4(lerp(envColor.rgb, combined.rgb, glow), glow + fresnel);
}

technique10 BubbleFx
{
    pass p0
    {
		SetVertexShader(CompileShader(vs_4_0, BubbleVS()));
		SetPixelShader(CompileShader(ps_4_0, BubblePS()));
		SetGeometryShader(NULL);
    }
}