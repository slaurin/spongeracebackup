cbuffer param
{ 
	float4x4 matWorldViewProj;   // la matrice totale 
	float4x4 matWorld;		// Position Mesh
	float4 vCamera;		// Matrice inverse de la cam
	float4 InternColor;   //Couleur interrieur
	float4 ExternColor;   //Couleur interrieur
}

struct VS_Sortie
{
    float4 Position		: SV_POSITION;
	float4 VNormal 		: NORMAL;
    float3 posCam		: TEXCOORD0;
};

VS_Sortie AdvTransVS(float4 Pos : POSITION, float3 Normale : NORMAL, float2 coordTex: TEXCOORD)
{
    VS_Sortie OUT = (VS_Sortie)0;
    float3 Pw = mul(Pos,matWorld).xyz;
	OUT.VNormal = float4(Normale.xyz,1);
    OUT.posCam = normalize(vCamera.xyz - Pw);
    OUT.Position = mul(Pos,matWorldViewProj);
    return OUT;
}

float4 AdvTransPS( VS_Sortie IN ) : SV_Target
{
	float coef = dot(IN.posCam.xyz,IN.VNormal.xyz);
    return float4(lerp(ExternColor, InternColor,coef).rgb*2,0.5);
	//return float4(255,0,255,0.5);
}
DepthStencilState DepthEnabling
{
	DepthEnable = TRUE;
};
technique11 AdvTrans
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, AdvTransVS()));
        SetPixelShader(CompileShader(ps_4_0, AdvTransPS()));
        SetGeometryShader(NULL);
		SetDepthStencilState(DepthEnabling, 0);
    }
}
