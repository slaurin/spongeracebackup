// Paramètres globaux
cbuffer GlobalParams
{
	float4x4 matWorldViewProj;	// Matrice WVP
}

// Sortie du Vertex Shader
struct VS_Sortie
{
	float4 Pos :	SV_Position;
	float3 Color :	COLOR;
};

// Vertex Shader
VS_Sortie ColorVS(
	float3 Position :	POSITION,
	float3 Color :		COLOR)
{
	// Initialisation de la structure de sortie
	VS_Sortie sortie = (VS_Sortie)0;
	
	// Position du vertex dans le viewport
	sortie.Pos = mul(float4(Position, 1.f), matWorldViewProj); 
	
	// Coleur du pixel
    sortie.Color = Color;

	return sortie;
}


// Pixel Shader sans Texture
float4 ColorPS(VS_Sortie vs) : SV_Target
{
	return float4(vs.Color, 0.);
}

// Technique de phong sans texture
technique11 tech0
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, ColorVS()));
        SetPixelShader(CompileShader(ps_4_0, ColorPS()));
        SetGeometryShader(NULL);
    }
}