cbuffer GlobalParams
{
    float4x4 matWorldViewProj;
};

struct VS_Output
{
	float4 Pos : SV_Position;
	float2 coordTex : TEXCOORD0;
};

VS_Output ParticleVertexShader(float4 pos : POSITION, float2 coordTex : TEXCOORD0)
{
    VS_Output vs;
	
	vs.Pos = mul(pos, matWorldViewProj); 
	vs.coordTex = coordTex;
	
	return vs;
}

Texture2D shaderTexture;
SamplerState SamplerType;

float4 ParticlePixelShader(VS_Output vs) : SV_TARGET
{
	return shaderTexture.Sample(SamplerType, vs.coordTex);
}

technique11 ParticleWaterfall
{
    pass pass0
    {
        SetVertexShader(CompileShader(vs_4_0, ParticleVertexShader()));
        SetPixelShader(CompileShader(ps_4_0, ParticlePixelShader()));
        SetGeometryShader(NULL);
    }
}